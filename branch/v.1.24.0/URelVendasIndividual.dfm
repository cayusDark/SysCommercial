object frmRelVendasIndividual: TfrmRelVendasIndividual
  Left = 0
  Top = 0
  Caption = 'Relat'#243'rio de vendas individual'
  ClientHeight = 480
  ClientWidth = 814
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 814
    Height = 42
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Label1: TLabel
      Left = 4
      Top = 9
      Width = 133
      Height = 19
      Caption = 'N'#250'mero do pedido'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object edtPesquisa: TEdit
      Left = 143
      Top = 6
      Width = 331
      Height = 27
      CharCase = ecUpperCase
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnChange = edtPesquisaChange
    end
    object BitBtn1: TBitBtn
      Left = 676
      Top = 4
      Width = 109
      Height = 32
      Caption = 'Im&primir'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      Glyph.Data = {
        46060000424D4606000000000000360400002800000016000000160000000100
        08000000000010020000C40E0000C40E00000001000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
        A6000020400000206000002080000020A0000020C0000020E000004000000040
        20000040400000406000004080000040A0000040C0000040E000006000000060
        20000060400000606000006080000060A0000060C0000060E000008000000080
        20000080400000806000008080000080A0000080C0000080E00000A0000000A0
        200000A0400000A0600000A0800000A0A00000A0C00000A0E00000C0000000C0
        200000C0400000C0600000C0800000C0A00000C0C00000C0E00000E0000000E0
        200000E0400000E0600000E0800000E0A00000E0C00000E0E000400000004000
        20004000400040006000400080004000A0004000C0004000E000402000004020
        20004020400040206000402080004020A0004020C0004020E000404000004040
        20004040400040406000404080004040A0004040C0004040E000406000004060
        20004060400040606000406080004060A0004060C0004060E000408000004080
        20004080400040806000408080004080A0004080C0004080E00040A0000040A0
        200040A0400040A0600040A0800040A0A00040A0C00040A0E00040C0000040C0
        200040C0400040C0600040C0800040C0A00040C0C00040C0E00040E0000040E0
        200040E0400040E0600040E0800040E0A00040E0C00040E0E000800000008000
        20008000400080006000800080008000A0008000C0008000E000802000008020
        20008020400080206000802080008020A0008020C0008020E000804000008040
        20008040400080406000804080008040A0008040C0008040E000806000008060
        20008060400080606000806080008060A0008060C0008060E000808000008080
        20008080400080806000808080008080A0008080C0008080E00080A0000080A0
        200080A0400080A0600080A0800080A0A00080A0C00080A0E00080C0000080C0
        200080C0400080C0600080C0800080C0A00080C0C00080C0E00080E0000080E0
        200080E0400080E0600080E0800080E0A00080E0C00080E0E000C0000000C000
        2000C0004000C0006000C0008000C000A000C000C000C000E000C0200000C020
        2000C0204000C0206000C0208000C020A000C020C000C020E000C0400000C040
        2000C0404000C0406000C0408000C040A000C040C000C040E000C0600000C060
        2000C0604000C0606000C0608000C060A000C060C000C060E000C0800000C080
        2000C0804000C0806000C0808000C080A000C080C000C080E000C0A00000C0A0
        2000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0E000C0C00000C0C0
        2000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0A000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFF08F7F7
        F7F7F7F7F7F7F7F7F7F7F7F7F6FFFFFF0000FFFFFFF707070707070707070707
        070707F7FFFFFFFF0000F6F7A4A407F6F6F6F6F6F608F6F6F6F6F6A4F7A4A408
        0000A4070707F7080808080808080808080807A4070707A40000A40807085252
        52525252525252525252495B080708A40000A4080808A4494949494949494949
        494949F70808F6A40000A4F60808F6F6F6F6F6F6F6F6F6F6F6F6F6F60808F6A4
        0000A4F6F6F6F6F6F6F6F6F6F6F6F6090908F6F6F6F6FFA40000A4FFFFFFFF09
        E2E2E2E2E2E2E2D9D9D9F6FFFFFFFFA40000A4FFFFFFFF09E2E3EBEBEBEBE2E2
        DAD909FFFFFFFFF70000A4FFFFFFFF09E2E2E2E2E2E2DAD9D9D909FFFFFFFFF7
        0000F7FFFFFFFF09E3ECECECEBEBEBE3EBE309FFFFFFFFF70000FFF707F6FF09
        090909090909090909EC09F60807F7FF0000FFFFFFF708ED0909090909090909
        09EC070708FFFFFF0000FFFFFF070808FFFFFFF6F6F6F608080808F7FFFFFFFF
        0000FFFFFF07F6F6FFFFFFFFF6F6F6F6F60808F7FFFFFFFF0000FFFFFF07F6F6
        FFFFFFFFFFF6F6F6F60808F7FFFFFFFF0000FFFFFFF7F6F6FFFFFFFFFFF6F6F6
        F6F608F7FFFFFFFF0000FFFFFF08F708FFFFFFFFFFFFF6F6F6F608F7FFFFFFFF
        0000FFFFFFFFF608FFFFFFFFFFFFFFF6F6F608F7FFFFFFFF0000FFFFFFFF0808
        F6F6F6F6F6F6F6F6F6F6F6F7FFFFFFFF0000FFFFFFFFFF070707070707070707
        0707F7F7FFFFFFFF0000}
      ParentFont = False
      TabOrder = 1
      OnClick = BitBtn1Click
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 42
    Width = 814
    Height = 438
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object DBGrid1: TDBGrid
      Left = 2
      Top = 21
      Width = 810
      Height = 415
      Align = alClient
      DataSource = DM.DSTabVenda
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -16
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnDrawColumnCell = DBGrid1DrawColumnCell
      Columns = <
        item
          Expanded = False
          FieldName = 'venId'
          Width = 59
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'venCodPedFornecedor'
          Width = 123
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Cliente'
          Width = 390
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'venDatVenda'
          Visible = True
        end>
    end
  end
  object frxDBDSRelVendasIndividual: TfrxDBDataset
    UserName = 'frxDBDSRelVendasIndividual'
    CloseDataSource = False
    DataSet = DM.queryRelVEndasIndividual
    BCDToCurrency = False
    Left = 208
    Top = 32
  end
  object frxRptRelVendasIndividual: TfrxReport
    Version = '4.13.2'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42374.719265798600000000
    ReportOptions.LastChange = 42374.719265798600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 368
    Top = 32
    Datasets = <
      item
        DataSet = frxDBDSRelVendasIndividual
        DataSetName = 'frxDBDSRelVendasIndividual'
      end>
    Variables = <
      item
        Name = ' RELATORIO'
        Value = Null
      end
      item
        Name = 'Status'
        Value = ''
      end
      item
        Name = 'Tipo'
        Value = ''
      end
      item
        Name = 'Frete'
        Value = ''
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageFooter1: TfrxPageFooter
        Height = 18.897650000000000000
        Top = 430.866420000000000000
        Width = 718.110700000000000000
        object Memo23: TfrxMemoView
          Left = 602.047620000000000000
          Top = 1.220470000000000000
          Width = 132.283550000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'P'#193'GINA [Page] DE [TotalPages#]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Height = 28.015770000000000000
        ParentFont = False
        Top = 264.567100000000000000
        Width = 718.110700000000000000
        DataSet = frxDBDSRelVendasIndividual
        DataSetName = 'frxDBDSRelVendasIndividual'
        RowCount = 0
        object Line3: TfrxLineView
          Top = 26.456710000000000000
          Width = 718.110236220000000000
          ShowHint = False
          Diagonal = True
        end
        object frxDBDSRelVendasIndividualIteVenProCodFornecedor: TfrxMemoView
          Left = 2.645669291338580000
          Top = 3.779527560000000000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'IteVenProCodFornecedor'
          DataSet = frxDBDSRelVendasIndividual
          DataSetName = 'frxDBDSRelVendasIndividual'
          Memo.UTF8W = (
            '[frxDBDSRelVendasIndividual."IteVenProCodFornecedor"]')
        end
        object frxDBDSRelVendasIndividualproNome: TfrxMemoView
          Left = 63.133890000000000000
          Top = 3.779527560000000000
          Width = 196.535560000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'proNome'
          DataSet = frxDBDSRelVendasIndividual
          DataSetName = 'frxDBDSRelVendasIndividual'
          Memo.UTF8W = (
            '[frxDBDSRelVendasIndividual."proNome"]')
        end
        object frxDBDSRelVendasIndividualiteVenQuantidade: TfrxMemoView
          Left = 269.448980000000000000
          Top = 3.779527560000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'iteVenQuantidade'
          DataSet = frxDBDSRelVendasIndividual
          DataSetName = 'frxDBDSRelVendasIndividual'
          Memo.UTF8W = (
            '[frxDBDSRelVendasIndividual."iteVenQuantidade"]')
        end
        object frxDBDSRelVendasIndividualiteVenQtdCaixas: TfrxMemoView
          Left = 322.362400000000000000
          Top = 3.779527560000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'iteVenQtdCaixas'
          DataSet = frxDBDSRelVendasIndividual
          DataSetName = 'frxDBDSRelVendasIndividual'
          Memo.UTF8W = (
            '[frxDBDSRelVendasIndividual."iteVenQtdCaixas"]')
        end
        object frxDBDSRelVendasIndividualiteVenValUnitario: TfrxMemoView
          Left = 383.393940000000000000
          Top = 3.779527559055120000
          Width = 83.149660000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = frxDBDSRelVendasIndividual
          DataSetName = 'frxDBDSRelVendasIndividual'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Memo.UTF8W = (
            '[frxDBDSRelVendasIndividual."iteVenValUnitario"]')
        end
        object frxDBDSRelVendasIndividualiteVenDesconto1: TfrxMemoView
          Left = 471.866420000000000000
          Top = 3.779527559055120000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = frxDBDSRelVendasIndividual
          DataSetName = 'frxDBDSRelVendasIndividual'
          Memo.UTF8W = (
            '[frxDBDSRelVendasIndividual."iteVenDesconto1"] %')
        end
        object frxDBDSRelVendasIndividualiteVenDesconto2: TfrxMemoView
          Left = 532.897960000000000000
          Top = 3.779527559055120000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = frxDBDSRelVendasIndividual
          DataSetName = 'frxDBDSRelVendasIndividual'
          Memo.UTF8W = (
            '[frxDBDSRelVendasIndividual."iteVenDesconto2"] %')
        end
        object frxDBDSRelVendasIndividualiteVenDesconto3: TfrxMemoView
          Left = 596.149970000000000000
          Top = 3.779527559055120000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = frxDBDSRelVendasIndividual
          DataSetName = 'frxDBDSRelVendasIndividual'
          Memo.UTF8W = (
            '[frxDBDSRelVendasIndividual."iteVenDesconto3"] %')
        end
        object frxDBDSRelVendasIndividualiteVenValTotal: TfrxMemoView
          Left = 645.079160000000000000
          Top = 3.779527560000000000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = frxDBDSRelVendasIndividual
          DataSetName = 'frxDBDSRelVendasIndividual'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Memo.UTF8W = (
            '[frxDBDSRelVendasIndividual."iteVenValTotal"]')
        end
      end
      object ReportTitle1: TfrxReportTitle
        Height = 64.913420000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          Left = 208.756030000000000000
          Top = 2.559060000000000000
          Width = 291.023810000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'RELAT'#211'RIO DE VENDAS INDIVIDUAL')
          ParentFont = False
        end
        object Date: TfrxMemoView
          Left = 507.732840000000000000
          Top = 45.354360000000000000
          Width = 211.653680000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd '#39'de'#39' mmmm '#39'de'#39' yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Manaus, [Date]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Height = 56.692950000000000000
        ParentFont = False
        Top = 351.496290000000000000
        Width = 718.110700000000000000
        object Shape1: TfrxShapeView
          Left = 0.779530000000000000
          Top = 2.779530000000000000
          Width = 718.110700000000000000
          Height = 45.354360000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Memo26: TfrxMemoView
          Left = 131.504020000000000000
          Top = 26.456692913385800000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Total L'#237'quido:')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 2.645669291338580000
          Top = 26.456692910000000000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Total Bruto:')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 506.457020000000000000
          Top = 26.456692913385800000
          Width = 124.724490000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Total L'#237'quido Faturado:')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 298.582870000000000000
          Top = 26.456692913385800000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Total Bruto Faturado:')
          ParentFont = False
        end
        object frxDBDSRelVendasIndividualvenValLiquido: TfrxMemoView
          Left = 206.874150000000000000
          Top = 26.456692910000000000
          Width = 90.708720000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = frxDBDSRelVendasIndividual
          DataSetName = 'frxDBDSRelVendasIndividual'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Memo.UTF8W = (
            '[frxDBDSRelVendasIndividual."venValLiquido"]')
        end
        object frxDBDSRelVendasIndividualiteVenValTotal1: TfrxMemoView
          Left = 67.031540000000000000
          Top = 26.456692913385800000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = frxDBDSRelVendasIndividual
          DataSetName = 'frxDBDSRelVendasIndividual'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Memo.UTF8W = (
            '[frxDBDSRelVendasIndividual."iteVenValTotal"]')
        end
        object frxDBDSRelVendasIndividualvenValFatBruto: TfrxMemoView
          Left = 411.968770000000000000
          Top = 26.456692910000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = frxDBDSRelVendasIndividual
          DataSetName = 'frxDBDSRelVendasIndividual'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Memo.UTF8W = (
            '[frxDBDSRelVendasIndividual."venValFatBruto"]')
        end
        object frxDBDSRelVendasIndividualvenValFatLiquido: TfrxMemoView
          Left = 631.181510000000000000
          Top = 26.456692910000000000
          Width = 83.149660000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = frxDBDSRelVendasIndividual
          DataSetName = 'frxDBDSRelVendasIndividual'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Memo.UTF8W = (
            '[frxDBDSRelVendasIndividual."venValFatLiquido"]')
        end
        object Memo13: TfrxMemoView
          Left = 2.645669291338580000
          Top = 5.677165350000000000
          Width = 79.370130000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Acrescimo IPI:')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 129.622140000000000000
          Top = 5.677165350000000000
          Width = 86.929190000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Desconto ICMS:')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 254.008040000000000000
          Top = 5.677165350000000000
          Width = 120.944960000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Desconto PIS/COFINS:')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 426.189240000000000000
          Top = 5.677165350000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Desconto Comercial:')
          ParentFont = False
        end
        object frxDBDSRelVendasIndividualvenAcrIPI: TfrxMemoView
          Left = 82.929190000000000000
          Top = 5.677165350000000000
          Width = 37.795300000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = frxDBDSRelVendasIndividual
          DataSetName = 'frxDBDSRelVendasIndividual'
          Memo.UTF8W = (
            '[frxDBDSRelVendasIndividual."venAcrIPI"] %')
        end
        object frxDBDSRelVendasIndividualvenDesICMS: TfrxMemoView
          Left = 216.992270000000000000
          Top = 5.677165350000000000
          Width = 30.236240000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = frxDBDSRelVendasIndividual
          DataSetName = 'frxDBDSRelVendasIndividual'
          Memo.UTF8W = (
            '[frxDBDSRelVendasIndividual."venDesICMS"] %')
        end
        object frxDBDSRelVendasIndividualvenDesPISCOFINS: TfrxMemoView
          Left = 375.953000000000000000
          Top = 5.677165350000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = frxDBDSRelVendasIndividual
          DataSetName = 'frxDBDSRelVendasIndividual'
          Memo.UTF8W = (
            '[frxDBDSRelVendasIndividual."venDesPISCOFINS"] %')
        end
        object frxDBDSRelVendasIndividualvenDesComercial: TfrxMemoView
          Left = 540.252320000000000000
          Top = 5.677165350000000000
          Width = 34.015770000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = frxDBDSRelVendasIndividual
          DataSetName = 'frxDBDSRelVendasIndividual'
          Memo.UTF8W = (
            '[frxDBDSRelVendasIndividual."venDesComercial"] %')
        end
      end
      object PageHeader1: TfrxPageHeader
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Height = 98.267780000000000000
        ParentFont = False
        Top = 105.826840000000000000
        Width = 718.110700000000000000
        object Shape2: TfrxShapeView
          Top = 3.779530000000000000
          Width = 718.110700000000000000
          Height = 64.252010000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Memo4: TfrxMemoView
          Left = 2.645669290000000000
          Top = 7.559055118110240000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'C'#243'd. venda:')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 2.645669290000000000
          Top = 49.913385830000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Cliente:')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 101.771800000000000000
          Top = 7.559055118110240000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'N. Pedido:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 263.433239290000000000
          Top = 49.913385830000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Vendedor:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 509.559370000000000000
          Top = 28.346456690000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Prazo de entrega:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 230.126160000000000000
          Top = 7.559055120000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Dt Venda:')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 1.897650000000000000
          Top = 28.346456692913400000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Fornecedor:')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 343.196970000000000000
          Top = 28.346456692913400000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Prazo de pagamento:')
          ParentFont = False
        end
        object frxDBDSRelVendasIndividualvenId: TfrxMemoView
          Left = 67.590600000000000000
          Top = 7.559055118110240000
          Width = 30.236240000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'venId'
          DataSet = frxDBDSRelVendasIndividual
          DataSetName = 'frxDBDSRelVendasIndividual'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBDSRelVendasIndividual."venId"]')
          ParentFont = False
        end
        object frxDBDSRelVendasIndividualvenCodPedFornecedor: TfrxMemoView
          Left = 156.519790000000000000
          Top = 7.559055120000000000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'venCodPedFornecedor'
          DataSet = frxDBDSRelVendasIndividual
          DataSetName = 'frxDBDSRelVendasIndividual'
          Memo.UTF8W = (
            '[frxDBDSRelVendasIndividual."venCodPedFornecedor"]')
        end
        object frxDBDSRelVendasIndividualcliNome: TfrxMemoView
          Left = 49.133890000000000000
          Top = 49.913385830000000000
          Width = 211.653680000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'cliNome'
          DataSet = frxDBDSRelVendasIndividual
          DataSetName = 'frxDBDSRelVendasIndividual'
          Memo.UTF8W = (
            '[frxDBDSRelVendasIndividual."cliNome"]')
        end
        object frxDBDSRelVendasIndividualvenDatVenda: TfrxMemoView
          Left = 281.141930000000000000
          Top = 7.559055120000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'venDatVenda'
          DataSet = frxDBDSRelVendasIndividual
          DataSetName = 'frxDBDSRelVendasIndividual'
          Memo.UTF8W = (
            '[frxDBDSRelVendasIndividual."venDatVenda"]')
        end
        object frxDBDSRelVendasIndividualforRazao: TfrxMemoView
          Left = 68.031540000000000000
          Top = 28.346456692913400000
          Width = 272.126160000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'forRazao'
          DataSet = frxDBDSRelVendasIndividual
          DataSetName = 'frxDBDSRelVendasIndividual'
          Memo.UTF8W = (
            '[frxDBDSRelVendasIndividual."forRazao"]')
        end
        object frxDBDSRelVendasIndividualprzPagDescricao: TfrxMemoView
          Left = 456.055350000000000000
          Top = 28.346456690000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'przPagDescricao'
          DataSet = frxDBDSRelVendasIndividual
          DataSetName = 'frxDBDSRelVendasIndividual'
          Memo.UTF8W = (
            '[frxDBDSRelVendasIndividual."przPagDescricao"]')
        end
        object frxDBDSRelVendasIndividualvenPrzEntrega: TfrxMemoView
          Left = 602.677490000000000000
          Top = 28.346456690000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'venPrzEntrega'
          DataSet = frxDBDSRelVendasIndividual
          DataSetName = 'frxDBDSRelVendasIndividual'
          Memo.UTF8W = (
            '[frxDBDSRelVendasIndividual."venPrzEntrega"]')
        end
        object frxDBDSRelVendasIndividualfunNome: TfrxMemoView
          Left = 318.480520000000000000
          Top = 49.913385830000000000
          Width = 253.228510000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'funNome'
          DataSet = frxDBDSRelVendasIndividual
          DataSetName = 'frxDBDSRelVendasIndividual'
          Memo.UTF8W = (
            '[frxDBDSRelVendasIndividual."funNome"]')
        end
        object Memo3: TfrxMemoView
          Left = 356.228510000000000000
          Top = 7.559055120000000000
          Width = 41.574830000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Status:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 516.055350000000000000
          Top = 7.582650310000000000
          Width = 34.015770000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Tipo:')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 619.779840000000000000
          Top = 7.559055120000000000
          Width = 34.015770000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Frete:')
          ParentFont = False
        end
        object Line4: TfrxLineView
          Top = 94.708720000000000000
          Width = 718.110221570000000000
          ShowHint = False
          Diagonal = True
        end
        object Memo10: TfrxMemoView
          Left = 2.645669291338580000
          Top = 78.614173230000000000
          Width = 34.015770000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'SKU')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 61.133890000000000000
          Top = 78.614173228346500000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 266.637910000000000000
          Top = 78.614173228346500000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 381.700990000000000000
          Top = 78.614173228346500000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'R$ Unit'#225'rio')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 316.330860000000000000
          Top = 78.614173230000000000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Qtde Caixa')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 471.275820000000000000
          Top = 78.614173228346500000
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Desc 1')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 530.527830000000000000
          Top = 78.614173228346500000
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Desc 2')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 589.779840000000000000
          Top = 78.614173228346500000
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Desc 3')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 642.299630000000000000
          Top = 78.614173230000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Subtotal')
          ParentFont = False
        end
        object Status: TfrxMemoView
          Left = 396.527830000000000000
          Top = 7.559060000000000000
          Width = 117.165430000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Memo.UTF8W = (
            '[Status]')
        end
        object Tipo: TfrxMemoView
          Left = 551.913730000000000000
          Top = 7.559060000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Memo.UTF8W = (
            '[Tipo]')
        end
        object Frete: TfrxMemoView
          Left = 653.858690000000000000
          Top = 7.559060000000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Memo.UTF8W = (
            '[Frete]')
        end
      end
    end
  end
  object frxPDFExport1: TfrxPDFExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    PrintOptimized = False
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    Left = 488
    Top = 32
  end
end
