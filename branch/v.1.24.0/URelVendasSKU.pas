unit URelVendasSKU;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ComCtrls,
  frxClass, frxExportPDF, frxDBSet,System.UITypes;

type
  TfrmRelVendasSKU = class(TForm)
    GroupBox4: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    DtTmPckInicio: TDateTimePicker;
    DtTmPckFim: TDateTimePicker;
    GroupBox3: TGroupBox;
    ChkBxRepresentada: TCheckBox;
    CmbBxRepresentada: TComboBox;
    GroupBox6: TGroupBox;
    BitBtn2: TBitBtn;
    frxDBDSVendasSKU: TfrxDBDataset;
    frxRptVendasSKU: TfrxReport;
    frxPDFExport1: TfrxPDFExport;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ChkBxRepresentadaClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure DtTmPckInicioExit(Sender: TObject);
    procedure DtTmPckFimExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelVendasSKU: TfrmRelVendasSKU;

implementation

{$R *.dfm}

uses UDM;

procedure TfrmRelVendasSKU.BitBtn2Click(Sender: TObject);
var
  datInicio, datFim : string;
begin
  try
    if ChkBxRepresentada.Checked = false then
    begin
      with dm.queryRelVendaSKU do
      begin
        Close;
        SQL.Clear;
        SQL.Add('SELECT DISTINCT');
        SQL.Add('   a.forRazao,');
        SQL.Add('   c.iteVenProCodFornecedor,');
        SQL.Add('   d.proNome,');
        SQL.Add('   (SELECT SUM(g.iteVenValTotal)');
        SQL.Add('    FROM fornecedores AS e, vendas AS f, itemVenda AS g, produtos AS h');
        SQL.Add('    WHERE  e.forCodigo = f.venForCodigo');
        SQL.Add('    AND f.venId = g.iteVenVenid');
        SQL.Add('    AND g.iteVenProCodFornecedor = h.proCodFornecedor');
        SQL.Add('    AND f.venTipo = 1');
        SQL.Add('    AND f.venDatVenda BETWEEN (:pDataInicio01) AND (:pDataFim01)');
        SQL.Add('    AND g.iteVenProCodFornecedor = d.proCodFornecedor ) AS itemVendaTotal');
        SQL.Add('FROM fornecedores AS a, vendas AS b, itemVenda AS c, produtos AS d');
        SQL.Add('WHERE  a.forCodigo = b.venForCodigo');
        SQL.Add('AND b.venId = c.iteVenVenid');
        SQL.Add('AND c.iteVenProCodFornecedor = d.proCodFornecedor');
        SQL.Add('AND b.venTipo = 1');
        SQL.Add('AND b.venDatVenda BETWEEN (:pDataInicio02) AND (:pDataFim02)');
        SQL.Add('AND a.forRazao = :pRazao');
        SQL.Add('ORDER BY a.forRazao, d.proNome');

        //passagem dos valores das variaveis
        Parameters.ParamByName('pDataInicio01').Value := DtTmPckInicio.DateTime;
        Parameters.ParamByName('pDataInicio02').Value := DtTmPckInicio.DateTime;
        Parameters.ParamByName('pDataFim01').Value := DtTmPckFim.DateTime;
        Parameters.ParamByName('pDataFim02').Value := DtTmPckFim.DateTime;
        Parameters.ParamByName('pRazao').Value := CmbBxRepresentada.Items[CmbBxRepresentada.ItemIndex];
        Open;
      end;
      if dm.queryRelComVendas.IsEmpty then
        MessageDlg('N�o h� dados!', mtConfirmation,[mbOK], 0)
      else
        begin
          datInicio := DateToStr(DtTmPckInicio.DateTime);
          frxRptVendasSKU.Variables['dataInicio'] := QuotedStr(datInicio);
          datFim := DateToStr(DtTmPckFim.DateTime);
          frxRptVendasSKU.Variables['dataFim'] := QuotedStr(datFim);
          frxRptVendasSKU.ShowReport();
         end;
    end
    else
      if ChkBxRepresentada.Checked = true then
      begin
        with dm.queryRelVendaSKU do
        begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT DISTINCT');
          SQL.Add('   a.forRazao,');
          SQL.Add('   c.iteVenProCodFornecedor,');
          SQL.Add('   d.proNome,');
          SQL.Add('   (SELECT SUM(g.iteVenValTotal)');
          SQL.Add('    FROM fornecedores AS e, vendas AS f, itemVenda AS g, produtos AS h');
          SQL.Add('    WHERE  e.forCodigo = f.venForCodigo');
          SQL.Add('    AND f.venId = g.iteVenVenid');
          SQL.Add('    AND g.iteVenProCodFornecedor = h.proCodFornecedor');
          SQL.Add('    AND f.venTipo = 1');
          SQL.Add('    AND f.venDatVenda BETWEEN (:pDataInicio01) AND (:pDataFim01)');
          SQL.Add('    AND g.iteVenProCodFornecedor = d.proCodFornecedor ) AS itemVendaTotal');
          SQL.Add('FROM fornecedores AS a, vendas AS b, itemVenda AS c, produtos AS d');
          SQL.Add('WHERE  a.forCodigo = b.venForCodigo');
          SQL.Add('AND b.venId = c.iteVenVenid');
          SQL.Add('AND c.iteVenProCodFornecedor = d.proCodFornecedor');
          SQL.Add('AND b.venTipo = 1');
          SQL.Add('AND b.venDatVenda BETWEEN (:pDataInicio02) AND (:pDataFim02)');
          SQL.Add('ORDER BY a.forRazao, d.proNome');

          //passagem dos valores das variaveis
          Parameters.ParamByName('pDataInicio01').Value := DtTmPckInicio.DateTime;
          Parameters.ParamByName('pDataInicio02').Value := DtTmPckInicio.DateTime;
          Parameters.ParamByName('pDataFim01').Value := DtTmPckFim.DateTime;
          Parameters.ParamByName('pDataFim02').Value := DtTmPckFim.DateTime;
          Open;
        end;
        if dm.queryRelComVendas.IsEmpty then
          MessageDlg('N�o h� dados!', mtConfirmation,[mbOK], 0)
        else
          begin
            datInicio := DateToStr(DtTmPckInicio.DateTime);
            frxRptVendasSKU.Variables['dataInicio'] := QuotedStr(datInicio);
            datFim := DateToStr(DtTmPckFim.DateTime);
            frxRptVendasSKU.Variables['dataFim'] := QuotedStr(datFim);
            frxRptVendasSKU.ShowReport();
          end;
      end;
  except
    on E:exception do
      ShowMessage('Error: ' + E.Message +#13+'class name = '+E.ClassName);

  end;


end;

procedure TfrmRelVendasSKU.ChkBxRepresentadaClick(Sender: TObject);
begin
  if ChkBxRepresentada.Checked= true then
    CmbBxRepresentada.Enabled := false
  else
    CmbBxRepresentada.Enabled := true;
end;

procedure TfrmRelVendasSKU.DtTmPckFimExit(Sender: TObject);
begin
  if DtTmPckFim.DateTime < DtTmPckInicio.DateTime then
  begin
    MessageDlg('Data final deve ser maior ou igual � data inicial', mtWarning,[mbOK], 0);
    DtTmPckFim.SetFocus;
  end;
end;

procedure TfrmRelVendasSKU.DtTmPckInicioExit(Sender: TObject);
begin
  if DtTmPckInicio.DateTime > DtTmPckFim.DateTime then
  begin
    MessageDlg('Data inicial deve ser menor ou igual � data final', mtWarning,[mbOK], 0);
    DtTmPckInicio.SetFocus;
  end;

end;

procedure TfrmRelVendasSKU.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //fecha a conexao com a tabela
  dm.tabFornecedores.Close;
end;

procedure TfrmRelVendasSKU.FormShow(Sender: TObject);
var
  rCampo : string;
begin
  //abre a conexao com a tabela
  dm.tabFornecedores.Open;

  ChkBxRepresentada.Checked := true;

  //filtro de fornecedores
  if not DM.tabFornecedores.Eof then
  begin
    dm.tabFornecedores.First;
    while not DM.tabFornecedores.Eof do
    begin
      if dm.tabFornecedoresforStatus.AsInteger = 1 then
      begin
        rCampo:= dm.tabFornecedoresforRazao.AsString;
        CmbBxRepresentada.Items.Add(rCampo);
      end;
      DM.tabFornecedores.Next;
    end;
    CmbBxRepresentada.ItemIndex:=-1;
  end;

  DtTmPckInicio.Date := now;
  DtTmPckFim.Date := now;
end;

end.
