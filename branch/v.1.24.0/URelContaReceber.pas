unit URelContaReceber;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, frxClass, frxDBSet, frxExportPDF,
  Vcl.StdCtrls, Vcl.Buttons, Vcl.ComCtrls;

type
  TfrmRelConAReceber = class(TForm)
    GroupBox4: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    DtTmPckInicio: TDateTimePicker;
    DtTmPckFim: TDateTimePicker;
    GroupBox3: TGroupBox;
    ChkBxCategorias: TCheckBox;
    CmbBxCategoria: TComboBox;
    GroupBox6: TGroupBox;
    BitBtn1: TBitBtn;
    frxRptRelConReceber: TfrxReport;
    frxPDFExport1: TfrxPDFExport;
    frxDBDSRelConReceber: TfrxDBDataset;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ChkBxCategoriasClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelConAReceber: TfrmRelConAReceber;

implementation

{$R *.dfm}

uses UDM, UFuncoes;

procedure TfrmRelConAReceber.BitBtn1Click(Sender: TObject);
var
  datInicio, datFim : string;
begin
  try
    if DtTmPckInicio.Date > DtTmPckFim.Date then
      MessageDlg('Data inicial maior que data final!! ', mtWarning,[mbOK], 0)
    else
      if ChkBxCategorias.Checked = true then
        with dm.queryRelConAReceber do
        begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT r.recDatVencimento, r.recValor,r.recFornecedor,');
          SQL.Add('r.recHistorico, t.tipLanDescricao, s.subTipDescricao');
          SQL.Add('FROM receber r,');
          SQL.Add('tiposLancamento t, subTipos s');
          SQL.Add('WHERE r.recTipLanCodigo = t.tipLanCodigo');
          SQL.Add('AND r.recSubTipCodigo = s.subTipCodigo');
          SQL.Add('AND r.recDatPaga  is null');
          SQL.Add('AND r.recValpago is null');
          SQL.Add('AND r.recDatVencimento between FORMAT(#'+DateToStr(DtTmPckInicio.Date)+'#,"mm/dd/yyyy")');
          SQL.Add('AND FORMAT(#'+DateToStr(DtTmPckFim.Date)+'#,"mm/dd/yyyy") ');
          SQL.Add('ORDER BY r.recDatVencimento');
          Open;
        end
      else
        with dm.queryRelConAReceber do
        begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT r.recDatVencimento, r.recValor,r.recFornecedor,');
          SQL.Add('r.recHistorico, t.tipLanDescricao, s.subTipDescricao');
          SQL.Add('FROM receber r,');
          SQL.Add('tiposLancamento t, subTipos s');
          SQL.Add('WHERE r.recTipLanCodigo = t.tipLanCodigo');
          SQL.Add('AND r.recSubTipCodigo = s.subTipCodigo');
          SQL.Add('AND r.recDatPaga  is null');
          SQL.Add('AND r.recValpago is null');
          SQL.Add('AND r.recDatVencimento between FORMAT(#'+DateToStr(DtTmPckInicio.Date)+'#,"mm/dd/yyyy")');
          SQL.Add('AND FORMAT(#'+DateToStr(DtTmPckFim.Date)+'#,"mm/dd/yyyy") ');
          SQL.Add('AND t.tipLanDescricao = '+ QuotedStr(CmbBxCategoria.Items[CmbBxCategoria.ItemIndex]));
          SQL.Add('ORDER BY r.recDatVencimento');
          Open;
        end;

      //chamada do relat�rio
      datInicio := DateToStr(DtTmPckInicio.DateTime);
      frxRptRelConReceber.Variables['datInicio'] := QuotedStr(datInicio);
      datFim := DateToStr(DtTmPckFim.DateTime);
      frxRptRelConReceber.Variables['datFim'] := QuotedStr(datFim);
      frxRptRelConReceber.ShowReport();
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro: '+ frmRelConAReceber.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro: '+ frmRelConAReceber.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmRelConAReceber.ChkBxCategoriasClick(Sender: TObject);
begin
  if ChkBxCategorias.Checked =  true then
    CmbBxCategoria.Enabled := false
  else
    CmbBxCategoria.Enabled := true;
end;

procedure TfrmRelConAReceber.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  //fecha a conexao com a tabela
  dm.tabTipLancamentos.Close;
end;

procedure TfrmRelConAReceber.FormShow(Sender: TObject);
var
  rCampo : string;
begin
  //abre a conex�o com a tabala de categorias
  dm.tabTipLancamentos.Open;

  ChkBxCategorias.Checked := true;

  //filtro de fornecedores
  if not DM.tabTipLancamentos.Eof then
  begin
    dm.tabTipLancamentos.First;
    while not DM.tabTipLancamentos.Eof do
    begin
      rCampo:= dm.tabTipLancamentostipLanDescricao.AsString;
      CmbBxCategoria.Items.Add(rCampo);
      DM.tabTipLancamentos.Next;
    end;
    CmbBxCategoria.ItemIndex:=0;
  end;

  DtTmPckInicio.Date := now-30;
  DtTmPckFim.Date := now;
end;

end.
