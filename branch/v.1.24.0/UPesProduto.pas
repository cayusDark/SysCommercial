unit UPesProduto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.Grids, Vcl.DBGrids,
  Vcl.StdCtrls, System.UITypes;

type
  TfrmPesProduto = class(TForm)
    Label1: TLabel;
    edtCodProduto: TEdit;
    DBGrid1: TDBGrid;
    StatusBar1: TStatusBar;
    procedure edtCodProdutoKeyPress(Sender: TObject; var Key: Char);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPesProduto: TfrmPesProduto;

implementation

{$R *.dfm}

uses UDM;

procedure TfrmPesProduto.DBGrid1DblClick(Sender: TObject);
begin
  //fecha a tela
  close;
end;

procedure TfrmPesProduto.DBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then// #13 � a representa��o da tecla Enter, o key � a chave
    Close;
end;

procedure TfrmPesProduto.edtCodProdutoKeyPress(Sender: TObject; var Key: Char);
var
  fornecedor: string;
begin
  fornecedor:=IntToStr(dm.tabVendavenForCodigo.AsInteger);
  MessageDlg(fornecedor,mtInformation,mbOKCancel,0);
end;

end.
