unit UCadGrpClientes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.Grids,
  Vcl.DBGrids, DB;

type
  TfrmCadGrpClientes = class(TForm)
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    BitBtn1: TBitBtn;
    GroupBox5: TGroupBox;
    BtnPrior: TBitBtn;
    BtnFirst: TBitBtn;
    BtnNext: TBitBtn;
    BtnLast: TBitBtn;
    BtnInsert: TBitBtn;
    BtnDelete: TBitBtn;
    BtnEdit: TBitBtn;
    BtnPost: TBitBtn;
    BtnCancel: TBitBtn;
    edtPesquisar: TEdit;
    DBGrdGrpClientes: TDBGrid;
    procedure BtnFirstClick(Sender: TObject);
    procedure BtnPriorClick(Sender: TObject);
    procedure BtnNextClick(Sender: TObject);
    procedure BtnLastClick(Sender: TObject);
    procedure BtnInsertClick(Sender: TObject);
    procedure BtnDeleteClick(Sender: TObject);
    procedure BtnEditClick(Sender: TObject);
    procedure BtnPostClick(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure edtPesquisarChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure DBGrdGrpClienteDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrdGrpClienteDblClick(Sender: TObject);
    procedure DBGrdGrpClienteKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure DBGrdGrpClientesDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrdGrpClientesKeyPress(Sender: TObject; var Key: Char);
    procedure DBGrdGrpClientesKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nRegistro : integer;
  end;

var
  frmCadGrpClientes: TfrmCadGrpClientes;

implementation

{$R *.dfm}

uses UDM, UCadGrpXClientes, UCadGrpXUsuarios;

procedure TfrmCadGrpClientes.BitBtn1Click(Sender: TObject);
begin
  frmCadGrpXClientes:= TfrmCadGrpXClientes.Create(Self); //cria��o manual
  frmCadGrpXClientes.ShowModal; //exibe a tela no modo modal
  frmCadGrpXClientes.Release; //libera a tela da mem�ria
  frmCadGrpXClientes:= nil; //atribui o conte�do nulo para a vari�vel frmCadGrpXUsuarios

  nRegistro :=DM.tabGrupoClientesgrpCliCodigo.AsInteger;
end;

procedure TfrmCadGrpClientes.BtnCancelClick(Sender: TObject);
begin
  //cancela a a�ao no registro
  dm.tabGrupoClientes.Cancel;

  //desabilita os bot�es
  BtnFirst.Enabled := true;
  BtnPrior.Enabled := true;
  edtPesquisar.Enabled := true;
  BtnNext.Enabled := true;
  BtnLast.Enabled := true;
  BtnInsert.Enabled := true;
  BtnDelete.Enabled := true;
  BtnEdit.Enabled := true;
  DBGrdGrpClientes.ReadOnly := true;

  //habilita os bot�es
  BtnPost.Enabled := false;
  BtnCancel.Enabled := false;
end;

procedure TfrmCadGrpClientes.BtnDeleteClick(Sender: TObject);
begin
  try
    //apaga um registro
    if MessageDlg('Deseja excluir o registro?', mtConfirmation,[mbYes, mbNo], 0) = mrYes then
      dm.tabGrupoClientes.Delete;
  except
    on E: Exception do
      MessageDlg('Erro ao excluir registro: '+E.Message+#13+
                 'Class name: '+E.ClassName,mtError,[mbOK],0);
  end;
end;

procedure TfrmCadGrpClientes.BtnEditClick(Sender: TObject);
begin
  //edita o registro
  dm.tabGrupoClientes.Edit;

  //desabilita os bot�es
  BtnFirst.Enabled := false;
  BtnPrior.Enabled := false;
  edtPesquisar.Enabled := false;
  BtnNext.Enabled := false;
  BtnLast.Enabled := false;
  BtnInsert.Enabled := false;
  BtnDelete.Enabled := false;
  BtnEdit.Enabled := false;
  DBGrdGrpClientes.ReadOnly := false;

  //habilita os bot�es
  BtnPost.Enabled := true;
  BtnCancel.Enabled := true;

  DBGrdGrpClientes.SetFocus;

end;

procedure TfrmCadGrpClientes.BtnFirstClick(Sender: TObject);
begin
  if (BtnNext.Enabled = false) or (BtnLast.Enabled = false) then
  begin
    BtnNext.Enabled := true;
    BtnLast.Enabled := true;
  end;

  BtnPrior.Enabled := false;
  BtnFirst.Enabled := false;

  //primeiro registro
  dm.tabGrupoClientes.First;
end;

procedure TfrmCadGrpClientes.BtnInsertClick(Sender: TObject);
begin
  //inseri um novo registro
  dm.tabGrupoClientes.Insert;

  //desabilita os bot�es
  BtnFirst.Enabled := false;
  BtnPrior.Enabled := false;
  edtPesquisar.Enabled := false;
  BtnNext.Enabled := false;
  BtnLast.Enabled := false;
  BtnInsert.Enabled := false;
  BtnDelete.Enabled := false;
  BtnEdit.Enabled := false;
  DBGrdGrpClientes.ReadOnly := false;

  //habilita os bot�es
  BtnPost.Enabled := true;
  BtnCancel.Enabled := true;

  // Joga o foco para uma coluna especifica
  DBGrdGrpClientes.SetFocus;
end;

procedure TfrmCadGrpClientes.BtnLastClick(Sender: TObject);
begin
  if (BtnPrior.Enabled = false) or (BtnFirst.Enabled = false) then
  begin
    BtnPrior.Enabled := true;
    BtnFirst.Enabled := true;
  end;

  BtnNext.Enabled :=false;
  BtnLast.Enabled := false;

  //ultimo registro
  dm.tabGrupoClientes.Last;
end;

procedure TfrmCadGrpClientes.BtnNextClick(Sender: TObject);
begin
  //enquanto n�o for o ultimo registro
  if dm.tabGrupoClientes.Eof then
  begin
    BtnNext.Enabled :=false;
    BtnLast.Enabled := false;
  end
  else
  begin
    if (BtnPrior.Enabled = false) or (BtnFirst.Enabled = false) then
    begin
      BtnPrior.Enabled := true;
      BtnFirst.Enabled := true;
    end;

    //proximo registro
    dm.tabGrupoClientes.Next;
  end;
end;

procedure TfrmCadGrpClientes.BtnPostClick(Sender: TObject);
begin
  //confirma as altera�oes no registro
  if (dm.tabGrupoClientes.State = dsEdit) or (dm.tabGrupoClientes.State = dsInsert) then
    dm.tabGrupoClientes.Post;

  //desabilita os bot�es
  BtnFirst.Enabled := true;
  BtnPrior.Enabled := true;
  edtPesquisar.Enabled := true;
  BtnNext.Enabled := true;
  BtnLast.Enabled := true;
  BtnInsert.Enabled := true;
  BtnDelete.Enabled := true;
  BtnEdit.Enabled := true;
  DBGrdGrpClientes.ReadOnly := true;

  //habilita os bot�es
  BtnPost.Enabled := false;
  BtnCancel.Enabled := false;
end;

procedure TfrmCadGrpClientes.BtnPriorClick(Sender: TObject);
begin
  //verifica se esta no primeiro registro
  if dm.tabGrupoClientes.Bof then
  begin
    BtnPrior.Enabled := false;
    BtnFirst.Enabled := false;

    if (BtnNext.Enabled = false) or (BtnLast.Enabled = false) then
    begin
      BtnNext.Enabled := true;
      BtnLast.Enabled := true;
    end;

  end
  else
  begin
    if (BtnNext.Enabled = false) or (BtnLast.Enabled = false) then
    begin
      BtnNext.Enabled := true;
      BtnLast.Enabled := true;
    end;

    //registro anterior
    dm.tabGrupoClientes.Prior;
  end;
end;

procedure TfrmCadGrpClientes.DBGrdGrpClienteDblClick(Sender: TObject);
begin
  if (dm.tabGrupoClientes.State <> dsInsert) or (dm.tabGrupoClientes.State <> dsEdit) then
  begin
    ShowMessage('Clique editar ou inserir para modifica��o no grid!!');
    DBGrdGrpClientes.SetFocus;
  end;

end;

procedure TfrmCadGrpClientes.DBGrdGrpClienteDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  //deixa o dbgrid zebrado
  If Odd(dm.tabGrupoClientes.RecNo) and (dm.tabGrupoClientes.State <> dsInsert) then
  begin //Lembre-se de colocar a unit DB na cl�usula uses na unit da tela.
      DBGrdGrpClientes.Canvas.Brush.Color := clMoneyGreen; // muda a cor do pincel
      DBGrdGrpClientes.Canvas.FillRect(Rect); // Preenche o fundo com a cor especificada
      DBGrdGrpClientes.DefaultDrawDataCell(Rect,Column.Field,State);// desenha as c�lulas da grade
  end;
end;

procedure TfrmCadGrpClientes.DBGrdGrpClienteKeyPress(Sender: TObject;
  var Key: Char);
begin
  Key := UpCase(Key);

   If Key = #13 Then Begin
    If HiWord(GetKeyState(VK_SHIFT)) <> 0 then begin
     with (Sender as TDBGrid) do
     if selectedindex > 0 then
      selectedindex := selectedindex - 1
     else begin
      DataSource.DataSet.Prior;
      selectedindex := fieldcount - 1;
     end;
    end else begin
     with (Sender as TDBGrid) do
     if selectedindex < (fieldcount - 1) then
      selectedindex := selectedindex + 1
     else begin
      DataSource.DataSet.Next;
      selectedindex := 0;
     end;
   end;
   Key := #0
   end;
end;

procedure TfrmCadGrpClientes.DBGrdGrpClientesDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  //deixa o dbgrid zebrado
  If Odd(dm.tabGrupoClientes.RecNo) and (dm.tabGrupoClientes.State <> dsInsert) then
  begin //Lembre-se de colocar a unit DB na cl�usula uses na unit da tela.
      DBGrdGrpClientes.Canvas.Brush.Color := clMoneyGreen; // muda a cor do pincel
      DBGrdGrpClientes.Canvas.FillRect(Rect); // Preenche o fundo com a cor especificada
      DBGrdGrpClientes.DefaultDrawDataCell(Rect,Column.Field,State);// desenha as c�lulas da grade
  end;
end;

procedure TfrmCadGrpClientes.DBGrdGrpClientesKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  confExcluir: Integer;
  nRegistro: Integer;
  nReg: Integer;
begin
  nRegistro := DM.tabGrupoClientesgrpCliCodigo.AsInteger;

  if Key = VK_DELETE Then
  begin
    confExcluir:= Application.MessageBox('Deseja excluir o lan�amento ?', 'Aten��o', MB_YesNo+mb_DefButton2+mb_IconQuestion);
    if confExcluir = IDYes then
    begin
      with dm.tabGrupoClientes do
      begin
        DM.queryItemVenda.Close;
        DM.queryItemVenda.SQL.Clear;
        DM.queryItemVenda.SQL.Add('DELETE FROM grupoClientes WHERE grpCliCodigo =:nReg');
        DM.queryItemVenda.Parameters.ParamByName('nReg').Value := nRegistro;
        DM.queryItemVenda.ExecSQL;
      end;
      dm.tabGrupoClientes.Close;
      dm.tabGrupoClientes.Open;
    end;
  end;
end;

procedure TfrmCadGrpClientes.DBGrdGrpClientesKeyPress(Sender: TObject;
  var Key: Char);
begin
  Key := UpCase(Key)
end;

procedure TfrmCadGrpClientes.DBGrid1DblClick(Sender: TObject);
begin
  if (dm.tabGrupoClientes.State <> dsInsert) or (dm.tabGrupoClientes.State <> dsEdit) then
  begin
    ShowMessage('Clique editar ou inserir para modifica��o no grid!!');
    DBGrdGrpClientes.SetFocus;
  end;
end;

procedure TfrmCadGrpClientes.edtPesquisarChange(Sender: TObject);
begin
  //pesquisa aproximada por descri�ao
  dm.tabGrupoClientes.Locate('grpCliDescricao', edtPesquisar.Text, [loCaseInsensitive, loPartialKey]);
end;

procedure TfrmCadGrpClientes.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  //FECHA a conex�o com a tabela
  if (dm.tabGrupoClientes.State=dsEdit) or (dm.tabGrupoClientes.State=dsInsert) then
  begin
    if MessageDlg('Cadastro em modo de inser��o ou edi��o. Deseja fechar a tela?', mtConfirmation,[mbYes, mbNo], 0) = mrYes then
    begin
      dm.tabGrupoClientes.Close;
      Action := caFree; //a��o do objeto TCloseAction para fechar a tela
    end
    else
      Action := caNone ; //a��o do objeto TCloseAction para n�o fechar a tela
  end
  else
    dm.tabGrupoClientes.Close;
end;

procedure TfrmCadGrpClientes.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  confExcluir: Integer;
  nRegistro: Integer;
  nReg: Integer;
begin
  nRegistro := DM.tabGrupoClientesgrpCliCodigo.AsInteger;

  if Key = VK_DELETE Then
  begin
    confExcluir:= Application.MessageBox('Deseja excluir o lan�amento ?', 'Aten��o', MB_YesNo+mb_DefButton2+mb_IconQuestion);
    if confExcluir = IDYes then
    begin
      with DM.queryGrpClientes do
      begin
        Close;
        SQL.Clear;
        SQL.Add('DELETE FROM GrupoClientes WHERE grpUsuCodigo =:nReg');
        Parameters.ParamByName('nReg').Value := nRegistro;
        ExecSQL;
      end;
      dm.tabGrupoClientes.Close;
      dm.tabGrupoClientes.Open;
    end;
  end;

end;

procedure TfrmCadGrpClientes.FormShow(Sender: TObject);
begin
  //abre a conex�o com a tabela
  dm.tabGrupoClientes.Open;
end;

end.
