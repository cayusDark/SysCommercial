object frmPesProduto: TfrmPesProduto
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = 'Pesquisa de produtos'
  ClientHeight = 452
  ClientWidth = 634
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 5
    Top = 9
    Width = 89
    Height = 13
    Caption = 'C'#243'digo do produto'
  end
  object edtCodProduto: TEdit
    Left = 101
    Top = 6
    Width = 121
    Height = 21
    TabOrder = 0
    OnKeyPress = edtCodProdutoKeyPress
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 40
    Width = 624
    Height = 382
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
    OnKeyPress = DBGrid1KeyPress
    Columns = <
      item
        Expanded = False
        FieldName = 'proCodfornecedor'
        Width = 113
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'proNome'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'proPreVenda'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'proUnidade'
        Visible = True
      end>
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 433
    Width = 634
    Height = 19
    Panels = <>
    ExplicitTop = 423
    ExplicitWidth = 624
  end
end
