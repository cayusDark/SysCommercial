unit URelContaPagar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ComCtrls,
  frxClass, frxExportPDF, frxDBSet;

type
  TfrmRelConAPagar = class(TForm)
    GroupBox4: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    DtTmPckInicio: TDateTimePicker;
    DtTmPckFim: TDateTimePicker;
    GroupBox3: TGroupBox;
    ChkBxCategorias: TCheckBox;
    CmbBxCategoria: TComboBox;
    GroupBox6: TGroupBox;
    BitBtn1: TBitBtn;
    frxDBDSRelConAPagar: TfrxDBDataset;
    frxRptRelConAPagar: TfrxReport;
    frxPDFExport1: TfrxPDFExport;
    procedure ChkBxCategoriasClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelConAPagar: TfrmRelConAPagar;

implementation

{$R *.dfm}

uses UDM, UFuncoes;

procedure TfrmRelConAPagar.BitBtn1Click(Sender: TObject);
var
  datInicio, datFim : string;
begin
  try
    if DtTmPckInicio.Date > DtTmPckFim.Date then
      MessageDlg('Data inicial maior que data final!! ', mtWarning,[mbOK], 0)
    else
      if ChkBxCategorias.Checked = true then
        with dm.queryRelConAPagar do
        begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT p.pagDatVencimento, p.pagValor,p.pagPagarA,p.pagHistorico,');
          SQL.Add('t.tipLanDescricao, s.subTipDescricao');
          SQL.Add('FROM pagar p,  tiposLancamento t, subTipos s');
          SQL.Add('WHERE p.pagTipLanCodigo = t.tipLanCodigo');
          SQL.Add('AND p.pagSubTipCodigo = s.subTipCodigo');
          SQL.Add('AND p.pagDatPaga is null');
          SQL.Add('AND p.pagValpago is null');
          SQL.Add('AND p.pagDatVencimento between FORMAT(#'+DateToStr(DtTmPckInicio.Date)+'#,"mm/dd/yyyy")');
          SQL.Add('AND FORMAT(#'+DateToStr(DtTmPckFim.Date)+'#,"mm/dd/yyyy")');
          SQL.Add('ORDER BY p.pagDatVencimento');
          Open;
        end
      else
        with dm.queryRelConAPagar do
        begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT p.pagDatVencimento, p.pagValor,p.pagPagarA,p.pagHistorico,');
          SQL.Add('t.tipLanDescricao, s.subTipDescricao');
          SQL.Add('FROM pagar p,  tiposLancamento t, subTipos s');
          SQL.Add('WHERE p.pagTipLanCodigo = t.tipLanCodigo');
          SQL.Add('AND p.pagSubTipCodigo = s.subTipCodigo');
          SQL.Add('AND p.pagDatPaga is null');
          SQL.Add('AND p.pagValpago is null');
          SQL.Add('AND p.pagDatVencimento between FORMAT(#'+DateToStr(DtTmPckInicio.Date)+'#,"mm/dd/yyyy")');
          SQL.Add('AND FORMAT(#'+DateToStr(DtTmPckFim.Date)+'#,"mm/dd/yyyy")');
          SQL.Add('AND t.tipLanDescricao = '+ QuotedStr(CmbBxCategoria.Items[CmbBxCategoria.ItemIndex]));
          SQL.Add('ORDER BY p.pagDatVencimento');
          Open;
        end;

      //chamada do relat�rio
      datInicio := DateToStr(DtTmPckInicio.DateTime);
      frxRptRelConAPagar.Variables['datInicio'] := QuotedStr(datInicio);
      datFim := DateToStr(DtTmPckFim.DateTime);
      frxRptRelConAPagar.Variables['datFim'] := QuotedStr(datFim);
      frxRptRelConAPagar.ShowReport();
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro: '+ frxRptRelConAPagar.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro: '+ frxRptRelConAPagar.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;

end;

procedure TfrmRelConAPagar.ChkBxCategoriasClick(Sender: TObject);
begin
  if ChkBxCategorias.Checked =  true then
    CmbBxCategoria.Enabled := false
  else
    CmbBxCategoria.Enabled := true;
end;

procedure TfrmRelConAPagar.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //fecha a conexao com a tabela
  dm.tabTipLancamentos.Close;

end;

procedure TfrmRelConAPagar.FormShow(Sender: TObject);
var
  rCampo : string;
begin
  //abre a conex�o com a tabala de categorias
  dm.tabTipLancamentos.Open;

  ChkBxCategorias.Checked := true;

  //filtro de fornecedores
  if not DM.tabTipLancamentos.Eof then
  begin
    dm.tabTipLancamentos.First;
    while not DM.tabTipLancamentos.Eof do
    begin
      rCampo:= dm.tabTipLancamentostipLanDescricao.AsString;
      CmbBxCategoria.Items.Add(rCampo);
      DM.tabTipLancamentos.Next;
    end;
    CmbBxCategoria.ItemIndex:=0;
  end;

  DtTmPckInicio.Date := now-30;
  DtTmPckFim.Date := now;

end;

end.
