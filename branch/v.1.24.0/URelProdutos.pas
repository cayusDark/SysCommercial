unit URelProdutos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.DBCtrls, Vcl.Buttons,
  Vcl.ExtCtrls, frxClass, frxDBSet, frxExportCSV, frxExportRTF, frxExportPDF;

type
  TfrmRelProdutos = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox3: TGroupBox;
    BitBtn1: TBitBtn;
    ChkBxRepresentada: TCheckBox;
    RdGrpCamOrdenacao: TRadioGroup;
    RdGrpOrdem: TRadioGroup;
    frxRptProdutos: TfrxReport;
    frxDBDSProdutos: TfrxDBDataset;
    frxPDFExport1: TfrxPDFExport;
    CmbBxRepresentada: TComboBox;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure DBCBxRepresentadaClick(Sender: TObject);
    procedure DBCBxRepresentadaExit(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure ChkBxRepresentadaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelProdutos: TfrmRelProdutos;

implementation

{$R *.dfm}

uses UDM;

procedure TfrmRelProdutos.BitBtn1Click(Sender: TObject);
begin
  try
    with dm.queryRelProdutos do
    begin
      close;
      SQL.Clear;
      SQL.Add('SELECT a.proId, a.proForCodigo,a.proCodFornecedor, a.proNome, a.proNomReduzido,');
      SQL.Add(' a.proStatus, b.forCodigo, b.forRazao, b.forNomFantasia, c.catCodigo, c.catDescricao');
      SQL.Add('FROM produtos AS a, fornecedores AS b , categorias AS c');
      SQL.Add('WHERE b.forCodigo = a.proForCodigo');
      SQL.Add('AND c.catCodigo = a.proCatCodigo');
      SQL.Add('AND b.forRazao = :pFornecedor');

      //ordena��o
      if RdGrpCamOrdenacao.ItemIndex = 0 then
        SQL.Add('ORDER BY  b.forRazao, c.catDescricao, a.proNome')
      else
        SQL.Add('ORDER BY b.forRazao, c.catDescricao, a.proCodFornecedor');
      if RdGrpOrdem.ItemIndex = 1 then
        SQL.Add('DESC');

      Parameters.ParamByName('pFornecedor').Value := CmbBxRepresentada.Items[CmbBxRepresentada.ItemIndex];;
      Open;
    end;

     //chamada do relat�rio
    if dm.queryRelProdutos.IsEmpty then
      MessageDlg('N�o h� dados!', mtConfirmation,[mbOK], 0)
    else
      begin
        frxRptProdutos.Variables['fornecedor'] := QuotedStr(CmbBxRepresentada.Items[CmbBxRepresentada.ItemIndex]);
        frxRptProdutos.ShowReport();
      end;
  except
    on E: Exception do
    begin
      ShowMessage('Error: ' + E.Message +#13+'class name = '+E.ClassName);
    end;
  end;

end;

procedure TfrmRelProdutos.ChkBxRepresentadaClick(Sender: TObject);
begin
  //ativa e desativa o cdbcombobox quando o checkbox � marcado
  if ChkBxRepresentada.Checked = true then
    CmbBxRepresentada.Enabled := false
  else
    CmbBxRepresentada.Enabled := true;
end;

procedure TfrmRelProdutos.DBCBxRepresentadaClick(Sender: TObject);
begin
  dm.tabFornecedores.Edit;
end;

procedure TfrmRelProdutos.DBCBxRepresentadaExit(Sender: TObject);
begin
  dm.tabFornecedores.Cancel;
end;

procedure TfrmRelProdutos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //fecha a conex�o com as tabelas
  dm.tabFornecedores.Close;
end;

procedure TfrmRelProdutos.FormShow(Sender: TObject);
var
  rCampo, cCampo: string;
begin

  dm.tabFornecedores.Open;

  //filtro de fornecedores
  if not DM.tabFornecedores.Eof then
  begin
    dm.tabFornecedores.First;
    while not DM.tabFornecedores.Eof do
    begin
      if dm.tabFornecedoresforStatus.AsInteger = 1 then
      begin
        rCampo:= dm.tabFornecedoresforRazao.AsString;
        CmbBxRepresentada.Items.Add(rCampo);
      end;
      DM.tabFornecedores.Next;
    end;
    CmbBxRepresentada.ItemIndex:=0;
  end;

  //flag deafult
  ChkBxRepresentada.Checked := true;
end;

end.
