unit UFinConAPagar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ComCtrls, DB,
  Vcl.Grids, Vcl.DBGrids;
type
  TfrmFinConAPagar = class(TForm)
    StatusBar1: TStatusBar;
    DBGrdConPagar: TDBGrid;
    GroupBox5: TGroupBox;
    BtnPrior: TBitBtn;
    BtnFirst: TBitBtn;
    BtnNext: TBitBtn;
    BtnLast: TBitBtn;
    BtnInsert: TBitBtn;
    BtnDelete: TBitBtn;
    BtnEdit: TBitBtn;
    BtnBaixa: TBitBtn;
    edtPesquisar: TEdit;
    procedure BtnInsertClick(Sender: TObject);
    procedure BtnEditClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure DBGrdConPagarKeyPress(Sender: TObject; var Key: Char);
    procedure DBGrdConPagarTitleClick(Column: TColumn);
    procedure BtnFirstClick(Sender: TObject);
    procedure BtnPriorClick(Sender: TObject);
    procedure edtPesquisarChange(Sender: TObject);
    procedure BtnNextClick(Sender: TObject);
    procedure BtnLastClick(Sender: TObject);
    procedure BtnBaixaClick(Sender: TObject);
    procedure BtnDeleteClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFinConAPagar: TfrmFinConAPagar;
  //variavel auxiliar para ordena��o do DBGRID clicando no titulo do grid
  ascendente : Boolean;
  statusContas : integer;

implementation

{$R *.dfm}

uses UDM, UCadConAPagar, UFuncoes, UMain, UBaiConAPagar;

procedure TfrmFinConAPagar.BtnBaixaClick(Sender: TObject);
begin
  //baixa de conta a pagar
  try
    //chama a tela de baixa de conta
    frmBaiConAPagar:= TfrmBaiConAPagar.Create(Self); //cria��o manual
    frmBaiConAPagar.ShowModal; //exibe a tela no modo modal
    frmBaiConAPagar.Release; //libera a tela da mem�ria
    frmBaiConAPagar:= nil; //atribui o conte�do nulo para a vari�vel frmConAPagar

    //log de inser��o
    Log('Baixa da conta a receber'+ dm.tabPagarpagHistorico.AsString  +' pelo usu�rio '+ frmMain.usuario+'.');
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro: '+ frmFinConAPagar.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro: '+ frmFinConAPagar.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmFinConAPagar.BtnDeleteClick(Sender: TObject);
var
  confExcluir: Integer;
  nRegistro: Integer;
  descricao: string ;
begin
  try
    nRegistro := DM.tabPagarpagCodigo.AsInteger;
    descricao := dm.tabPagarpagHistorico.AsString;

    confExcluir:= Application.MessageBox('Deseja excluir o lan�amento?', 'Aten��o', MB_YesNo+mb_DefButton2+mb_IconQuestion);
    if confExcluir = IDYes then
    begin
      with dm.queryDelSubTipo do
      begin
        Close;
        SQL.Clear;
        SQL.Text := 'DELETE FROM pagar WHERE pagCodigo= '+ IntToStr(nRegistro);
        ExecSQL;
      end;
      dm.tabPagar.Close;
      dm.tabPagar.Open;
      Log('Conta a pagar '+ descricao+' apagado pelo usu�rio '+ frmMain.usuario+'.');
    end;
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro: '+ frmFinConAPagar.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro: '+ frmFinConAPagar.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmFinConAPagar.BtnEditClick(Sender: TObject);
begin
  try
    //status conta como ativo
    statusContas := 0;

    //log de edi�ao

    frmCadConAPagar:=TfrmCadConAPagar.Create(Self);//cria��o manual
    frmCadConAPagar.ShowModal;//chama a tela de backup e restore no modo modal
    frmCadConAPagar.Release;//libera a tela da mem�ria
    frmCadConAPagar:=nil;//atribui o conte�do nulo para a vari�vel frmCadConAPagar
    //frmCadConAPagar.Free;
    //Log('Edi��o da conta a receber'+ dm.tabReceberrecHistorico.AsString  +' pelo usu�rio '+ frmMain.usuario+'.');

  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro: '+ frmFinConAPagar.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro: '+ frmFinConAPagar.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmFinConAPagar.BtnFirstClick(Sender: TObject);
begin
  //vai ao primeiro registro da tabela
  dm.tabPagar.First;
end;

procedure TfrmFinConAPagar.BtnInsertClick(Sender: TObject);
begin
  try
    //status conta como ativo
    statusContas := 1;

    //chama a tela de cadastro de contas a pagar
    frmCadConAPagar:=TfrmCadConAPagar.Create(Self);//cria��o manual
    frmCadConAPagar.ShowModal;//chama a tela de backup e restore no modo modal
    frmCadConAPagar.Release;//libera a tela da mem�ria
    frmCadConAPagar:=nil;//atribui o conte�do nulo para a vari�vel frmCadConAPagar
    //frmCadConAPagar.Free;
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro: '+ frmFinConAPagar.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro: '+ frmFinConAPagar.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmFinConAPagar.BtnLastClick(Sender: TObject);
begin
  //vai ao ultimo registro da tabela
  dm.tabPagar.Last;
end;

procedure TfrmFinConAPagar.BtnNextClick(Sender: TObject);
begin
  //vai ao pr�ximo registro
  dm.tabPagar.Next;
end;

procedure TfrmFinConAPagar.BtnPriorClick(Sender: TObject);
begin
  //vai ao registro anterior da tabela
  dm.tabPagar.Prior;
end;

procedure TfrmFinConAPagar.DBGrdConPagarKeyPress(Sender: TObject; var Key: Char);
begin
  //DBGRID com letras em maiusculo
  Key := UpCase(Key);
end;

procedure TfrmFinConAPagar.DBGrdConPagarTitleClick(Column: TColumn);
begin
  //ordenar os dados da grade ao clicar no t�tulo do campo
  ascendente:= not ascendente ;
  If ascendente then
    Dm.tabPagar.IndexFieldNames := Column.FieldName + '   ASC'
  else
    Dm.tabPagar.IndexFieldNames := Column.FieldName + '    DESC';
end;

procedure TfrmFinConAPagar.edtPesquisarChange(Sender: TObject);
begin
  //pesquisa aproximada por descri��o
  dm.tabPagar.Locate('pagHistorico', edtPesquisar.Text, [loCaseInsensitive, loPartialKey]);
end;

procedure TfrmFinConAPagar.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //fecha a conex�o com a tabela
  if (dm.tabPagar.State=dsEdit) or (dm.tabPagar.State=dsInsert) then
  begin
    if MessageDlg('Cadastro em modo de inser��o ou edi��o. Deseja fechar a tela?', mtConfirmation,[mbYes, mbNo], 0) = mrYes then
    begin
      dm.tabPagar.Close;
      dm.tabTipLancamentos.Close;;
      dm.tabSubTipos.Close;
      Action := caFree; //a��o do objeto TCloseAction para fechar a tela
    end
    else
      Action := caNone ; //a��o do objeto TCloseAction para n�o fechar a tela
  end
  else
  begin
    dm.tabPagar.Close;
    dm.tabTipLancamentos.Close;;
    dm.tabSubTipos.Close;
  end;
end;

procedure TfrmFinConAPagar.FormShow(Sender: TObject);
begin
  //abre a conex�o com as tabelas
  dm.tabPagar.Open;
  dm.tabTipLancamentos.Open;
  dm.tabSubTipos.Open;

   //variavel da ordena��o
  ascendente := False;


  with dm.tabPagar do
  begin
    Filtered := false;
    Filter := 'pagDatPaga = NULL AND pagValPago = NULL';
    Filtered := true;
  end;
end;

end.
