unit UMovVenda;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.StdCtrls, Vcl.Buttons, DB,
  Vcl.DBCtrls, Vcl.Mask, Vcl.Grids, Vcl.DBGrids, Vcl.ExtCtrls, System.DateUtils, System.UITypes;

type
  TfrmMovVenda = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox5: TGroupBox;
    BtnPrior: TBitBtn;
    BtnFirst: TBitBtn;
    BtnNext: TBitBtn;
    BtnLast: TBitBtn;
    BtnInsert: TBitBtn;
    BtnDelete: TBitBtn;
    BtnEdit: TBitBtn;
    BtnPost: TBitBtn;
    BtnCancel: TBitBtn;
    edtPesquisar: TEdit;
    Label1: TLabel;
    DBEdtVenId: TDBEdit;
    DBLkpCmbBxCliente: TDBLookupComboBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    DBEdtDatVenda: TDBEdit;
    Label5: TLabel;
    DBEdtValPedido: TDBEdit;
    Label6: TLabel;
    DBEdtNumPedido: TDBEdit;
    Label8: TLabel;
    DBLkpCmbBxVendedor: TDBLookupComboBox;
    Label9: TLabel;
    DBEdit7: TDBEdit;
    Label10: TLabel;
    Label11: TLabel;
    DBLKpCmbBxPrzPagamento: TDBLookupComboBox;
    Label12: TLabel;
    DBEdit9: TDBEdit;
    Label13: TLabel;
    DBEdtDesICSM: TDBEdit;
    Label14: TLabel;
    DBEdit11: TDBEdit;
    Label15: TLabel;
    DBEdtValLiquido: TDBEdit;
    Label16: TLabel;
    DBEdtValFaturado: TDBEdit;
    Label17: TLabel;
    DBEdtAcreIPI: TDBEdit;
    Label18: TLabel;
    DBEdtValFatLiquido: TDBEdit;
    Label19: TLabel;
    DBEdit16: TDBEdit;
    Label20: TLabel;
    DBEdit17: TDBEdit;
    Label21: TLabel;
    DBLkpCmbBxFornecedor: TDBLookupComboBox;
    DBGrdIteVenda: TDBGrid;
    DatTimPckDatVenda: TDateTimePicker;
    lblStatus: TLabel;
    Label7: TLabel;
    GrpBxTipo: TGroupBox;
    RadBtnBonificacao: TRadioButton;
    RadBtnVenda: TRadioButton;
    GrpBxFrete: TGroupBox;
    RadBtnCIF: TRadioButton;
    RadBtnFOB: TRadioButton;
    DBEdtPrzPagamento: TDBEdit;
    btnProcessar: TBitBtn;
    Label22: TLabel;
    DBEdit1: TDBEdit;
    StatusBar1: TStatusBar;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure BtnFirstClick(Sender: TObject);
    procedure BtnPriorClick(Sender: TObject);
    procedure BtnNextClick(Sender: TObject);
    procedure BtnLastClick(Sender: TObject);
    procedure BtnInsertClick(Sender: TObject);
    procedure BtnDeleteClick(Sender: TObject);
    procedure BtnEditClick(Sender: TObject);
    procedure BtnPostClick(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure RadBtnBonificacaoClick(Sender: TObject);
    procedure RadBtnVendaClick(Sender: TObject);
    procedure RadBtnCIFClick(Sender: TObject);
    procedure RadBtnFOBClick(Sender: TObject);
    procedure DatTimPckDatVendaChange(Sender: TObject);
    procedure edtPesquisarChange(Sender: TObject);
    procedure DBGrdIteVendaEnter(Sender: TObject);
    procedure BtnInsertEnter(Sender: TObject);
    procedure DBGrdIteVendaColEnter(Sender: TObject);
    procedure DBGrdIteVendaExit(Sender: TObject);
    procedure DBGrdIteVendaColExit(Sender: TObject);
    procedure DBGrdIteVendaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdtAcreIPIExit(Sender: TObject);
    procedure btnProcessarClick(Sender: TObject);
    procedure DBGrdIteVendaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrdIteVendaTitleClick(Column: TColumn);
    procedure DBGrdIteVendaDblClick(Sender: TObject);
    procedure lblStatusDblClick(Sender: TObject);

  private
    { Private declarations }
    fornecedor : Integer;
  public
    { Public declarations }
  end;

var
  frmMovVenda: TfrmMovVenda;
  proCodFornecedor : string;
  //variavel auxiliar para ordena��o do DBGRID clicando no titulo do grid
  ascendente : Boolean;

implementation

{$R *.dfm}

uses UDM, UPesProduto, UFuncoes, UData, UMain;

procedure TfrmMovVenda.BtnCancelClick(Sender: TObject);
begin
  //cancela a a��o atual na base de dados
  try
    dm.tabVenda.Cancel;
    //dm.tabItemVenda.Cancel;

    //desativa os objetos
    BtnPost.Enabled := false;
    BtnCancel.Enabled := false;
    GroupBox1.Enabled := false;
    //GroupBox2.Enabled := false;
    GroupBox3.Enabled := false;
    btnProcessar.Enabled := false;

    //ativa os bot�es
    BtnFirst.Enabled:= true;
    BtnPrior.Enabled:= true;
    BtnNext.Enabled:= true;
    BtnLast.Enabled:= true;
    BtnInsert.Enabled := true;
    BtnDelete.Enabled := true;
    BtnEdit.Enabled := true;

    //log de cancelamento
    Log('A��o de inser��o ou edi��o da venda n�mero '+ DBEdtNumPedido.Text+' cancelado pelo usu�rio '+ frmMain.usuario+'.');
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro:'+ frmMovVenda.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro:'+ frmMovVenda.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmMovVenda.BtnDeleteClick(Sender: TObject);
var
  meta : integer;
begin
  //exclui um regitro na tabela
  try
    if MessageDlg('Deseja excluir o registro?', mtConfirmation,[mbYes, mbNo], 0) = mrYes then
    begin
      meta := dm.tabVendavenMetCodigo.AsInteger;
      dm.tabItemVenda.First;

      with dm.queryDelItemVenda do
      begin
        Close;
        Parameters.ParamByName('pIteVenVenId').Value := dm.tabVendavenId.Value;
        ExecSQL;
      end;

      dm.tabVenda.Delete;

      //atualiza as meta
      with dm.queryMeta do
      begin
        Close;
        SQL.Clear;
        SQL.Add('SELECT  SUM (b.venValLiquido) AS metaAtual');
        SQL.Add('FROM metas AS a INNER JOIN vendas AS b ON A.metCodigo = b.venMetCodigo');
        SQL.Add('WHERE a.metCodigo = pMetaCodigo');
        Parameters.ParamByName('pMetaCodigo').Value := IntToStr(meta);
        Open;
      end;

      with DM.queryUpdateMeta do
      begin
        Close;
        SQL.Clear;
        SQL.Add('UPDATE metas SET metValAtual = :pSumValLiquido WHERE metCodigo =:pMetCodigo');
        Parameters.ParamByName('pSumValLiquido').Value := FloatToStr(dm.queryMetametaAtual.AsCurrency);
        Parameters.ParamByName('pMetCodigo').Value := IntToStr(meta);
        ExecSQL;
      end;
      Log('Venda n�mero  '+DBEdtNumPedido.Text+' apagado pelo usu�rio '+ frmMain.usuario+'.');
    end;
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro:'+ frmMovVenda.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro:'+ frmMovVenda.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmMovVenda.BtnEditClick(Sender: TObject);
begin
  //edita um registro da tabela
  try
    dm.tabVenda.Edit;
    dm.tabPrecoProduto.Edit;

    //filtra fornecedores ativos
    with dm.tabFornecedores do
    begin
      Filtered := false;
      Filter := 'forStatus = 1';
      Filtered := true;
    end;

    //filtra clientes ativos
    with dm.tabClientes do
    begin
      Filtered := false;
      Filter := 'cliStatus = 1';
      Filtered := true;
    end;

    //filtra funcionarios ativos e com funcao de vendedor
    with dm.tabFuncionarios do
    begin
      Filtered := false;
      Filter := 'funStatus=1 and funFcaCodigo=1';
      Filtered := true;
    end;

    //ativa os objetos
    BtnPost.Enabled := true;
    BtnCancel.Enabled := true;
    GroupBox1.Enabled := true;
    //GroupBox2.Enabled := true;
    GroupBox3.Enabled := true;
    if DM.tabVendavenStatus.Value = 0 then
      btnProcessar.Enabled := true;

    //desativa os bot�es
    BtnFirst.Enabled := false;
    BtnPrior.Enabled := false;
    BtnNext.Enabled := false;
    BtnLast.Enabled := false;
    BtnInsert.Enabled := false;
    BtnDelete.Enabled := false;
    BtnEdit.Enabled := false;

    //torna o objeto visivel
    DatTimPckDatVenda.Visible := true;

    //torna o objeto invisivel
    DBEdtDatVenda.Visible := false;

    //ao inserir novo regitro cursor vai ao  campo DBEdtDatAdesao
    DBEdtNumPedido.SetFocus;

    //valores default
    //DATA DA VENDA
    DatTimPckDatVenda.DateTime := dm.tabVendavenDatVenda.AsDateTime;

    //TIPO
    if dm.tabVendavenTipo.AsInteger=0 then
      RadBtnBonificacao.Checked := true
    else
      RadBtnVenda.Checked := true;
    //frete

    if dm.tabVendavenFrete.AsInteger = 0 then
      RadBtnCIF.Checked := true
    else
      RadBtnFOB.Checked := true;

    if dm.tabVendavenStatus.AsInteger = 0 then
    begin
      DBEdtValFaturado.ReadOnly := false;
      DBEdtValFatLiquido.ReadOnly := false;
    end;
    //log de edi��o
    Log('Venda n�mero '+DBEdtNumPedido.Text+' editado pelo usu�rio '+ frmMain.usuario+'.');
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro:'+ frmMovVenda.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro:'+ frmMovVenda.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmMovVenda.BtnFirstClick(Sender: TObject);
begin
  if (BtnNext.Enabled = false) or (BtnLast.Enabled = false) then
  begin
    BtnNext.Enabled := true;
    BtnLast.Enabled := true;
  end;

  BtnPrior.Enabled := false;
  BtnFirst.Enabled := false;

  //vai ao primeiro registro da tabela
  dm.tabVenda.First;

  //o valor checked dos RadioGrup irao atualizar
  if dm.tabVendavenTipo.Value =  0 then
    RadBtnBonificacao.Checked :=  true;
  if dm.tabVendavenTipo.Value =  1 then
    RadBtnVenda.Checked :=  true;

 //O valor checked dos RadioButton frete irao atualizar
  if dm.tabVendavenFrete.Value =  0 then
    RadBtnCIF.Checked :=  true;
  if dm.tabVendavenFrete.Value =  1 then
    RadBtnFOB.Checked :=  true;

 //o caption do label lblStatus ira atualizar
  if dm.tabVendavenStatus.Value =  0 then
    lblStatus.Caption :=  'N�O PROCESSADO';
  if dm.tabVendavenStatus.Value =  1 then
    lblStatus.Caption :=  'PROCESSADO';

  //atualiza a data da venda no dateTimerPicker
   DatTimPckDatVenda.DateTime := dm.tabVendavenDatVenda.AsDateTime;

  //define a quantidade de itens na venda
  frmMovVenda.StatusBar1.Panels[0].Text := 'Quantidade de itens: '+IntToStr(countItemVenda(StrToInt(frmMovVenda.DBEdtVenId.Text)));
end;

procedure TfrmMovVenda.BtnInsertClick(Sender: TObject);
begin
  //insere um registro na tabela
  try
    dm.tabVenda.Insert;

    //filtra fornecedores ativos
    with dm.tabFornecedores do
    begin
      Filter := 'forStatus = 1';
      Filtered := true;
    end;

    //filtra clientes ativos
    with dm.tabClientes do
    begin
      Filter := 'cliStatus = 1';
      Filtered := true;
    end;

    //filtra funcionarios ativos e com funcao de vendedor
    with dm.tabFuncionarios do
    begin
      Filter := 'funStatus=1 and funFcaCodigo=1';
      Filtered := true;
    end;

    //ativa os objetos
    BtnPost.Enabled := true;
    BtnCancel.Enabled := true;
    GroupBox1.Enabled := true;
    //GroupBox2.Enabled := true;
    GroupBox3.Enabled := true;

    //desativa os bot�es
    BtnFirst.Enabled := false;
    BtnPrior.Enabled := false;
    BtnNext.Enabled := false;
    BtnLast.Enabled := false;
    BtnInsert.Enabled := false;
    BtnDelete.Enabled := false;
    BtnEdit.Enabled := false;

    //torna o objeto visivel
    DatTimPckDatVenda.Visible := true;

    //torna o objeto invisivel
    DBEdtDatVenda.Visible := false;

    //ao inserir novo regitro cursor vai ao  campo DBEdtDatAdesao
    DBEdtNumPedido.SetFocus;

    //valores default
    DatTimPckDatVenda.DateTime := date;
    dm.tabVendavenDatVenda.AsDateTime := DatTimPckDatVenda.DateTime;

    //status de pedido, 0 � n�o processado e 1 processado
    lblStatus.Caption := 'N�O PROCESSADO';
    DM.tabVendavenStatus.AsInteger := 0;

    //tipo de venda, onde 1 � venda e 0 bonifica��o
    RadBtnVenda.Checked := true;
    dm.tabVendavenTipo.AsInteger := 1;

    //frete, onde 0 � cif e 1 � fob
    RadBtnCIF.Checked := true;
    dm.tabVendavenFrete.AsInteger :=0;
    //log de inser��o
    Log('Inser��o de uma nova venda pelo usu�rio '+ frmMain.usuario+'.');
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro:'+ frmMovVenda.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro:'+ frmMovVenda.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;

end;

procedure TfrmMovVenda.BtnInsertEnter(Sender: TObject);
begin
  //inser��o
  dm.tabItemVenda.Insert;
end;

procedure TfrmMovVenda.BtnLastClick(Sender: TObject);
begin
  if (BtnPrior.Enabled = false) or (BtnFirst.Enabled = false) then
  begin
    BtnPrior.Enabled := true;
    BtnFirst.Enabled := true;
  end;

  BtnNext.Enabled :=false;
  BtnLast.Enabled := false;

  //vai ao �ltimo registro da tabela
  dm.tabVenda.Last;

  //o valor checked dos RadioGrup irao atualizar
  if dm.tabVendavenTipo.Value =  0 then
    RadBtnBonificacao.Checked :=  true;
  if dm.tabVendavenTipo.Value =  1 then
    RadBtnVenda.Checked :=  true;

 //O valor checked dos RadioButton frete irao atualizar
  if dm.tabVendavenFrete.Value =  0 then
    RadBtnCIF.Checked :=  true;
  if dm.tabVendavenFrete.Value =  1 then
    RadBtnFOB.Checked :=  true;

 //o caption do label lblStatus ira atualizar
  if dm.tabVendavenStatus.Value =  0 then
    lblStatus.Caption :=  'N�O PROCESSADO';
  if dm.tabVendavenStatus.Value =  1 then
    lblStatus.Caption :=  'PROCESSADO';

 //atualiza a data da venda no dateTimerPicker
  DatTimPckDatVenda.DateTime := dm.tabVendavenDatVenda.AsDateTime;

  //define a quantidade de itens na venda
  frmMovVenda.StatusBar1.Panels[0].Text := 'Quantidade de itens: '+IntToStr(countItemVenda(StrToInt(frmMovVenda.DBEdtVenId.Text)));
end;

procedure TfrmMovVenda.BtnNextClick(Sender: TObject);
begin
  //enquanto n�o for o ultimo registro
  if dm.tabVenda.Eof then
  begin
    BtnNext.Enabled :=false;
    BtnLast.Enabled := false;
  end
  else
  begin
    if (BtnPrior.Enabled = false) or (BtnFirst.Enabled = false) then
    begin
      BtnPrior.Enabled := true;
      BtnFirst.Enabled := true;
    end;

    //vai ao pr�ximo registro da tabela
    dm.tabVenda.Next;

    //o valor checked dos RadioGrup irao atualizar
    if dm.tabVendavenTipo.Value =  0 then
      RadBtnBonificacao.Checked :=  true;
    if dm.tabVendavenTipo.Value =  1 then
      RadBtnVenda.Checked :=  true;

   //O valor checked dos RadioButton frete irao atualizar
    if dm.tabVendavenFrete.Value =  0 then
      RadBtnCIF.Checked :=  true;
    if dm.tabVendavenFrete.Value =  1 then
      RadBtnFOB.Checked :=  true;

   //o caption do label lblStatus ira atualizar
    if dm.tabVendavenStatus.Value =  0 then
      lblStatus.Caption :=  'N�O PROCESSADO';
    if dm.tabVendavenStatus.Value =  1 then
      lblStatus.Caption :=  'PROCESSADO';

    //atualiza a data da venda no dateTimerPicker
    DatTimPckDatVenda.DateTime := dm.tabVendavenDatVenda.AsDateTime;

    //define a quantidade de itens na venda
    frmMovVenda.StatusBar1.Panels[0].Text := 'Quantidade de itens: '+IntToStr(countItemVenda(StrToInt(frmMovVenda.DBEdtVenId.Text)));
  end;
end;

procedure TfrmMovVenda.BtnPostClick(Sender: TObject);
var
  metDatInicio, metDatFim : TDate;
  metCodigo : Integer;
begin
    {desativado no dia 23/11/2015 devido zerar os valores do pedidos ao salvar, mesmo com pedido
    sem itens}
    try
      With dm.queryVenda do
      Begin
        Close; // fecha a query
        Parameters[0].Value := frmMovVenda.DBEdtVenId.Text; //define par�metro de pesquisa
        Open; //abre a query e executa os comandos SQL
      End;

      //aqui define se o valor dos produtos para venda ou bonifica��o
      if (dm.tabVendavenTipo.Value = 1) then
        dm.tabVendavenValProdutos.Value := dm.queryVendasubTotal.Value
      else
        dm.tabVendavenValBonBruto.Value := dm.queryVendasubTotal.Value;

    {verifica se a data da venda entra nos periodos descritos em alguma meta cadastrada,
    caso n�o, ele ira mostra uma mensagem
    ALtera��o 29/09/2015 22:15 v.1.1.2}
    if dm.tabVendavenTipo.AsInteger = 1 then
    begin
      dm.tabMetas.First;
      if not DM.tabMetas.Eof then
      begin
      dm.tabVenda.Edit;
        while not DM.tabMetas.Eof do
        begin
            if  dm.tabMetasmetDatInicio.AsDateTime <= DM.tabVendavenDatVenda.AsDateTime then
              if dm.tabMetasmetDatFim.AsDateTime  >= dm.tabVendavenDatVenda.AsDateTime   then
                if dm.tabVendavenForCodigo.AsInteger = dm.tabMetasmetForCodigo.AsInteger then
                begin
                  dm.tabVendavenMetCodigo.asInteger := dm.tabMetasmetCodigo.AsInteger;
                  //ShowMessage(IntToStr(dm.tabVendavenMetCodigo.asInteger));
                end;
          DM.tabMetas.Next;
        end;

        if dm.tabVendavenMetCodigo.IsNull then
          MessageDlg('Meta n�o cadastrada ou resgitro n�o localizado para o per�odo da venda!!',mtInformation,[mbOK],0);
      end;
    end;

    if not ((dm.tabItemVenda.State = dsInsert) or  (dm.tabItemVenda.State =dsEdit)) then
        dm.tabItemVenda.Edit;
    //confirma uma a��o de inser��o ou edi��o no banco
    dm.tabVenda.Post;

    //atualiza as meta

    with dm.queryMeta do
    begin
      Close;
      SQL.Clear;
      SQL.Add('SELECT  SUM (b.venValLiquido) AS metaAtual');
      SQL.Add('FROM metas AS a INNER JOIN vendas AS b ON A.metCodigo = b.venMetCodigo');
      SQL.Add('WHERE a.metCodigo = pMetaCodigo');
      Parameters.ParamByName('pMetaCodigo').Value := dm.tabVendavenMetCodigo.AsInteger;
      Open;
      //ShowMessage('Meta atual R$:' +FloatToStr(DM.queryMetametaAtual.AsCurrency));
    end;

    with DM.queryUpdateMeta do
    begin
      Close;
      SQL.Clear;
      SQL.Add('UPDATE metas SET metValAtual = :pSumValLiquido WHERE metCodigo =:pMetCodigo');
      Parameters.ParamByName('pSumValLiquido').Value := FloatToStr(dm.queryMetametaAtual.AsCurrency);
      Parameters.ParamByName('pMetCodigo').Value := IntToStr(dm.tabVendavenMetCodigo.asInteger);
      ExecSQL;
    end;

    //desativa os objetos
    BtnPost.Enabled := false;
    BtnCancel.Enabled := false;
    GroupBox1.Enabled := false;
    //GroupBox2.Enabled := false;
    GroupBox3.Enabled := false;
    btnProcessar.Enabled := false;

    //ativa os bot�es
    BtnFirst.Enabled:= true;
    BtnPrior.Enabled:= true;
    BtnNext.Enabled:= true;
    BtnLast.Enabled:= true;
    BtnInsert.Enabled := true;
    BtnDelete.Enabled := true;
    BtnEdit.Enabled := true;

    //aponta para o ultimo registro
    dm.tabVenda.Last;

   //define a quantidade de itens na venda
   frmMovVenda.StatusBar1.Panels[0].Text := 'Quantidade de itens: '+IntToStr(countItemVenda(StrToInt(frmMovVenda.DBEdtVenId.Text)));

    //filtra fornecedores desativados
    with dm.tabFornecedores do
    begin
      Filtered := false;
    end;

    //filtra clientes desativados
    with dm.tabClientes do
    begin
      Filtered := false;
    end;

    //log de confirma��o
    Log('A��o de inser��o ou edi��o da venda n�mero '+ DBEdtNumPedido.Text+' confirmado pelo usu�rio '+ frmMain.usuario+'.');
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro:'+ frmMovVenda.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro:'+ frmMovVenda.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmMovVenda.BtnPriorClick(Sender: TObject);
begin
  //verifica se esta no primeiro registro
  if dm.tabVenda.Bof then
  begin
    BtnPrior.Enabled := false;
    BtnFirst.Enabled := false;
  end
  else
  begin
    if (BtnNext.Enabled = false) or (BtnLast.Enabled = false) then
    begin
      BtnNext.Enabled := true;
      BtnLast.Enabled := true;
    end;

    //vai ao registro anterior
    dm.tabVenda.Prior;

     //o valor checked dos RadioGrup irao atualizar
    if dm.tabVendavenTipo.Value =  0 then
      RadBtnBonificacao.Checked :=  true;
    if dm.tabVendavenTipo.Value =  1 then
      RadBtnVenda.Checked :=  true;

   //O valor checked dos RadioButton frete irao atualizar
    if dm.tabVendavenFrete.Value =  0 then
      RadBtnCIF.Checked :=  true;
    if dm.tabVendavenFrete.Value =  1 then
      RadBtnFOB.Checked :=  true;

   //o caption do label lblStatus ira atualizar
    if dm.tabVendavenStatus.Value =  0 then
      lblStatus.Caption :=  'N�O PROCESSADO';
    if dm.tabVendavenStatus.Value =  1 then
      lblStatus.Caption :=  'PROCESSADO';

   //atualiza a data da venda no dateTimerPicker
    DatTimPckDatVenda.DateTime := dm.tabVendavenDatVenda.AsDateTime;

    //define a quantidade de itens na venda
    frmMovVenda.StatusBar1.Panels[0].Text := 'Quantidade de itens: '+IntToStr(countItemVenda(StrToInt(frmMovVenda.DBEdtVenId.Text)));
  end;
end;

procedure TfrmMovVenda.btnProcessarClick(Sender: TObject);
var
 i : integer;
 ok : Boolean;
 datFaturamento : string;
begin
/////////Valor do campo venStatus atualizado para 1, onde 1 � PROCESSADO///////////
  try
    if ((DBEdtValFaturado.Text='') or (DBEdtValFatLiquido.Text='')) AND (dm.tabVendavenTipo.AsInteger = 1) then
    begin
      MessageDlg('Necessario preencher os valores faturados!!!',mtWarning,[mbOK],0);
      DBEdtValFaturado.SetFocus;
    end
    else if dm.tabVendavenStatus.AsInteger = 0 then
      begin
        try
          //update do status da venda
          with dm.queryItemVenda do
          Begin
            Close; // fecha a query
            SQL.Clear;
            SQL.Add('UPDATE vendas set venStatus = 1 where venID = '+ DBEdtVenId.Text);
            ExecSQL; //abre a query e executa os comandos SQL
          End;
        except
          on E: Exception do
            MessageDlg('Ocorreu um erro:'+ frmMovVenda.Name +#13+
                       'Error: '+E.Message+#13+
                       'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
        end;

        //inser��o da data de faturamento
        frmData.ShowModal;
        datFaturamento := frmMain.DatVenda;
        try
          with dm.queryItemVenda do
          Begin
            Close; // fecha a query
            SQL.Clear;
            SQL.Add('UPDATE vendas set venDatFaturamento =' + QuotedStr(datFaturamento)+ ' where venID = '+ DBEdtVenId.Text);
            ExecSQL; //abre a query e executa os comandos SQL
          End;
        except
        on E: Exception do
          MessageDlg('Ocorreu um erro:'+ frmMovVenda.Name +#13+
                     'Error: '+E.Message+#13+
                     'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
        end;

        lblStatus.Caption := 'PROCESSADO' ;

        {verifica se a data da venda entra nos periodos descritos em alguma meta cadastrada,
        caso n�o, ele ira mostra uma mensagem
        ALtera��o 01/10/2015 22:15 v.1.1.3}
        if dm.tabVendavenTipo.AsInteger = 1 then
        begin
          dm.tabMetas.First;
          if not DM.tabMetas.Eof then
          begin
            dm.tabVenda.Edit;
            while not DM.tabMetas.Eof do
            begin
              if  dm.tabMetasmetDatInicio.AsDateTime <= DM.tabVendavenDatVenda.AsDateTime then
                if dm.tabMetasmetDatFim.AsDateTime  >= dm.tabVendavenDatVenda.AsDateTime   then
                  if dm.tabVendavenForCodigo.AsInteger = dm.tabMetasmetForCodigo.AsInteger then
                  begin
                    dm.tabVendavenMetCodigo.asInteger := dm.tabMetasmetCodigo.AsInteger;
                  end;
            DM.tabMetas.Next;
            end;

            if dm.tabVendavenMetCodigo.IsNull then
              MessageDlg('Meta n�o cadastrada ou resgitro n�o localizado para o per�odo da venda!!',mtInformation,[mbOK],0);
          end;
        end;

        i := dm.tabVendavenId.AsInteger;

        //confirma uma a��o de inser��o ou edi��o no banco
        dm.tabVenda.Post;
        dm.tabVenda.Refresh;

        lblStatus.Caption := 'PROCESSADO';

        //desativa os objetos
        BtnPost.Enabled := false;
        BtnCancel.Enabled := false;
        GroupBox1.Enabled := false;
        //GroupBox2.Enabled := false;
        GroupBox3.Enabled := false;
        btnProcessar.Enabled := false;

        //ativa os bot�es
        BtnFirst.Enabled:= true;
        BtnPrior.Enabled:= true;
        BtnNext.Enabled:= true;
        BtnLast.Enabled:= true;
        BtnInsert.Enabled := true;
        BtnDelete.Enabled := true;
        BtnEdit.Enabled := true;

        Dm.tabVenda.Locate( 'venId',i, [loCaseInsensitive, loPartialKey] );

        //atualiza as meta
        with dm.queryMeta do
        begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT  SUM (b.venValFatLiquido) AS metaAtual');
          SQL.Add('FROM metas AS a INNER JOIN vendas AS b ON A.metCodigo = b.venMetCodigo');
          SQL.Add('WHERE a.metCodigo = pMetaCodigo');
          Parameters.ParamByName('pMetaCodigo').Value := dm.tabVendavenMetCodigo.AsInteger;
          Open;
          dm.tabMetas.First;

          while not DM.tabMetas.Eof do
          begin
            if dm.tabVendavenMetCodigo.AsInteger = dm.tabMetasmetCodigo.AsInteger then
            begin
              dm.tabMetas.Edit;
              dm.tabMetasmetValFatLiquido.AsCurrency := dm.queryMetametaAtual.AsCurrency;
              dm.tabMetas.Post;
            end;
            DM.tabMetas.Next;
          end;
        end;
      end
      else
      begin
        ShowMessage('Venda ja processada!!');
        dm.tabVenda.Post;
      end;

    //filtra fornecedores desativados
    with dm.tabFornecedores do
    begin
      Filtered := false;
    end;

    //filtra clientes desativados
    with dm.tabClientes do
    begin
      Filtered := false;
    end;
    //log de processamento
    Log('Venda n�mero '+DBEdtNumPedido.Text+' processada pelo usu�rio '+ frmMain.usuario+'.');
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro:'+ frmMovVenda.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro:'+ frmMovVenda.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;

end;

procedure TfrmMovVenda.DatTimPckDatVendaChange(Sender: TObject);
begin
  //data da venda
  dm.tabVendavenDatVenda.AsDateTime := DatTimPckDatVenda.DateTime;
end;

procedure TfrmMovVenda.DBEdtAcreIPIExit(Sender: TObject);
var
  acresIPI, descICMS, descComercial, descPisCofins : Currency;
begin

////////////calculo do valor l�quido do peiddo menos o percentual de ICMS
  if ((dm.tabVenda.State = dsInsert) or (dm.tabVenda.State = dsEdit)) then
  begin
    if (dm.tabVendavenTipo.Value = 1) then
    begin
      acresIPI := (dm.tabVendavenValProdutos.value * (dm.tabVendavenAcrIPI.Value / 100));
      descICMS := (dm.tabVendavenValProdutos.value * (dm.tabVendavenDesICMS.Value / 100));
      descComercial := (dm.tabVendavenValProdutos.value * (dm.tabVendavenDesComercial.Value / 100));
      descPisCofins := (dm.tabVendavenValProdutos.value * (dm.tabVendavenDesPISCOFINS.Value / 100));
      dm.tabVendavenValLiquido.Value := ((dm.tabVendavenValProdutos.Value +  acresIPI)- (descICMS+descComercial+descPisCofins));
    end
    else
    begin
      acresIPI := (dm.tabVendavenValBonBruto.value * (dm.tabVendavenAcrIPI.Value / 100));
      descICMS := (dm.tabVendavenValBonBruto.value * (dm.tabVendavenDesICMS.Value / 100));
      descComercial := (dm.tabVendavenValBonBruto.value * (dm.tabVendavenDesComercial.Value / 100));
      descPisCofins := (dm.tabVendavenValBonBruto.value * (dm.tabVendavenDesPISCOFINS.Value / 100));
      dm.tabVendavenValBonLiquido.Value := ((dm.tabVendavenValBonBruto.Value +  acresIPI)- (descICMS+descComercial+descPisCofins));
    end;
  end;
end;

procedure TfrmMovVenda.DBGrdIteVendaColEnter(Sender: TObject);
var
  nRegistro: Integer;
begin
  if (dm.tabVenda.State = dsEdit) or (dm.tabVenda.State = dsInsert) then
  begin
    nRegistro := dm.tabVendavenId.AsInteger;
    if (DBGrdIteVenda.SelectedIndex = 4) then
      With dm.queryVenda do
      Begin
        Close; // fecha a query
        SQL.Clear;
        SQL.Add('SELECT sum(iteVenValTotal) as subTotal  FROM itemVenda where iteVenVenId=:pVenId');
        Parameters.ParamByName('pVenId').Value := nRegistro;
        open;
      End;
    if (dm.tabVendavenTipo.Value = 1) then
      dm.tabVendavenValProdutos.Value := dm.queryVendasubTotal.Value
    else
      dm.tabVendavenValBonBruto.Value := dm.queryVendasubTotal.Value;
  end;

end;

procedure TfrmMovVenda.DBGrdIteVendaColExit(Sender: TObject);
var
  nRegistro: Integer;
begin
  if (dm.tabVenda.State = dsEdit) or (dm.tabVenda.State = dsInsert) then
  begin
    nRegistro := dm.tabVendavenId.AsInteger;

    With dm.queryVenda do
    Begin
      Close; // fecha a query
      SQL.Clear;
      SQL.Add('SELECT sum(iteVenValTotal) as subTotal  FROM itemVenda where iteVenVenId=:pVenId');
      Parameters.ParamByName('pVenId').Value := nRegistro;
      open;
    End;

    if (dm.tabVendavenTipo.Value = 1) then
      dm.tabVendavenValProdutos.Value := dm.queryVendasubTotal.Value
    else
      dm.tabVendavenValBonBruto.Value := dm.queryVendasubTotal.Value;
  end;

 //define a quantidade de itens na venda
    frmMovVenda.StatusBar1.Panels[0].Text := 'Quantidade de itens: '+IntToStr(countItemVenda(StrToInt(frmMovVenda.DBEdtVenId.Text)));
end;

procedure TfrmMovVenda.DBGrdIteVendaDblClick(Sender: TObject);
begin
  if (dm.tabVenda.State <> dsInsert) or (dm.tabVenda.State <> dsEdit) then
  begin
    ShowMessage('Clique editar ou inserir para modifica��o no grid!!');
    BtnEdit.SetFocus;
  end;
end;

procedure TfrmMovVenda.DBGrdIteVendaDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  //deixa o dbgrid zebrado
  If Odd(dm.tabItemVenda.RecNo) and (dm.tabItemVenda.State <> dsInsert) then
  begin //Lembre-se de colocar a unit DB na cl�usula uses na unit da tela.
      DBGrdIteVenda.Canvas.Brush.Color := clMoneyGreen; // muda a cor do pincel
      DBGrdIteVenda.Canvas.FillRect(Rect); // Preenche o fundo com a cor especificada
      DBGrdIteVenda.DefaultDrawDataCell(Rect,Column.Field,State);// desenha as c�lulas da grade
  end;

end;

procedure TfrmMovVenda.DBGrdIteVendaEnter(Sender: TObject);
begin
  if (dm.tabVenda.State = dsEdit) or (dm.tabVenda.State = dsInsert) then
  begin
    dm.tabVenda.Post;
    dm.tabVenda.Edit;
    dm.tabItemVenda.Edit;

    //filtra produtos ativos e do fornecedor corrente
    with dm.tabProdutos do
    begin
      Filtered := false;
      Filter := 'proStatus = 1 and proForCodigo ='+ IntToStr(dm.tabVendavenForCodigo.AsInteger);
      Filtered := true;
    end;
  end;
end;

procedure TfrmMovVenda.DBGrdIteVendaExit(Sender: TObject);
begin
////////////////////////Soma do campo venValProdutos////////////////////////////
  With dm.queryVenda do
  Begin
    Close; // fecha a query
    SQL.Clear;
    SQL.Add('SELECT sum(iteVenValTotal) as subTotal  FROM itemVenda where iteVenVenId=:pVenId');
    Parameters.ParamByName('pVenId').Value := DBEdtVenId.Text;
    ShowMessage(DM.queryItemVenda.SQL.Text);
    ExecSQL;
  End;
  //ShowMessage (CurrToStr(dm.queryItemVendasubTotal.AsCurrency)); }
  dm.tabVendavenValProdutos.Value := dm.queryVendasubTotal.Value;
end;

procedure TfrmMovVenda.DBGrdIteVendaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  confExcluir: Integer;
  nRegistro: Integer;
  nReg: Integer;
begin
  nRegistro := DM.tabItemVendaiteVenId.AsInteger;

  if Key = VK_DELETE Then
  begin
    confExcluir:= Application.MessageBox('Deseja excluir o lan�amento ?', 'Aten��o', MB_YesNo+mb_DefButton2+mb_IconQuestion);
    if confExcluir = IDYes then
    begin
      with dm.tabItemVenda do
      begin
        DM.queryItemVenda.Close;
        DM.queryItemVenda.SQL.Clear;
        DM.queryItemVenda.SQL.Add('DELETE FROM itemVenda WHERE iteVenId =:nReg');
        DM.queryItemVenda.Parameters.ParamByName('nReg').Value := nRegistro;
        //ShowMessage(DM.queryItemVenda.SQL.Text);
        DM.queryItemVenda.ExecSQL;
      end;
        dm.tabItemVenda.Close;
        dm.tabItemVenda.Open;

       nReg := dm.tabVendavenId.AsInteger;
       With dm.queryVenda do
       Begin
          Close; // fecha a query
          SQL.Clear;
          SQL.Add('SELECT sum(iteVenValTotal) as subTotal  FROM itemVenda where iteVenVenId=:pVenId');
          Parameters.ParamByName('pVenId').Value := nReg;
          open;
       End;
     if (dm.tabVendavenTipo.Value = 1) then
      dm.tabVendavenValProdutos.Value := dm.queryVendasubTotal.Value
     else
      dm.tabVendavenValBonBruto.Value := dm.queryVendasubTotal.Value;
    end;
  end;
end;

procedure TfrmMovVenda.DBGrdIteVendaTitleClick(Column: TColumn);
begin
  //ordenar os dados da grade ao clicar no t�tulo do campo
  //ascendente:= not ascendente ;
  //If ascendente then
  //  Dm.tabItemVenda.IndexFieldNames := Column.FieldName + '   ASC'
  //else
  //  Dm.tabItemVenda.IndexFieldNames := Column.FieldName + '    DESC';
end;

procedure TfrmMovVenda.edtPesquisarChange(
Sender: TObject);
begin
  //pesquisa parcial por c�digo
   if Dm.tabVenda.Locate( 'venCodPedFornecedor',edtPesquisar.Text, [loCaseInsensitive, loPartialKey] ) then
   begin
    //label lblStatus ira atualizar
    if dm.tabVendavenStatus.Value =  0 then
      lblStatus.Caption :=  'N�O PROCESSADO';
    if dm.tabVendavenStatus.Value =  1 then
      lblStatus.Caption :=  'PROCESSADO';
   end;

end;

procedure TfrmMovVenda.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //fecha a conex�o com as tabelas
  if (dm.tabCategorias.State=dsEdit) or (dm.tabCategorias.State=dsInsert) then
  begin
    if MessageDlg('Cadastro em modo de inser��o ou edi��o. Deseja fechar a tela?', mtConfirmation,[mbYes, mbNo], 0) = mrYes then
    begin
      //desativa os filtros
      with dm.tabFornecedores do
        Filtered := false;
      with dm.tabClientes do
        Filtered := false;
      with dm.tabFuncionarios do
        Filtered := false;
      with dm.tabProdutos do
        Filtered := false;

      //fecha tabelas
      dm.tabVenda.Close;
      dm.tabItemVenda.Close;
      dm.tabFornecedores.Close;
      dm.tabFuncionarios.Close;
      dm.tabPrzPagamentos.Close;
      dm.tabClientes.Close;
      dm.tabProdutos.Close;
      dm.tabPrecoProduto.Close;
      dm.queryItemVenda.Close;
      dm.tabMetas.Close;
      Action := caFree; //a��o do objeto TCloseAction para fechar a tela

      with dm.tabFornecedores do
        Filtered := false;
    end
    else
      Action := caNone ; //a��o do objeto TCloseAction para n�o fechar a tela
  end
  else
  begin
    //desativa os filtros
    with dm.tabFornecedores do
      Filtered := false;
    with dm.tabClientes do
      Filtered := false;
    with dm.tabFuncionarios do
      Filtered := false;
    with dm.tabProdutos do
      Filtered :=false;

    //fecha tabelas
    dm.tabVenda.Close;
    dm.tabItemVenda.Close;
    dm.tabFornecedores.Close;
    dm.tabFuncionarios.Close;
    dm.tabPrzPagamentos.Close;
    dm.tabClientes.Close;
    dm.tabProdutos.Close;
    dm.tabPrecoProduto.Close;
    dm.queryItemVenda.close;
  end;
end;

procedure TfrmMovVenda.FormShow(Sender: TObject);
begin
  //abre a conex�o com as tabelas
  dm.tabVenda.Open;
  dm.tabItemVenda.Open;
  dm.tabFornecedores.Open;
  dm.tabFuncionarios.Open;
  dm.tabClientes.Open;
  dm.tabPrzPagamentos.Open;
  dm.tabProdutos.Open;
  dm.tabPrecoProduto.Open;
  dm.tabMetas.Open;

  {posiciona no ultimo registro da tabela}
  dm.tabVenda.Last;

 //ao abrir a tela o valor checked dos RadioButton tipo de venda irao atualizar
  if dm.tabVendavenTipo.Value =  0 then
    RadBtnBonificacao.Checked :=  true;
  if dm.tabVendavenTipo.Value =  1 then
    RadBtnVenda.Checked :=  true;

 //ao abrir a tela o valor checked dos RadioButton frete irao atualizar
  if dm.tabVendavenFrete.Value =  0 then
    RadBtnCIF.Checked :=  true;
  if dm.tabVendavenFrete.Value =  1 then
    RadBtnFOB.Checked :=  true;

 //ao abrir a tela o caption do label lblStatus ira atualizar
  if dm.tabVendavenStatus.Value =  0 then
    lblStatus.Caption :=  'N�O PROCESSADO';
  if dm.tabVendavenStatus.Value =  1 then
    lblStatus.Caption :=  'PROCESSADO';

 //atualiza a data da venda no dateTimerPicker
  DatTimPckDatVenda.DateTime := dm.tabVendavenDatVenda.AsDateTime;

  //variavel da ordena��o
  ascendente := False;

  //define a quantidade de itens na venda
  frmMovVenda.StatusBar1.Panels[0].Text := 'Quantidade de itens: '+IntToStr(countItemVenda(StrToInt(frmMovVenda.DBEdtVenId.Text)));
end;

procedure TfrmMovVenda.lblStatusDblClick(Sender: TObject);
var
  datFaturamento : string;
begin
//inser��o da data de faturamento
  try
    if (dm.tabVenda.State = dsInsert) or (dm.tabVenda.State = dsEdit) then
      if dm.tabVendavenStatus.AsInteger = 1 then
      begin
        frmData.ShowModal;
        datFaturamento := frmMain.DatVenda;

        with dm.queryItemVenda do
        Begin
          Close; // fecha a query
          SQL.Clear;
          SQL.Add('UPDATE vendas set venDatFaturamento =' + QuotedStr(datFaturamento)+ ' where venID = '+ DBEdtVenId.Text);
          ExecSQL; //abre a query e executa os comandos SQL
        End;

        //log de processamento
        Log('Data de processamento da venda n�mero '+DBEdtNumPedido.Text+' alterada pelo usu�rio '+ frmMain.usuario+'.');
      end;
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro:'+ frmMovVenda.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro:'+ frmMovVenda.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmMovVenda.RadBtnBonificacaoClick(Sender: TObject);
begin
  //tipo de venda � bonifica��o
  if (dm.tabVenda.State = dsEdit) or (dm.tabVenda.State = dsInsert) then
    dm.tabVendavenTipo.AsInteger := 0;
end;

procedure TfrmMovVenda.RadBtnCIFClick(Sender: TObject);
begin
  //se frete cif
  if (dm.tabVenda.State = dsEdit) or (dm.tabVenda.State = dsInsert) then
    dm.tabVendavenFrete.AsInteger := 0;
end;

procedure TfrmMovVenda.RadBtnFOBClick(Sender: TObject);
begin
  //se frete for fob
  if (dm.tabVenda.State = dsEdit) or (dm.tabVenda.State = dsInsert) then
    dm.tabVendavenFrete.AsInteger := 1;
end;

procedure TfrmMovVenda.RadBtnVendaClick(Sender: TObject);
begin
  //tipo de venda � venda
  if (dm.tabVenda.State = dsEdit) or (dm.tabVenda.State = dsInsert) then
    dm.tabVendavenTipo.AsInteger := 1;
end;

end.
