object frmRelMetaSku: TfrmRelMetaSku
  Left = 0
  Top = 0
  Caption = 'Relat'#243'rio de meta por SKU'
  ClientHeight = 151
  ClientWidth = 328
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox2: TGroupBox
    Left = 0
    Top = 52
    Width = 328
    Height = 56
    Align = alTop
    Caption = 'P'#233'riodo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object Label8: TLabel
      Left = 8
      Top = 21
      Width = 27
      Height = 19
      Caption = 'M'#234's'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel
      Left = 181
      Top = 21
      Width = 29
      Height = 19
      Caption = 'Ano'
    end
    object CmbBxMes: TComboBox
      Left = 38
      Top = 18
      Width = 137
      Height = 27
      CharCase = ecUpperCase
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemIndex = 0
      ParentFont = False
      TabOrder = 0
      Text = 'JANEIRO'
      Items.Strings = (
        'JANEIRO'
        'FEVEIRO'
        'MAR'#199'O'
        'ABRIL'
        'MAIO'
        'JUNHO'
        'JULHO'
        'AGOSTO'
        'SETEMBRO'
        'OUTUBRO'
        'NOVEMBRO'
        'DEZEMBRO')
    end
    object CmbBxAno: TComboBox
      Left = 216
      Top = 18
      Width = 99
      Height = 27
      TabOrder = 1
    end
  end
  object GroupBox3: TGroupBox
    Left = 0
    Top = 108
    Width = 328
    Height = 41
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object BitBtn1: TBitBtn
      Left = 96
      Top = 4
      Width = 109
      Height = 32
      Caption = 'Im&primir'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      Glyph.Data = {
        46060000424D4606000000000000360400002800000016000000160000000100
        08000000000010020000C40E0000C40E00000001000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
        A6000020400000206000002080000020A0000020C0000020E000004000000040
        20000040400000406000004080000040A0000040C0000040E000006000000060
        20000060400000606000006080000060A0000060C0000060E000008000000080
        20000080400000806000008080000080A0000080C0000080E00000A0000000A0
        200000A0400000A0600000A0800000A0A00000A0C00000A0E00000C0000000C0
        200000C0400000C0600000C0800000C0A00000C0C00000C0E00000E0000000E0
        200000E0400000E0600000E0800000E0A00000E0C00000E0E000400000004000
        20004000400040006000400080004000A0004000C0004000E000402000004020
        20004020400040206000402080004020A0004020C0004020E000404000004040
        20004040400040406000404080004040A0004040C0004040E000406000004060
        20004060400040606000406080004060A0004060C0004060E000408000004080
        20004080400040806000408080004080A0004080C0004080E00040A0000040A0
        200040A0400040A0600040A0800040A0A00040A0C00040A0E00040C0000040C0
        200040C0400040C0600040C0800040C0A00040C0C00040C0E00040E0000040E0
        200040E0400040E0600040E0800040E0A00040E0C00040E0E000800000008000
        20008000400080006000800080008000A0008000C0008000E000802000008020
        20008020400080206000802080008020A0008020C0008020E000804000008040
        20008040400080406000804080008040A0008040C0008040E000806000008060
        20008060400080606000806080008060A0008060C0008060E000808000008080
        20008080400080806000808080008080A0008080C0008080E00080A0000080A0
        200080A0400080A0600080A0800080A0A00080A0C00080A0E00080C0000080C0
        200080C0400080C0600080C0800080C0A00080C0C00080C0E00080E0000080E0
        200080E0400080E0600080E0800080E0A00080E0C00080E0E000C0000000C000
        2000C0004000C0006000C0008000C000A000C000C000C000E000C0200000C020
        2000C0204000C0206000C0208000C020A000C020C000C020E000C0400000C040
        2000C0404000C0406000C0408000C040A000C040C000C040E000C0600000C060
        2000C0604000C0606000C0608000C060A000C060C000C060E000C0800000C080
        2000C0804000C0806000C0808000C080A000C080C000C080E000C0A00000C0A0
        2000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0E000C0C00000C0C0
        2000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0A000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFF08F7F7
        F7F7F7F7F7F7F7F7F7F7F7F7F6FFFFFF0000FFFFFFF707070707070707070707
        070707F7FFFFFFFF0000F6F7A4A407F6F6F6F6F6F608F6F6F6F6F6A4F7A4A408
        0000A4070707F7080808080808080808080807A4070707A40000A40807085252
        52525252525252525252495B080708A40000A4080808A4494949494949494949
        494949F70808F6A40000A4F60808F6F6F6F6F6F6F6F6F6F6F6F6F6F60808F6A4
        0000A4F6F6F6F6F6F6F6F6F6F6F6F6090908F6F6F6F6FFA40000A4FFFFFFFF09
        E2E2E2E2E2E2E2D9D9D9F6FFFFFFFFA40000A4FFFFFFFF09E2E3EBEBEBEBE2E2
        DAD909FFFFFFFFF70000A4FFFFFFFF09E2E2E2E2E2E2DAD9D9D909FFFFFFFFF7
        0000F7FFFFFFFF09E3ECECECEBEBEBE3EBE309FFFFFFFFF70000FFF707F6FF09
        090909090909090909EC09F60807F7FF0000FFFFFFF708ED0909090909090909
        09EC070708FFFFFF0000FFFFFF070808FFFFFFF6F6F6F608080808F7FFFFFFFF
        0000FFFFFF07F6F6FFFFFFFFF6F6F6F6F60808F7FFFFFFFF0000FFFFFF07F6F6
        FFFFFFFFFFF6F6F6F60808F7FFFFFFFF0000FFFFFFF7F6F6FFFFFFFFFFF6F6F6
        F6F608F7FFFFFFFF0000FFFFFF08F708FFFFFFFFFFFFF6F6F6F608F7FFFFFFFF
        0000FFFFFFFFF608FFFFFFFFFFFFFFF6F6F608F7FFFFFFFF0000FFFFFFFF0808
        F6F6F6F6F6F6F6F6F6F6F6F7FFFFFFFF0000FFFFFFFFFF070707070707070707
        0707F7F7FFFFFFFF0000}
      ParentFont = False
      TabOrder = 0
      OnClick = BitBtn1Click
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 328
    Height = 52
    Align = alTop
    Caption = 'Representada'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object CmbBxRepresentada: TComboBox
      Left = 2
      Top = 19
      Width = 313
      Height = 27
      CharCase = ecUpperCase
      TabOrder = 0
    end
  end
  object frxRptRelMetaProdutosItem: TfrxReport
    Version = '4.13.2'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42344.607829062500000000
    ReportOptions.LastChange = 42344.607829062500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 264
    Top = 108
    Datasets = <
      item
        DataSet = frxDBDtstMetaSku
        DataSetName = 'frxDBDtstMetaProdutoItem'
      end>
    Variables = <
      item
        Name = ' variavel'
        Value = Null
      end
      item
        Name = 'periodo'
        Value = Null
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 215.900000000000000000
      PaperHeight = 279.400000000000000000
      PaperSize = 1
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageFooter1: TfrxPageFooter
        Height = 18.897650000000000000
        Top = 306.141930000000000000
        Width = 740.409927000000000000
        object Memo23: TfrxMemoView
          Left = 602.047620000000000000
          Top = 1.220470000000000000
          Width = 132.283550000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'P'#193'GINA [Page] DE [TotalPages#]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Height = 22.677180000000000000
        ParentFont = False
        Top = 222.992270000000000000
        Width = 740.409927000000000000
        DataSet = frxDBDtstMetaSku
        DataSetName = 'frxDBDtstMetaProdutoItem'
        RowCount = 0
        object frxDBDtstMetaProdutoItemproCodFornecedor: TfrxMemoView
          Left = 3.779530000000000000
          Top = 3.779530000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'proCodFornecedor'
          DataSet = frxDBDtstMetaSku
          DataSetName = 'frxDBDtstMetaProdutoItem'
          Memo.UTF8W = (
            '[frxDBDtstMetaProdutoItem."proCodFornecedor"]')
        end
        object frxDBDtstMetaProdutoItemproNome: TfrxMemoView
          Left = 92.165430000000000000
          Top = 3.779530000000000000
          Width = 249.448980000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'proNome'
          DataSet = frxDBDtstMetaSku
          DataSetName = 'frxDBDtstMetaProdutoItem'
          Memo.UTF8W = (
            '[frxDBDtstMetaProdutoItem."proNome"]')
        end
        object frxDBDtstMetaProdutoItemmetProIteMeta: TfrxMemoView
          Left = 369.795610000000000000
          Top = 3.779530000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'metProIteMeta'
          DataSet = frxDBDtstMetaSku
          DataSetName = 'frxDBDtstMetaProdutoItem'
          Memo.UTF8W = (
            '[frxDBDtstMetaProdutoItem."metProIteMeta"]')
        end
        object frxDBDtstMetaProdutoItemmetAlcancada: TfrxMemoView
          Left = 453.165740000000000000
          Top = 3.779530000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'metAlcancada'
          DataSet = frxDBDtstMetaSku
          DataSetName = 'frxDBDtstMetaProdutoItem'
          Memo.UTF8W = (
            '[frxDBDtstMetaProdutoItem."metAlcancada"]')
        end
        object frxDBDtstMetaProdutoItemmetPercentual: TfrxMemoView
          Left = 532.535870000000000000
          Top = 3.779530000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDBDtstMetaSku
          DataSetName = 'frxDBDtstMetaProdutoItem'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Highlight.Font.Charset = DEFAULT_CHARSET
          Highlight.Font.Color = clGreen
          Highlight.Font.Height = -11
          Highlight.Font.Name = 'Arial'
          Highlight.Font.Style = []
          Highlight.Condition = 'Value >= 100'
          Memo.UTF8W = (
            '[frxDBDtstMetaProdutoItem."metPercentual"]')
          ParentFont = False
        end
        object frxDBDtstMetaProdutoItemmetFaturada: TfrxMemoView
          Left = 602.094930000000000000
          Top = 3.779530000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'metFaturada'
          DataSet = frxDBDtstMetaSku
          DataSetName = 'frxDBDtstMetaProdutoItem'
          Memo.UTF8W = (
            '[frxDBDtstMetaProdutoItem."metFaturada"]')
        end
        object frxDBDtstMetaProdutoItemmetPerFaturada: TfrxMemoView
          Left = 684.094930000000000000
          Top = 3.779530000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDBDtstMetaSku
          DataSetName = 'frxDBDtstMetaProdutoItem'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Highlight.Font.Charset = DEFAULT_CHARSET
          Highlight.Font.Color = clGreen
          Highlight.Font.Height = -11
          Highlight.Font.Name = 'Arial'
          Highlight.Font.Style = []
          Highlight.Condition = 'Value >= 100'
          Memo.UTF8W = (
            '[frxDBDtstMetaProdutoItem."metPerFaturada"]')
          ParentFont = False
        end
      end
      object ReportTitle1: TfrxReportTitle
        Height = 56.692950000000000000
        Top = 18.897650000000000000
        Width = 740.409927000000000000
        object Memo1: TfrxMemoView
          Left = 207.756030000000000000
          Top = 2.559060000000000000
          Width = 343.937230000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'RELAT'#211'RIO DE META POR SKU')
          ParentFont = False
        end
        object Date: TfrxMemoView
          Left = 528.732840000000000000
          Top = 30.354360000000000000
          Width = 211.653680000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd '#39'de'#39' mmmm '#39'de'#39' yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Manaus, [Date]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 3.779530000000000000
          Top = 30.354360000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd '#39'de'#39' mmmm '#39'de'#39' yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'P'#233'riodo: ')
          ParentFont = False
        end
        object frxDBDtstMetaProdutoItemmetProMesReferencia: TfrxMemoView
          Left = 54.692950000000000000
          Top = 30.236240000000000000
          Width = 162.519790000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDBDtstMetaSku
          DataSetName = 'frxDBDtstMetaProdutoItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            
              '[frxDBDtstMetaProdutoItem."metProMesReferencia"] - [frxDBDtstMet' +
              'aProdutoItem."metProAnoReferencia"]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 64.252010000000000000
        Top = 136.063080000000000000
        Width = 740.409927000000000000
        Condition = 'frxDBDtstMetaProdutoItem."forRazao"'
        object Shape1: TfrxShapeView
          Left = 1.000000000000000000
          Top = 6.110148350000000000
          Width = 737.008350000000000000
          Height = 22.677180000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Memo11: TfrxMemoView
          Left = 3.779527560000000000
          Top = 8.220470000000000000
          Width = 68.031540000000000000
          Height = 18.897637800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Fornecedor:')
          ParentFont = False
        end
        object frxDBDtstMetaProdutoItemforRazao: TfrxMemoView
          Left = 70.031540000000000000
          Top = 8.377952760000000000
          Width = 343.937230000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'forRazao'
          DataSet = frxDBDtstMetaSku
          DataSetName = 'frxDBDtstMetaProdutoItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBDtstMetaProdutoItem."forRazao"]')
          ParentFont = False
        end
        object frxDBDtstMetaProdutoItemmetProDatFim: TfrxMemoView
          Left = 626.842920000000000000
          Top = 8.377952760000000000
          Width = 109.606370000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'metProDatFim'
          DataSet = frxDBDtstMetaSku
          DataSetName = 'frxDBDtstMetaProdutoItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBDtstMetaProdutoItem."metProDatFim"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 571.244187240000000000
          Top = 8.377952760000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Data fim:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 419.944928270000000000
          Top = 8.377952760000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Data inicio:')
          ParentFont = False
        end
        object frxDBDtstMetaProdutoItemmetProDatInicio: TfrxMemoView
          Left = 483.220780000000000000
          Top = 8.377952760000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'metProDatInicio'
          DataSet = frxDBDtstMetaSku
          DataSetName = 'frxDBDtstMetaProdutoItem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBDtstMetaProdutoItem."metProDatInicio"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 3.779530000000000000
          Top = 43.913420000000000000
          Width = 68.031540000000000000
          Height = 18.897637800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 90.708720000000000000
          Top = 43.913420000000000000
          Width = 68.031540000000000000
          Height = 18.897637800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 366.795610000000000000
          Top = 43.913420000000000000
          Width = 68.031540000000000000
          Height = 18.897637800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Meta')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 450.165740000000000000
          Top = 43.913420000000000000
          Width = 68.031540000000000000
          Height = 18.897637800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Enviado')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 522.535870000000000000
          Top = 43.913420000000000000
          Width = 71.811070000000000000
          Height = 18.897637800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '% Enviado')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 602.094930000000000000
          Top = 43.842519685039400000
          Width = 68.031540000000000000
          Height = 18.897637800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Faturado')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 676.535870000000000000
          Top = 43.842519690000000000
          Width = 68.031540000000000000
          Height = 18.897637800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '% Faturado')
          ParentFont = False
        end
      end
    end
  end
  object frxPDFExport1: TfrxPDFExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    PrintOptimized = False
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    Left = 88
    Top = 108
  end
  object frxDBDtstMetaSku: TfrxDBDataset
    UserName = 'frxDBDtstMetaProdutoItem'
    CloseDataSource = False
    DataSet = DM.queryRelMetaSKU
    BCDToCurrency = False
    Left = 45
    Top = 108
  end
end
