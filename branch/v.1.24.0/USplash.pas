unit USplash;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, pngimage, ExtCtrls;

type
  TfrmSplash = class(TForm)
    Image1: TImage;
    lblNamSistema: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSplash: TfrmSplash;

implementation

uses UFuncoes;

{$R *.dfm}

procedure TfrmSplash.FormCreate(Sender: TObject);
begin
  Brush.Style := bsClear;
  lblNamSistema.Caption := 'SysCommercial v.'+UFuncoes.VersaoExe;
end;

end.
