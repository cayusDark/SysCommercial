﻿unit UCadProdutos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Mask, Vcl.DBCtrls,
  Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls, DB, Vcl.Grids, Vcl.DBGrids;

type
  TfrmCadProdutos = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    GroupBox5: TGroupBox;
    BtnPrior: TBitBtn;
    BtnFirst: TBitBtn;
    BtnNext: TBitBtn;
    BtnLast: TBitBtn;
    BtnInsert: TBitBtn;
    BtnDelete: TBitBtn;
    BtnEdit: TBitBtn;
    BtnPost: TBitBtn;
    BtnCancel: TBitBtn;
    edtPesquisar: TEdit;
    StatusBar1: TStatusBar;
    Label2: TLabel;
    DBEdtCodFornecedor: TDBEdit;
    Label3: TLabel;
    DBEdtNome: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    DBLkpCmbBxForRazao: TDBLookupComboBox;
    Label5: TLabel;
    Label6: TLabel;
    DBLkpCmbBxCatDescricao: TDBLookupComboBox;
    Label8: TLabel;
    DBEdit6: TDBEdit;
    Label9: TLabel;
    DBEdit7: TDBEdit;
    Label10: TLabel;
    DBEdit8: TDBEdit;
    Label11: TLabel;
    DBEdit9: TDBEdit;
    Label12: TLabel;
    DBEdit10: TDBEdit;
    Label13: TLabel;
    DBEdit11: TDBEdit;
    Label14: TLabel;
    DBEdit12: TDBEdit;
    Label15: TLabel;
    DBEdit13: TDBEdit;
    Label17: TLabel;
    DBEdit15: TDBEdit;
    Label18: TLabel;
    DBEdit16: TDBEdit;
    Label19: TLabel;
    DBEdit17: TDBEdit;
    Label20: TLabel;
    DBEdit18: TDBEdit;
    Label21: TLabel;
    DBEdit19: TDBEdit;
    Label22: TLabel;
    DBEdit20: TDBEdit;
    Label23: TLabel;
    DBEdit21: TDBEdit;
    Label24: TLabel;
    DBEdit22: TDBEdit;
    Label25: TLabel;
    DBEdit23: TDBEdit;
    Label27: TLabel;
    DBEdit25: TDBEdit;
    RdoGrpStatus: TRadioGroup;
    GroupBox6: TGroupBox;
    DBGrdPreVenda: TDBGrid;
    procedure BtnFirstClick(Sender: TObject);
    procedure BtnPriorClick(Sender: TObject);
    procedure BtnNextClick(Sender: TObject);
    procedure BtnLastClick(Sender: TObject);
    procedure BtnInsertClick(Sender: TObject);
    procedure BtnDeleteClick(Sender: TObject);
    procedure BtnEditClick(Sender: TObject);
    procedure BtnPostClick(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure edtPesquisarChange(Sender: TObject);
    procedure rdoGrpStatusClick(Sender: TObject);
    procedure DBEdit17KeyPress(Sender: TObject; var Key: Char);
    procedure DBGrdPreVendaKeyPress(Sender: TObject; var Key: Char);
    procedure DBGrdPreVendaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrdPreVendaTitleClick(Column: TColumn);
    procedure DBGrdPreVendaEnter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCadProdutos: TfrmCadProdutos;
  //variavel auxiliar para ordenação do DBGRID clicando no titulo do grid
  ascendente : Boolean;
implementation

{$R *.dfm}

uses UDM, UFuncoes, UMain;

procedure TfrmCadProdutos.BtnCancelClick(Sender: TObject);
begin
  //cancela a ação atual na base de dados
  dm.tabProdutos.Cancel;
  //dm.tabPrecoProduto.Cancel;

  //desativa edição no radiogroup
  RdoGrpStatus.Enabled := false;

  //desativa os componentes
  BtnPost.Enabled := false;
  BtnCancel.Enabled := false;
  GroupBox1.Enabled := false;
  GroupBox2.Enabled := false;
  GroupBox3.Enabled := false;
  GroupBox4.Enabled := false;
  GroupBox6.Enabled := false;

  //ativa os botões
  BtnFirst.Enabled:= true;
  BtnPrior.Enabled:= true;
  BtnNext.Enabled:= true;
  BtnLast.Enabled:= true;
  BtnInsert.Enabled := true;
  BtnDelete.Enabled := true;
  BtnEdit.Enabled := true;
  edtPesquisar.Enabled := true;
end;

procedure TfrmCadProdutos.BtnDeleteClick(Sender: TObject);
begin
  //exclui um regitro na tabela
  if MessageDlg('Deseja excluir o registro?', mtConfirmation,[mbYes, mbNo], 0) = mrYes then
  begin
    dm.tabProdutos.Delete;
    Log('Produto apagado pelo usuário '+ frmMain.usuario);
    dm.tabProdutos.Refresh;
  end;
end;

procedure TfrmCadProdutos.BtnEditClick(Sender: TObject);
begin
  //edita um registro da tabela
  dm.tabProdutos.Edit;
  //dm.tabPrecoProduto.Edit;

  //filtra apenas categorias ativas
  With dm.tabCategorias do
  begin
    Filtered := false;
    Filter := 'catStatus = 1';
    Filtered := true;
  end;

  //filtra apenas fornecedores ativos
  with dm.tabFornecedores do
  begin
    Filtered := false;
    Filter := 'forStatus = 1';
    Filtered := true;
  end;

  //permite edição no groupRadio
  RdoGrpStatus.Enabled := true;

  //ativa os componentes
  BtnPost.Enabled := true;
  BtnCancel.Enabled := true;
  GroupBox1.Enabled := true;
  GroupBox2.Enabled := true;
  GroupBox3.Enabled := true;
  GroupBox4.Enabled := true;
  GroupBox6.Enabled := true;

  //desativa os botões
  BtnFirst.Enabled := false;
  BtnPrior.Enabled := false;
  BtnNext.Enabled := false;
  BtnLast.Enabled := false;
  BtnInsert.Enabled := false;
  BtnDelete.Enabled := false;
  BtnEdit.Enabled := false;
  edtPesquisar.Enabled := false;

  //ao inserir novo regitro cursor vai ao  campo DBEdtDatAdesao
  DBEdtCodFornecedor.SetFocus;
  Log('Produto editado pelo usuário '+ frmMain.usuario);
end;

procedure TfrmCadProdutos.BtnFirstClick(Sender: TObject);
begin
  //vai ao primeiro registro da tabela
  dm.tabProdutos.First;

  //o valor ItemIndex do RdoGrpStatus irá atualizar
  if dm.tabProdutosproStatus.Value =  0 then
    rdoGrpStatus.ItemIndex :=  0;
  if dm.tabProdutosproStatus.Value =  1 then
    rdoGrpStatus.ItemIndex :=  1;


end;

procedure TfrmCadProdutos.BtnInsertClick(Sender: TObject);
begin
  //insere um registro na tabela
  dm.tabProdutos.Insert;
  //dm.tabPrecoProduto.Insert;

  //filtra apenas categorias ativas
  With dm.tabCategorias do
  begin
    Filtered := false;
    Filter := 'catStatus = 1';
    Filtered := true;
  end;

  //filtra apenas fornecedores ativos
  with dm.tabFornecedores do
  begin
    Filtered := false;
    Filter := 'forStatus = 1';
    Filtered := true;
  end;

  //ativa edição no radioGroup
  RdoGrpStatus.Enabled := true;

  //ativa os componentes
  BtnPost.Enabled := true;
  BtnCancel.Enabled := true;
  GroupBox1.Enabled := true;
  GroupBox2.Enabled := true;
  GroupBox3.Enabled := true;
  GroupBox4.Enabled := true;
  GroupBox6.Enabled := true;


  //desativa os botões
  BtnFirst.Enabled := false;
  BtnPrior.Enabled := false;
  BtnNext.Enabled := false;
  BtnLast.Enabled := false;
  BtnInsert.Enabled := false;
  BtnDelete.Enabled := false;
  BtnEdit.Enabled := false;
  edtPesquisar.Enabled := false;

  //ao inserir novo regitro cursor vai ao  campo DBEdtDatAdesao
  DBEdtCodFornecedor.SetFocus;

  //inicia modo de inserção com valor default 1
  dm.tabProdutosproStatus.Value :=  1;
  rdoGrpStatus.ItemIndex :=  1;

  Log('Adição de produto pelo usuário '+ frmMain.usuario);
end;

procedure TfrmCadProdutos.BtnLastClick(Sender: TObject);
begin
  //vai ao último registro da tabela
  dm.tabProdutos.Last;

  //o valor ItemIndex do RdoGrpStatus irá atualizar
  if dm.tabProdutosproStatus.Value =  0 then
    rdoGrpStatus.ItemIndex :=  0;
  if dm.tabProdutosproStatus.Value =  1 then
    rdoGrpStatus.ItemIndex :=  1;


end;

procedure TfrmCadProdutos.BtnNextClick(Sender: TObject);
begin
  //vai ao próximo registro da tabela
  dm.tabProdutos.Next;

  //o valor ItemIndex do RdoGrpStatus irá atualizar
  if dm.tabProdutosproStatus.Value =  0 then
    rdoGrpStatus.ItemIndex :=  0;
  if dm.tabProdutosproStatus.Value =  1 then
    rdoGrpStatus.ItemIndex :=  1;


end;

procedure TfrmCadProdutos.BtnPostClick(Sender: TObject);
begin
  if DBEdtNome.Text='' then
    ShowMessage('Campo nome obrigatorio!')
  else
    if DBLkpCmbBxForRazao.Text ='' then
      ShowMessage('Campo fornecedor obrigatorio!')
    else
      if DBLkpCmbBxCatDescricao.Text='' then
        ShowMessage('Campo categoria obrigatorio!')
      else
      begin
        //confirma uma ação de inserção ou edição no banco
        dm.tabProdutos.Post;
        //dm.tabPrecoProduto.Post;

        //desativa edição no radiogroup
        RdoGrpStatus.Enabled := false;

        //desativa os componentes
        BtnPost.Enabled := false;
        BtnCancel.Enabled := false;
        GroupBox1.Enabled := false;
        GroupBox2.Enabled := false;
        GroupBox3.Enabled := false;
        GroupBox4.Enabled := false;
        GroupBox6.Enabled := false;


        //ativa os botões
        BtnFirst.Enabled:= true;
        BtnPrior.Enabled:= true;
        BtnNext.Enabled:= true;
        BtnLast.Enabled:= true;
        BtnInsert.Enabled := true;
        BtnDelete.Enabled := true;
        BtnEdit.Enabled := true;
        edtPesquisar.Enabled := true;
        Log('Produto editado ou inserido pelo usuário '+ frmMain.usuario);
        dm.tabProdutos.Refresh;
      end;
end;

procedure TfrmCadProdutos.BtnPriorClick(Sender: TObject);
begin
  //vai ao registro anterior
  dm.tabProdutos.Prior;

  //o valor ItemIndex do RdoGrpStatus irá atualizar
  if dm.tabProdutosproStatus.Value =  0 then
    rdoGrpStatus.ItemIndex :=  0;
  if dm.tabProdutosproStatus.Value =  1 then
    rdoGrpStatus.ItemIndex :=  1;


end;

procedure TfrmCadProdutos.DBEdit17KeyPress(Sender: TObject; var Key: Char);
begin
  ApenasNumero(Key);
end;

procedure TfrmCadProdutos.DBGrdPreVendaDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin

  //Deixa o dbgrid zebrado
  If Odd(dm.tabPrecoProduto.RecNo) and (dm.tabPrecoProduto.State <> dsInsert) then
  begin //Lembre-se de colocar a unit DB na cláusula uses na unit da tela.
      DBGrdPreVenda.Canvas.Brush.Color := clMoneyGreen; // muda a cor do pincel
      DBGrdPreVenda.Canvas.FillRect(Rect); // Preenche o fundo com a cor especificada
      DBGrdPreVenda.DefaultDrawDataCell(Rect,Column.Field,State);// desenha as células da grade
  end;

end;

procedure TfrmCadProdutos.DBGrdPreVendaEnter(Sender: TObject);
begin
  dm.tabProdutos.Post;
  dm.tabProdutos.Edit;
end;

procedure TfrmCadProdutos.DBGrdPreVendaKeyPress(Sender: TObject; var Key: Char);
begin
  //DBGRID com letras em maiusculo
  Key := UpCase(Key);
end;

procedure TfrmCadProdutos.DBGrdPreVendaTitleClick(Column: TColumn);
begin
  //ordenar os dados da grade ao clicar no título do campo
  ascendente:= not ascendente ;
  If ascendente then
    Dm.tabPrecoProduto.IndexFieldNames := Column.FieldName + '   ASC'
  else
    Dm.tabPrecoProduto.IndexFieldNames := Column.FieldName + '    DESC';
end;

procedure TfrmCadProdutos.edtPesquisarChange(Sender: TObject);
begin
  //pesquisa aproximada por código
  dm.tabProdutos.Locate('proCodFornecedor', edtPesquisar.Text, [loCaseInsensitive, loPartialKey]);

end;

procedure TfrmCadProdutos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //fecha a tabela de conexão com
  if (dm.tabProdutos.State=dsEdit) or (dm.tabProdutos.State=dsInsert) then
  begin
    if MessageDlg('Cadastro em modo de inserção ou edição. Deseja fechar a tela?', mtConfirmation,[mbYes, mbNo], 0) = mrYes then
    begin
        dm.tabProdutos.Close;
        dm.tabFornecedores.Close;
        dm.tabCategorias.Close;
        dm.tabPrecoProduto.Close;

        //desativa os filtros
        With dm.tabCategorias do
          Filtered := false;
        with dm.tabFornecedores do
          Filtered := false;
        With dm.tabPrecoProduto do
          Filtered := false;

        Action := caFree; //ação do objeto TCloseAction para fechar a tela
    end
    else
      Action := caNone ; //ação do objeto TCloseAction para não fechar a tela
  end
  else
  begin
    dm.tabProdutos.Close;
    dm.tabFornecedores.Close;
    dm.tabCategorias.Close;
    dm.tabPrecoProduto.Close;

    //desativa os filtros
    With dm.tabCategorias do
      Filtered := false;
    with dm.tabFornecedores do
      Filtered := false;
    with dm.tabPrecoProduto do
      Filtered := false;
  end;
end;

procedure TfrmCadProdutos.FormShow(Sender: TObject);
begin
  //abre a conexão com a tabela
  dm.tabProdutos.Open;
  dm.tabFornecedores.Open;
  dm.tabCategorias.Open;
  dm.tabPrecoProduto.Open;

  //o valor ItemIndex do RdoGrpStatus irá atualizar
  if dm.tabProdutosproStatus.Value =  0 then
    rdoGrpStatus.ItemIndex :=  0;
  if dm.tabProdutosproStatus.Value =  1 then
    rdoGrpStatus.ItemIndex :=  1;

  //variavel da ordenação
  ascendente := False;

end;

procedure TfrmCadProdutos.rdoGrpStatusClick(Sender: TObject);
begin
  //Caso tabela entre em modo de manutenção o campo cliStatus recebe o valor do ItemIndex do
  //radioGroup rdoGrpStatus.
  if (dm.tabProdutos.State=dsInsert) or (dm.tabProdutos.State=dsEdit) then
  begin
    if rdoGrpStatus.ItemIndex=0 then
      dm.tabProdutosproStatus.Value :=  0;
    if rdoGrpStatus.ItemIndex =  1 then
      dm.tabProdutosproStatus.Value :=  1;
  end;
end;

end.
