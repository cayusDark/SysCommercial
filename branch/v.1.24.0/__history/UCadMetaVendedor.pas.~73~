unit UCadMetaVendedor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.StdCtrls, Vcl.Buttons, DB,
  Vcl.Mask, Vcl.DBCtrls, Vcl.Grids, Vcl.DBGrids;

type
  TfrmCadMetVendedor = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox5: TGroupBox;
    BtnPrior: TBitBtn;
    BtnFirst: TBitBtn;
    BtnNext: TBitBtn;
    BtnLast: TBitBtn;
    BtnInsert: TBitBtn;
    BtnDelete: TBitBtn;
    BtnEdit: TBitBtn;
    BtnPost: TBitBtn;
    BtnCancel: TBitBtn;
    edtPesquisar: TEdit;
    StatusBar1: TStatusBar;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    DBEdtMesReferencia: TDBEdit;
    Label6: TLabel;
    DBEdtAnoReferencia: TDBEdit;
    DBLkpCmbBxVendedor: TDBLookupComboBox;
    CBxMesReferencia: TComboBox;
    CBxAnoReferencia: TComboBox;
    DatTimPckDatFim: TDateTimePicker;
    DatTimPckDatInicio: TDateTimePicker;
    GroupBox2: TGroupBox;
    DBGrid1: TDBGrid;
    procedure BtnInsertClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnEditClick(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure DatTimPckDatInicioChange(Sender: TObject);
    procedure DatTimPckDatFimChange(Sender: TObject);
    procedure CBxMesReferenciaEnter(Sender: TObject);
    procedure CBxMesReferenciaExit(Sender: TObject);
    procedure CBxAnoReferenciaExit(Sender: TObject);
    procedure DBEdtAnoReferenciaEnter(Sender: TObject);
    procedure BtnFirstClick(Sender: TObject);
    procedure BtnPriorClick(Sender: TObject);
    procedure BtnNextClick(Sender: TObject);
    procedure BtnLastClick(Sender: TObject);
    procedure edtPesquisarChange(Sender: TObject);
    procedure BtnDeleteClick(Sender: TObject);
    procedure BtnPostClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBEdtMesReferenciaEnter(Sender: TObject);
    procedure DBGrid1ColEnter(Sender: TObject);
    procedure CBxAnoReferenciaEnter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCadMetVendedor: TfrmCadMetVendedor;

implementation

{$R *.dfm}

uses UDM, UMain, UFuncoes;

procedure TfrmCadMetVendedor.BtnCancelClick(Sender: TObject);
begin

  //cancela adi��o ou edi��o no registro
  dm.tabMetaVendedor.cancel;

  //desativa os componentes
  GroupBox1.Enabled := false;
  BtnPost.Enabled := false;
  BtnCancel.Enabled := false;
  GroupBox2.Enabled := false;

  //ativa manipula��o nos componentes
  BtnFirst.Enabled := true;
  BtnPrior.Enabled := true;
  edtPesquisar.Enabled := true;
  BtnNext.Enabled := true;
  BtnLast.Enabled := true;
  BtnInsert.Enabled := true;
  BtnEdit.Enabled := true;
  BtnDelete.Enabled := true;

    //torna os componentes invisiveis
    CBxMesReferencia.Visible := false;
    CBxAnoReferencia.Visible := false;
end;

procedure TfrmCadMetVendedor.BtnDeleteClick(Sender: TObject);
begin
  //apaga um registro
  try
    if MessageDlg('Deseja excluir o registro?', mtConfirmation,[mbYes, mbNo], 0) = mrYes then
    begin
      dm.tabMetaVendedor.Delete;
      Log('Meta do vendedor apagado pelo usu�rio '+ frmMain.usuario+'.');
    end;
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro:'+ frmCadMetVendedor.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro:'+ frmCadMetVendedor.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmCadMetVendedor.BtnEditClick(Sender: TObject);
var
  rcampo: string;
begin
  try
    //ativa os componentes
    GroupBox1.Enabled := true;
    BtnPost.Enabled := true;
    BtnCancel.Enabled := true;
    GroupBox2.Enabled := True;

    //desativa manipula��o nos componentes
    BtnFirst.Enabled := false;
    BtnPrior.Enabled := false;
    edtPesquisar.Enabled := false;
    BtnNext.Enabled := false;
    BtnLast.Enabled := false;
    BtnInsert.Enabled := false;
    BtnEdit.Enabled := false;
    BtnDelete.Enabled := false;

    //edi��o de registro
    dm.tabMetaVendedor.Edit;

    //torna os objetos visiveis
    CBxMesReferencia.Visible := true;
    CBxAnoReferencia.Visible := true;

    //foca no objeto incial
    DBLkpCmbBxVendedor.SetFocus;

    //valores padr�o
    DatTimPckDatInicio.DateTime := dm.tabMetaVendedormetVenDatInicio.AsDateTime;
    DatTimPckDatFim.DateTime := dm.tabMetaVendedormetVenDatFim.AsDateTime;

    //limpa o comboBox
    CBxAnoReferencia.Items.Clear;
    //adiciona o campos
    rCampo:= FormatDateTime('yyyy',IncMonth(Now(),-12));
    CBxAnoReferencia.Items.Add(rCampo);
    rCampo:= FormatDateTime('yyyy',Now);
    CBxAnoReferencia.Items.Add(rCampo);
    rCampo:= FormatDateTime('yyyy',IncMonth(Now(),12));
    CBxAnoReferencia.Items.Add(rCampo);
    CBxAnoReferencia.ItemIndex:=1;

    //filtra apenas funcionarios que tenham como fun��o vendedor
    with dm.tabFuncionarios do
    begin
      Filter := 'funFcaCodigo = 1 AND funStatus = 1';
      Filtered := true;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro:'+ frmCadClientes.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro:'+ frmCadClientes.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmCadMetVendedor.BtnFirstClick(Sender: TObject);
begin
  //vai ao primeiro registro da tabela
  dm.tabMetaVendedor.First;

  //datetimepicker atualiza��o
  DatTimPckDatInicio.Date := dm.tabMetaVendedormetVenDatInicio.AsDateTime;
  DatTimPckDatFim.Date := dm.tabMetaVendedormetVenDatFim.AsDateTime;
end;

procedure TfrmCadMetVendedor.BtnInsertClick(Sender: TObject);
var
  rcampo : string;
begin
  try
    //ativa os componentes
    GroupBox1.Enabled := true;
    BtnPost.Enabled := true;
    BtnCancel.Enabled := true;
    GroupBox2.Enabled := true;

    //desativa manipula��o nos componentes
    BtnFirst.Enabled := false;
    BtnPrior.Enabled := false;
    edtPesquisar.Enabled := false;
    BtnNext.Enabled := false;
    BtnLast.Enabled := false;
    BtnInsert.Enabled := false;
    BtnEdit.Enabled := false;
    BtnDelete.Enabled := false;

    //adi��o de registro
    dm.tabMetaVendedor.Insert;

    //torna os objetos visiveis
    CBxMesReferencia.Visible := true;
    CBxAnoReferencia.Visible := true;

    CBxMesReferencia.ItemIndex :=0;

    //foca no objeto incial
    DBLkpCmbBxVendedor.SetFocus;

    //valores padr�o
    DatTimPckDatInicio.DateTime := date;
    DatTimPckDatFim.DateTime := date+30;

    //limpa o comboBox
    CBxAnoReferencia.Items.Clear;
    //adiciona o campos
    rCampo:= FormatDateTime('yyyy',IncMonth(Now(),-12));
    CBxAnoReferencia.Items.Add(rCampo);
    rCampo:= FormatDateTime('yyyy',Now);
    CBxAnoReferencia.Items.Add(rCampo);
    rCampo:= FormatDateTime('yyyy',IncMonth(Now(),12));
    CBxAnoReferencia.Items.Add(rCampo);
    CBxAnoReferencia.ItemIndex:=1;

    //filtra apenas funcionarios que tenham como fun��o vendedor
    with dm.tabFuncionarios do
    begin
      Filtered := false;
      Filter := 'funFcaCodigo = 1 AND funStatus = 1';
      Filtered := true;
    end;
    Log('Inser��o de uma nova meta de vendedor pelo usu�rio '+ frmMain.usuario+'.');
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro:'+ frmCadMetVendedor.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro:'+ frmCadMetVendedor.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmCadMetVendedor.BtnLastClick(Sender: TObject);
begin
  //�ltimo registro
  dm.tabMetaVendedor.Last;

  //datetimepicker atualiza��o
  DatTimPckDatInicio.Date := dm.tabMetaVendedormetVenDatInicio.AsDateTime;
  DatTimPckDatFim.Date := dm.tabMetaVendedormetVenDatFim.AsDateTime;
end;

procedure TfrmCadMetVendedor.BtnNextClick(Sender: TObject);
begin
  //pr�ximo registro
  dm.tabMetaVendedor.Next;

  //datetimepicker atualiza��o
  DatTimPckDatInicio.Date := dm.tabMetaVendedormetVenDatInicio.AsDateTime;
  DatTimPckDatFim.Date := dm.tabMetaVendedormetVenDatFim.AsDateTime;
end;

procedure TfrmCadMetVendedor.BtnPostClick(Sender: TObject);
begin
  if DatTimPckDatFim.DateTime < DatTimPckDatInicio.DateTime  then
  begin
    MessageDlg('Data final menor que data inicial!! ', mtWarning, [mbOk], 0);
    DatTimPckDatFim.SetFocus;
  end
  else
  begin
    //confirma adi��o ou edi��o no registro
    dm.tabMetaVendedor.Post;

    //desativa os componentes
    GroupBox1.Enabled := false;
    BtnPost.Enabled := false;
    BtnCancel.Enabled := false;
    GroupBox2.Enabled := false;

    //ativa manipula��o nos componentes
    BtnFirst.Enabled := true;
    BtnPrior.Enabled := true;
    edtPesquisar.Enabled := true;
    BtnNext.Enabled := true;
    BtnLast.Enabled := true;
    BtnInsert.Enabled := true;
    BtnEdit.Enabled := true;
    BtnDelete.Enabled := true;

    //torna os componentes invisiveis
    CBxMesReferencia.Visible := false;
    CBxAnoReferencia.Visible := false;
  end;
end;

procedure TfrmCadMetVendedor.BtnPriorClick(Sender: TObject);
begin
  //registro anterior
  dm.tabMetaVendedor.Prior;

  //datetimepicker atualiza��o
  DatTimPckDatInicio.Date := dm.tabMetaVendedormetVenDatInicio.AsDateTime;
  DatTimPckDatFim.Date := dm.tabMetaVendedormetVenDatFim.AsDateTime;
end;

procedure TfrmCadMetVendedor.CBxAnoReferenciaEnter(Sender: TObject);
begin
  DBEdtAnoReferencia.Visible := false;
  CBxAnoReferencia.Visible := true;
end;

procedure TfrmCadMetVendedor.CBxAnoReferenciaExit(Sender: TObject);
begin
  DBEdtAnoReferencia.Visible := true;
  CBxAnoReferencia.Visible := false;

  // resultado enviado a base de dados
  if (dm.tabMetaVendedor.State=dsInsert)or(dm.tabMetaVendedor.State=dsEdit) then
    DBEdtAnoReferencia.Text:=CBxAnoReferencia.Items[CBxAnoReferencia.ItemIndex];

end;

procedure TfrmCadMetVendedor.CBxMesReferenciaEnter(Sender: TObject);
begin
  DBEdtMesReferencia.Visible := false;
  CBxMesReferencia.Visible := true;
end;

procedure TfrmCadMetVendedor.CBxMesReferenciaExit(Sender: TObject);
begin
  DBEdtMesReferencia.Visible := true;
  CBxMesReferencia.Visible := false;
  // resultado enviado a base de dados
  if (dm.tabMetaVendedor.State=dsInsert)or(dm.tabMetaVendedor.State=dsEdit) then
    DBEdtMesReferencia.Text:=CBxMesReferencia.Items[CBxMesReferencia.ItemIndex];
end;

procedure TfrmCadMetVendedor.DatTimPckDatFimChange(Sender: TObject);
begin
  //campo recebe o valor do dateTimePicker
  dm.tabMetaVendedormetVenDatFim.AsDateTime := StrToDate(FormatDateTime('dd/mm/yyyy', DatTimPckDatFim.date));
end;

procedure TfrmCadMetVendedor.DatTimPckDatInicioChange(Sender: TObject);
begin
  //campo recebe o valor do dateTimePicker
  dm.tabMetaVendedormetVenDatInicio.AsDateTime := StrToDate(FormatDateTime('dd/mm/yyyy', DatTimPckDatInicio.date));
end;

procedure TfrmCadMetVendedor.DBEdtAnoReferenciaEnter(Sender: TObject);
begin
  CBxAnoReferencia.Visible := true;
  CBxAnoReferencia.SetFocus;
  DBEdtAnoReferencia.Visible := false;
end;

procedure TfrmCadMetVendedor.DBEdtMesReferenciaEnter(Sender: TObject);
begin
  CBxMesReferencia.Visible := true;
  CBxMesReferencia.SetFocus;
  DBEdtMesReferencia.Visible := false;
end;

procedure TfrmCadMetVendedor.DBGrid1ColEnter(Sender: TObject);
begin
  if (dm.tabMetaVendedor.State = dsEdit) or  (dm.tabMetaVendedor.State = dsInsert)then
  begin
    dm.tabMetaVendedor.Post;
    dm.tabMetaVendedor.Edit;
  end;

end;

procedure TfrmCadMetVendedor.edtPesquisarChange(Sender: TObject);
begin
  //pesquisa aproximada por c�digo
  if (dm.tabMetaVendedor.Locate('metVenCodigo', edtPesquisar.Text, [loCaseInsensitive, loPartialKey])) then
  begin
    //datetimepicker atualiza��o
    DatTimPckDatInicio.Date := dm.tabMetaVendedormetVenDatInicio.AsDateTime;
    DatTimPckDatFim.Date := dm.tabMetaVendedormetVenDatFim.AsDateTime;
  end;

end;

procedure TfrmCadMetVendedor.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  //FECHA A CONEX�0 COM A TABELA
  if (dm.tabMetaVendedor.State=dsEdit) or (dm.tabMetaVendedor.State=dsInsert) then
  begin
    if MessageDlg('Cadastro em modo de inser��o ou edi��o. Deseja fechar a tela?', mtConfirmation,[mbYes, mbNo], 0) = mrYes then
    begin
      dm.tabMetaVendedor.Close;
      dm.tabMetaVendedorItem.Close;
      DM.tabFuncionarios.Close;

      //desativa o filtro apenas funcionarios que tenham como fun��o vendedor
      with dm.tabFuncionarios do
      begin
        Filtered := false;
      end;

      Action := caFree; //a��o do objeto TCloseAction para fechar a tela
    end
    else
      Action := caNone ; //a��o do objeto TCloseAction para n�o fechar a tela
  end
  else
  begin
    dm.tabMetaVendedor.Close;
    dm.tabMetaVendedorItem.Close;
    dm.tabFuncionarios.Close;
  end;

end;

procedure TfrmCadMetVendedor.FormShow(Sender: TObject);
begin
  //abre a conex�o com a tabela
  dm.tabMetaVendedor.Open;
  dm.tabMetaVendedorItem.Open;
  dm.tabFuncionarios.Open;
end;

end.
