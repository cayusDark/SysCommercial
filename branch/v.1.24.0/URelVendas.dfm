object frmRelVendas: TfrmRelVendas
  Left = 0
  Top = 0
  Caption = 'Relat'#243'rio de vendas por vendedor'
  ClientHeight = 255
  ClientWidth = 406
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 19
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 406
    Height = 50
    Align = alTop
    Caption = 'Estado'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object CmbBxUf: TComboBox
      Left = 2
      Top = 18
      Width = 53
      Height = 27
      CharCase = ecUpperCase
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      Items.Strings = (
        'AC'#9
        'AL'#9
        'AM'#9
        'AP'#9
        'BA'#9
        'CE'#9
        'DF'#9
        'ES'#9
        'GO'#9
        'MA'#9
        'MG'#9
        'MS'#9
        'MT'#9
        'PA'#9
        'PB'#9
        'PE'#9
        'PI'#9
        'PR'#9
        'RJ'#9
        'RN'#9
        'RO'#9
        'RR'#9
        'RS'#9
        'SC'#9
        'SE'#9
        'SP'#9
        'TO'#9)
    end
    object ChkBxUf: TCheckBox
      Left = 335
      Top = 25
      Width = 67
      Height = 17
      Caption = 'Todos'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = ChkBxUfClick
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 102
    Width = 406
    Height = 52
    Align = alTop
    Caption = 'Vendedor'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object DBCBxVendedor: TDBComboBox
      Left = 2
      Top = 19
      Width = 326
      Height = 27
      CharCase = ecUpperCase
      DataField = 'funNome'
      DataSource = DM.DSTabFuncionarios
      TabOrder = 0
    end
    object ChkBxVendedor: TCheckBox
      Left = 335
      Top = 29
      Width = 60
      Height = 17
      Caption = 'Todos'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = ChkBxVendedorClick
    end
  end
  object GroupBox3: TGroupBox
    Left = 0
    Top = 50
    Width = 406
    Height = 52
    Align = alTop
    Caption = 'Representada'
    TabOrder = 2
    object DBCBxRepresentada: TDBComboBox
      Left = 2
      Top = 20
      Width = 326
      Height = 27
      CharCase = ecUpperCase
      DataField = 'forRazao'
      DataSource = DM.DSTabFornecedores
      TabOrder = 0
    end
    object ChkBxRepresentada: TCheckBox
      Left = 335
      Top = 23
      Width = 86
      Height = 17
      Caption = 'Todos'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = ChkBxRepresentadaClick
    end
  end
  object GroupBox4: TGroupBox
    Left = 0
    Top = 154
    Width = 406
    Height = 56
    Align = alTop
    Caption = 'P'#233'riodo'
    TabOrder = 3
    object Label1: TLabel
      Left = 2
      Top = 23
      Width = 39
      Height = 19
      Caption = #205'nicio'
    end
    object Label2: TLabel
      Left = 173
      Top = 23
      Width = 26
      Height = 19
      Caption = 'Fim'
    end
    object DtTmPckInicio: TDateTimePicker
      Left = 45
      Top = 19
      Width = 122
      Height = 27
      Date = 42242.000000000000000000
      Time = 42242.000000000000000000
      TabOrder = 0
    end
    object DtTmPckFim: TDateTimePicker
      Left = 206
      Top = 19
      Width = 122
      Height = 27
      Date = 42242.000000000000000000
      Time = 42242.000000000000000000
      TabOrder = 1
    end
  end
  object GroupBox6: TGroupBox
    Left = 0
    Top = 210
    Width = 406
    Height = 45
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    object BitBtn1: TBitBtn
      Left = 141
      Top = 6
      Width = 109
      Height = 32
      Caption = 'Im&primir'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      Glyph.Data = {
        46060000424D4606000000000000360400002800000016000000160000000100
        08000000000010020000C40E0000C40E00000001000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
        A6000020400000206000002080000020A0000020C0000020E000004000000040
        20000040400000406000004080000040A0000040C0000040E000006000000060
        20000060400000606000006080000060A0000060C0000060E000008000000080
        20000080400000806000008080000080A0000080C0000080E00000A0000000A0
        200000A0400000A0600000A0800000A0A00000A0C00000A0E00000C0000000C0
        200000C0400000C0600000C0800000C0A00000C0C00000C0E00000E0000000E0
        200000E0400000E0600000E0800000E0A00000E0C00000E0E000400000004000
        20004000400040006000400080004000A0004000C0004000E000402000004020
        20004020400040206000402080004020A0004020C0004020E000404000004040
        20004040400040406000404080004040A0004040C0004040E000406000004060
        20004060400040606000406080004060A0004060C0004060E000408000004080
        20004080400040806000408080004080A0004080C0004080E00040A0000040A0
        200040A0400040A0600040A0800040A0A00040A0C00040A0E00040C0000040C0
        200040C0400040C0600040C0800040C0A00040C0C00040C0E00040E0000040E0
        200040E0400040E0600040E0800040E0A00040E0C00040E0E000800000008000
        20008000400080006000800080008000A0008000C0008000E000802000008020
        20008020400080206000802080008020A0008020C0008020E000804000008040
        20008040400080406000804080008040A0008040C0008040E000806000008060
        20008060400080606000806080008060A0008060C0008060E000808000008080
        20008080400080806000808080008080A0008080C0008080E00080A0000080A0
        200080A0400080A0600080A0800080A0A00080A0C00080A0E00080C0000080C0
        200080C0400080C0600080C0800080C0A00080C0C00080C0E00080E0000080E0
        200080E0400080E0600080E0800080E0A00080E0C00080E0E000C0000000C000
        2000C0004000C0006000C0008000C000A000C000C000C000E000C0200000C020
        2000C0204000C0206000C0208000C020A000C020C000C020E000C0400000C040
        2000C0404000C0406000C0408000C040A000C040C000C040E000C0600000C060
        2000C0604000C0606000C0608000C060A000C060C000C060E000C0800000C080
        2000C0804000C0806000C0808000C080A000C080C000C080E000C0A00000C0A0
        2000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0E000C0C00000C0C0
        2000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0A000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFF08F7F7
        F7F7F7F7F7F7F7F7F7F7F7F7F6FFFFFF0000FFFFFFF707070707070707070707
        070707F7FFFFFFFF0000F6F7A4A407F6F6F6F6F6F608F6F6F6F6F6A4F7A4A408
        0000A4070707F7080808080808080808080807A4070707A40000A40807085252
        52525252525252525252495B080708A40000A4080808A4494949494949494949
        494949F70808F6A40000A4F60808F6F6F6F6F6F6F6F6F6F6F6F6F6F60808F6A4
        0000A4F6F6F6F6F6F6F6F6F6F6F6F6090908F6F6F6F6FFA40000A4FFFFFFFF09
        E2E2E2E2E2E2E2D9D9D9F6FFFFFFFFA40000A4FFFFFFFF09E2E3EBEBEBEBE2E2
        DAD909FFFFFFFFF70000A4FFFFFFFF09E2E2E2E2E2E2DAD9D9D909FFFFFFFFF7
        0000F7FFFFFFFF09E3ECECECEBEBEBE3EBE309FFFFFFFFF70000FFF707F6FF09
        090909090909090909EC09F60807F7FF0000FFFFFFF708ED0909090909090909
        09EC070708FFFFFF0000FFFFFF070808FFFFFFF6F6F6F608080808F7FFFFFFFF
        0000FFFFFF07F6F6FFFFFFFFF6F6F6F6F60808F7FFFFFFFF0000FFFFFF07F6F6
        FFFFFFFFFFF6F6F6F60808F7FFFFFFFF0000FFFFFFF7F6F6FFFFFFFFFFF6F6F6
        F6F608F7FFFFFFFF0000FFFFFF08F708FFFFFFFFFFFFF6F6F6F608F7FFFFFFFF
        0000FFFFFFFFF608FFFFFFFFFFFFFFF6F6F608F7FFFFFFFF0000FFFFFFFF0808
        F6F6F6F6F6F6F6F6F6F6F6F7FFFFFFFF0000FFFFFFFFFF070707070707070707
        0707F7F7FFFFFFFF0000}
      ParentFont = False
      TabOrder = 0
      OnClick = BitBtn1Click
    end
  end
  object frxRptVendas: TfrxReport
    Version = '4.13.2'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42049.537297442100000000
    ReportOptions.Name = 'Relat'#243'rio de vendas'
    ReportOptions.LastChange = 42257.947419942100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 304
    Top = 207
    Datasets = <
      item
        DataSet = frxDBDSVendas
        DataSetName = 'frxDBDSVendas'
      end>
    Variables = <
      item
        Name = ' Vendas'
        Value = Null
      end
      item
        Name = 'datInicio'
        Value = Null
      end
      item
        Name = 'datFim'
        Value = Null
      end
      item
        Name = 'uF'
        Value = Null
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageFooter1: TfrxPageFooter
        Height = 18.897650000000000000
        Top = 600.945270000000000000
        Width = 718.110700000000000000
        object TotalPages: TfrxMemoView
          Left = 604.724800000000000000
          Top = 1.220470000000000000
          Width = 132.283550000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'P'#193'GINA [Page] DE [TotalPages#]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 28.015770000000000000
        Top = 306.141930000000000000
        Width = 718.110700000000000000
        DataSet = frxDBDSVendas
        DataSetName = 'frxDBDSVendas'
        RowCount = 0
        object frxDBDSVendascliNome: TfrxMemoView
          Top = 1.000000000000000000
          Width = 309.921460000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'cliNome'
          DataSet = frxDBDSVendas
          DataSetName = 'frxDBDSVendas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBDSVendas."cliNome"]')
          ParentFont = False
        end
        object frxDBDSVendasvenValLiquido: TfrxMemoView
          Left = 584.449290000000000000
          Top = 1.220470000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDBDSVendas
          DataSetName = 'frxDBDSVendas'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBDSVendas."venValLiquido"]')
          ParentFont = False
        end
        object frxDBDSVendasvenValProdutos: TfrxMemoView
          Left = 429.008040000000000000
          Top = 1.000000000000000000
          Width = 136.063080000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDBDSVendas
          DataSetName = 'frxDBDSVendas'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBDSVendas."venValProdutos"]')
          ParentFont = False
        end
        object frxDBDSVendasvenDatVenda: TfrxMemoView
          Left = 314.787570000000000000
          Top = 1.000000000000000000
          Width = 105.826840000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'venDatVenda'
          DataSet = frxDBDSVendas
          DataSetName = 'frxDBDSVendas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBDSVendas."venDatVenda"]')
          ParentFont = False
        end
        object Line3: TfrxLineView
          Top = 20.456710000000000000
          Width = 718.110023860000000000
          ShowHint = False
          Diagonal = True
        end
      end
      object ReportTitle1: TfrxReportTitle
        Height = 86.929190000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Shape4: TfrxShapeView
          Left = -0.220470000000000000
          Top = 28.236240000000000000
          Width = 714.331170000000000000
          Height = 41.574830000000000000
          ShowHint = False
        end
        object Memo1: TfrxMemoView
          Left = 234.756030000000000000
          Top = 2.559060000000000000
          Width = 306.141930000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'RELAT'#211'RIO DE VENDAS')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = -5.763760000000000000
          Top = 30.574830000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Estado:')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = -0.204700000000000000
          Top = 48.149660000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
        end
        object Date: TfrxMemoView
          Left = 821.732840000000000000
          Top = 45.354360000000000000
          Width = 211.653680000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd '#39'de'#39' mmmm '#39'de'#39' yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Manaus, [Date]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 137.740260000000000000
          Top = 45.488250000000000000
          Width = 11.338590000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '-')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 48.252010000000000000
          Top = 30.015770000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[uF]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 65.031540000000000000
          Top = 48.267780000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[datInicio]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 147.740260000000000000
          Top = 48.267780000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[datFim]')
          ParentFont = False
        end
      end
      object GrpHdrRepresentada: TfrxGroupHeader
        Height = 37.795300000000000000
        Top = 166.299320000000000000
        Width = 718.110700000000000000
        Condition = 'frxDBDSVendas."forRazao"'
        object Shape1: TfrxShapeView
          Width = 411.968770000000000000
          Height = 22.677180000000000000
          ShowHint = False
        end
        object frxDBDSVendasforRazao1: TfrxMemoView
          Left = 81.779530000000000000
          Top = 2.779530000000000000
          Width = 328.819110000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDBDSVendas
          DataSetName = 'frxDBDSVendas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDBDSVendas."forRazao"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 1.338590000000000000
          Top = 2.559060000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Representada:')
          ParentFont = False
        end
      end
      object GrpFtrVendedor: TfrxGroupFooter
        Height = 37.795300000000000000
        Top = 355.275820000000000000
        Width = 718.110700000000000000
        object Shape3: TfrxShapeView
          Left = 169.448980000000000000
          Width = 272.125984250000000000
          Height = 22.677167800000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Shape2: TfrxShapeView
          Left = 447.228820000000000000
          Width = 268.346630000000000000
          Height = 22.677180000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Memo15: TfrxMemoView
          Left = 582.071430000000000000
          Top = 3.000000000000000000
          Width = 132.283550000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[SUM(<frxDBDSVendas."venValLiquido">,MasterData1,1)]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 451.401980000000000000
          Top = 3.000000000000000000
          Width = 128.504020000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Total L'#237'quido vendedor: ')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 170.693260000000000000
          Top = 3.000000000000000000
          Width = 117.165430000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Total Bruto vendedor:')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 289.299630000000000000
          Top = 3.000000000000000000
          Width = 147.401670000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = frxDBDSVendas
          DataSetName = 'frxDBDSVendas'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[SUM(<frxDBDSVendas."venValProdutos">,MasterData1,1)]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 56.692950000000000000
        Top = 521.575140000000000000
        Width = 718.110700000000000000
        object Shape10: TfrxShapeView
          Left = 451.850650000000000000
          Top = 3.338590000000000000
          Width = 264.567100000000000000
          Height = 22.677180000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Shape8: TfrxShapeView
          Left = 451.543600000000000000
          Top = 30.236240000000000000
          Width = 264.567100000000000000
          Height = 22.677180000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Memo19: TfrxMemoView
          Left = 463.827150000000000000
          Top = 32.779530000000000000
          Width = 90.708720000000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Total l'#237'quido:')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 552.110700000000000000
          Top = 33.779530000000000000
          Width = 158.740260000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = frxDBDSVendas
          DataSetName = 'frxDBDSVendas'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[SUM(<frxDBDSVendas."venValLiquido">,MasterData1)]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 464.268090000000000000
          Top = 5.000000000000000000
          Width = 83.149660000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Total bruto:')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 548.094930000000000000
          Top = 5.000000000000000000
          Width = 162.519790000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataSet = frxDBDSVendas
          DataSetName = 'frxDBDSVendas'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[SUM(<frxDBDSVendas."venValProdutos">,MasterData1)]')
          ParentFont = False
        end
      end
      object GrpHdrVendedor: TfrxGroupHeader
        Height = 56.692950000000000000
        Top = 226.771800000000000000
        Width = 718.110700000000000000
        Condition = 'frxDBDSVendas."funNome"'
        object Memo4: TfrxMemoView
          Top = 28.897650000000000000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'CLIENTE')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 585.126470000000000000
          Top = 29.472480000000000000
          Width = 109.606370000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'VALOR LIQUIDO')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 430.094620000000000000
          Top = 28.897650000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'VALOR BRUTO')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 314.196970000000000000
          Top = 28.897650000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'DATA')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Top = 44.559060000000000000
          Width = 718.110023860000000000
          ShowHint = False
          Diagonal = True
        end
        object Shape5: TfrxShapeView
          Width = 411.968770000000000000
          Height = 22.677180000000000000
          ShowHint = False
        end
        object Memo2: TfrxMemoView
          Top = 3.779530000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Vendedor:')
          ParentFont = False
        end
        object frxDBDSVendasfunNome: TfrxMemoView
          Left = 57.488250000000000000
          Top = 3.779530000000000000
          Width = 351.496290000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'funNome'
          DataSet = frxDBDSVendas
          DataSetName = 'frxDBDSVendas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDBDSVendas."funNome"]')
          ParentFont = False
        end
      end
      object GrpFtrRepresentada: TfrxGroupFooter
        Height = 45.354360000000000000
        Top = 415.748300000000000000
        Width = 718.110700000000000000
        object Shape6: TfrxShapeView
          Left = 168.291590000000000000
          Width = 272.125984250000000000
          Height = 22.677167800000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Memo3: TfrxMemoView
          Left = 169.535870000000000000
          Top = 2.000000000000000000
          Width = 136.063080000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Total Bruto representada:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 307.945270000000000000
          Top = 2.000000000000000000
          Width = 128.504020000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[SUM(<frxDBDSVendas."venValProdutos">,MasterData1,1)]')
          ParentFont = False
        end
        object Shape7: TfrxShapeView
          Left = 447.110700000000000000
          Width = 268.346630000000000000
          Height = 22.677180000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          Left = 448.283860000000000000
          Top = 2.000000000000000000
          Width = 128.504020000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Total L'#237'quido vendedor: ')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 578.646260000000000000
          Top = 2.000000000000000000
          Width = 132.283550000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[SUM(<frxDBDSVendas."venValLiquido">,MasterData1,1)]')
          ParentFont = False
        end
      end
    end
  end
  object frxDBDSVendas: TfrxDBDataset
    UserName = 'frxDBDSVendas'
    CloseDataSource = False
    DataSet = DM.queryRelVendas
    BCDToCurrency = False
    Left = 93
    Top = 207
  end
  object frxPDFExport1: TfrxPDFExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    PrintOptimized = False
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    Left = 16
    Top = 218
  end
end
