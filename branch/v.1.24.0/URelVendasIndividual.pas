unit URelVendasIndividual;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Grids, Vcl.DBGrids, Vcl.StdCtrls, DB,
  Vcl.Buttons, frxClass, frxExportPDF, frxDBSet,System.UITypes;

type
  TfrmRelVendasIndividual = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    edtPesquisa: TEdit;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    DBGrid1: TDBGrid;
    frxDBDSRelVendasIndividual: TfrxDBDataset;
    frxRptRelVendasIndividual: TfrxReport;
    frxPDFExport1: TfrxPDFExport;
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure edtPesquisaChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelVendasIndividual: TfrmRelVendasIndividual;

implementation

{$R *.dfm}

uses UDM;

procedure TfrmRelVendasIndividual.BitBtn1Click(Sender: TObject);
begin
  try
    with DM.queryRelVendasIndividual do
    begin
      close;
      SQL.Clear;
      SQL.Add('SELECT');
      SQL.Add('   d.venId,');
      SQL.Add('   d.venCodPedFornecedor,');
      SQL.Add('   c.cliNome,');
      SQL.Add('   d.venDatVenda,');
      SQL.Add('   b.forRazao,');
      SQL.Add('   g.przPagDescricao,');
      SQL.Add('   d.venPrzEntrega,');
      SQL.Add('   a.funNome,');
      SQL.Add('   d.venStatus,');
      SQL.Add('   d.venTipo,');
      SQL.Add('   d.venFrete,');
      SQL.Add('   d.venPrzPagCodigo,');
      SQL.Add('   d.venValProdutos,');
      SQL.Add('   d.venAcrIPI,');
      SQL.Add('   d.venDesComercial,');
      SQL.Add('   d.venDesICMS,');
      SQL.Add('   d.venDesPISCOFINS,');
      SQL.Add('   d.venValLiquido,');
      SQL.Add('   d.venValFatBruto,');
      SQL.Add('   d.venValFatLiquido,');
      SQL.Add('   f.IteVenProCodFornecedor,');
      SQL.Add('   e.proNome,');
      SQL.Add('   f.iteVenValUnitario,');
      SQL.Add('   f.iteVenQuantidade,');
      SQL.Add('   f.iteVenDesconto1,');
      SQL.Add('   f.iteVenDesconto2,');
      SQL.Add('   f.iteVenDesconto3,');
      SQL.Add('   f.iteVenQtdCaixas,');
      SQL.Add('   f.iteVenValTotal');
      SQL.Add('FROM funcionarios AS a, fornecedores AS b, clientes AS c, vendas AS d,');
      SQL.Add('   Produtos AS e, itemVenda AS f, prazoPagamentos AS g');
      SQL.Add('WHERE c.cliCodigo = d.venCliCodigo');
      SQL.Add('AND b.forCodigo = d.venForCodigo');
      SQL.Add('AND a.funCodigo = d.venFunCodigo');
      SQL.Add('AND e.ProCodFornecedor =      f.IteVenProCodFornecedor');
      SQL.Add('AND d.venId = f.iteVenVenId');
      SQL.Add('AND g.przPagCodigo = d.venPrzPagCodigo');
      SQL.Add('AND d.venTipo =1');
      SQL.Add('AND d.venId= :pVenId;');

      //passagem dos valores dos parametros
      Parameters.ParamByName('pVenId').Value := DBgrid1.Columns[0].Field.AsString;
      Open;
    end;
    if dm.queryRelVEndasIndividual.IsEmpty then
      MessageDlg('N�o h� dados!', mtConfirmation,[mbOK], 0)
    else
      begin
        if DM.queryRelVEndasIndividualvenStatus.AsInteger = 0 then
          frxRptRelVendasIndividual.Variables['Status'] := QuotedStr('N�O PROCESSADO')
        else
          frxRptRelVendasIndividual.Variables['Status'] := QuotedStr('PROCESSADO');

        if DM.queryRelVEndasIndividualvenTipo.AsInteger = 0 then
          frxRptRelVendasIndividual.Variables['Tipo'] := QuotedStr('BONIFICA��O')
        else
          frxRptRelVendasIndividual.Variables['Tipo'] := QuotedStr('VENDA');

        if DM.queryRelVEndasIndividualvenFrete.AsInteger = 0 then
          frxRptRelVendasIndividual.Variables['Frete'] := QuotedStr('CIF')
        else
          frxRptRelVendasIndividual.Variables['Status'] := QuotedStr('FOB');

        frxRptRelVendasIndividual.ShowReport();
      end;
  except
    on E: Exception do
    begin
      ShowMessage('Error: ' + E.Message +#13+'class name = '+E.ClassName);
    end;

  end;
end;

procedure TfrmRelVendasIndividual.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
If Odd(dm.tabVenda.RecNo) and (dm.tabVenda.State <> dsInsert) then
  begin //Lembre-se de colocar a unit DB na cl�usula uses na unit da tela.
      DBGrid1.Canvas.Brush.Color := clMoneyGreen; // muda a cor do pincel
      DBGrid1.Canvas.FillRect(Rect); // Preenche o fundo com a cor especificada
      DBGrid1.DefaultDrawDataCell(Rect,Column.Field,State);// desenha as c�lulas da grade
  end;

end;

procedure TfrmRelVendasIndividual.edtPesquisaChange(Sender: TObject);
begin
  //pesquisa por aproxima��o
  //Dm.tabVenda.Locate( 'venCodPedFornecedor',edtPesquisa.Text, [loCaseInsensitive, loPartialKey] );
  try
    if edtPesquisa.Text <> '' then
    begin
      with dm.tabVenda do
      begin
        Filtered := false;
        Filter := 'venCodPedFornecedor LIKE %' + edtPesquisa.Text+'%';
        Filtered := true;
      end;
    end;
  except
    on E: EDatabaseError do
    begin
      // Generic database error handling
      ShowMessage('Error: ' + E.Message +#13+'class name = '+E.ClassName);
    end;

    on E: Exception do
    begin
      ShowMessage('Error: ' + E.Message +#13+'class name = '+E.ClassName);
    end;
  end;
end;

procedure TfrmRelVendasIndividual.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  //fecha a conex�o com as tabelas
  dm.tabVenda.Close;
  dm.tabClientes.Close;

  with dm.tabVenda do
  begin
    Filtered := false;
  end;
end;

procedure TfrmRelVendasIndividual.FormShow(Sender: TObject);
begin
  //abre a conex�o com a tabela
  dm.tabClientes.Open;
  dm.tabVenda.Open;
end;

end.
