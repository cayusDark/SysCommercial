unit URelComVendas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ComCtrls,
  frxClass, frxExportPDF, frxDBSet;

type
  TfrmRelComVendas = class(TForm)
    GroupBox6: TGroupBox;
    BitBtn1: TBitBtn;
    GroupBox3: TGroupBox;
    CmbBxRepresentada: TComboBox;
    frxDBDSRelComVendas: TfrxDBDataset;
    frxRptRelComVendas: TfrxReport;
    frxPDFExport1: TfrxPDFExport;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    CmbBxMesReferencia2: TComboBox;
    CmbBxAnoReferencia2: TComboBox;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    CmbBxMesReferencia1: TComboBox;
    CmbBxAnoReferencia1: TComboBox;
    procedure FormShow(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelComVendas: TfrmRelComVendas;

implementation

{$R *.dfm}

uses UDM;

procedure TfrmRelComVendas.BitBtn1Click(Sender: TObject);
begin

  try
    //verifica se filtros utilizados s�o iguais para p�riodo 1 e periodo 2
    if (CmbBxMesReferencia1.Text = CmbBxMesReferencia2.Text) and (CmbBxAnoReferencia2.Text = CmbBxAnoReferencia2.Text) then
    begin
        ShowMessage('Per�odo filtrado duplicado!!'+#13+
                    'Escolher um p�riodo diferente!!');
        CmbBxMesReferencia1.SetFocus;
    end
    else
      begin
        //chamada de relat�rio de meta
        with dm.queryRelComVendas do
        begin
          close;
          SQL.Clear;
          SQL.Add('SELECT ');
          SQL.Add('   a.forRazao, b.metMesReferencia, b.metAnoReferencia,');
          SQL.Add('   SUM (c.venValLiquido) AS valLiquido ');
          SQL.Add('FROM fornecedores AS a, metas AS b, vendas AS c');
          SQL.Add('WHERE a.forCodigo = b.metForCodigo');
          SQL.Add('AND a.forCodigo = c.venForCodigo');
          SQL.Add('AND c.venTipo = 1');
          SQL.Add('AND b.metMesReferencia = :pMes1');
          SQL.Add('AND b.metAnoReferencia = :pAno1');
          SQL.Add('AND a.forRazao = :pRazao1');
          SQL.Add('AND c.venDatVenda BETWEEN (b.metDatInicio) AND (b.metDatFim)');
          SQL.Add('GROUP BY    a.forRazao,b.metMesReferencia, b.metAnoReferencia ');
          SQL.Add('UNION');
          SQL.Add('SELECT');
          SQL.Add('   a.forRazao,b.metMesReferencia, b.metAnoReferencia,');
          SQL.Add('   SUM (c.venValLiquido) AS valLiquido');
          SQL.Add('FROM fornecedores AS a, metas AS b, vendas AS c');
          SQL.Add('WHERE a.forCodigo = b.metForCodigo');
          SQL.Add('AND a.forCodigo = c.venForCodigo');
          SQL.Add('AND c.venTipo = 1');
          SQL.Add('AND b.metMesReferencia =:pMes2');
          SQL.Add('AND b.metAnoReferencia = :pAno2');
          SQL.Add('AND a.forRazao = :pRazao2');
          SQL.Add('AND c.venDatVenda BETWEEN (b.metDatInicio) AND (b.metDatFim)');
          SQL.Add('GROUP BY    a.forRazao,b.metMesReferencia, b.metAnoReferencia');

          //passagem de paramentros
          Parameters.ParamByName('pMes1').Value := CmbBxMesReferencia1.Items[CmbBxMesReferencia1.ItemIndex];
          Parameters.ParamByName('pAno1').Value := CmbBxAnoReferencia1.Items[CmbBxAnoReferencia1.ItemIndex];
          Parameters.ParamByName('pRazao1').Value := CmbBxRepresentada.Items[CmbBxRepresentada.ItemIndex];
          Parameters.ParamByName('pMes2').Value := CmbBxMesReferencia2.Items[CmbBxMesReferencia2.ItemIndex];
          Parameters.ParamByName('pAno2').Value := CmbBxAnoReferencia1.Items[CmbBxAnoReferencia2.ItemIndex];
          Parameters.ParamByName('pRazao2').Value := CmbBxRepresentada.Items[CmbBxRepresentada.ItemIndex];
          Open;
        end;

      if dm.queryRelComVendas.IsEmpty then
        MessageDlg('N�o h� dados!', mtConfirmation,[mbOK], 0)
      else
        begin
          frxRptRelComVendas.Variables['mes1'] := QuotedStr(CmbBxMesReferencia1.Items[CmbBxMesReferencia1.ItemIndex]);
          frxRptRelComVendas.Variables['ano1'] := QuotedStr(CmbBxAnoReferencia1.Items[CmbBxAnoReferencia1.ItemIndex]);
          frxRptRelComVendas.Variables['mes2'] := QuotedStr(CmbBxMesReferencia2.Items[CmbBxMesReferencia2.ItemIndex]);
          frxRptRelComVendas.Variables['ano3'] := QuotedStr(CmbBxAnoReferencia2.Items[CmbBxAnoReferencia2.ItemIndex]);
          frxRptRelComVendas.ShowReport();
        end;
      end;
  Except
    on E: Exception do
      ShowMessage('Ocorreu um erro na aplica��o.'+#13+
                  'Error: '+E.Message+#13+
                  'Classe Name: '+E.ClassName);

  end;

end;

procedure TfrmRelComVendas.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dm.tabFornecedores.Close;
end;

procedure TfrmRelComVendas.FormShow(Sender: TObject);
var
  rCampo : string;
  mes: integer;
begin
 //abre a conexao com as tabelas
  dm.tabFornecedores.Open;


  /////////////////////////filtro dos comboBox///////////////////////
  //filtro de fornecedores
  if not DM.tabFornecedores.Eof then
  begin
    dm.tabFornecedores.First;
    while not DM.tabFornecedores.Eof do
    begin
      if dm.tabFornecedoresforStatus.AsInteger = 1 then
      begin
        rCampo:= dm.tabFornecedoresforRazao.AsString;
        CmbBxRepresentada.Items.Add(rCampo);
      end;
      DM.tabFornecedores.Next;
    end;
    CmbBxRepresentada.ItemIndex:=-1;
  end;

  //limpa o CmbBxAnoReferencia1
  CmbBxAnoReferencia1.Items.Clear;

  //adiciona o campos
  rCampo:= FormatDateTime('yyyy',IncMonth(Now(),-12));
  CmbBxAnoReferencia1.Items.Add(rCampo);
  rCampo:= FormatDateTime('yyyy',Now);
  CmbBxAnoReferencia1.Items.Add(rCampo);
  rCampo:= FormatDateTime('yyyy',IncMonth(Now(),12));
  CmbBxAnoReferencia1.Items.Add(rCampo);
  CmbBxAnoReferencia1.ItemIndex:=0;

  //limpa o CmbBxAnoReferencia2
  CmbBxAnoReferencia2.Items.Clear;

  //adiciona o campos
  rCampo:= FormatDateTime('yyyy',IncMonth(Now(),-12));
  CmbBxAnoReferencia2.Items.Add(rCampo);
  rCampo:= FormatDateTime('yyyy',Now);
  CmbBxAnoReferencia2.Items.Add(rCampo);
  rCampo:= FormatDateTime('yyyy',IncMonth(Now(),12));
  CmbBxAnoReferencia2.Items.Add(rCampo);
  CmbBxAnoReferencia2.ItemIndex:=1;

  mes := StrToInt(FormatDateTime('MM', Now));
  CmbBxMesReferencia2.ItemIndex := mes -1;
  CmbBxMesReferencia1.ItemIndex := mes -1;


end;

end.
