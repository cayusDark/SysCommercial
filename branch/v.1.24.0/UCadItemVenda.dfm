inherited frmMovItemVenda: TfrmMovItemVenda
  Caption = 'Movimenta'#231#227'o de itens da venda'
  OnClose = FormClose
  OnCreate = FormCreate
  ExplicitWidth = 640
  ExplicitHeight = 480
  PixelsPerInch = 96
  TextHeight = 13
  inherited GroupBox1: TGroupBox
    inherited edtPesquisa: TEdit
      OnChange = edtPesquisaChange
    end
  end
  inherited GroupBox2: TGroupBox
    inherited DBGrid1: TDBGrid
      DataSource = DM.DSTabItemVenda
      OnDrawColumnCell = DBGrid1DrawColumnCell
      OnTitleClick = DBGrid1TitleClick
      Columns = <
        item
          Expanded = False
          FieldName = 'iteVenNumItem'
          Width = 26
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'iteVenProId'
          Title.Caption = 'C'#243'digo'
          Width = 98
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Produtos'
          Width = 232
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'iteVenQuantidade'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'iteVenDesItem'
          Width = 49
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'iteVenValUnitario'
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Total'
          Visible = True
        end>
    end
  end
  inherited GroupBox3: TGroupBox
    inherited DBNvgInsDelEdit: TDBNavigator
      DataSource = DM.DSTabItemVenda
      Hints.Strings = ()
      OnClick = DBNvgInsDelEditClick
    end
    inherited DBNvgPosCancel: TDBNavigator
      DataSource = DM.DSTabItemVenda
      Hints.Strings = ()
    end
    inherited DBNvigFirPriNexLast: TDBNavigator
      DataSource = DM.DSTabItemVenda
      Hints.Strings = ()
    end
  end
  inherited GroupBox4: TGroupBox
    object Label3: TLabel
      Left = 3
      Top = -1
      Width = 30
      Height = 13
      Caption = 'Venda'
      FocusControl = DBEdit2
    end
    object Label4: TLabel
      Left = 185
      Top = -1
      Width = 38
      Height = 13
      Caption = 'Produto'
    end
    object Label5: TLabel
      Left = 3
      Top = 78
      Width = 63
      Height = 13
      Caption = 'Valor unit'#225'rio'
      FocusControl = DBEdtValUnitario
    end
    object Label6: TLabel
      Left = 3
      Top = 40
      Width = 45
      Height = 13
      Caption = 'Desconto'
      FocusControl = DBEdtDesconto
    end
    object Label7: TLabel
      Left = 3
      Top = 112
      Width = 56
      Height = 13
      Caption = 'Quantidade'
      FocusControl = DBEdtQuantidade
    end
    object Label8: TLabel
      Left = 50
      Top = -1
      Width = 33
      Height = 13
      Caption = 'C'#243'digo'
      FocusControl = DBEdtCodigo
    end
    object DBEdit2: TDBEdit
      Left = 3
      Top = 15
      Width = 46
      Height = 21
      CharCase = ecUpperCase
      DataField = 'iteVenVenId'
      DataSource = DM.DSTabItemVenda
      ReadOnly = True
      TabOrder = 0
    end
    object DBEdtValUnitario: TDBEdit
      Left = 3
      Top = 94
      Width = 63
      Height = 21
      CharCase = ecUpperCase
      DataField = 'iteVenValUnitario'
      DataSource = DM.DSTabItemVenda
      TabOrder = 4
      OnKeyPress = DBEdtValUnitarioKeyPress
    end
    object DBEdtDesconto: TDBEdit
      Left = 3
      Top = 56
      Width = 63
      Height = 21
      CharCase = ecUpperCase
      DataField = 'iteVenDesItem'
      DataSource = DM.DSTabItemVenda
      TabOrder = 3
      OnKeyPress = DBEdtDescontoKeyPress
    end
    object DBEdtQuantidade: TDBEdit
      Left = 3
      Top = 128
      Width = 63
      Height = 21
      CharCase = ecUpperCase
      DataField = 'iteVenQuantidade'
      DataSource = DM.DSTabItemVenda
      TabOrder = 5
      OnKeyPress = DBEdtQuantidadeKeyPress
    end
    object DBLkpCbBxProduto: TDBLookupComboBox
      Left = 185
      Top = 15
      Width = 274
      Height = 21
      DataField = 'Produtos'
      DataSource = DM.DSTabItemVenda
      ReadOnly = True
      TabOrder = 2
      OnExit = DBEdtCodigoExit
    end
    object DBEdtCodigo: TDBEdit
      Left = 50
      Top = 15
      Width = 134
      Height = 21
      DataField = 'iteVenProId'
      DataSource = DM.DSTabItemVenda
      TabOrder = 1
      OnExit = DBEdtCodigoExit
    end
  end
end
