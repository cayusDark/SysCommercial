unit UFuncoes;

interface
  Function VersaoExe: String;
  Procedure Copia(Origem, Destino: String);
  procedure ApenasNumero (var Key: Char);
  function VerificaData( Texto : String) : Boolean;
  function TransformaData( Texto : String) : string;
  function countItemVenda(venId : integer): integer;
  procedure Log (msg: string);
  procedure ErroGrave (msg: string);

implementation

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ShellAPI, StdCtrls, pngimage, ExtCtrls, Buttons,DB, ComCtrls,
  DBCtrls, Grids, DBGrids, Mask,ComObj, UDM, UMovVenda;


//
//gera��o de logs
//
procedure Log (msg: string);
var
  loLista: TStringList;
  pathExe: string;
begin
  try
    //cria uma lista de strings para armazenar o conteudo em log
    loLista := TStringList.Create;
    pathExe:=ExtractFilePath(Application.ExeName);
    try
      //se o log ja existe carrega ele
      if FileExists(pathExe+'\Log'+FormatDateTime('dd-mm-yyyy',now)+ '.log') then
        loLista.LoadFromFile(pathExe+'\Log'+FormatDateTime('dd-mm-yyyy',now)+ '.log');
      //adiciona a nova string ao log
      loLista.Add(TimeToStr(now)+': '+ msg);
    except
      on e:Exception do
        loLista.Add(TimeToStr(now)+': Erro '+ e.Message);
    end;
  finally
    //atualiza o log
    loLista.SaveToFile(pathExe+'\Log'+FormatDateTime('dd-mm-yyyy',now)+ '.log');
    //libera a lista
    loLista.Free;
  end;
end;

procedure ErroGrave (msg: string);
var
  loLista: TStringList;
  pathExe: string;
begin
  try
    //cria uma lista de strings para armazenar o conteudo em log
    loLista := TStringList.Create;
    pathExe:=ExtractFilePath(Application.ExeName);
    try
      //se o log ja existe carrega ele
      if FileExists(pathExe+'\ErroGrave'+FormatDateTime('dd-mm-yyyy',now)+ '.log') then
        loLista.LoadFromFile(pathExe+'\ErroGrave'+FormatDateTime('dd-mm-yyyy',now)+ '.log');
      //adiciona a nova string ao log
      loLista.Add(TimeToStr(now)+': '+ msg);
    except
      on e:Exception do
        loLista.Add(TimeToStr(now)+': Erro '+ e.Message);
    end;
  finally
    //atualiza o log
    loLista.SaveToFile(pathExe+'\ErroGrave'+FormatDateTime('dd-mm-yyyy',now)+ '.log');
    //libera a lista
    loLista.Free;
  end;
end;

//
//fun��o para obten��o da vers�o da aplica��o
//
Function VersaoExe: String;
type
  PFFI = ^vs_FixedFileInfo;
var
  F : PFFI;
  Handle : Dword;
  Len : Longint;
  Data : Pchar;
  Buffer : Pointer;
  Tamanho : Dword;
  Parquivo: Pchar;
  Arquivo : String;
begin
  Arquivo := Application.ExeName;
  Parquivo := StrAlloc(Length(Arquivo) + 1);
  StrPcopy(Parquivo, Arquivo);
  Len := GetFileVersionInfoSize(Parquivo, Handle);
  Result := '';
  if Len > 0 then
  begin
    Data:=StrAlloc(Len+1);
    if GetFileVersionInfo(Parquivo,Handle,Len,Data) then
    begin
      VerQueryValue(Data, '\',Buffer,Tamanho);
      F := PFFI(Buffer);
      Result := Format('%d.%d.%d',
      [HiWord(F^.dwFileVersionMs),
      LoWord(F^.dwFileVersionMs),
      HiWord(F^.dwFileVersionLs),
      Loword(F^.dwFileVersionLs)]
      );
    end;
    StrDispose(Data);
  end;
  StrDispose(Parquivo);
end;

//
// procedimento para copia da base de dados
//
Procedure Copia(Origem, Destino: String);
var
  FileOpInfo: TSHFileOpStruct;
begin
  With FileOpInfo Do
  Begin
     Wnd   := Application.Handle;
     wFunc := FO_COPY;
     pFrom := Pchar (Origem+#0+#0);
     pTo   := Pchar (Destino);
     fFlags := FOF_WANTMAPPINGHANDLE;
  end;
  SHFileOperation (FileOpInfo);
  ShFreeNameMappings (GlobalHandle(FileOpInfo.hNameMappings));
end;

procedure ApenasNumero (var Key: Char);
begin
    if not (Key in['0'..'9',Chr(8)]) then
    begin
      Key:= #0;
      MessageDlg('Campo para inser��o exclusiva de n�meros!!', mtInformation, mbOKCancel, 0);
    end;
end;

//verifica se uma data � valida
function VerificaData( Texto : String) : Boolean;
var
   Data, datValida : String;
begin
    Data := Copy(Texto,1,2) + '/' + Copy(Texto,3,2) + '/' + Copy(Texto,5,4);
    try
       StrToDate(Data);
       Result := true;

    except
       ShowMessage('Data Inv�lida !');
       Result := False;
    end;
end;

//pega a data da function verifica data e transforma para o padr�o DD/MM/AAAA
function TransformaData( Texto : String) : string;
var
   Data : String;
begin
    Data := Copy(Texto,1,2) + '/' + Copy(Texto,3,2) + '/' + Copy(Texto,5,4);
    Result := Data;
end;

//conta a quantidade de itens do pedido
function countItemVenda(venId : integer): integer;
var
  qtd : integer;
begin
  qtd := 0;
  with dm.queryCountItemVenda do
  begin
    close;
    Parameters.ParamByName('pVenId').Value := frmMovVenda.DBEdtVenId.Text;
    open;
  end;
  qtd := dm.queryCountItemVendaqtdItem.Value;
  //ShowMessage(IntToStr(qtd));
  Result := qtd;
end;
end.
