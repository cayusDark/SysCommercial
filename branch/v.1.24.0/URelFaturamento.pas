unit URelFaturamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ComCtrls,
  frxClass, frxExportPDF, frxDBSet,System.UITypes;

type
  TfrmRelFaturamento = class(TForm)
    GroupBox4: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    DtTmPckInicio: TDateTimePicker;
    DtTmPckFim: TDateTimePicker;
    GroupBox3: TGroupBox;
    ChkBxRepresentada: TCheckBox;
    CmbBxRepresentada: TComboBox;
    GroupBox6: TGroupBox;
    BitBtn1: TBitBtn;
    frxDBDSRelFaturamento: TfrxDBDataset;
    frxRptRelFaturamento: TfrxReport;
    frxPDFExport1: TfrxPDFExport;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure DtTmPckInicioExit(Sender: TObject);
    procedure DtTmPckFimExit(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure ChkBxRepresentadaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelFaturamento: TfrmRelFaturamento;

implementation

{$R *.dfm}

uses UDM;

procedure TfrmRelFaturamento.BitBtn1Click(Sender: TObject);
var
  datInicio, datFim : string;
begin
  try
    if ChkBxRepresentada.Checked = false then
    begin
      with dm.queryRelFaturamento do
      begin
        Close;
        SQL.Clear;
        SQL.Add('SELECT ');
        SQL.Add('   c.forRazao,');
        SQL.Add('   a.venCodPedFornecedor,');
        SQL.Add('   b.cliNome,');
        SQL.Add('   a.venValFatLiquido, ');
        SQL.Add('   a.venDatVenda,');
        SQL.Add('   a.venDatFaturamento,');
        SQL.Add('   d.funNome');
        SQL.Add('FROM ');
        SQL.Add('   vendas AS a,');
        SQL.Add('   clientes AS b,');
        SQL.Add('   fornecedores AS c,');
        SQL.Add('   funcionarios AS d');
        SQL.Add('WHERE a.venCliCodigo = b.cliCodigo');
        SQL.Add('AND c.forCodigo = a.venForCodigo');
        SQL.Add('AND d.funCodigo = a.venFunCodigo');
        SQL.Add('AND  a.venStatus = 1');
        SQL.Add('AND a.venTipo = 1');
        SQL.Add('AND a.venDatVenda BETWEEN (:pDataInicio) AND (:pDataFim)');
        SQL.Add('AND c.forRazao = :pForRazao');
        SQL.Add('ORDER BY  c.forRazao,a.venDatVenda, a.venDatFaturamento');

        //passagem dos valores das variaveis
        Parameters.ParamByName('pDataInicio').Value := DtTmPckInicio.DateTime;
        Parameters.ParamByName('pDataFim').Value := DtTmPckFim.DateTime;
        Parameters.ParamByName('pForRazao').Value := CmbBxRepresentada.Items[CmbBxRepresentada.ItemIndex];
        Open;
      end;

      datInicio := DateToStr(DtTmPckInicio.DateTime);
      frxRptRelFaturamento.Variables['datInicio'] := QuotedStr(datInicio);
      datFim := DateToStr(DtTmPckFim.DateTime);
      frxRptRelFaturamento.Variables['datFim'] := QuotedStr(datFim);
      frxRptRelFaturamento.ShowReport();
    end
    else
      if ChkBxRepresentada.Checked = true then
      begin
        with dm.queryRelFaturamento do
        begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT ');
          SQL.Add('   c.forRazao,');
          SQL.Add('   a.venCodPedFornecedor,');
          SQL.Add('   b.cliNome,');
          SQL.Add('   a.venValFatLiquido, ');
          SQL.Add('   a.venDatVenda,');
          SQL.Add('   a.venDatFaturamento,');
          SQL.Add('   d.funNome');
          SQL.Add('FROM ');
          SQL.Add('   vendas AS a,');
          SQL.Add('   clientes AS b,');
          SQL.Add('   fornecedores AS c,');
          SQL.Add('   funcionarios AS d');
          SQL.Add('WHERE a.venCliCodigo = b.cliCodigo');
          SQL.Add('AND c.forCodigo = a.venForCodigo');
          SQL.Add('AND d.funCodigo = a.venFunCodigo');
          SQL.Add('AND  a.venStatus = 1');
          SQL.Add('AND a.venTipo = 1');
          SQL.Add('AND a.venDatVenda BETWEEN (:pDataInicio) AND (:pDataFim)');
          SQL.Add('ORDER BY  c.forRazao,a.venDatVenda, a.venDatFaturamento');

          //passagem dos valores das variaveis
          Parameters.ParamByName('pDataInicio').Value := DtTmPckInicio.DateTime;
          //ShowMessage('DtTmPckInicio.DateTime = '+DateToStr( DtTmPckInicio.DateTime));
          Parameters.ParamByName('pDataInicio').Value := DtTmPckInicio.DateTime;
          Parameters.ParamByName('pDataFim').Value := DtTmPckFim.DateTime;
          //ShowMessage('DtTmPckFim.DateTime = '+DateToStr( DtTmPckFim.DateTime));
          Open;
        end;

          datInicio := DateToStr(DtTmPckInicio.DateTime);
          frxRptRelFaturamento.Variables['datInicio'] := QuotedStr(datInicio);
          datFim := DateToStr(DtTmPckFim.DateTime);
          frxRptRelFaturamento.Variables['datFim'] := QuotedStr(datFim);
          frxRptRelFaturamento.ShowReport();
      end;
  except
    on E: Exception do
    begin
      ShowMessage('Error: ' + E.Message +#13+'class name = '+E.ClassName);
    end;
  end;
end;

procedure TfrmRelFaturamento.ChkBxRepresentadaClick(Sender: TObject);
begin
  //ativa e desativa o combobox quando o checkbox � marcado
  if ChkBxRepresentada.Checked = true then
    CmbBxRepresentada.Enabled := false
  else
    CmbBxRepresentada.Enabled := true;
end;

procedure TfrmRelFaturamento.DtTmPckFimExit(Sender: TObject);
begin
  if DtTmPckFim.DateTime < DtTmPckInicio.DateTime then
  begin
    MessageDlg('Data final deve ser maior ou igual � data inicial', mtWarning,[mbOK], 0);
    DtTmPckFim.SetFocus;
  end;
end;

procedure TfrmRelFaturamento.DtTmPckInicioExit(Sender: TObject);
begin
  if DtTmPckInicio.DateTime > DtTmPckFim.DateTime then
  begin
    MessageDlg('Data inicial deve ser menor ou igual � data final', mtWarning,[mbOK], 0);
    DtTmPckInicio.SetFocus;
  end;
end;

procedure TfrmRelFaturamento.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  //fecha a conex�o com a tabela
  dm.tabFornecedores.Close;
end;

procedure TfrmRelFaturamento.FormShow(Sender: TObject);
var
  rCampo : string;
begin
  //abre a conexao com a tabela
  dm.tabFornecedores.Open;

  ChkBxRepresentada.Checked := true;

  //filtro de fornecedores
  if not DM.tabFornecedores.Eof then
  begin
    dm.tabFornecedores.First;
    while not DM.tabFornecedores.Eof do
    begin
      if dm.tabFornecedoresforStatus.AsInteger = 1 then
      begin
        rCampo:= dm.tabFornecedoresforRazao.AsString;
        CmbBxRepresentada.Items.Add(rCampo);
      end;
      DM.tabFornecedores.Next;
    end;
    CmbBxRepresentada.ItemIndex:=0;
  end;

  DtTmPckInicio.Date := now;
  DtTmPckFim.Date := now;
end;

end.
