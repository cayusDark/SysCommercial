unit UGraVendas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, VclTee.TeeGDIPlus, VCLTee.TeEngine,
  VCLTee.Series, Vcl.ExtCtrls, VCLTee.TeeProcs, VCLTee.Chart, VCLTee.DBChart,
  Vcl.StdCtrls, Vcl.Buttons, VCLTee.TeeEdiGene;


type
  TfrmGraVenRepresentada = class(TForm)
    GroupBox3: TGroupBox;
    CmbBxRepresentada: TComboBox;
    GroupBox4: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    CmbBxMesReferencia: TComboBox;
    CmbBxAnoReferencia: TComboBox;
    btnImprimir: TBitBtn;
    GroupBox1: TGroupBox;
    DBChart1: TDBChart;
    Label1: TLabel;
    Series3: TBarSeries;
    Series1: TBarSeries;
    Series2: TBarSeries;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure CmbBxRepresentadaChange(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGraVenRepresentada: TfrmGraVenRepresentada;

implementation

{$R *.dfm}

uses UDM;

procedure TfrmGraVenRepresentada.btnImprimirClick(Sender: TObject);
begin
  ChartPreview(Owner,DBChart1);
end;

procedure TfrmGraVenRepresentada.CmbBxRepresentadaChange(Sender: TObject);
begin
    with dm.queryGraVendas do
    begin
      close;
      Parameters.ParamByName('pForRazao').Value := CmbBxRepresentada.Items[CmbBxRepresentada.ItemIndex];
      Parameters.ParamByName('pMes').Value := CmbBxMesReferencia.Items[CmbBxMesReferencia.ItemIndex];
      Parameters.ParamByName('pAno').Value := CmbBxAnoReferencia.Items[CmbBxAnoReferencia.ItemIndex];
      Open;
    end;
end;

procedure TfrmGraVenRepresentada.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //fecha a conex�o com as tabelas
  dm.tabFornecedores.Close;
end;

procedure TfrmGraVenRepresentada.FormShow(Sender: TObject);
var
  rcampo : string;
  mes: Integer;
begin
  try
    //abre a conex�o com a tabela
    dm.tabFornecedores.Open;

    //mostra o mes corrente no  CmbBxMesReferencia
    mes := StrToInt(FormatDateTime('MM', Now));
    CmbBxMesReferencia.ItemIndex := mes -1;

    //limpa o comboBox
    CmbBxAnoReferencia.Items.Clear;

    //inicializa��o do componente CBxAno
    rCampo:= FormatDateTime('yyyy',IncMonth(Now(),-24));
    CmbBxAnoReferencia.Items.Add(rCampo);
    rCampo:= FormatDateTime('yyyy',IncMonth(Now(),-12));
    CmbBxAnoReferencia.Items.Add(rCampo);
    rCampo:= FormatDateTime('yyyy',Now);
    CmbBxAnoReferencia.Items.Add(rCampo);
    CmbBxAnoReferencia.ItemIndex:=2;

    //filtro de fornecedores
    if not DM.tabFornecedores.Eof then
    begin
      dm.tabFornecedores.First;
      while not DM.tabFornecedores.Eof do
      begin
        if dm.tabFornecedoresforStatus.AsInteger = 1 then
        begin
          rCampo:= dm.tabFornecedoresforRazao.AsString;
          CmbBxRepresentada.Items.Add(rCampo);
        end;
        DM.tabFornecedores.Next;
      end;
      CmbBxRepresentada.ItemIndex:=0;
    end;

    //inicializa��o do gr�fico
    with dm.queryGraVendas do
    begin
      close;
      Parameters.ParamByName('pForRazao').Value := CmbBxRepresentada.Items[CmbBxRepresentada.ItemIndex];
      Parameters.ParamByName('pMes').Value := CmbBxMesReferencia.Items[CmbBxMesReferencia.ItemIndex];
      Parameters.ParamByName('pAno').Value := CmbBxAnoReferencia.Items[CmbBxAnoReferencia.ItemIndex];
      Open;
    end;
  except
    on E: exception do
      ShowMessage('Error: ' + E.Message +#13+'class name = '+E.ClassName);
  end;
end;

end.
