unit URelMetaSku;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, frxClass,
  frxExportPDF, frxDBSet;

type
  TfrmRelMetaSku = class(TForm)
    GroupBox2: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    CmbBxMes: TComboBox;
    CmbBxAno: TComboBox;
    GroupBox3: TGroupBox;
    BitBtn1: TBitBtn;
    GroupBox1: TGroupBox;
    CmbBxRepresentada: TComboBox;
    frxRptRelMetaProdutosItem: TfrxReport;
    frxPDFExport1: TfrxPDFExport;
    frxDBDtstMetaSku: TfrxDBDataset;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelMetaSku: TfrmRelMetaSku;

implementation

{$R *.dfm}

uses UDM;

procedure TfrmRelMetaSku.BitBtn1Click(Sender: TObject);
begin
  {filtro de dados do relatorio de meta por SKU}
  with dm.queryRelMetaSKU do
  begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT');
    SQL.Add('   b.forRazao,');
    SQL.Add('   a.metProCodigo,');
    SQL.Add('   a.metProDatInicio,');
    SQL.Add('   a.metProDatFim,');
    SQL.Add('   a.metProMesReferencia,');
    SQL.Add('   a.metProAnoReferencia,');
    SQL.Add('   d.proCodFornecedor,');
    SQL.Add('   d.proNome,');
    SQL.Add('   c.metProIteMeta,');
    SQL.Add('   (SELECT SUM(f.iteVenQuantidade)');
    SQL.Add('    FROM vendas AS e, itemVenda AS f');
    SQL.Add('    WHERE e.venId = f.iteVenVenId');
    SQL.Add('    AND e.venDatVenda BETWEEN (a.metProDatInicio) AND (a.metProDatFim)');
    SQL.Add('    AND e.venForCodigo = b.forCodigo');
    SQL.Add('    AND f.iteVenProCodFornecedor = c.metProIteproCodFornecedor) AS metAlcancada,');
    SQL.Add('    (SELECT SUM(f.iteVenQtdFaturada)');
    SQL.Add('    FROM vendas AS e, itemVenda AS f');
    SQL.Add('    WHERE e.venId = f.iteVenVenId');
    SQL.Add('    AND e.venDatVenda BETWEEN (a.metProDatInicio) AND (a.metProDatFim)');
    SQL.Add('    AND e.venForCodigo = b.forCodigo');
    SQL.Add('    AND f.iteVenProCodFornecedor = c.metProIteproCodFornecedor) AS metFaturada,');
    SQL.Add('   (metAlcancada / c.metProIteMeta * 100) AS metPercentual,');
    SQL.Add('   (metFaturada / c.metProIteMeta * 100) AS metPerFaturada');
    SQL.Add('FROM');
    SQL.Add('   metaProdutos AS a,');
    SQL.Add('   fornecedores AS b,');
    SQL.Add('   metaProdutosItem AS c,');
    SQL.Add('   produtos AS d');
    SQL.Add('WHERE a.metProForCodigo = b.forCodigo');
    SQL.Add('AND c.metProIteMetProCodigo = a.metProCodigo');
    SQL.Add('AND d.proCodFornecedor = c.metProIteproCodFornecedor');
    SQL.Add('AND a.metProMesReferencia = :pMes');
    SQL.Add('AND a.metProAnoReferencia = :pAno');
    SQL.Add('AND b.forRazao = :pRazao');
    SQL.Add('ORDER BY b.forRazao, a.metProDatInicio;');

    //passagem dos paramentros
    Parameters.ParamByName('pMes').Value := CmbBxMes.Items[CmbBxMes.ItemIndex];
    Parameters.ParamByName('pAno').Value := CmbBxAno.Items[CmbBxAno.ItemIndex];
    Parameters.ParamByName('pRazao').Value := CmbBxRepresentada.Items[CmbBxRepresentada.ItemIndex];

    Open;
  end;
  if dm.queryRelMetaSKU.IsEmpty then
    MessageDlg('N�o h� dados!', mtConfirmation,[mbOK], 0)
  else
    frxRptRelMetaProdutosItem.ShowReport();
end;

procedure TfrmRelMetaSku.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //fecha a conexao con as tabelas
  dm.tabFornecedores.Close;
  dm.tabMetaProdutosItem.Close;
  dm.tabMetaProduto.Close;

  //desativa os filtros
  with dm.tabFornecedores do
    Filtered := false;
end;

procedure TfrmRelMetaSku.FormShow(Sender: TObject);
var
  rCampo : string;
  mes: integer;
begin
  //abre a conexao com as tabelas
  dm.tabFornecedores.Open;
  dm.tabMetaProdutosItem.Open;
  dm.tabMetaProduto.Open;


  //filtro de fornecedores
  if not DM.tabFornecedores.Eof then
  begin
    dm.tabFornecedores.First;
    while not DM.tabFornecedores.Eof do
    begin
      if dm.tabFornecedoresforStatus.AsInteger = 1 then
      begin
        rCampo:= dm.tabFornecedoresforRazao.AsString;
        CmbBxRepresentada.Items.Add(rCampo);
      end;
      DM.tabFornecedores.Next;
    end;
    CmbBxRepresentada.ItemIndex:=0;
  end;

  //inicializa��o do componente CBxAno
  rCampo:= FormatDateTime('yyyy',IncMonth(Now(),-24));
  CmbBxAno.Items.Add(rCampo);
  rCampo:= FormatDateTime('yyyy',IncMonth(Now(),-12));
  CmbBxAno.Items.Add(rCampo);
  rCampo:= FormatDateTime('yyyy',Now);
  CmbBxAno.Items.Add(rCampo);
  CmbBxAno.ItemIndex:=2;

  //posicionamento do cmbBxMesReferencia
  mes := StrToInt(FormatDateTime('MM', Now));
  CmbBxMes.ItemIndex := mes -1;
end;

end.
