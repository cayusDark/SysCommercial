unit UFunction;

interface

Uses
  ComObj;

type
  //importacao do fornecedor Cless
  function XlsToCless (AXLSFile: string): Boolean;

  private

  public
    xls : string;
  end;

implementation

uses UDM;

function XlsToCless (AXLSFile: string): Boolean;
var
  excel, planilha: OleVariant;
  linha, coluna, tipVenda, frete, przEntrega, quantidade: Integer;
  datVenda: datetime;
  acrIPI, desPISCOFINS, desICMS, desComercial: double;
  numPedido, cliente, fornecedor, vendedor, conPagamento : string;
  valPedido, valPedLiquido, valBonBruto, valBonLiquido : currency
begin
  //importa��o de pedido cless

  try
  //cursor tempo comunicar ao usu�rio para que ele aguarde enquanto o formul�rio est� sendo carregado
  //mudando o cursor para ampulheta
    Screen.Cursor:=crHourglass;

    //Crio o objeto que gerencia o arquivo excel
    excel:= CreateOleObject('Excel.Application');
  begin
    //Abrindo o arquimvo
    excel.WorkBooks.open(AXLSFile);

    //Pego a primeira planilha do arquivo
    planilha:= excel.WorkSheets[1];

    //aqui pego o texto da celula data pedido
    linha :=  5;
    coluna := 13;

    datVenda := sheet.cells[linha, coluna].Text;

    //aqui pego o texto da celula numero do pedido cless
    linha := 2;
    coluna := 14;

    numPedido := sheet.cells[linha, coluna].Text;


    //aqui pego o texto da celula tipo pedido
    linha := 2;
    coluna := 14;

    tipVenda := sheet.cells[linha, coluna].Text;

    //Aqui pego o texto da c�lula cliente
    linha:= 7;
    coluna:= 2;

    cliente := sheet.cells[linha, coluna].Text;


    //aqui pego o texto da celula condicao de pagamento
    linha := 1;
    coluna := 14;

    conPagamento := sheet.cells[linha, coluna].Text;

    //aqui pego o texto da celula  vendedor
    linha := 3;
    coluna := 14;

    vendedor := sheet.cells[linha, coluna].Text;

    //aqui pego o texto da celular ICMS
    linha := 5;
    coluna := 13;

    desICMS := sheet.cells[linha, coluna].Text;

    //aqui pego o texto da celular investimento
    linha := 7;
    coluna := 13;

    desComercial := sheet.cells[linha, coluna].text;

    //aqui pego o texto da celula   Total Liquido
    linha := 6;
    coluna := 17;

    valLiquido := sheet.cells[linha, coluna].text;

    //aqui pego o texto da celula subtotal
    linha :=  2;
    coluna := 17;

    valPedido := sheet.cells[linha, coluna].text;

    //inseri os dados do pedido
    with dm.queryVenda do
    begin
      close;
      SQL.clear;
      SQL.ADD ('INSERT INTO vendas (venCliCodigo, venDatVenda, venValProdutos,');
      SQL.ADD ('venQuantidade, venCodPedFornecedor, venStatus, venTipo, venFunCodigo,');
      SQL.ADD ('venPrzEntrega, venFrete, venPrzPagCodigo, venDesComercial, venDesICMS,');
      SQL.ADD ('venDesPISCOFINS, venValLiquido, venValFatBruto, venAcrIPI,');
      SQL.ADD ('venValFatLiquido, venValBonBruto, venValBonLiquido, venForCodigo)');
      SQL.ADD ('VALUES '+ venDatVenda, );
    end;

    //Fecho a planilha
    planilha.WorkBooks.Close;
  end;
  except
    Application.MessageBox('Erro desconhecido durante convers�o. '+
    'Por favor, verifique seu arquivo MS�Excel.','Error',MB_OK + MB_ICONEXCLAMATION);
  end;


end.
