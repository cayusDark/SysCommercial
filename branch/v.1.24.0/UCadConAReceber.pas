unit UCadConAReceber;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Mask, Vcl.DBCtrls,
  Vcl.Buttons, Vcl.ComCtrls, Vcl.DBLookup, DB;

type
  TfrmCadConAReceber = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    BtnPost: TBitBtn;
    BtnCancel: TBitBtn;
    DatTimPckDatVencimento: TDateTimePicker;
    CmbBxReceberDe: TComboBox;
    CmbBxTipo: TComboBox;
    edtDescricao: TEdit;
    edtValor: TEdit;
    CmbBxSubTIpo: TComboBox;
    Label6: TLabel;
    procedure FormShow(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnPostClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmbBxTipoExit(Sender: TObject);
    procedure edtValorKeyPress(Sender: TObject; var Key: Char);
    procedure CmbBxSubTIpoExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCadConAReceber: TfrmCadConAReceber;
  recCodigo : integer;

implementation

{$R *.dfm}

uses UDM, UFuncoes, UMain, UFinConAReceber;

procedure TfrmCadConAReceber.BtnCancelClick(Sender: TObject);
begin
  try
    if statusContas = 1 then
      //log de cancelamento
      Log('A��o de inser��o da conta � receber  '+ EdtDescricao.Text+' cancelado pelo usu�rio '+ frmMain.usuario+'.')
    else
      //log de cancelamento
      Log('A��o de edi��o da conta � receber  '+ EdtDescricao.Text+' cancelado pelo usu�rio '+ frmMain.usuario+'.');

    //fecha a conex�o com as tabelas
    dm.tabFornecedores.Close;
    dm.tabFuncionarios.Close;
    dm.tabContatos.close;
    dm.tabTipLancamentos.Close;

    //fecha a tela
    frmCadConAReceber.Close;
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro: '+ frmCadConAReceber.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro: '+ frmCadConAReceber.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmCadConAReceber.BtnPostClick(Sender: TObject);
begin
  try
    //abre conex�o com a base de dados
    if edtDescricao.Text = '' then
      MessageDlg('Campo descri��o obrigatorio!!', mtWarning,[mbOK], 0)
    else
      if CmbBxReceberDe.Text = '' then
        MessageDlg('Campo receber de obrigatorio!!', mtWarning,[mbOK], 0)
      else
        if CmbBxTipo.Text = '' then
          MessageDlg('Campo categoria obrigatorio!!', mtWarning,[mbOK], 0)
        else
          if edtValor.Text = '' then
            MessageDlg('Campo valor obrigatorio!!', mtWarning,[mbOK], 0)
          else
            begin
              dm.conexao.BeginTrans;

              if statusContas=0 then
              begin
                with dm.queryConAReceberInsert do
                begin
                  SQL.Clear;
                  Close;
                  SQL.Add('UPDATE Receber SET recDatVencimento = FORMAT(#'+DateToStr(DatTimPckDatVencimento.Date)+'#,"mm/dd/yyyy"),');
                  SQL.Add('           recHistorico = '+QuotedStr(edtDescricao.Text)+',');
                  SQL.Add('           recFornecedor = '+QuotedStr(CmbBxReceberDe.Text)+ ',');
                  SQL.Add('           recTipLanCodigo = '+IntToStr(dm.querySelTipLancamentotipLanCodigo.AsInteger)+',');

                  if CmbBxSubTIpo.Text <>'' then
                    SQL.Add('           recSubTipCodigo = '+IntToStr(dm.querySelSubTiposubTipCodigo.AsInteger)+',')
                  else
                    SQL.Add('             recSubTipCodigo = 0,');

                  SQL.Add('           recValor = '+QuotedStr(edtValor.Text)+',');
                  SQL.Add('           recObservacao = '+QuotedStr(' ') );
                  SQL.Add('WHERE recCodigo = '+ IntToStr(recCodigo) +';');
                  ExecSQL;
                end;
                dm.conexao.CommitTrans;
                //log de confirma��o
                Log('A��o de edi�ao da conta � pagar '+ EdtDescricao.Text+' confirmado pelo usu�rio '+ frmMain.usuario+'.');

              end
              else
                begin
                  with dm.queryConAReceberInsert do
                  begin
                    SQL.Clear;
                    Close;
                    SQL.Add('INSERT INTO Receber ( recDatVencimento,');
                    SQL.Add(              'recHistorico,');
                    SQL.Add(              'recFornecedor,');
                    SQL.Add(              'recTipLanCodigo,');

                    if CmbBxSubTIpo.Text <> '' then
                      SQL.Add(              'recSubTipCodigo,');
                    SQL.Add(              'recValor,');
                    SQL.Add(              'recObservacao)');
                    SQL.Add('VALUES ( FORMAT(#'+DateToStr(DatTimPckDatVencimento.Date)+'#,"mm/dd/yyyy"),');
                    SQL.Add(  QuotedStr(edtDescricao.Text)+',');
                    SQL.Add(  QuotedStr(CmbBxReceberDe.Text)+ ',');
                    SQL.Add(  IntToStr(dm.querySelTipLancamentotipLanCodigo.AsInteger)+',');
                    if CmbBxSubTIpo.Text <> '' then
                      SQL.Add(  IntToStr(dm.querySelSubTiposubTipCodigo.AsInteger)+',');
                    SQL.Add(  QuotedStr(edtValor.Text)+',');
                    SQL.Add(  QuotedStr(' ')+')');
                    ExecSQL;
                  end;
                  dm.conexao.CommitTrans;
                  //log de confirma��o
                  Log('A��o de inser��o da conta � pagar '+ EdtDescricao.Text+' confirmado pelo usu�rio '+ frmMain.usuario+'.');
                end;

              frmCadConAReceber.Close;

              dm.tabReceber.Close;
              dm.tabReceber.Open;
            end;
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro:'+ frmCadConAReceber.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro:'+ frmCadConAReceber.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
      dm.conexao.RollbackTrans;
    end;
  end;

end;

procedure TfrmCadConAReceber.CmbBxSubTIpoExit(Sender: TObject);
begin
  try
    //filtra categoria
    with DM.querySelSubTipo do
    begin
      Close;
      SQL.Clear;
      SQL.Text := 'SELECT subTipCodigo,subTipDescricao FROM subTipos WHERE subTipDescricao LIKE  '+ QuotedStr(CmbBxSubTIpo.Items[CmbBxSubTIpo.ItemIndex]);
      Open;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('Subtipo de lan�amento '+ CmbBxSubTIpo.Text +' n�o localizada!',mtWarning,[mbOK], 0);
      ErroGrave('Subtipo de lan�amento  '+CmbBxSubTIpo.Text +' n�o localizada!');
      CmbBxSubTIpo.SetFocus;
    end;
  end;
end;

procedure TfrmCadConAReceber.CmbBxTipoExit(Sender: TObject);
var
  rCampo : string;
begin
  try
    if statusContas = 0 then
    begin
      //filtra categoria
      with DM.querySeltipLancamento do
      begin
        Close;
        SQL.Clear;
        SQL.Text := 'select tipLanCodigo from TiposLancamento where tipLanDescricao LIKE '+ QuotedStr(CmbBxTipo.Text);
        Open;
      end;

      //filtra os subtipos
      with DM.querySelSubTipo do
      begin
        Close;
        SQL.Clear;
        SQL.Text := 'SELECT subTipDescricao, subTipCodigo FROM subTIpos WHERE subTipTipLanCodigo = '+ IntToStr(dm.querySelTipLancamentotipLanCodigo.AsInteger);
        Open;
      end;

      CmbBxSubTIpo.Clear;

      dm.querySelSubTipo.First;

      while not dm.querySelSubTipo.Eof do
      begin
        rCampo := dm.querySelSubTiposubTipDescricao.AsString;
        CmbBxSubTIpo.Items.Add(rCampo);
        dm.querySelSubTipo.Next;
        if dm.tabReceber.FieldByName('recSubTipo').asString = rCampo then
          CmbBxSubTIpo.Text := dm.tabReceber.FieldByName('recSubTipo').asString;
      end;
    end
    else
      begin
        //filtra categoria
        with DM.querySeltipLancamento do
        begin
          Close;
          SQL.Clear;
          SQL.Text := 'select tipLanCodigo from TiposLancamento where tipLanDescricao LIKE '+ QuotedStr(CmbBxTipo.Items[CmbBxTipo.ItemIndex]);
          Open;
        end;

        //filtra os subtipos
        with DM.querySelSubTipo do
        begin
          Close;
          SQL.Clear;
          SQL.Text := 'SELECT subTipDescricao, subTipCodigo FROM subTIpos WHERE subTipTipLanCodigo = '+ IntToStr(dm.querySelTipLancamentotipLanCodigo.AsInteger);
          Open;
        end;

        CmbBxSubTIpo.Clear;

        dm.querySelSubTipo.First;

        while not dm.querySelSubTipo.Eof do
        begin
          rCampo := dm.querySelSubTiposubTipDescricao.AsString;
          CmbBxSubTIpo.Items.Add(rCampo);
          dm.querySelSubTipo.Next;
        end;

        CmbBxSubTIpo.ItemIndex :=0;
      end;
  except
    on E: Exception do
    begin
      MessageDlg('Tipo de lan�amento '+ CmbBxTipo.Text +' n�o localizada!',mtWarning,[mbOK], 0);
      ErroGrave('Tipo de lan�amento  '+CmbBxTipo.Text +' n�o localizada!');
      CmbBxTipo.SetFocus;
    end;
  end;
end;

procedure TfrmCadConAReceber.edtValorKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',',',#8]) then
	  Key:= #0;
end;

procedure TfrmCadConAReceber.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  try
    //fecha a conex�o com as tabelas
    dm.tabFornecedores.Close;
    dm.tabFuncionarios.Close;
    dm.tabContatos.close;
    dm.tabTipLancamentos.Close;

    //fecha a tela
    frmCadConAReceber.Close;
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro: '+ frmCadConAReceber.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro: '+ frmCadConAReceber.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmCadConAReceber.FormShow(Sender: TObject);
var
  rCampo : string;
begin
  try
    //abre a conex�o com as tabelas
    dm.tabFornecedores.Open;
    dm.tabFuncionarios.Open;
    dm.tabContatos.Open;
    dm.tabTipLancamentos.Open;

    if statusContas = 0 then
    begin
      //log de edi�ao
      Log('Edi��o da conta a receber '+dm.tabReceber.FieldByName('recHistorico').asString+' pelo usu�rio '+ frmMain.usuario+'.');

      DatTimPckDatVencimento.Date := dm.tabReceber.FieldByName('recDatVencimento').AsDateTime;
      edtDescricao.Text := dm.tabReceber.FieldByName('recHistorico').asString;
      CmbBxReceberDe.Text := dm.tabReceber.FieldByName('recFornecedor').asString;
      CmbBxTipo.Text := dm.tabReceber.FieldByName('recTipo').asString;
      CmbBxSubTIpo.Text := dm.tabReceber.FieldByName('recSubTipo').asString;
      edtValor.Text := dm.tabReceber.FieldByName('recValor').asString;
      recCodigo := dm.tabReceber.FieldByName('recCodigo').AsInteger;
    end
    else
      begin
        //log de edi�ao
        Log('Inser��o de uma conta a receber pelo usu�rio '+ frmMain.usuario+'.');

        DatTimPckDatVencimento.SetFocus;

        //inicializa a data de vencimento
        DatTimPckDatVencimento.DateTime := date;

        //limpa o combobox
        CmbBxReceberDe.Clear;
      end;

    //filtro dos comboBox
    //filtro de fornecedores
    dm.tabFornecedores.First;
    while not DM.tabFornecedores.Eof do
    begin
      if dm.tabFornecedoresforStatus.AsInteger = 1 then
      begin
        rCampo:= dm.tabFornecedoresforRazao.AsString;
        CmbBxReceberDe.Items.Add(rCampo);
      end;
      DM.tabFornecedores.Next;
    end;

    //filtro de vendedores

    dm.tabFuncionarios.First;
    while not DM.tabFuncionarios.Eof do
    begin
      if dm.tabFuncionariosfunStatus.AsInteger = 1 then
      begin
        rCampo:= dm.tabFuncionariosfunNome.AsString;
        CmbBxReceberDe.Items.Add(rCampo);
      end;
      DM.tabFuncionarios.Next;
    end;

    //filtro de contatos
    dm.tabContatos.First;
    while not DM.tabContatos.Eof do
    begin
      rCampo:= dm.tabContatosConNome.AsString;
      CmbBxReceberDe.Items.Add(rCampo);
      DM.tabContatos.Next;
    end;


    //filtro de tipos de lan�amento
    dm.tabTipLancamentos.First;
    while not dm.tabTipLancamentos.Eof do
    begin
      rCampo := dm.tabTipLancamentostipLanDescricao.AsString;
      CmbBxTipo.Items.Add(rCampo);
      dm.tabTipLancamentos.Next;
    end;

  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro: '+ frmCadConAReceber.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro: '+ frmCadConAReceber.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;

end;

end.
