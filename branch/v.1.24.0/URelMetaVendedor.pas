unit URelMetaVendedor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, frxClass,
  frxDBSet, frxExportPDF;

type
  TfrmRelMetaVendedor = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    cmbBxVendedor: TComboBox;
    ChkBxVendedor: TCheckBox;
    CmbBxMes: TComboBox;
    CmbBxAno: TComboBox;
    Label8: TLabel;
    Label9: TLabel;
    BitBtn1: TBitBtn;
    frxRptRelMetaVendedor: TfrxReport;
    frxDBDSMetaVendedor: TfrxDBDataset;
    frxPDFExport1: TfrxPDFExport;
    procedure FormShow(Sender: TObject);
    procedure ChkBxVendedorClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelMetaVendedor: TfrmRelMetaVendedor;

implementation

{$R *.dfm}

uses UDM;

procedure TfrmRelMetaVendedor.BitBtn1Click(Sender: TObject);
begin
  if ChkBxVendedor.Checked = true then
  begin
    //execu��o do select sem filtro
    with dm.queryRelMetaVendedor do
    begin
      Close;
      SQL.Clear;
      SQL.Add('SELECT DISTINCT');
      SQL.Add('   b.funCodigo,');
      SQL.Add('   b.funNome,');
      SQL.Add('   d.metVenMesReferencia,');
      SQL.Add('   d.metVenAnoReferencia,');
      SQL.Add('   d.metVenDatInicio,');
      SQL.Add('   d.metVenDatFim,');
      SQL.Add('   k.forRazao,');
      SQL.Add('   e.metVenIteMeta,');
      SQL.Add('   (SELECT SUM (f.venValFatLiquido)');
      SQL.Add('    FROM');
      SQL.Add('       vendas AS f,');
      SQL.Add('       funcionarios AS g,');
      SQL.Add('       fornecedores AS h');
      SQL.Add('    WHERE');
      SQL.Add('       f.venFunCodigo =g.funCodigo');
      SQL.Add('       AND f.venForCodigo = h.forCodigo');
      SQL.Add('       AND g.funCodigo =  b.funCodigo');
      SQL.Add('       AND h.forCodigo =  k.forCodigo');
      SQL.Add('       AND f.venDatVenda BETWEEN (d.metVenDatInicio) AND (  d.metVenDatFim)) AS valLiquido,');
      SQL.Add('   ((valLiquido/e.metVenIteMeta)*100) AS perMetaVendedor,');
      SQL.Add('   (SELECT SUM(i.metValor)');
      SQL.Add('    FROM metas AS i');
      SQL.Add('    WHERE  i.metMesReferencia = d.metVenMesReferencia');
      SQL.Add('    AND i.metAnoReferencia =d.metVenAnoReferencia) AS metGlobal,');
      SQL.Add('   ((valLiquido/metGlobal)*100) AS perMetGlobal');
      SQL.Add('FROM');
      SQL.Add('   vendas AS a,');
      SQL.Add('   funcionarios AS b,');
      SQL.Add('   metas AS c,');
      SQL.Add('   metaVendedor AS d,');
      SQL.Add('   metaVendedorItem AS e,');
      SQL.Add('   fornecedores AS k');
      SQL.Add('WHERE a.venFunCodigo=b.funCodigo');
      SQL.Add('AND a.venMetCodigo=c.metCodigo');
      SQL.Add('AND b.funCodigo=d.metVenFunCodigo');
      SQL.Add('AND d.metVenCodigo=e.metVenIteMetVenCodigo');
      SQL.Add('AND e.metVenIteForCodigo=k.forCodigo');
      SQL.Add('AND d.metVenMesReferencia=:pMes');
      SQL.Add('AND d.metVenAnoReferencia=:pAno');
      SQL.Add('ORDER BY b.funNome;');

      //passagem dos parametros
      Parameters.ParamByName('pMes').Value := CmbBxMes.Items[CmbBxMes.ItemIndex];
      Parameters.ParamByName('pAno').Value := CmbBxAno.Items[CmbBxAno.ItemIndex];
      Open;
    end;

    //chamada do relat�rio
    if dm.queryRelMetaVendedor.IsEmpty then
      MessageDlg('N�o h� dados!', mtConfirmation,[mbOK], 0)
    else
      begin
        frxRptRelMetaVendedor.Variables['periodo'] := QuotedStr(CmbBxMes.Items[CmbBxMes.ItemIndex]+'/'+ CmbBxAno.Items[CmbBxAno.ItemIndex]);
        frxRptRelMetaVendedor.ShowReport();
      end;
  end
  else if ChkBxVendedor.Checked = false then
    begin
      //execu��o do select sem filtro
      with dm.queryRelMetaVendedor do
      begin
        Close;
        SQL.Clear;
        SQL.Add('SELECT DISTINCT');
        SQL.Add('   b.funCodigo,');
        SQL.Add('   b.funNome,');
        SQL.Add('   d.metVenMesReferencia,');
        SQL.Add('   d.metVenAnoReferencia,');
        SQL.Add('   d.metVenDatInicio,');
        SQL.Add('   d.metVenDatFim,');
        SQL.Add('   k.forRazao,');
        SQL.Add('   e.metVenIteMeta,');
        SQL.Add('   (SELECT SUM (f.venValFatLiquido)');
        SQL.Add('    FROM');
        SQL.Add('       vendas AS f,');
        SQL.Add('       funcionarios AS g,');
        SQL.Add('       fornecedores AS h');
        SQL.Add('    WHERE');
        SQL.Add('       f.venFunCodigo =g.funCodigo');
        SQL.Add('       AND f.venForCodigo = h.forCodigo');
        SQL.Add('       AND g.funCodigo =  b.funCodigo');
        SQL.Add('       AND h.forCodigo =  k.forCodigo');
        SQL.Add('       AND f.venDatVenda BETWEEN (d.metVenDatInicio) AND (  d.metVenDatFim)) AS valLiquido,');
        SQL.Add('   ((valLiquido/e.metVenIteMeta)*100) AS perMetaVendedor,');
        SQL.Add('   (SELECT SUM(i.metValor)');
        SQL.Add('    FROM metas AS i');
        SQL.Add('    WHERE  i.metMesReferencia = d.metVenMesReferencia');
        SQL.Add('    AND i.metAnoReferencia =d.metVenAnoReferencia) AS metGlobal,');
        SQL.Add('   ((valLiquido/metGlobal)*100) AS perMetGlobal');
        SQL.Add('FROM');
        SQL.Add('   vendas AS a,');
        SQL.Add('   funcionarios AS b,');
        SQL.Add('   metas AS c,');
        SQL.Add('   metaVendedor AS d,');
        SQL.Add('   metaVendedorItem AS e,');
        SQL.Add('   fornecedores AS k');
        SQL.Add('WHERE a.venFunCodigo=b.funCodigo');
        SQL.Add('AND a.venMetCodigo=c.metCodigo');
        SQL.Add('AND b.funCodigo=d.metVenFunCodigo');
        SQL.Add('AND d.metVenCodigo=e.metVenIteMetVenCodigo');
        SQL.Add('AND e.metVenIteForCodigo=k.forCodigo');
        SQL.Add('AND b.funNome = :pfunNome');
        SQL.Add('AND d.metVenMesReferencia=:pMes');
        SQL.Add('AND d.metVenAnoReferencia=:pAno');
        SQL.Add('ORDER BY b.funNome;');

        //passagem dos parametros
        Parameters.ParamByName('pFunNome').Value := cmbBxVendedor.Items[cmbBxVendedor.ItemIndex];
        Parameters.ParamByName('pMes').Value := CmbBxMes.Items[CmbBxMes.ItemIndex];
        Parameters.ParamByName('pAno').Value := CmbBxAno.Items[CmbBxAno.ItemIndex];
        Open;
      end;
      if dm.queryRelMetaVendedor.IsEmpty then
        MessageDlg('N�o h� dados!', mtConfirmation,[mbOK], 0)
      else
        begin
          //chamada do relat�rio
          frxRptRelMetaVendedor.Variables['periodo'] := QuotedStr(CmbBxMes.Items[CmbBxMes.ItemIndex]+'/'+ CmbBxAno.Items[CmbBxAno.ItemIndex]);
          frxRptRelMetaVendedor.ShowReport();
        end;
    end;

end;

procedure TfrmRelMetaVendedor.ChkBxVendedorClick(Sender: TObject);
begin
  //ativa e desativa o combobox quando o checkbox � marcado
  if ChkBxVendedor.Checked = true then
    cmbBxVendedor.Enabled := false
  else
    cmbBxVendedor.Enabled := true;
end;

procedure TfrmRelMetaVendedor.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  //fecha a conexao com as tabelas
  dm.tabFuncionarios.Close;
end;

procedure TfrmRelMetaVendedor.FormShow(Sender: TObject);
var
  rCampo : string;
  mes : integer;
begin
  //abre a conexao com as tabelas
  dm.tabFuncionarios.Open;

   mes := StrToInt(FormatDateTime('MM', Now));
   CmbBxMes.ItemIndex := mes -1;

  //inicializa��o dos campos
  ChkBxVendedor.Checked := true;

  //filtro de vendedores
  if not DM.tabFuncionarios.Eof then
  begin
    dm.tabFuncionarios.First;
    while not DM.tabFuncionarios.Eof do
    begin
      if dm.tabFuncionariosfunFcaCodigo.AsInteger = 1 then
      begin
        rCampo:= dm.tabFuncionariosfunNome.AsString;
        cmbBxVendedor.Items.Add(rCampo);
      end;
      DM.tabFuncionarios.Next;
    end;
    cmbBxVendedor.ItemIndex:=0;
  end;

  //inicializa��o do componente CBxAno
  rCampo:= FormatDateTime('yyyy',IncMonth(Now(),-24));
  CmbBxAno.Items.Add(rCampo);
  rCampo:= FormatDateTime('yyyy',IncMonth(Now(),-12));
  CmbBxAno.Items.Add(rCampo);
  rCampo:= FormatDateTime('yyyy',Now);
  CmbBxAno.Items.Add(rCampo);
  CmbBxAno.ItemIndex:=2;
end;

end.
