unit UFuncoes;

interface
  Function VersaoExe: String;
  Procedure Copia(Origem, Destino: String);
  procedure ApenasNumero (var Key: Char);
  function VerificaData( Texto : String) : Boolean;
  function TransformaData( Texto : String) : string;
  function countItemVenda(venId : integer): integer;
  procedure Log (msg: string);
  procedure ErroGrave (msg: string);
  function atualizaValorConta(cotDescricao : string):double;
  function verificaCotCodigo (cotDescricao : string) : integer;
  procedure InsereContaAReceberVendaProcessada (przPagId,venId : integer);
  //calcula em que dia cai a pascoa
  function BuscaPascoa(ano: Word): TDate;
  //calcular se determinado dia � feriado
  function Feriado(data: TDate): Boolean;
  //rotina que determina se determinado dia � �til ou n�o
  function DiaUtil(dia: TDate; sabado: Boolean): Boolean;
  //calcula o 5� dia �til
  function QuintoDiaUtil(mes, ano: Integer): TDate;
  {Retorna o pr�ximo dia �til caso a data informada caia em um fim de semana}
  Function ProximoDiaUtil (dData : TDateTime) : TDateTime;
implementation

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ShellAPI, StdCtrls, pngimage, ExtCtrls, Buttons,DB, ComCtrls,
  DBCtrls, Grids, DBGrids, Mask,ComObj, UDM, UMovVenda, UFinMovimentacoes,math,DateUtils;


//
//gera��o de logs
//
procedure Log (msg: string);
var
  loLista: TStringList;
  pathExe: string;
begin
  try
    //cria uma lista de strings para armazenar o conteudo em log
    loLista := TStringList.Create;
    pathExe:=ExtractFilePath(Application.ExeName);
    try
      //se o log ja existe carrega ele
      if FileExists(pathExe+'\Log'+FormatDateTime('dd-mm-yyyy',now)+ '.log') then
        loLista.LoadFromFile(pathExe+'\Log'+FormatDateTime('dd-mm-yyyy',now)+ '.log');
      //adiciona a nova string ao log
      loLista.Add(TimeToStr(now)+': '+ msg);
    except
      on e:Exception do
        loLista.Add(TimeToStr(now)+': Erro '+ e.Message);
    end;
  finally
    //atualiza o log
    loLista.SaveToFile(pathExe+'\Log'+FormatDateTime('dd-mm-yyyy',now)+ '.log');
    //libera a lista
    loLista.Free;
  end;
end;

procedure ErroGrave (msg: string);
var
  loLista: TStringList;
  pathExe: string;
begin
  try
    //cria uma lista de strings para armazenar o conteudo em log
    loLista := TStringList.Create;
    pathExe:=ExtractFilePath(Application.ExeName);
    try
      //se o log ja existe carrega ele
      if FileExists(pathExe+'\ErroGrave'+FormatDateTime('dd-mm-yyyy',now)+ '.log') then
        loLista.LoadFromFile(pathExe+'\ErroGrave'+FormatDateTime('dd-mm-yyyy',now)+ '.log');
      //adiciona a nova string ao log
      loLista.Add(TimeToStr(now)+': '+ msg);
    except
      on e:Exception do
        loLista.Add(TimeToStr(now)+': Erro '+ e.Message);
    end;
  finally
    //atualiza o log
    loLista.SaveToFile(pathExe+'\ErroGrave'+FormatDateTime('dd-mm-yyyy',now)+ '.log');
    //libera a lista
    loLista.Free;
  end;
end;

//
//fun��o para obten��o da vers�o da aplica��o
//
Function VersaoExe: String;
type
  PFFI = ^vs_FixedFileInfo;
var
  F : PFFI;
  Handle : Dword;
  Len : Longint;
  Data : Pchar;
  Buffer : Pointer;
  Tamanho : Dword;
  Parquivo: Pchar;
  Arquivo : String;
begin
  Arquivo := Application.ExeName;
  Parquivo := StrAlloc(Length(Arquivo) + 1);
  StrPcopy(Parquivo, Arquivo);
  Len := GetFileVersionInfoSize(Parquivo, Handle);
  Result := '';
  if Len > 0 then
  begin
    Data:=StrAlloc(Len+1);
    if GetFileVersionInfo(Parquivo,Handle,Len,Data) then
    begin
      VerQueryValue(Data, '\',Buffer,Tamanho);
      F := PFFI(Buffer);
      Result := Format('%d.%d.%d',
      [HiWord(F^.dwFileVersionMs),
      LoWord(F^.dwFileVersionMs),
      HiWord(F^.dwFileVersionLs),
      Loword(F^.dwFileVersionLs)]
      );
    end;
    StrDispose(Data);
  end;
  StrDispose(Parquivo);
end;

//
// procedimento para copia da base de dados
//
Procedure Copia(Origem, Destino: String);
var
  FileOpInfo: TSHFileOpStruct;
begin
  With FileOpInfo Do
  Begin
     Wnd   := Application.Handle;
     wFunc := FO_COPY;
     pFrom := Pchar (Origem+#0+#0);
     pTo   := Pchar (Destino);
     fFlags := FOF_WANTMAPPINGHANDLE;
  end;
  SHFileOperation (FileOpInfo);
  ShFreeNameMappings (GlobalHandle(FileOpInfo.hNameMappings));
end;

procedure ApenasNumero (var Key: Char);
begin
    if not (Key in['0'..'9',Chr(8)]) then
    begin
      Key:= #0;
      MessageDlg('Campo para inser��o exclusiva de n�meros!!', mtInformation, mbOKCancel, 0);
    end;
end;

//verifica se uma data � valida
function VerificaData( Texto : String) : Boolean;
var
   Data, datValida : String;
begin
    Data := Copy(Texto,1,2) + '/' + Copy(Texto,3,2) + '/' + Copy(Texto,5,4);
    try
       StrToDate(Data);
       Result := true;

    except
       ShowMessage('Data Inv�lida !');
       Result := False;
    end;
end;

//pega a data da function verifica data e transforma para o padr�o DD/MM/AAAA
function TransformaData( Texto : String) : string;
var
   Data : String;
begin
    Data := Copy(Texto,1,2) + '/' + Copy(Texto,3,2) + '/' + Copy(Texto,5,4);
    Result := Data;
end;

//conta a quantidade de itens do pedido
function countItemVenda(venId : integer): integer;
var
  qtd : integer;
begin
  qtd := 0;
  with dm.queryCountItemVenda do
  begin
    close;
    Parameters.ParamByName('pVenId').Value := frmMovVenda.DBEdtVenId.Text;
    open;
  end;
  qtd := dm.queryCountItemVendaqtdItem.Value;
  //ShowMessage(IntToStr(qtd));
  Result := qtd;
end;

//
//atualiza o valor total da conta com base na descri��o
//
function atualizaValorConta(cotDescricao : string): double;
var
  valor : currency;
begin
  try
    //soma o valor da conta em quest�o
    with dm.querySumMovimentacao do
    begin
      close;
      SQL.Clear;
      SQL.Add('SELECT c.cotCodigo, ((select IIF(iSNULL(sum(m1.movValor)),0,sum(m1.movValor) ) from movimentacao m1 where m1.movCotCodigo=c.cotCodigo and m1.movTipo=1)');
      SQL.Add('-(select IIF(iSNULL(sum(m2.movValor)),0,sum(m2.movValor) ) from movimentacao m2 where m2.movCotCodigo=c.cotCodigo and m2.movTipo=0)) as Valor');
      SQL.Add('FROM movimentacao AS m, contas AS c');
      SQL.Add('WHERE m.movCotCodigo = c.cotCodigo');
      SQL.Add('and c.cotDescricao =' + QuotedStr(cotDescricao));
      SQL.Add('group by c.cotCodigo;');
      //ShowMessage(SQL.Text);
      Open;
     end;


     cotCodigo := DM.querySumMovimentacaocotCodigo.AsInteger;
     valor :=  dm.querySumMovimentacaoValor.AsCurrency;
    Result := valor;
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro: '+#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro: '+#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;

end;

//
//verifica qual o c�digo da conta com base em sua descri��o
//
function verificaCotCodigo(cotDescricao : string):integer;
var
  codigo:integer;
begin
  try
    //soma o valor da conta em quest�o
    with dm.querySelCotCodigo do
    begin
      close;
      SQL.Clear;
      SQL.Add('SELECT c.cotCodigo');
      SQL.Add('FROM contas AS c');
      SQL.Add('WHERE c.cotDescricao =' + QuotedStr(cotDescricao));
      //ShowMessage(SQL.Text);
      Open;
     end;

    if dm.querySelCotCodigo.IsEmpty then
       raise Exception.Create('Conta n�o localizada no banco de dados!!!')
    else
      result := DM.querySelCotCodigocotCodigo.AsInteger;
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro: '+#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro: '+#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

{* faz a inser��o dos valores em contas � receber quando o pedido � processado
com base nna quantidade de parcelas e nos dias que esta cadastrada na mesma,
*}
procedure InsereContaAReceberVendaProcessada (przPagId,venId : integer);
var
  qtdParcelas,i :integer;
  campo :string;
  valorParcela : Double;
  przPagPar: array[0..4] of Integer;
begin
  try
    //abre transa�ao com o banco de dados
    dm.conexao.BeginTrans;
    //MessageDlg('Id prazo de pagamento:' + IntToStr( przPagId) ,mtWarning,[mbOK],0);
    //verifica a quantidade de de parcelas no banco
    with dm.queryVerificaPrazoPagamento do
    begin
      Close;
      SQL.Clear;
      SQL.Add('select przPagPar1,przPagPar2,przPagPar3,przPagPar4,przPagPar5 from prazoPagamentos WHERE przPagCodigo = '+IntToStr( przPagId));
      Open;
    end;

    qtdParcelas :=0;

    if dm.queryVerificaPrazoPagamento.IsEmpty then
      MessageDlg('Prazo de pagamento n�o localizado!',mtWarning,[mbOK],0)
    else
      begin
        if not dm.queryVerificaPrazoPagamentoprzPagPar1.IsNull then
        begin
          qtdParcelas :=  qtdParcelas+1;
          przPagPar[0] := dm.queryVerificaPrazoPagamentoprzPagPar1.AsInteger;
        end;
        if not dm.queryVerificaPrazoPagamentoprzPagPar2.IsNull then
        begin
          qtdParcelas :=  qtdParcelas+1;
          przPagPar[1] := dm.queryVerificaPrazoPagamentoprzPagPar2.AsInteger;
        end;
        if not dm.queryVerificaPrazoPagamentoprzPagPar3.IsNull then
        begin
          qtdParcelas :=  qtdParcelas+1;
          przPagPar[2] := dm.queryVerificaPrazoPagamentoprzPagPar3.AsInteger;
        end;
        if not dm.queryVerificaPrazoPagamentoprzPagPar4.IsNull then
        begin
          qtdParcelas :=  qtdParcelas+1;
          przPagPar[3] := dm.queryVerificaPrazoPagamentoprzPagPar4.AsInteger;
        end;
        if not dm.queryVerificaPrazoPagamentoprzPagPar5.IsNull then
        begin
          qtdParcelas :=  qtdParcelas+1;
          przPagPar[4] := dm.queryVerificaPrazoPagamentoprzPagPar5.AsInteger;
        end;
      end;

    //MessageDlg('Qtd de parcelas:' + IntToStr(qtdParcelas) +' Prazo de pagamento'+ IntToStr( przPagId) ,mtWarning,[mbOK],0);
    if qtdParcelas > 0 then
    begin
      with dm.queryVerificaVendaAReceber do
      begin
        Close;
        SQL.Clear;
        SQL.Add('select f.forRazao,f.forComissao, f.forLiquidacao, v.venDatFaturamento,v.venValFatLiquido,v.venCodPedFornecedor');
        SQL.Add('from vendas v, fornecedores f WHERE v.venId = '+IntToStr(venId));
        SQL.Add('and v.venForCodigo = f.forCodigo');
        Open;
      end;

      if dm.queryVerificaVendaAReceber.IsEmpty then
        MessageDlg('Venda n�o localizada!!',mtWarning,[mbOK],0)
      else
        begin
          if dm.queryVerificaVendaAReceberforLiquidacao.AsInteger=0 then
          begin
            //ShowMessage('percentual: '+FloatToStr(dm.queryVerificaVendaAReceberforComissao.AsFloat/100));
            //ShowMessage('valor:'+FloatToStr(dm.queryVerificaVendaARecebervenValFatLiquido.AsFloat) );
            //ShowMessage('parcela: '+FloatToStr(SimpleRoundTo((dm.queryVerificaVendaARecebervenValFatLiquido.AsFloat*(dm.queryVerificaVendaAReceberforComissao.AsFloat/100))/qtdParcelas,-2)));
            valorParcela := SimpleRoundTo(((dm.queryVerificaVendaARecebervenValFatLiquido.AsFloat*(dm.queryVerificaVendaAReceberforComissao.AsFloat/100))/qtdParcelas),-2);
            //MessageDlg('Valor da parcela da venda '+dm.queryVerificaVendaARecebervenCodPedFornecedor.AsString+': R$ '+ FloatToStr( valorParcela),mtWarning,[mbOK],0)

            for I := 0 to qtdParcelas-1 do
            begin
              with dm.queryConAReceberInsert do
              begin
                SQL.Clear;
                Close;
                SQL.Add('INSERT INTO Receber ( recDatVencimento,');
                SQL.Add(              'recHistorico,');
                SQL.Add(              'recFornecedor,');
                SQL.Add('              recTipLanCodigo,');
                SQL.Add(              'recValor,');
                SQL.Add(              'recObservacao)');
                SQL.Add('VALUES ( FORMAT(#'+DateToStr(dm.queryVerificaVendaARecebervenDatFaturamento.AsDateTime+przPagPar[i] )+'#,"mm/dd/yyyy"),');
                SQL.Add(  QuotedStr('COMISS�O VENDA '+ DM.queryVerificaVendaARecebervenCodPedFornecedor.AsString+' '+IntToStr(i+1)+'/'+IntToStr(qtdParcelas))+',');
                SQL.Add(  QuotedStr(dm.queryVerificaVendaAReceberforRazao.asString)+ ',');
                SQL.Add('1,');
                SQL.Add(  QuotedStr(FloatToStr(valorParcela))+',');
                SQL.Add(  QuotedStr(' ')+')');
                ExecSQL;
              end;
            end;
          end
          else
            begin
              //calcula o valor da comiss�o
              //lorParcela := SimpleRoundTo((dm.queryVerificaVendaARecebervenValFatLiquido.AsFloat*(dm.queryVerificaVendaAReceberforComissao.AsFloat/100)),-2);
              //ssageDlg('Valor da parcela da venda R$ '+ FloatToStr( valorParcela)+' Data de recebimento: '+DateToStr(quintoDiaUtil(MonthOf(IncMonth(dm.queryVerificaVendaARecebervenDatFaturamento.AsDateTime,1)),YearOf(IncMonth(dm.queryVerificaVendaARecebervenDatFaturamento.AsDateTime,1)))) ,mtWarning,[mbOK],0);
              qtdParcelas := 1;
              valorParcela := SimpleRoundTo(((dm.queryVerificaVendaARecebervenValFatLiquido.AsFloat*(dm.queryVerificaVendaAReceberforComissao.AsFloat/100))/qtdParcelas),-2);

              with dm.queryConAReceberInsert do
              begin
                SQL.Clear;
                Close;
                SQL.Add('INSERT INTO Receber ( recDatVencimento,');
                SQL.Add(              'recHistorico,');
                SQL.Add(              'recFornecedor,');
                SQL.Add('              recTipLanCodigo,');
                SQL.Add(              'recValor,');
                SQL.Add(              'recObservacao)');
                SQL.Add('VALUES ( FORMAT(#'+DateToStr(quintoDiaUtil(MonthOf(IncMonth(dm.queryVerificaVendaARecebervenDatFaturamento.AsDateTime,1)),YearOf(IncMonth(dm.queryVerificaVendaARecebervenDatFaturamento.AsDateTime,1))))+'#,"mm/dd/yyyy"),');
                SQL.Add(  QuotedStr('COMISS�O VENDA '+ DM.queryVerificaVendaARecebervenCodPedFornecedor.AsString+' '+IntToStr(qtdParcelas)+'/'+IntToStr(qtdParcelas))+',');
                SQL.Add(  QuotedStr(dm.queryVerificaVendaAReceberforRazao.asString)+ ',');
                SQL.Add('1,');
                SQL.Add(  QuotedStr(FloatToStr(valorParcela))+',');
                SQL.Add(  QuotedStr(' ')+')');
                ExecSQL;
              end;
            end;
        end;
    end;

    dm.conexao.CommitTrans;

  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro: '+#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro: '+#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
      dm.conexao.RollbackTrans;
    end;
  end;
end;

//calcular em que dia vai cair o domingo de p�scoa
function BuscaPascoa(ano: Word): TDate;
var
 n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12: Integer;
 mes, dia: Word;

begin

  n1  := ano mod 19;
  n2  := trunc(ano/100);
  n3  := ano mod 100;
  n4  := trunc(n2/4);
  n5  := n2 mod 4;
  n6  := trunc((n2+8)/25);
  n7  := trunc((n2-n6+1)/3);
  n8  := (19*n1+n2-n4-n7+15) mod 30;
  n9  := trunc(n3/4);
  n10 := n3 mod 4;
  n11 := (32+2*n5+2*n9-n8-n10) mod 7;
  n12 := trunc((n1+11*n8+22*n11)/451);

  mes := trunc((n8+n11-7*n12+114)/31);
  dia := (n8+n11-7*n12+114) mod 31;

  Result := IncDay(StrToDateTime(IntToStr(dia) + '/' + IntToStr(mes) + '/' + IntToStr(ano)),1);

end;

// calcular se determinado dia � feriado
function Feriado(data: TDate): Boolean;
var
  i: integer;
  dia, mes, ano: Word;
  n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12: Integer;
  pascoa, carnaval, paixao, corpus: TDate;

begin

  Result := false;

  dia := DayOf(data);
  mes := MonthOf(data);


  // feriados fixos
  if ((dia = 1)   and (mes = 1))  or
     ((dia = 21)  and (mes = 4))  or
     ((dia = 1)   and (mes = 5))  or
     ((dia = 7)   and (mes = 9))  or
     ((dia = 12)  and (mes = 10)) or
     ((dia = 2)   and (mes = 11)) or
     ((dia = 15)  and (mes = 11)) or
     ((dia = 25)  and (mes = 12)) then
  begin
    Result := true;
    Exit;
  end;

  ano := YearOf(data);

  // feriados m�veis
  pascoa := BuscaPascoa(ano);
  carnaval := IncDay(pascoa, -47);
  paixao := IncDay(pascoa, -2);
  corpus := IncDay(pascoa, 60);

  if (data = pascoa) or
     (data = carnaval) or
     (data = paixao) or
     (data = corpus) then
  begin
    Result := true;
    Exit;
  end;

end;

// rotina que determina se determinado dia � �til ou n�o
function DiaUtil(dia: TDate; sabado: Boolean): Boolean;
begin
  Result := true;
  if (Feriado(dia)) or (DayOfWeek(dia) = 1) then
    Result := False
  else
  begin
    if (not sabado) and (DayOfWeek(dia) = 7) then
      Result := false;
  end;
end;

//calcula o 5� dia �til
function QuintoDiaUtil(mes, ano: Integer): TDate;
var
  i: integer;
  vContDiaUtil: integer;
begin

  vContDiaUtil := 0;
  Result := StrToDateTime('01/' + IntToStr(mes) + '/' + IntToStr(ano));

  while vContDiaUtil < 5 do
  begin
    if DiaUtil(Result, false) then
        Inc(vContDiaUtil);
    Result := Result + 1;
  end;
  Result := Result-1;
end;

{Retorna o pr�ximo dia �til caso a data informada caia em um fim de semana}
Function ProximoDiaUtil (dData : TDateTime) : TDateTime;
begin
  Result :=  dData;
  if DayOfWeek(Result) = 7 then
    Result := Result + 2
  else
  if DayOfWeek(Result) = 1 then
    Result := Result + 1;
  Result := dData;

  if DiaUtil(Result, false) then
    Result := Result + 1;
end;
end.

