unit UFinMovimentacoes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.Grids,
  Vcl.DBGrids,DB;

type
  TfrmFinMovimentacoes = class(TForm)
    DBGrdMovimentacoes: TDBGrid;
    GroupBox5: TGroupBox;
    GroupBox1: TGroupBox;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    BitBtn7: TBitBtn;
    edtPesquisar: TEdit;
    Label2: TLabel;
    CmbBxContas: TComboBox;
    Label1: TLabel;
    lblTotalEmConta: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure BtnFirstClick(Sender: TObject);
    procedure BtnPriorClick(Sender: TObject);
    procedure BtnNextClick(Sender: TObject);
    procedure BtnLastClick(Sender: TObject);
    procedure BtnInsertClick(Sender: TObject);
    procedure BtnDeleteClick(Sender: TObject);
    procedure BtnEditClick(Sender: TObject);
    procedure CmbBxContasChange(Sender: TObject);
    procedure edtPesquisarChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFinMovimentacoes: TfrmFinMovimentacoes;
  statusContas : integer;
  cotCodigo: integer;

implementation

{$R *.dfm}

uses UDM, UFinMovIncluir, UMain, UFuncoes;

procedure TfrmFinMovimentacoes.BtnDeleteClick(Sender: TObject);
var
  confExcluir: Integer;
  nRegistro: Integer;
  descricao: string ;
begin
  try
    nRegistro := DM.tabMovimentacaomovCodigo.AsInteger;
    descricao := dm.tabMovimentacaomovDescricao.AsString;

    confExcluir:= Application.MessageBox('Deseja excluir o lan�amento?', 'Aten��o', MB_YesNo+mb_DefButton2+mb_IconQuestion);
    if confExcluir = IDYes then
    begin
      with dm.queryDelSubTipo do
      begin
        Close;
        SQL.Clear;
        SQL.Text := 'DELETE FROM movimentacao WHERE movCodigo= '+ IntToStr(nRegistro);
        ExecSQL;
      end;
      dm.tabMovimentacao.Close;
      dm.tabMovimentacao.Open;

      //chama a fun��o atualizaValorConta para atualizar o valor da conta no label
      lblTotalEmConta.Caption := 'R$ '+ FloatToStr(atualizaValorConta(CmbBxContas.Items[CmbBxContas.ItemIndex]));

      Log('Movimenta��o '+ descricao+' apagado pelo usu�rio '+ frmMain.usuario+'.');
    end;
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro: '+ frmFinMovimentacoes.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro: '+ frmFinMovimentacoes.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmFinMovimentacoes.BtnEditClick(Sender: TObject);
begin
  //edi��o de registro
  try
    //status conta como ativo
    statusContas := 0;

    //chama a tela de cadastro de contas a pagar
    frmFinMovIncluir:=TfrmFinMovIncluir.Create(Self);//cria��o manual
    frmFinMovIncluir.ShowModal;//chama a tela de backup e restore no modo modal
    frmFinMovIncluir.Release;//libera a tela da mem�ria
    frmFinMovIncluir:=nil;//atribui o conte�do nulo para a vari�vel frmCadConAPagar

  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro: '+ frmFinMovIncluir.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro: '+ frmFinMovIncluir.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmFinMovimentacoes.BtnFirstClick(Sender: TObject);
begin
  //primeiro registro
  dm.tabMovimentacao.First;
end;

procedure TfrmFinMovimentacoes.BtnInsertClick(Sender: TObject);
begin
  //novo registro
  try
    //status conta como ativo
    statusContas := 1;

    //chama a tela de cadastro de contas a pagar
    frmFinMovIncluir:=TfrmFinMovIncluir.Create(Self);//cria��o manual
    frmFinMovIncluir.ShowModal;//chama a tela de backup e restore no modo modal
    frmFinMovIncluir.Release;//libera a tela da mem�ria
    frmFinMovIncluir:=nil;//atribui o conte�do nulo para a vari�vel frmCadConAPagar


  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro: '+ frmFinMovIncluir.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro: '+ frmFinMovIncluir.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmFinMovimentacoes.BtnLastClick(Sender: TObject);
begin
  //ultimo registro
  dm.tabMovimentacao.Last;
end;

procedure TfrmFinMovimentacoes.BtnNextClick(Sender: TObject);
begin
  //pr�ximo registro
  dm.tabMovimentacao.Next;
end;

procedure TfrmFinMovimentacoes.BtnPriorClick(Sender: TObject);
begin
  //vai ao registro anterior
  dm.tabMovimentacao.Prior;
end;

procedure TfrmFinMovimentacoes.CmbBxContasChange(Sender: TObject);
begin
  //chama a fun��o atualizaValorConta para atualizar o valor da conta no label
  lblTotalEmConta.Caption := 'R$ '+ FloatToStr(atualizaValorConta(CmbBxContas.Items[CmbBxContas.ItemIndex]));

  //filtra a somente as movimenta��es da conta atual
  with dm.tabMovimentacao do
  begin
    Filtered := false;
    Filter := 'movCotCodigo = '+ IntToStr (cotCodigo);
    Filtered := true;
  end;

  dm.tabMovimentacao.Close;
  dm.tabMovimentacao.Open;
end;

procedure TfrmFinMovimentacoes.edtPesquisarChange(Sender: TObject);
begin
  //pesquisa parcial por c�digo
  if edtPesquisar.Text <> '' then
    Dm.tabMovimentacao.Locate( 'movDescricao',edtPesquisar.Text, [loCaseInsensitive, loPartialKey] )

end;

procedure TfrmFinMovimentacoes.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  //fechamento das tabelas
  dm.tabMovimentacao.Close;
  dm.tabContas.Close;
  dm.tabFornecedores.Close;
end;

procedure TfrmFinMovimentacoes.FormShow(Sender: TObject);
var
  rCampo : string;
begin
  //abre a conex�o
  dm.tabMovimentacao.Open;
  dm.tabContas.Open;
  dm.tabFornecedores.Open;

  //adiciona as contas no cmbBxContas
  dm.tabContas.First;
  while not DM.tabContas.Eof do
  begin
    rCampo:= dm.tabContascotDescricao.AsString;
    CmbBxContas.Items.Add(rCampo);
    DM.tabContas.Next;
  end;
  CmbBxContas.ItemIndex:=0;

  //chama a fun��o atualizaValorConta para atualizar o valor da conta no label
  lblTotalEmConta.Caption := 'R$ '+ FloatToStr(atualizaValorConta(CmbBxContas.Items[CmbBxContas.ItemIndex]));

  //filtra a somente as movimenta��es da conta atual
  with dm.tabMovimentacao do
  begin
    Filtered := false;
    Filter := 'movCotCodigo = '+ IntToStr (cotCodigo);
    Filtered := true;
  end;

  dm.tabMovimentacao.Close;
  dm.tabMovimentacao.Open;

end;

end.
