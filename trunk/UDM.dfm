�
 TDM 0�m TPF0TDMDMOldCreateOrderHeight�WidthY 	TADOTabletabClientes
Connectionconexao
CursorTypectStatic	TableNameclientesLeft Top0 TAutoIncFieldtabClientescliCodigoDisplayLabel   Código	FieldName	cliCodigoReadOnly	  TWideStringFieldtabClientescliNomeDisplayLabelNome Fantasia	FieldNamecliNomeSize2  TWideStringFieldtabClientescliEnderecoDisplayLabel	   Endereço	FieldNamecliEnderecoSize<  TWideStringFieldtabClientescliCepDisplayLabelCEP	FieldNamecliCepEditMask99999\-999;0;_Size	  TWideStringFieldtabClientescliCidadeDisplayLabelCidade	FieldName	cliCidadeSize#  TWideStringFieldtabClientescliUfDisplayLabelUF	FieldNamecliUfSize  TWideStringFieldtabClientescliNumTelefoneDisplayLabelTelefone	FieldNamecliNumTelefoneEditMask!\(99\)99999-9999;0;_Size  TWideStringFieldtabClientescliEMailDisplayLabelE-Mail	FieldNamecliEMailSize2  TWideStringFieldtabClientescliInsEstadualDisplayLabel   Inscrição Estadual	FieldNamecliInsEstadualSize  TWideStringFieldtabClientescliCNPJDisplayLabelCNPJ	FieldNamecliCNPJEditMask99.999.999/9999-99;0;_Size  TDateTimeFieldtabClientescliDatAdessaoDisplayLabel   Data adessão	FieldNamecliDatAdessaoEditMask!99/99/9999;1;_  TWideStringFieldtabClientescliRazSocialDisplayLabel   Razão Social	FieldNamecliRazSocialSize�   TWideStringFieldtabClientescliNumTelefone2DisplayLabelCelular	FieldNamecliNumTelefone2EditMask!\(99\)99999-9999;0;_Size  TWideStringFieldtabClientescliSuframaDisplayLabelSuframa	FieldName
cliSuframaSize�   TDateTimeFieldtabClientescliValSuframaDisplayLabelValidade	FieldNamecliValSuframaEditMask!99/99/9999;1;_  TWideStringFieldtabClientescliFaxDisplayLabelFax	FieldNamecliFaxEditMask!\(99\)9999-9999;0;_Size  	TBCDFieldtabClientescliDebitoDisplayLabel   Débito	FieldName	cliDebitocurrency		Precision  TWideStringFieldtabClientescliSiteDisplayLabelSite	FieldNamecliSiteSize�   TIntegerFieldtabClientescliStatusDisplayLabelStatus	FieldName	cliStatus  TIntegerFieldtabClientescliFunCodigo	FieldNamecliFunCodigo  	TBCDFieldtabClientescliLimCreditoDisplayLabel   Limite de Crédito	FieldNamecliLimCreditocurrency		Precision  TWideStringFieldtabClientescliObservacoes	FieldNamecliObservacoesSize�   TIntegerFieldtabClientescliForCodigo	FieldNamecliForCodigo  TWideStringFieldtabClientescliEndComplementoDisplayLabelComplemento	FieldNamecliEndComplementoSize�   TIntegerFieldtabClientescliConCodigo	FieldNamecliConCodigo  TStringFieldtabClientesVendedor	FieldKindfkLookup	FieldNameVendedorLookupDataSettabFuncionariosLookupKeyFields	funCodigoLookupResultFieldfunNome	KeyFieldscliFunCodigoLookup	  TStringFieldtabClientesFornecedor	FieldKindfkLookup	FieldName
FornecedorLookupDataSettabFornecedoresLookupKeyFields	forCodigoLookupResultFieldforRazao	KeyFieldscliForCodigoLookup	  TStringFieldtabClientesContato	FieldKindfkLookup	FieldNameContatoLookupDataSettabContatosLookupKeyFields	conCodigoLookupResultFieldConNome	KeyFieldscliConCodigoLookup	  TWideStringFieldtabClientescliEndNumeroDisplayLabel   Número	FieldNamecliEndNumeroSize
  TWideStringFieldtabClientescliEndBairroDisplayLabelBairro	FieldNamecliEndBairroSize�    	TADOTabletabFornecedores
Connectionconexao
CursorTypectStatic	TableNamefornecedoresLeft� Top0 TAutoIncFieldtabFornecedoresforCodigoDisplayLabel   Código	FieldName	forCodigoReadOnly	  TWideStringFieldtabFornecedoresforRazaoDisplayLabel   Razão social	FieldNameforRazaoSize2  TWideStringFieldtabFornecedoresforCidadeDisplayLabelCidade	FieldName	forCidadeSize�   TWideStringFieldtabFornecedoresforUfDisplayLabelUF	FieldNameforUfSize  TWideStringFieldtabFornecedoresforCepDisplayLabelCEP	FieldNameforCepEditMask99999\-999;0;_Size	  TWideStringFieldtabFornecedoresforSuframaDisplayLabelSuframa	FieldName
forSuframaSize2  TWideStringFieldtabFornecedoresforEnderecoDisplayLabel	   Endereço	FieldNameforEnderecoSize<  TDateTimeField"tabFornecedoresforDatRepresentacaoDisplayLabel   Data adessão	FieldNameforDatRepresentacaoEditMask!99/99/9999;1;_  TWideStringFieldtabFornecedoresforNomFantasiaDisplayLabelNome Fantasia	FieldNameforNomFantasiaSize2  TWideStringFieldtabFornecedoresforCNPJDisplayLabelCNPJ	FieldNameforCNPJEditMask99.999.999/9999-99;0;_Size2  TWideStringFieldtabFornecedoresforInsMunicipalDisplayLabel   Inscrição Municipal	FieldNameforInsMunicipalSize2  TDateTimeFieldtabFornecedoresforSufValidadeDisplayLabelValidade	FieldNameforSufValidade  TWideStringField tabFornecedoresforEndComplementoDisplayLabelComplemento	FieldNameforEndComplementoSize�   TWideStringFieldtabFornecedoresforNumTelefone2DisplayLabelCelular	FieldNameforNumTelefone2EditMask!\(99\)99999-9999;0;_Size  TWideStringFieldtabFornecedoresforSiteDisplayLabelSite	FieldNameforSiteSize2  TWideStringFieldtabFornecedoresforObservacoesDisplayLabel   Observações	FieldNameforObservacoesSize�   TIntegerFieldtabFornecedoresforStatusDisplayLabelStatus	FieldName	forStatus  TWideStringFieldtabFornecedoresforInsEstadualDisplayLabel   Inscrição Estadual	FieldNameforInsEstadualSize2  TIntegerFieldtabFornecedoresforConCodigo	FieldNameforConCodigo  TStringFieldtabFornecedoresContato	FieldKindfkLookup	FieldNameContatoLookupDataSettabContatosLookupKeyFields	conCodigoLookupResultFieldConNome	KeyFieldsforConCodigoSizeLookup	  TWideStringFieldtabFornecedoresforEndNumeroDisplayLabel   Número	FieldNameforEndNumeroSize
  TWideStringFieldtabFornecedoresforEndBairroDisplayLabelBairro	FieldNameforEndBairroSize2  TFloatFieldtabFornecedoresforComissao	FieldNameforComissao  TWideStringFieldtabFornecedoresforNumTelefone	FieldNameforNumTelefoneEditMask!\(99\)9999-9999;0;_Size  TWideStringFieldtabFornecedoresforFax	FieldNameforFaxEditMask!\(99\)9999-9999;0;_Size  TIntegerFieldtabFornecedoresforLiquidacaoDisplayLabel   Liquidação	FieldNameforLiquidacao   	TADOTabletabCategorias
Connectionconexao
CursorTypectStatic	TableName
categoriasLeftTop0 TAutoIncFieldtabCategoriascatCodigoDisplayLabel   Código	FieldName	catCodigoReadOnly	  TWideStringFieldtabCategoriascatDescricaoDisplayLabel   Descrição	FieldNamecatDescricaoSize  TIntegerFieldtabCategoriascatStatusDisplayLabelStatus	FieldName	catStatus  TWideStringFieldtabCategoriascatObservacoesDisplayLabel   Observações	FieldNamecatObservacoesSize�    	TADOTabletabFuncionarios
ConnectionconexaoCursorLocationclUseServer	TableNamefuncionariosLeftyTop0 TAutoIncFieldtabFuncionariosfunCodigoDisplayLabel   Código	FieldName	funCodigoReadOnly	  TWideStringFieldtabFuncionariosfunNomeDisplayLabelNome	FieldNamefunNomeSize2  TWideStringFieldtabFuncionariosfunCepDisplayLabelCEP	FieldNamefunCepEditMask99999\-999;0;_Size	  TWideStringFieldtabFuncionariosfunUfDisplayLabelUF	FieldNamefunUfSize  TWideStringFieldtabFuncionariosfunNumTelefoneDisplayLabelTelefone	FieldNamefunNumTelefoneEditMask!\(99\)9999-9999;0;_Size  TWideStringFieldtabFuncionariosfunCidadeDisplayLabelCidade	FieldName	funCidadeSize#  	TBCDFieldtabFuncionariosfunSalarioDisplayLabel   Salário	FieldName
funSalariocurrency		Precision  TWideStringFieldtabFuncionariosfunEnderecoDisplayLabel	   Endereço	FieldNamefunEnderecoSize(  TIntegerFieldtabFuncionariosfunFcaCodigo	FieldNamefunFcaCodigo  TWideStringFieldtabFuncionariosfunLoginDisplayLabelLogin	FieldNamefunLoginSize  TDateTimeFieldtabFuncionariosfunDatNascimentoDisplayLabelData Nascimento	FieldNamefunDatNascimento  TWideStringFieldtabFuncionariosfunCelularDisplayLabelCelular	FieldName
funCelularEditMask!\(99\)99999-9999;0;_Size  TWideStringFieldtabFuncionariosfunCPFDisplayLabelCPF	FieldNamefunCPFSize  TWideStringFieldtabFuncionariosfunRGDisplayLabelRG	FieldNamefunRG  TWideStringFieldtabFuncionariosfunEstCivilDisplayLabelEstado Civil	FieldNamefunEstCivilSize
  TWideStringFieldtabFuncionariosfunConjugeDisplayLabel   Conjugê	FieldName
funConjugeSize  TWideStringFieldtabFuncionariosfunNaturalidadeDisplayLabelNaturalidade	FieldNamefunNaturalidadeSize2  TWideStringFieldtabFuncionariosfunNatUFDisplayLabelEstado	FieldNamefunNatUFSize  TDateTimeFieldtabFuncionariosfunDatDemissaoDisplayLabel   Data Demissão	FieldNamefunDatDemissao  TWideStringFieldtabFuncionariosfunCTPSDisplayLabelCTPS	FieldNamefunCTPSSize2  TWideStringFieldtabFuncionariosfunCTPSSerieDisplayLabel   Série	FieldNamefunCTPSSerieSize
  TIntegerFieldtabFuncionariosfunForCodigo	FieldNamefunForCodigo  TWideStringFieldtabFuncionariosfunSenhaDisplayLabelSenha	FieldNamefunSenha  TIntegerFieldtabFuncionariosfunStatusDisplayLabelStatus	FieldName	funStatus  TIntegerFieldtabFuncionariosfunGrpUsuCodigo	FieldNamefunGrpUsuCodigo  TWideStringField tabFuncionariosfunEndComplementoDisplayLabelComplemento	FieldNamefunEndComplementoSize�   TWideStringFieldtabFuncionariosfunRGOrgEmissorDisplayLabel   Orgão Emissor	FieldNamefunRGOrgEmissorSize
  TIntegerFieldtabFuncionariosfunSexo	FieldNamefunSexo  TWideStringFieldtabFuncionariosfunEndNumeroDisplayLabel   Número	FieldNamefunEndNumeroSize
  TWideStringFieldtabFuncionariosfunEndBairroDisplayLabelBairro	FieldNamefunEndBairroSize2  TDateTimeFieldtabFuncionariosfunDatAdmissao	FieldNamefunDatAdmissao   	TADOTabletabVenda
ConnectionconexaoCursorLocationclUseServer	TableNamevendasLeft�Top0 TAutoIncFieldtabVendavenIdDisplayLabel   Código	FieldNamevenIdReadOnly	  TIntegerFieldtabVendavenCliCodigoDisplayLabelCliente	FieldNamevenCliCodigo  TDateTimeFieldtabVendavenDatVendaDisplayLabel
Data venda	FieldNamevenDatVendaEditMask!99/99/9999;1;_  	TBCDFieldtabVendavenValProdutosDisplayLabelValor Pedido	FieldNamevenValProdutoscurrency		Precision  TIntegerFieldtabVendavenQuantidadeDisplayLabel
Quantidade	FieldNamevenQuantidade  TIntegerFieldtabVendavenStatusDisplayLabelStatus	FieldName	venStatus  TWideStringFieldtabVendavenCodPedFornecedorDisplayLabel   Número Pedido	FieldNamevenCodPedFornecedorSize�   TIntegerFieldtabVendavenTipoDisplayLabelTipo	FieldNamevenTipo  TIntegerFieldtabVendavenFunCodigoDisplayLabelVendedor	FieldNamevenFunCodigo  TIntegerFieldtabVendavenPrzEntregaDisplayLabelPrazo Entrega	FieldNamevenPrzEntrega  TIntegerFieldtabVendavenPrzPagCodigoDisplayLabelPrazo Pagamento 	FieldNamevenPrzPagCodigo  	TBCDFieldtabVendavenValLiquidoDisplayLabel   Valor pedido líquido	FieldNamevenValLiquidocurrency		Precision  	TBCDFieldtabVendavenValFatBrutoDisplayLabelValor faturado	FieldNamevenValFatBrutocurrency		Precision  	TBCDFieldtabVendavenValFatLiquidoDisplayLabel   Valor faturado líquido	FieldNamevenValFatLiquidocurrency		Precision  	TBCDFieldtabVendavenValBonBrutoDisplayLabel   Valor bonificação bruto	FieldNamevenValBonBrutocurrency		Precision  	TBCDFieldtabVendavenValBonLiquidoDisplayLabel   valor bonificação líquido	FieldNamevenValBonLiquidocurrency		Precision  TIntegerFieldtabVendavenForCodigoDisplayLabel
Fornecedor	FieldNamevenForCodigo  TIntegerFieldtabVendavenFrete	FieldNamevenFrete  	TBCDFieldtabVendavenAcrIPI	FieldName	venAcrIPI	PrecisionSize   TFloatFieldtabVendavenDesPISCOFINS	FieldNamevenDesPISCOFINS  	TBCDFieldtabVendavenDesComercial	FieldNamevenDesComercial	PrecisionSize   	TBCDFieldtabVendavenDesICMS	FieldName
venDesICMS	PrecisionSize   TIntegerFieldtabVendavenMetCodigo	FieldNamevenMetCodigo  TStringFieldtabVendaCliente	FieldKindfkLookup	FieldNameClienteLookupDataSettabClientesLookupKeyFields	cliCodigoLookupResultFieldcliNome	KeyFieldsvenCliCodigoSizeLookup	  TDateTimeFieldtabVendavenDatFaturamento	FieldNamevenDatFaturamentoEditMask!99/99/9999;1;_  TWideStringFieldtabVendavenObservacoesDisplayLabel   Observações	FieldNamevenObservacoesSized   	TADOTabletabItemVenda
Connectionconexao
CursorTypectStaticIndexFieldNamesiteVenVenIdMasterFieldsvenIdMasterSource
DSTabVenda	TableName	itemVendaLeftETop. TAutoIncFieldtabItemVendaiteVenId	FieldNameiteVenIdReadOnly	  TIntegerFieldtabItemVendaiteVenVenId	FieldNameiteVenVenId  TIntegerFieldtabItemVendaiteVenProId	FieldNameiteVenProId  	TBCDFieldtabItemVendaiteVenValUnitarioDisplayLabel   Preço de venda	FieldNameiteVenValUnitariocurrency		Precision  TIntegerFieldtabItemVendaiteVenQuantidadeDisplayLabel
Quantidade	FieldNameiteVenQuantidade
OnValidate$tabItemVendaiteVenQuantidadeValidate  TIntegerFieldtabItemVendaiteVenQtdCaixasDisplayLabel
Qtd Caixas	FieldNameiteVenQtdCaixas  TIntegerFieldtabItemVendaiteVenQtdFaturadaDisplayLabelQtd Faturada	FieldNameiteVenQtdFaturada  TIntegerField tabItemVendaiteVenQtdBonificacaoDisplayLabel   Qtd Bonificação	FieldNameiteVenQtdBonificacao  	TBCDFieldtabItemVendaiteVenValTotal	FieldNameiteVenValTotalcurrency		Precision  TWideStringField"tabItemVendaIteVenProCodFornecedor	FieldNameIteVenProCodFornecedor
OnValidate*tabItemVendaIteVenProCodFornecedorValidateSized  TStringFieldtabItemVendaProduto	FieldKindfkLookup	FieldNameProdutoLookupDataSettabProdutosLookupKeyFieldsproCodfornecedorLookupResultFieldproNome	KeyFieldsIteVenProCodFornecedorSizeLookup	  TFloatFieldtabItemVendaiteVenDesconto1DisplayLabel
Desconto 1	FieldNameiteVenDesconto1
OnValidate#tabItemVendaiteVenDesconto1Validate  TFloatFieldtabItemVendaiteVenDesconto2DisplayLabel
Desconto 2	FieldNameiteVenDesconto2
OnValidate#tabItemVendaiteVenDesconto1Validate  TFloatFieldtabItemVendaiteVenDesconto3DisplayLabel
Desconto 3	FieldNameiteVenDesconto3
OnValidate#tabItemVendaiteVenDesconto1Validate   	TADOTabletabContatos
Connectionconexao
CursorTypectStatic	TableNamecontatosLeft�Top0 TAutoIncFieldtabContatosconCodigoDisplayLabel   Código	FieldName	conCodigoReadOnly	  TWideStringFieldtabContatosconEnderecoDisplayLabel	   Endereço	FieldNameconEnderecoSize�   TWideStringFieldtabContatosconCepDisplayLabelCEP	FieldNameconCepEditMask99999\-999;0;_Size�   TWideStringFieldtabContatosconCidadeDisplayLabelCidade	FieldName	conCidadeSize�   TWideStringFieldtabContatosconUfDisplayLabelUF	FieldNameconUfSize  TDateTimeFieldtabContatosconDatNascimentoDisplayLabelData Nascimento	FieldNameconDatNascimentoEditMask!99/99/9999;1;_  TWideStringFieldtabContatosconCargoDisplayLabelCargo	FieldNameconCargoSize�   TWideStringFieldtabContatosConNomeDisplayLabelNome	FieldNameConNomeSize�   TWideStringFieldtabContatosconTelefoneDisplayLabelTelefone	FieldNameconTelefoneEditMask!\(99\)99999-9999;0;_Size  TWideStringFieldtabContatosconEMailDisplayLabelE-Mail	FieldNameconEMailSize2  TIntegerFieldtabContatosconForCodigo	FieldNameconForCodigo  TIntegerFieldtabContatosconCliCodigo	FieldNameconCliCodigo  TIntegerFieldtabContatosconSexoDisplayLabelSexo	FieldNameconSexo  TWideStringFieldtabContatosconEndComplemento	FieldNameconEndComplementoSize2  TWideStringFieldtabContatosconEndBairroDisplayLabelBairro	FieldNameconEndBairroSize2  TWideStringFieldtabContatosconEndNumeroDisplayLabel   Número	FieldNameconEndNumeroSize
   	TADOTabletabProdutos
Connectionconexao
CursorTypectStatic	TableNameprodutosLeft�Top0 TAutoIncFieldtabProdutosproIdDisplayLabel   Código	FieldNameproIdReadOnly	  TIntegerFieldtabProdutosproCatCodigoDisplayLabel	Categoria	FieldNameproCatCodigo  TWideStringFieldtabProdutosproNomeDisplayLabelNome	FieldNameproNomeSize2  TIntegerFieldtabProdutosproQtdEstoqueDisplayLabelEstoque	FieldNameproQtdEstoque  TWideStringFieldtabProdutosproUnidadeDisplayLabelUnidade	FieldName
proUnidadeSize  TIntegerFieldtabProdutosproForCodigoDisplayLabel
Fornecedor	FieldNameproForCodigo  TWideStringFieldtabProdutosproCodfornecedorDisplayLabel   Código no fornecedor	FieldNameproCodfornecedorSize�   TIntegerFieldtabProdutosproQtdCaixaDisplayLabel	Qtd Caixa	FieldNameproQtdCaixa  TIntegerFieldtabProdutosproValidadeDisplayLabelValidade	FieldNameproValidade  TWideStringFieldtabProdutosproClaFiscalDisplayLabel   Classificação Fiscal	FieldNameproClaFiscalSize2  TWideStringFieldtabProdutosproSitTributariaDisplayLabel   Situação Tributaria	FieldNameproSitTributaria  TWideStringFieldtabProdutosproEAN13DisplayLabelEAN13	FieldNameproEAN13Size  TWideStringFieldtabProdutosproDUN14DisplayLabelDUN14	FieldNameproDUN14Size  TIntegerFieldtabProdutosproStatusDisplayLabelStatus	FieldName	proStatus  	TBCDFieldtabProdutosproUndPesLiquido	FieldNameproUndPesLiquido	PrecisionSize   	TBCDFieldtabProdutosproUndPesBruto	FieldNameproUndPesBruto	PrecisionSize   	TBCDFieldtabProdutosproUndComprimento	FieldNameproUndComprimento	PrecisionSize   	TBCDFieldtabProdutosproUndLargura	FieldNameproUndLargura	PrecisionSize   	TBCDFieldtabProdutosproCxaPesLiquido	FieldNameproCxaPesLiquido	PrecisionSize   	TBCDFieldtabProdutosproCxPesBruto	FieldNameproCxPesBruto	PrecisionSize   	TBCDFieldtabProdutosproCxaComprimento	FieldNameproCxaComprimento	PrecisionSize   	TBCDFieldtabProdutosproCxaLargura	FieldNameproCxaLargura	PrecisionSize   	TBCDFieldtabProdutosproICMS	FieldNameproICMS	PrecisionSize   	TBCDFieldtabProdutosproICMSFonte	FieldNameproICMSFonte	PrecisionSize   	TBCDFieldtabProdutosproIPI	FieldNameproIPI	PrecisionSize   TWideStringFieldtabProdutosproNomReduzido	FieldNameproNomReduzidoSize(   TDataSourceDSTabCategoriasDataSettabCategoriasLeftTopb  TDataSourceDSTabFuncionariosDataSettabFuncionariosLeftyTopb  TDataSourceDSTabClientesDataSettabClientesLeft Topb  TDataSourceDSTabFornecedoresDataSettabFornecedoresLeft� Topb  TDataSourceDSTabContatosDataSettabContatosLeft�Topb  TDataSourceDSTabItemVendaDataSettabItemVendaLeftETopb  TDataSource
DSTabVendaAutoEditDataSettabVendaLeft�Topb  TDataSourceDStabProdutosDataSettabProdutosLeft�Topb  	TADOQueryqueryRelVendas
Connectionconexao
CursorTypectStatic
Parameters SQL.StringsXSELECT a.venId, a.venDatVenda, a.venValProdutos, a.venCodPedFornecedor, a.venValLiquido,)b.cliNome, c.funNome, d.forRazao, d.forUfEFROM vendas AS a, clientes AS b, funcionarios AS c, fornecedores AS d"WHERE b.cliCodigo = a.venCliCodigo AND c.funCodigo = a.venFunCodigo AND d.forCodigo = a.venForCodigo    'ORDER BY d.forRazao,c.funNome,b.cliNome LeftxTopG TAutoIncFieldqueryRelVendasvenId	FieldNamevenIdReadOnly	  TDateTimeFieldqueryRelVendasvenDatVenda	FieldNamevenDatVenda  	TBCDFieldqueryRelVendasvenValProdutos	FieldNamevenValProdutos	Precision  TWideStringField!queryRelVendasvenCodPedFornecedor	FieldNamevenCodPedFornecedorSize7  	TBCDFieldqueryRelVendasvenValLiquido	FieldNamevenValLiquido	Precision  TWideStringFieldqueryRelVendascliNome	FieldNamecliNomeSize2  TWideStringFieldqueryRelVendasfunNome	FieldNamefunNomeSize2  TWideStringFieldqueryRelVendasforRazao	FieldNameforRazaoSize2  TWideStringFieldqueryRelVendasforUf	FieldNameforUfSize   	TADOQuery	queryMeta
Connectionconexao
CursorTypectStatic
ParametersName
pMetCodigo
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value   SQL.Strings*SELECT  SUM (b.venValLiquido) AS metaAtualFFROM metas AS a INNER JOIN vendas AS b ON A.metCodigo = b.venMetCodigoWHERE a.metCodigo = :pMetCodigo LeftTopG 	TBCDFieldqueryMetametaAtual	FieldName	metaAtual	Precision   	TADOQueryqueryCategorias
Connectionconexao
CursorTypectStatic
Parameters SQL.StringsSelect * From categorias  LeftjTopG TAutoIncFieldqueryCategoriascatCodigoDisplayLabel   Código	FieldName	catCodigoReadOnly	  TWideStringFieldqueryCategoriascatDescricaoDisplayLabel   Descrição	FieldNamecatDescricaoSize   	TADOQueryqueryItemVenda
Connectionconexao
CursorTypectStatic
ParametersNamepPrzPagCodigo
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProCodFornecedor
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value   SQL.Strings&SELECT b.preProPreVenda,c.przPagCodigo;FROM produtos AS a, precoProduto AS b, prazoPagamentos AS c0WHERE a.proCodFornecedor = b.preProCodFornecedor&AND b.prePrzPagCodigo = c.przPagCodigo#AND c.przPagCodigo = :pPrzPagCodigo*AND a.proCodFornecedor =:pProCodFornecedorAND a.proStatus = 1; LeftmTopH 	TBCDFieldqueryItemVendapreProPreVenda	FieldNamepreProPreVendacurrency		Precision   	TADOQuery
queryVenda
Connectionconexao
CursorTypectStatic
ParametersNamepVenIdl
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value   SQL.Strings7SELECT sum(iteVenValTotal) as subTotal  FROM itemVenda where iteVenVenId=:pVenIdl     Left� TopG 	TBCDFieldqueryVendasubTotal	FieldNamesubTotal	Precision   	TADOQueryqueryRelProdutos
Connectionconexao
CursorTypectStatic
Parameters SQL.Strings_SELECT a.proId, a.proForCodigo,a.proCodFornecedor, a.proCatCodigo, a.proNome, a.proNomReduzido,T a.proStatus, b.forCodigo, b.forRazao, b.forNomFantasia, c.catCodigo, c.catDescricao8FROM produtos AS a, fornecedores AS b , categorias AS c "WHERE b.forCodigo = a.proForCodigo AND c.catCodigo = a.proCatCodigo'ORDER BY b.forRazao, a.proCodFornecedor Left0TopG TAutoIncFieldqueryRelProdutosproId	FieldNameproIdReadOnly	  TIntegerFieldqueryRelProdutosproForCodigo	FieldNameproForCodigo  TWideStringField queryRelProdutosproCodFornecedor	FieldNameproCodFornecedorSize�   TWideStringFieldqueryRelProdutosproNome	FieldNameproNomeSize2  TWideStringFieldqueryRelProdutosproNomReduzido	FieldNameproNomReduzidoSize  TIntegerFieldqueryRelProdutosproStatus	FieldName	proStatus  TAutoIncFieldqueryRelProdutosforCodigo	FieldName	forCodigoReadOnly	  TWideStringFieldqueryRelProdutosforRazao	FieldNameforRazaoSize2  TWideStringFieldqueryRelProdutosforNomFantasia	FieldNameforNomFantasiaSize2  TAutoIncFieldqueryRelProdutoscatCodigo	FieldName	catCodigoReadOnly	  TWideStringFieldqueryRelProdutoscatDescricao	FieldNamecatDescricaoSize   	TADOQueryqueryPesPrzPagamento
Connectionconexao
CursorTypectStatic
ParametersNamepVenId
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value   SQL.Strings"select venPrzPagCodigo from vendaswhere venId = :pVenId LeftMTopO TIntegerField#queryPesPrzPagamentovenPrzPagCodigo	FieldNamevenPrzPagCodigo   	TADOQueryqueryRelProducao
Connectionconexao
CursorTypectStaticOnCalcFieldsqueryRelProducaoCalcFields
Parameters SQL.Strings�SELECT a.venId,a.venCodPedFornecedor,a.venDatVenda,b.cliRazSocial, b.cliUf,c.funNome, a.venValLiquido, d.forRazao, year ( a.venDatVenda) AS ano, e.metValor,(a.venValLiquido/e.metValor*100) AS percQFROM vendas AS a, clientes AS b, funcionarios AS c, fornecedores AS d, metas AS e"WHERE a.venCliCodigo = b.cliCodigoAND a.venFunCodigo=c.funCodigoAnd e.metCodigo=a.venMetCodigoAND a.venTipo = 18AND a.venDatVenda BETWEEN #01/11/2015# AND #12/11/2015# #ORDER BY d.forRazao, a.venDatVenda;         LeftTopG TAutoIncFieldqueryRelProducaovenId	FieldNamevenIdReadOnly	  TWideStringField#queryRelProducaovenCodPedFornecedor	FieldNamevenCodPedFornecedorSize7  TDateTimeFieldqueryRelProducaovenDatVenda	FieldNamevenDatVenda  TWideStringFieldqueryRelProducaocliRazSocial	FieldNamecliRazSocialSize2  TWideStringFieldqueryRelProducaofunNome	FieldNamefunNomeSize2  	TBCDFieldqueryRelProducaovenValLiquido	FieldNamevenValLiquido	Precision  TWideStringFieldqueryRelProducaoforRazao	FieldNameforRazaoSize2  TStringFieldqueryRelProducaoano	FieldKindfkCalculated	FieldNameanoSize
Calculated	  	TBCDFieldqueryRelProducaometValor	FieldNamemetValor	Precision  TFloatFieldqueryRelProducaoperc	FieldNamepercReadOnly	  TWideStringFieldqueryRelProducaocliUf	FieldNamecliUfSize   	TADOQueryqueryRelMeta
Connectionconexao
CursorTypectStaticOnCalcFieldsqueryRelMetaCalcFields
Parameters SQL.StringsKSELECT a.metCodigo, a.metDatInicio, a.metDatFim, a.metValor, a.metValAtual,O  a.metValFaturado, a.metValFatLiquido, a.metMesReferencia, a.metAnoReferencia,%  b.forRazao,(SELECT SUM (a.metValor)"FROM metas AS a, fornecedores AS b"WHERE a.metForCodigo = b.forCodigo#AND a.metMesReferencia = 'SETEMBRO'-AND a.metAnoReferencia = '2015') AS metGlobal"FROM metas AS a, fornecedores AS b"WHERE a.metForCodigo = b.forCodigo5   AND b.ForRazao = 'CLESS COMERCIO DE COSMÉTICOS LTDA'AND b.forUF = 'SP	'#AND a.metMesReferencia = 'SETEMBRO'AND a.metAnoReferencia = '2015'ORDER BY b.forRazao ; Left�TopG TAutoIncFieldqueryRelMetametCodigo	FieldName	metCodigoReadOnly	  TDateTimeFieldqueryRelMetametDatInicio	FieldNamemetDatInicio  TDateTimeFieldqueryRelMetametDatFim	FieldName	metDatFim  	TBCDFieldqueryRelMetametValor	FieldNamemetValor	Precision  	TBCDFieldqueryRelMetametValAtual	FieldNamemetValAtual	Precision  	TBCDFieldqueryRelMetametValFaturado	FieldNamemetValFaturado	Precision  	TBCDFieldqueryRelMetametValFatLiquido	FieldNamemetValFatLiquido	Precision  TWideStringFieldqueryRelMetametMesReferencia	FieldNamemetMesReferencia  TWideStringFieldqueryRelMetametAnoReferencia	FieldNamemetAnoReferenciaSize  TWideStringFieldqueryRelMetaforRazao	FieldNameforRazaoSize2  TFloatFieldqueryRelMetametPercentual	FieldKindfkCalculated	FieldNamemetPercentual
Calculated	  	TBCDFieldqueryRelMetametGlobal	FieldName	metGlobal	Precision  TFloatFieldqueryRelMetaMetGloPercentual	FieldKindfkCalculated	FieldNameMetGloPercentual
Calculated	   	TADOTable
tabFuncoes
Connectionconexao
CursorTypectStatic	TableNamefuncoesLeftNTop/ TAutoIncFieldtabFuncoesfcaCodigoDisplayLabel   Código	FieldName	fcaCodigoReadOnly	  TWideStringFieldtabFuncoesfcaDescricaoDisplayLabel   Descrição	FieldNamefcaDescricaoSize2   TDataSourceDSTabFuncoesDataSet
tabFuncoesLeftNTopb  	TADOTabletabGrupoUsuarios
Connectionconexao
CursorTypectStatic	TableNameGrupoUsuariosLeft�Top/ TAutoIncFieldtabGrupoUsuariosgrpUsuCodigoDisplayLabel   Código	FieldNamegrpUsuCodigoReadOnly	  TWideStringFieldtabGrupoUsuariosgrpUsuDescricaoDisplayLabel   Descrição	FieldNamegrpUsuDescricaoSize2   TDataSourceDSTabGrpUsuariosDataSettabGrupoUsuariosLeft�Topb  	TADOTabletabMetas
ConnectionconexaoCursorLocationclUseServer	TableNamemetasLeft�Top0 TAutoIncFieldtabMetasmetCodigoDisplayLabel   Código	FieldName	metCodigoReadOnly	  TIntegerFieldtabMetasmetForCodigo	FieldNamemetForCodigo  TDateTimeFieldtabMetasmetDatFimDisplayLabelData fim	FieldName	metDatFimEditMask!99/99/9999;0;_  	TBCDFieldtabMetasmetValorDisplayLabelMeta	FieldNamemetValorcurrency		Precision  	TBCDFieldtabMetasmetValAtualDisplayLabelValor atual	FieldNamemetValAtualcurrency		Precision  	TBCDFieldtabMetasmetValFaturadoDisplayLabelValor Faturado	FieldNamemetValFaturadocurrency		Precision  	TBCDFieldtabMetasmetValFatLiquidoDisplayLabel   Valor faturado líquido	FieldNamemetValFatLiquidocurrency		Precision  TStringFieldtabMetasFornecedor	FieldKindfkLookup	FieldName
FornecedorLookupDataSettabFornecedoresLookupKeyFields	forCodigoLookupResultFieldforRazao	KeyFieldsmetForCodigoLookup	  TDateTimeFieldtabMetasmetDatInicioDisplayWidth
	FieldNamemetDatInicioEditMask!99/99/9999;0;_  TWideStringFieldtabMetasmetMesReferencia	FieldNamemetMesReferencia  TWideStringFieldtabMetasmetAnoReferencia	FieldNamemetAnoReferenciaSize   TDataSource
DSTabMetasDataSettabMetasLeft�Topb  TDataSourceDSTabMetProdutoDataSettabMetaProdutoLeftYTopb  TDataSourceDSTabPrzPagamentosDataSettabPrzPagamentosLeft�Topb  	TADOTabletabPrzPagamentos
Connectionconexao
CursorTypectStatic	TableNameprazoPagamentosLeft�Top0 TAutoIncFieldtabPrzPagamentosprzPagCodigo	FieldNameprzPagCodigoReadOnly	  TWideStringFieldtabPrzPagamentosprzPagDescricao	FieldNameprzPagDescricaoSize�   TFloatFieldtabPrzPagamentosprzPagPar1	FieldName
przPagPar1  TFloatFieldtabPrzPagamentosprzPagPar2	FieldName
przPagPar2  TIntegerFieldtabPrzPagamentosprzPagPar3	FieldName
przPagPar3  TIntegerFieldtabPrzPagamentosprzPagPar4	FieldName
przPagPar4  TIntegerFieldtabPrzPagamentosprzPagPar5	FieldName
przPagPar5   	TADOTabletabPrecoProduto
Connectionconexao
CursorTypectStaticIndexFieldNamespreProCodigoMasterFieldsproIdMasterSourceDStabProdutos	TableNameprecoProdutoLeft7Top0 TAutoIncFieldtabPrecoProdutopreCodigo	FieldName	preCodigoReadOnly	  TIntegerFieldtabPrecoProdutopreProCodigo	FieldNamepreProCodigo  	TBCDFieldtabPrecoProdutopreProPreVenda	FieldNamepreProPreVenda
OnValidate%tabPrecoProdutopreProPreVendaValidatecurrency		Precision  TIntegerFieldtabPrecoProdutoprePrzPagCodigo	FieldNameprePrzPagCodigoOnChange$tabPrecoProdutoprePrzPagCodigoChange  	TBCDFieldtabPrecoProdutoprePrcCaixa	FieldNameprePrcCaixacurrency		Precision  TStringFieldtabPrecoProdutoPrzPagDescricaoDisplayWidth2	FieldKindfkLookup	FieldNamePrzPagDescricaoLookupDataSettabPrzPagamentosLookupKeyFieldsprzPagCodigoLookupResultFieldprzPagDescricao	KeyFieldsprePrzPagCodigoSize2Lookup	  TWideStringField"tabPrecoProdutopreProCodfornecedor	FieldNamepreProCodfornecedorSize   TDataSourceDSTabPrecoProdutoDataSettabPrecoProdutoLeft9Topb  	TADOTabletabMovimentacao
Connectionconexao
CursorTypectStaticOnCalcFieldstabMovimentacaoCalcFieldsIndexFieldNamesmovData	TableNamemovimentacaoLeft�Top0 TAutoIncFieldtabMovimentacaomovCodigoDisplayLabel   Código	FieldName	movCodigoReadOnly	  TDateTimeFieldtabMovimentacaomovDataDisplayLabelData	FieldNamemovDataEditMask!99/99/0000;0;_  TWideStringFieldtabMovimentacaomovDescricaoDisplayLabel   Descrição	FieldNamemovDescricaoSized  TIntegerFieldtabMovimentacaomovTipLanCodigo	FieldNamemovTipLanCodigo  TWideStringFieldtabMovimentacaomovDocumentoDisplayLabel	Documento	FieldNamemovDocumentoSize2  TIntegerFieldtabMovimentacaomovSubTipCodigo	FieldNamemovSubTipCodigo  TStringFieldtabMovimentacaomovLanDescricao	FieldKindfkLookup	FieldNamemovLanDescricaoLookupDataSettabTipLancamentosLookupKeyFieldstipLanCodigoLookupResultFieldtipLanDescricao	KeyFieldsmovTipLanCodigoSizeLookup	  TStringField!tabMovimentacaomovSubTipDescricao	FieldKindfkLookup	FieldNamemovSubTipDescricaoLookupDataSettabSubTiposLookupKeyFieldssubTipCodigoLookupResultFieldsubTipDescricao	KeyFieldsmovSubTipCodigoSizeLookup	  TIntegerFieldtabMovimentacaomovRecCodigo	FieldNamemovRecCodigo  TIntegerFieldtabMovimentacaomovPagCodigo	FieldNamemovPagCodigo  TWideStringFieldtabMovimentacaomovContatoDisplayLabel
Fornecedor	FieldName
movContatoSize�   	TBCDFieldtabMovimentacaomovValorDisplayLabelValor	FieldNamemovValorcurrency		Precision  TIntegerFieldtabMovimentacaomovTipo	FieldNamemovTipo  TStringFieldtabMovimentacaomovTipTexto	FieldKindfkCalculated	FieldNamemovTipTextoSize
Calculated	   	TADOTabletabTipLancamentos
Connectionconexao
CursorTypectStatic	TableNameTiposLancamentoLeft"Top0 TWideStringField tabTipLancamentostipLanDescricaoDisplayLabel   Descrição	FieldNametipLanDescricaoSize2  TAutoIncFieldtabTipLancamentostipLanCodigoDisplayLabel   Código	FieldNametipLanCodigoReadOnly	   	TADOTable	tabTmpPgt
Connectionconexao
CursorTypectStatic	TableNametmpPgtLeft�Top0 TDateTimeFieldtabTmpPgtData	FieldNameData  TWideStringFieldtabTmpPgtValor	FieldNameValorSize   	TADOTabletabSubTipos
Connectionconexao
CursorTypectStaticIndexFieldNamessubTipTipLanCodigoMasterFieldstipLanCodigoMasterSourceDSTabTipLancamentos	TableNameSubtiposLeft�Top0 TAutoIncFieldtabSubTipossubTipCodigoDisplayLabel   Código	FieldNamesubTipCodigoReadOnly	  TIntegerFieldtabSubTipossubTipTipLanCodigo	FieldNamesubTipTipLanCodigo  TWideStringFieldtabSubTipossubTipDescricaoDisplayLabel   Descrição	FieldNamesubTipDescricaoSize2   	TADOTable
tabReceber
Connectionconexao
CursorTypectStaticIndexFieldNamesrecDatVencimento	TableNameReceberLeft+Top0 TDateTimeFieldtabReceberrecDatVencimentoDisplayLabelDt Vencimento	FieldNamerecDatVencimentoEditMask!99/99/9999;0;_  TAutoIncFieldtabReceberrecCodigo	FieldName	recCodigoReadOnly	  	TBCDFieldtabReceberrecValorDisplayLabelValor	FieldNamerecValorcurrency		Precision  TWideStringFieldtabReceberrecHistoricoDisplayLabel   Descrição	FieldNamerecHistoricoSize2  TWideStringFieldtabReceberrecObservacaoDisplayLabel   Observação	FieldNamerecObservacaoSize2  TWideStringFieldtabReceberrecBancoDisplayLabelBanco	FieldNamerecBancoSize2  TIntegerFieldtabReceberrecTipLanCodigo	FieldNamerecTipLanCodigo  TDateTimeFieldtabReceberrecDatPagaDisplayLabelDt Pagamento	FieldName
recDatPagaEditMask!99/99/9999;0;_  	TBCDFieldtabReceberrecValPagoDisplayLabel
Valor Pago	FieldName
recValPagocurrency		Precision  TIntegerFieldtabReceberrecSubTipCodigo	FieldNamerecSubTipCodigo  TStringFieldtabReceberrecTipo	FieldKindfkLookup	FieldNamerecTipoLookupDataSettabTipLancamentosLookupKeyFieldstipLanCodigoLookupResultFieldtipLanDescricao	KeyFieldsrecTipLanCodigoLookup	  TStringFieldtabReceberrecSubTipo	FieldKindfkLookup	FieldName
recSubTipoLookupDataSettabSubTiposLookupKeyFieldssubTipCodigoLookupResultFieldsubTipDescricao	KeyFieldsrecSubTipCodigoSizeLookup	  TWideStringFieldtabReceberrecFornecedor	FieldNamerecFornecedorSize2   	TADOTable	tabContas
Connectionconexao
CursorTypectStatic	TableNamecontasLeft�Top0 TAutoIncFieldtabContascotCodigo	FieldName	cotCodigoReadOnly	  TWideStringFieldtabContascotDescricao	FieldNamecotDescricaoSize2   TDataSourceDSTabMovimentacaoDataSettabMovimentacaoLeft�Topb  TDataSourceDSTabContasDataSet	tabContasLeft�Topb  TDataSourceDSTabReceberDataSet
tabReceberLeft*Topb  TDataSourceDSTabRecibosDataSet
tabRecibosLeftsTopb  TDataSourceDSTabSubTiposDataSettabSubTiposLeft�Topb  TDataSourceDSTabTipLancamentosDataSettabTipLancamentosLeft Topb  	TADOTable
tabRecibos
Connectionconexao
CursorTypectStatic	TableNameRecibosLeftrTop0 TDateTimeFieldtabRecibosData	FieldNameData  TWideStringFieldtabRecibosRecebeuDe	FieldName	RecebeuDeSize�   TWideMemoFieldtabRecibosReferente	FieldName	ReferenteBlobType
ftWideMemo  TWideStringFieldtabRecibosTempo	FieldNameTempoSize2  TWideStringFieldtabRecibosValor	FieldNameValorSize�    TDataSourceDSTabTmpPgtDataSet	tabTmpPgtLeft�Topb  	TADOTabletabPagar
Connectionconexao
CursorTypectStaticIndexFieldNamespagDatVencimento	TableNamePagarLeft�Top0 TAutoIncFieldtabPagarpagCodigoDisplayLabel   Código	FieldName	pagCodigoReadOnly	  TDateTimeFieldtabPagarpagDatVencimentoDisplayLabelData de vencimento	FieldNamepagDatVencimentoEditMask!99/99/9999;1;_  	TBCDFieldtabPagarpagValorDisplayLabelValor	FieldNamepagValorcurrency		Precision  TWideStringFieldtabPagarpagPagarADisplayLabelPagar A	FieldName	pagPagarASize2  TWideStringFieldtabPagarpagHistoricoDisplayLabel
   Histórico	FieldNamepagHistoricoSize2  TWideStringFieldtabPagarpagObservacaoDisplayLabel   Observação	FieldNamepagObservacaoSize2  TWideStringFieldtabPagarpagBancoDisplayLabelBanco	FieldNamepagBancoSize2  TIntegerFieldtabPagarpagTipLanCodigo	FieldNamepagTipLanCodigo  TIntegerFieldtabPagarpagSubTipCodigo	FieldNamepagSubTipCodigo  TDateTimeFieldtabPagarpagDatPagaDisplayLabelData do pagamento	FieldName
pagDatPagaEditMask!99/99/9999;1;_  	TBCDFieldtabPagarpagValPagoDisplayLabelValor do pagamento	FieldName
pagValPagocurrency		Precision  TStringFieldtabPagarCategoriaDisplayWidth	FieldKindfkLookup	FieldName	CategoriaLookupDataSettabTipLancamentosLookupKeyFieldstipLanCodigoLookupResultFieldtipLanDescricao	KeyFieldspagTipLanCodigoReadOnly	Lookup	  TStringFieldtabPagarSubtipoDisplayLabel   DescriçãoDisplayWidth	FieldKindfkLookup	FieldNameSubtipoLookupDataSettabSubTiposLookupKeyFieldssubTipCodigoLookupResultFieldsubTipDescricao	KeyFieldspagSubTipCodigoReadOnly	Lookup	  TIntegerFieldtabPagarpagCotCodigo	FieldNamepagCotCodigo   TDataSource
DSTabPagarDataSettabPagarLeft�Topc  TDataSourceDSQueryItemVendaDataSetqueryItemVendaLeftHTop   	TADOQueryqueryDelMetaProdutosItem
Connectionconexao
CursorTypectStatic
ParametersNamepmetProCodigo
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value   SQL.StringsKDELETE FROM metaProdutosItem WHERE metProIteMetProCodigo = :pmetProCodigo;  Left�TopH  	TADOQueryqueryProdutos
Connectionconexao
CursorTypectStatic
ParametersNamepPrePrzPagCodigo
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProForCodigo
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepPreProCodfornecedor
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value   SQL.StringsSELECT e.preProPreVendaMFROM prazoPagamentos AS a, fornecedores AS c, produtos AS d,precoProduto AS e"WHERE c.forCodigo = d.proForCodigoAND d.proId = e.preProCodigo&AND a.przPagCodigo = e.prePrzPagCodigoAND d.proStatus = 1'AND e.prePrzPagCodigo=:pPrePrzPagCodigo!AND d.proForCodigo=:pProForCodigo0AND e.preProCodfornecedor=:pPreProCodfornecedor; Left�TopP 	TBCDFieldqueryProdutospreProPreVenda	FieldNamepreProPreVenda	Precision   	TADOQueryqueryImpXls
Connectionconexao
CursorTypectStatic
Parameters SQL.Strings)SELECT cliCodigo, cliStatus FROM clientesWHERE cliNome like 'LOJAS ONE'AND cliStatus = 1; Left�TopO  	TADOQueryqueryImpXlsCliente
Connectionconexao
CursorTypectStatic
Parameters SQL.Strings)SELECT cliCodigo, cliStatus FROM clientesWHERE cliNome like 'LOJAS ONE'AND cliStatus = 1; LeftPTopN TAutoIncFieldqueryImpXlsClientecliCodigo	FieldName	cliCodigoReadOnly	  TIntegerFieldqueryImpXlsClientecliStatus	FieldName	cliStatus   	TADOQueryqueryImpXlsPrzPagamento
Connectionconexao
CursorTypectStatic
Parameters SQL.Strings(SELECT przPagCodigo FROM prazoPagamentos&WHERE przPagDescricao LIKE '30/60/90*' Left?TopP TAutoIncField#queryImpXlsPrzPagamentoprzPagCodigo	FieldNameprzPagCodigoReadOnly	   	TADOQueryqueryImpXlsFornecedor
Connectionconexao
CursorTypectStatic
Parameters SQL.StringsSELECT forCodigo, forStatusFROM fornecedoresWHERE forRazao LIKE 'CLESS*'AND forStatus = 1; Left�TopO TAutoIncFieldqueryImpXlsFornecedorforCodigo	FieldName	forCodigoReadOnly	  TIntegerFieldqueryImpXlsFornecedorforStatus	FieldName	forStatus   	TADOQueryqueryImpXlsVendedor
Connectionconexao
CursorTypectStatic
Parameters SQL.Strings"SELECT funCodigo FROM funcionariosWHERE funNome LIKE 'CAIO*'AND funFcaCodigo = 1 Left�TopP TAutoIncFieldqueryImpXlsVendedorfunCodigo	FieldName	funCodigoReadOnly	   TADOConnectionconexaoLoginPromptModecmShareDenyNoneProviderMicrosoft.ACE.OLEDB.12.0Left   	TADOQueryqueryImpVenda
Connectionconexao
CursorTypectStatic
Parameters SQL.StringsSELECT * FROM vendas     Left0TopX TAutoIncFieldqueryImpVendavenId	FieldNamevenIdReadOnly	  TIntegerFieldqueryImpVendavenCliCodigo	FieldNamevenCliCodigo  TDateTimeFieldqueryImpVendavenDatVenda	FieldNamevenDatVenda  	TBCDFieldqueryImpVendavenValProdutos	FieldNamevenValProdutos	Precision  TIntegerFieldqueryImpVendavenQuantidade	FieldNamevenQuantidade  TWideStringField queryImpVendavenCodPedFornecedor	FieldNamevenCodPedFornecedorSize7  TIntegerFieldqueryImpVendavenStatus	FieldName	venStatus  TIntegerFieldqueryImpVendavenTipo	FieldNamevenTipo  TIntegerFieldqueryImpVendavenFunCodigo	FieldNamevenFunCodigo  TIntegerFieldqueryImpVendavenPrzEntrega	FieldNamevenPrzEntrega  TSmallintFieldqueryImpVendavenFrete	FieldNamevenFrete  TIntegerFieldqueryImpVendavenPrzPagCodigo	FieldNamevenPrzPagCodigo  TFloatFieldqueryImpVendavenDesComercial	FieldNamevenDesComercial  TFloatFieldqueryImpVendavenDesICMS	FieldName
venDesICMS  TFloatFieldqueryImpVendavenDesPISCOFINS	FieldNamevenDesPISCOFINS  	TBCDFieldqueryImpVendavenValLiquido	FieldNamevenValLiquido	Precision  	TBCDFieldqueryImpVendavenValFatBruto	FieldNamevenValFatBruto	Precision  	TBCDFieldqueryImpVendavenAcrIPI	FieldName	venAcrIPI	PrecisionSize   	TBCDFieldqueryImpVendavenValFatLiquido	FieldNamevenValFatLiquido	Precision  	TBCDFieldqueryImpVendavenValBonBruto	FieldNamevenValBonBruto	Precision  	TBCDFieldqueryImpVendavenValBonLiquido	FieldNamevenValBonLiquido	Precision  TIntegerFieldqueryImpVendavenForCodigo	FieldNamevenForCodigo  TIntegerFieldqueryImpVendavenMetCodigo	FieldNamevenMetCodigo   	TADOQueryqueryImpItemVenda
Connectionconexao
CursorTypectStatic
Parameters SQL.Stringsselect * from itemVenda Left�TopX TAutoIncFieldqueryImpItemVendaiteVenId	FieldNameiteVenIdReadOnly	  TIntegerFieldqueryImpItemVendaiteVenVenId	FieldNameiteVenVenId  TIntegerFieldqueryImpItemVendaiteVenProId	FieldNameiteVenProId  	TBCDField"queryImpItemVendaiteVenValUnitario	FieldNameiteVenValUnitario	Precision  TIntegerField!queryImpItemVendaiteVenQuantidade	FieldNameiteVenQuantidade  TIntegerField queryImpItemVendaiteVenDesconto1	FieldNameiteVenDesconto1  TIntegerField queryImpItemVendaiteVenDesconto2	FieldNameiteVenDesconto2  TIntegerField queryImpItemVendaiteVenDesconto3	FieldNameiteVenDesconto3  TIntegerField queryImpItemVendaiteVenQtdCaixas	FieldNameiteVenQtdCaixas  TIntegerField"queryImpItemVendaiteVenQtdFaturada	FieldNameiteVenQtdFaturada  TIntegerField%queryImpItemVendaiteVenQtdBonificacao	FieldNameiteVenQtdBonificacao  	TBCDFieldqueryImpItemVendaiteVenValTotal	FieldNameiteVenValTotal	Precision  TWideStringField'queryImpItemVendaIteVenProCodFornecedor	FieldNameIteVenProCodFornecedorSized   	TADOQueryquerySelVenda
Connectionconexao
CursorTypectStatic
Parameters SQL.StringsSELECT venId FROM vendasWHERE venCliCodigo = 11AND venDatVenda = #27/09/2015# AND VenCodPedFornecedor = '2271'AND VenStatus = 0AND VenTipo = 1AND VenFunCodigo = 2AND VenPrzPagCodigo = 17AND VenForCodigo = 3 Left�TopX TAutoIncFieldquerySelVendavenId	FieldNamevenIdReadOnly	   	TADOQueryquerySelProdutos
Connectionconexao
CursorTypectStatic
Parameters SQL.Strings2SELECT proId, proStatus, proQtdCaixa FROM produtosWHERE proCodFornecedor = '2271' Left@TopX TAutoIncFieldquerySelProdutosproId	FieldNameproIdReadOnly	  TIntegerFieldquerySelProdutosproStatus	FieldName	proStatus  TIntegerFieldquerySelProdutosproQtdCaixa	FieldNameproQtdCaixa   	TADOQueryqueryDelVenda
Connectionconexao
CursorTypectStatic
ParametersNamepVenId
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value   SQL.Strings(delete from vendas where venid = :pVenId Left�TopX TAutoIncFieldAutoIncField1	FieldNamevenIdReadOnly	   	TADOQueryqueryUpdateMeta
Connectionconexao
CursorTypectStatic
ParametersNamesumValLiquido
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  Name
pMetCodigo
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value   SQL.StringsJUPDATE metas SET metValAtual = :sumValLiquido WHERE metCodigo =:pMetCodigo Left�Top� 	TBCDField	BCDField1	FieldNamesubTotal	Precision   	TADOQueryqueryDelItemVenda
Connectionconexao
CursorTypectStatic
ParametersNamepIteVenVenId
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value   SQL.Strings7DELETE FROM itemVenda WHERE iteVenVenId = :pIteVenVenId LeftmTop�  	TADOTabletabMetaProdutosItem
Connectionconexao
CursorTypectStaticIndexFieldNamesmetProIteMetProCodigoMasterFieldsmetProCodigoMasterSourceDSTabMetProduto	TableNamemetaProdutosItemLeftNTop0 TAutoIncField"tabMetaProdutosItemmetProIteCodigo	FieldNamemetProIteCodigoReadOnly	  TWideStringField,tabMetaProdutosItemmetProIteProCodfornecedorDisplayLabel   Código do produto	FieldNamemetProIteProCodfornecedorSize2  TIntegerField(tabMetaProdutosItemmetProIteMetProCodigo	FieldNamemetProIteMetProCodigo  TStringFieldtabMetaProdutosItemproduto	FieldKindfkLookup	FieldNameprodutoLookupDataSettabProdutosLookupKeyFieldsproCodfornecedorLookupResultFieldproNome	KeyFieldsmetProIteProCodfornecedorSize2Lookup	  TIntegerField tabMetaProdutosItemmetProIteMeta	FieldNamemetProIteMeta   TDataSourceDSTabMetaProdutosItemDataSettabMetaProdutosItemLeftNTopc  	TADOTabletabMetaVendedor
Connectionconexao
CursorTypectStatic	TableNamemetaVendedorLeft�Top0 TAutoIncFieldtabMetaVendedormetVenCodigoDisplayLabel   Código	FieldNamemetVenCodigoReadOnly	  TIntegerFieldtabMetaVendedormetVenFunCodigo	FieldNamemetVenFunCodigo  TDateTimeFieldtabMetaVendedormetVenDatInicioDisplayLabelData inicio	FieldNamemetVenDatInicioEditMask!99/99/9999;0;_  TDateTimeFieldtabMetaVendedormetVenDatFimDisplayLabelData fim	FieldNamemetVenDatFimEditMask!99/99/9999;0;_  TWideStringField"tabMetaVendedormetVenMesReferenciaDisplayLabel   Mês referencia	FieldNamemetVenMesReferencia  TWideStringField"tabMetaVendedormetVenAnoReferenciaDisplayLabelAno referencia	FieldNamemetVenAnoReferenciaSize   TDataSourceDSTabMetaVendedorDataSettabMetaVendedorLeft�Topc  	TADOTabletabMetaProduto
Connectionconexao
CursorTypectStatic	TableNamemetaProdutosLeftXTop0 TIntegerFieldtabMetaProdutometProForCodigo	FieldNamemetProForCodigo  TAutoIncFieldtabMetaProdutometProCodigo	FieldNamemetProCodigoReadOnly	  TDateTimeFieldtabMetaProdutometProDatInicio	FieldNamemetProDatInicioEditMask!99/99/0000;0;_  TDateTimeFieldtabMetaProdutometProDatFim	FieldNamemetProDatFimEditMask!99/99/0000;0;_  TWideStringField!tabMetaProdutometProMesReferencia	FieldNamemetProMesReferencia  TWideStringField!tabMetaProdutometProAnoReferencia	FieldNamemetProAnoReferenciaSize   	TADOQueryqueryRelMetaVendedor
Connectionconexao
CursorTypectStatic
Parameters SQL.Strings    SELECT DISTINCT    b.funCodigo,    b.funNome,    d.metVenMesReferencia,    d.metVenAnoReferencia,    d.metVenDatInicio,    d.metVenDatFim,    k.forRazao,    e.metVenIteMeta,     (SELECT SUM (f.venValLiquido)	    FROM        vendas AS f,        funcionarios AS g,         fornecedores AS h      
    WHERE "       f.venFunCodigo =g.funCodigo'       AND f.venForCodigo = h.forCodigo%       AND g.funCodigo =  b.funCodigo%       AND h.forCodigo =  k.forCodigo[       AND f.venDatVenda BETWEEN (d.metVenDatInicio) AND (  d.metVenDatFim)) AS valLiquido,9   ((valLiquido/e.metVenIteMeta)*100) AS perMetaVendedor,   (SELECT SUM(i.metValor)      FROM metas AS i 5    WHERE  i.metMesReferencia = d.metVenMesReferencia@    AND i.metAnoReferencia =d.metVenAnoReferencia) AS metGlobal,/   ((valLiquido/metGlobal)*100) AS perMetGlobalFROM    vendas AS a,    funcionarios AS b,    metas AS c,    metaVendedor AS d,    metaVendedorItem AS e,    fornecedores AS k!WHERE a.venFunCodigo=b.funCodigo AND a.venMetCodigo=c.metCodigo "AND b.funCodigo=d.metVenFunCodigo +AND d.metVenCodigo=e.metVenIteMetVenCodigo %AND e.metVenIteForCodigo=k.forCodigo %AND d.metVenMesReferencia='NOVEMBRO'  AND d.metVenAnoReferencia='2015'ORDER BY b.funNome; Left TopX TAutoIncFieldqueryRelMetaVendedorfunCodigo	FieldName	funCodigoReadOnly	  TWideStringFieldqueryRelMetaVendedorfunNome	FieldNamefunNomeSize2  TWideStringField'queryRelMetaVendedormetVenMesReferencia	FieldNamemetVenMesReferencia  TWideStringField'queryRelMetaVendedormetVenAnoReferencia	FieldNamemetVenAnoReferenciaSize  TDateTimeField#queryRelMetaVendedormetVenDatInicio	FieldNamemetVenDatInicio  TDateTimeField queryRelMetaVendedormetVenDatFim	FieldNamemetVenDatFim  TWideStringFieldqueryRelMetaVendedorforRazao	FieldNameforRazaoSize2  	TBCDField!queryRelMetaVendedormetVenIteMeta	FieldNamemetVenIteMeta	Precision  	TBCDFieldqueryRelMetaVendedorvalLiquido	FieldName
valLiquido	Precision  TFloatField#queryRelMetaVendedorperMetaVendedor	FieldNameperMetaVendedor  	TBCDFieldqueryRelMetaVendedormetGlobal	FieldName	metGlobal	Precision  TFloatField queryRelMetaVendedorperMetGlobal	FieldNameperMetGlobal   	TADOTabletabMetaVendedorItem
Connectionconexao
CursorTypectStaticIndexFieldNamesmetVenIteMetVenCodigoMasterFieldsmetVenCodigoMasterSourceDSTabMetaVendedor	TableNamemetaVendedorItemLeft@Top�  TAutoIncField"tabMetaVendedorItemmetVenIteCodigo	FieldNamemetVenIteCodigoReadOnly	  TIntegerField%tabMetaVendedorItemmetVenIteForCodigo	FieldNamemetVenIteForCodigo  TIntegerField(tabMetaVendedorItemmetVenIteMetVenCodigo	FieldNamemetVenIteMetVenCodigo  	TBCDField tabMetaVendedorItemmetVenIteMeta	FieldNamemetVenIteMetacurrency		Precision  TStringFieldtabMetaVendedorItemfornecedorDisplayLabelRepresentadaDisplayWidth(	FieldKindfkLookup	FieldName
fornecedorLookupDataSettabFornecedoresLookupKeyFields	forCodigoLookupResultFieldforRazao	KeyFieldsmetVenIteForCodigoSize(Lookup	   TDataSourceDSTabMetaVendedorItemDataSettabMetaVendedorItemLeftHTop�   	TADOQueryqueryRelMetaSKU
Connectionconexao
CursorTypectStatic
Parameters SQL.StringsSELECT    b.forRazao,   a.metProCodigo,   a.metProDatInicio,   a.metProDatFim,   a.metProMesReferencia,   a.metProAnoReferencia,   d.proCodFornecedor,   d.proNome,   c.metProIteMeta,"   (SELECT SUM(f.iteVenQuantidade)$    FROM vendas AS e, itemVenda AS f!    WHERE e.venId = f.iteVenVenIdF    AND e.venDatVenda BETWEEN (a.metProDatInicio) AND (a.metProDatFim)$    AND e.venForCodigo = b.forCodigoP    AND f.iteVenProCodFornecedor = c.metProIteproCodFornecedor) AS metAlcancada,#   (SELECT SUM(f.iteVenQtdFaturada)$    FROM vendas AS e, itemVenda AS f!    WHERE e.venId = f.iteVenVenIdF    AND e.venDatVenda BETWEEN (a.metProDatInicio) AND (a.metProDatFim)$    AND e.venForCodigo = b.forCodigoO    AND f.iteVenProCodFornecedor = c.metProIteproCodFornecedor) AS metFaturada,;   (metAlcancada / c.metProIteMeta * 100) AS metPercentual,:   (metFaturada / c.metProIteMeta * 100) AS metPerFaturadaFROM    metaProdutos AS a,    fornecedores AS b,   metaProdutosItem AS c,   produtos AS d%WHERE a.metProForCodigo = b.forCodigo,AND c.metProIteMetProCodigo = a.metProCodigo4AND d.proCodFornecedor = c.metProIteproCodFornecedor%AND a.metProMesReferencia = 'JANEIRO'"AND a.metProAnoReferencia = '2016'5   AND b.forRazao = 'CLESS COMERCIO DE COSMÉTICOS LTDA''ORDER BY b.forRazao, a.metProDatInicio; LefthTopP TWideStringFieldqueryRelMetaSKUforRazao	FieldNameforRazaoSize2  TAutoIncFieldqueryRelMetaSKUmetProCodigo	FieldNamemetProCodigoReadOnly	  TDateTimeFieldqueryRelMetaSKUmetProDatInicio	FieldNamemetProDatInicio  TDateTimeFieldqueryRelMetaSKUmetProDatFim	FieldNamemetProDatFim  TWideStringField"queryRelMetaSKUmetProMesReferencia	FieldNamemetProMesReferencia  TWideStringField"queryRelMetaSKUmetProAnoReferencia	FieldNamemetProAnoReferenciaSize  TWideStringFieldqueryRelMetaSKUproCodFornecedor	FieldNameproCodFornecedorSize�   TWideStringFieldqueryRelMetaSKUproNome	FieldNameproNomeSize2  TIntegerFieldqueryRelMetaSKUmetProIteMeta	FieldNamemetProIteMeta  TFloatFieldqueryRelMetaSKUmetAlcancada	FieldNamemetAlcancada  TFloatFieldqueryRelMetaSKUmetPercentual	FieldNamemetPercentualReadOnly	  TFloatFieldqueryRelMetaSKUmetFaturada	FieldNamemetFaturada  TFloatFieldqueryRelMetaSKUmetPerFaturada	FieldNamemetPerFaturadaReadOnly	   	TADOQueryqueryRelVendaSKU
Connectionconexao
CursorTypectStatic
Parameters SQL.StringsSELECT DISTINCT   a.forRazao,    c.iteVenProCodFornecedor,   d.proNome,     (SELECT SUM(g.iteVenValTotal)F    FROM fornecedores AS e, vendas AS f, itemVenda AS g, produtos AS h'    WHERE  e.forCodigo = f.venForCodigo    AND f.venId = g.iteVenVenid5    AND g.iteVenProCodFornecedor = h.proCodFornecedor    AND f.venTipo = 1?    AND f.venDatVenda BETWEEN (#11/01/2015#) AND (#30/11/2015#)I    AND g.iteVenProCodFornecedor = d.proCodFornecedor ) AS itemVendaTotalBFROM fornecedores AS a, vendas AS b, itemVenda AS c, produtos AS d#WHERE  a.forCodigo = b.venForCodigoAND b.venId = c.iteVenVenid1AND c.iteVenProCodFornecedor = d.proCodFornecedorAND b.venTipo = 1;AND b.venDatVenda BETWEEN (#11/01/2015#) AND (#30/11/2015#)ORDER BY a.forRazao, d.proNome Left�TopP TWideStringFieldqueryRelVendaSKUforRazao	FieldNameforRazaoSize2  TWideStringField&queryRelVendaSKUiteVenProCodFornecedor	FieldNameiteVenProCodFornecedorSized  TWideStringFieldqueryRelVendaSKUproNome	FieldNameproNomeSize2  	TBCDFieldqueryRelVendaSKUitemVendaTotal	FieldNameitemVendaTotal	Precision   	TADOQueryqueryRelVEndasIndividual
Connectionconexao
CursorTypectStatic
Parameters SQL.StringsSELECT    d.venId,   d.venCodPedFornecedor,    c.cliNome,     d.venDatVenda,    b.forRazao,    g.przPagDescricao,   d.venPrzEntrega,    a.funNome,   d.venStatus,    d.venTipo,    d.venFrete,    d.venPrzPagCodigo,    d.venValProdutos,    d.venAcrIPI,    d.venDesComercial,    d.venDesICMS,    d.venDesPISCOFINS,    d.venValLiquido,    d.venValFatBruto,    d.venValFatLiquido,    f.IteVenProCodFornecedor,    e.proNome,    f.iteVenValUnitario,      f.iteVenQuantidade,    f.iteVenDesconto1,    f.iteVenDesconto2,    f.iteVenDesconto3,    f.iteVenQtdCaixas,    f.iteVenValTotalzFROM funcionarios AS a, fornecedores AS b, clientes AS c, vendas AS d, Produtos AS e, itemVenda AS f, prazoPagamentos AS g"WHERE c.cliCodigo = d.venCliCodigo!AND b.forCodigo = d.venForCodigo  AND a.funCodigo = d.venFunCodigo6AND e.ProCodFornecedor =      f.IteVenProCodFornecedorAND d.venId = f.iteVenVenId&AND g.przPagCodigo = d.venPrzPagCodigoAND d.venTipo =1; Left@	TopP TAutoIncFieldqueryRelVEndasIndividualvenId	FieldNamevenIdReadOnly	  TWideStringField+queryRelVEndasIndividualvenCodPedFornecedor	FieldNamevenCodPedFornecedorSize7  TWideStringFieldqueryRelVEndasIndividualcliNome	FieldNamecliNomeSize2  TDateTimeField#queryRelVEndasIndividualvenDatVenda	FieldNamevenDatVenda  TWideStringField queryRelVEndasIndividualforRazao	FieldNameforRazaoSize2  TWideStringField'queryRelVEndasIndividualprzPagDescricao	FieldNameprzPagDescricaoSize�   TIntegerField%queryRelVEndasIndividualvenPrzEntrega	FieldNamevenPrzEntrega  TWideStringFieldqueryRelVEndasIndividualfunNome	FieldNamefunNomeSize2  TIntegerField!queryRelVEndasIndividualvenStatus	FieldName	venStatus  TIntegerFieldqueryRelVEndasIndividualvenTipo	FieldNamevenTipo  TSmallintField queryRelVEndasIndividualvenFrete	FieldNamevenFrete  TIntegerField'queryRelVEndasIndividualvenPrzPagCodigo	FieldNamevenPrzPagCodigo  	TBCDField&queryRelVEndasIndividualvenValProdutos	FieldNamevenValProdutos	Precision  TFloatField!queryRelVEndasIndividualvenAcrIPI	FieldName	venAcrIPI  TFloatField'queryRelVEndasIndividualvenDesComercial	FieldNamevenDesComercial  TFloatField"queryRelVEndasIndividualvenDesICMS	FieldName
venDesICMS  TFloatField'queryRelVEndasIndividualvenDesPISCOFINS	FieldNamevenDesPISCOFINS  	TBCDField%queryRelVEndasIndividualvenValLiquido	FieldNamevenValLiquido	Precision  	TBCDField&queryRelVEndasIndividualvenValFatBruto	FieldNamevenValFatBruto	Precision  	TBCDField(queryRelVEndasIndividualvenValFatLiquido	FieldNamevenValFatLiquido	Precision  TWideStringField.queryRelVEndasIndividualIteVenProCodFornecedor	FieldNameIteVenProCodFornecedorSized  TWideStringFieldqueryRelVEndasIndividualproNome	FieldNameproNomeSize2  	TBCDField)queryRelVEndasIndividualiteVenValUnitario	FieldNameiteVenValUnitario	Precision  TIntegerField(queryRelVEndasIndividualiteVenQuantidade	FieldNameiteVenQuantidade  TFloatField'queryRelVEndasIndividualiteVenDesconto1	FieldNameiteVenDesconto1  TFloatField'queryRelVEndasIndividualiteVenDesconto2	FieldNameiteVenDesconto2  TFloatField'queryRelVEndasIndividualiteVenDesconto3	FieldNameiteVenDesconto3  TIntegerField'queryRelVEndasIndividualiteVenQtdCaixas	FieldNameiteVenQtdCaixas  	TBCDField&queryRelVEndasIndividualiteVenValTotal	FieldNameiteVenValTotal	Precision   	TADOQueryqueryRelFaturamento
Connectionconexao
CursorTypectStatic
Parameters SQL.StringsSELECT    c.forRazao,    a.venCodPedFornecedor,    b.cliNome,   a.venValFatLiquido,    a.venDatVenda,    a.venDatFaturamento,    d.funNomeFROM    vendas AS a,   clientes AS b,   fornecedores AS c,   funcionarios AS d"WHERE a.venCliCodigo = b.cliCodigo AND c.forCodigo = a.venForCodigo AND d.funCodigo = a.venFunCodigoAND  a.venStatus = 1AND a.venTipo = 1.ORDER BY a.venDatVenda, a.venDatFaturamento    Left�	TopP TWideStringFieldqueryRelFaturamentoforRazao	FieldNameforRazaoSize2  TWideStringField&queryRelFaturamentovenCodPedFornecedor	FieldNamevenCodPedFornecedorSize7  TWideStringFieldqueryRelFaturamentocliNome	FieldNamecliNomeSize2  	TBCDField#queryRelFaturamentovenValFatLiquido	FieldNamevenValFatLiquido	Precision  TDateTimeFieldqueryRelFaturamentovenDatVenda	FieldNamevenDatVenda  TDateTimeField$queryRelFaturamentovenDatFaturamento	FieldNamevenDatFaturamento  TWideStringFieldqueryRelFaturamentofunNome	FieldNamefunNomeSize2   	TADOQueryqueryRelComVendas
Connectionconexao
CursorTypectStatic
Parameters SQL.StringsSELECT 6   a.forRazao, b.metMesReferencia, b.metAnoReferencia,&   SUM (c.venValLiquido) AS valLiquido/FROM fornecedores AS a, metas AS b, vendas AS c"WHERE a.forCodigo = b.metForCodigo AND a.forCodigo = c.venForCodigoAND c.venTipo = 1!AND b.metMesReferencia ='OUTUBRO'AND b.metAnoReferencia = '2015'(AND a.forRazao = 'LENOX IND. E COM LTDA'=AND c.venDatVenda BETWEEN (b.metDatInicio) AND (b.metDatFim) <GROUP BY    a.forRazao,b.metMesReferencia,b.metAnoReferencia    UNION    SELECT 6   a.forRazao, b.metMesReferencia, b.metAnoReferencia,&   SUM (c.venValLiquido) AS valLiquido/FROM fornecedores AS a, metas AS b, vendas AS c"WHERE a.forCodigo = b.metForCodigo AND a.forCodigo = c.venForCodigoAND c.venTipo = 1"AND b.metMesReferencia ='NOVEMBRO'AND b.metAnoReferencia = '2015'(AND a.forRazao = 'LENOX IND. E COM LTDA'=AND c.venDatVenda BETWEEN (b.metDatInicio) AND (b.metDatFim) >GROUP BY    a.forRazao,b.metMesReferencia,  b.metAnoReferencia Left
TopP TWideStringFieldqueryRelComVendasforRazao	FieldNameforRazaoSize�   TWideStringField!queryRelComVendasmetMesReferencia	FieldNamemetMesReferenciaSize�   TWideStringField!queryRelComVendasmetAnoReferencia	FieldNamemetAnoReferenciaSize�   	TBCDFieldqueryRelComVendasvalLiquido	FieldName
valLiquido	Precision   	TADOQueryqueryCountItemVenda
Connectionconexao
CursorTypectStatic
ParametersNamepVenId
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value   SQL.Strings(SELECT COUNT ( b.iteVenVenId) as qtdItem!FROM vendas AS a , itemVenda AS bWHERE a.venId = b.iteVenVenIdAND a.venId = :pVenId Left�Top� TIntegerFieldqueryCountItemVendaqtdItem	FieldNameqtdItem   	TADOQueryqueryGraMetRepresentada
Connectionconexao
CursorTypectStatic
ParametersName	pForRazao
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepMes
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepAno
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value   SQL.StringsSELECT    a.metValor,   a.metValAtual,    (SELECT SUM (c.metValor)     FROM metas AS c,        fornecedores AS d&    WHERE c.metForCodigo = d.forCodigo/    AND c.metMesReferencia = a.metMesReferencia=    AND c.metAnoReferencia = a.metAnoReferencia) AS metGlobal%   FROM metas AS a, fornecedores AS b"WHERE a.metForCodigo = b.forCodigoAND b.ForRazao =  :pForRazaoAND a.metMesReferencia = :pMesAND a.metAnoReferencia = :pAnoORDER BY b.forRazao ; Left0Top� 	TBCDFieldqueryGraMetRepresentadametValor	FieldNamemetValor	Precision  	TBCDField"queryGraMetRepresentadametValAtual	FieldNamemetValAtual	Precision  	TBCDField queryGraMetRepresentadametGlobal	FieldName	metGlobal	Precision   TDataSourceDSqueryGraMetRepresentadaDataSetqueryGraMetRepresentadaLeft5Top�  TDataSourceDSQueryGraMetVendedorDataSetqueryGraMetVendedorLeft� Top�  	TADOQueryqueryGraMetVendedor
Connectionconexao
CursorTypectStatic
ParametersNamepMes
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepAno
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepFunNome
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  Name	pForRazao
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value   SQL.Strings    SELECT DISTINCT    e.metVenIteMeta,     (SELECT SUM (f.venValLiquido)	    FROM        vendas AS f,        funcionarios AS g,         fornecedores AS h      
    WHERE "       f.venFunCodigo =g.funCodigo'       AND f.venForCodigo = h.forCodigo%       AND g.funCodigo =  b.funCodigo%       AND h.forCodigo =  k.forCodigo[       AND f.venDatVenda BETWEEN (d.metVenDatInicio) AND (  d.metVenDatFim)) AS valLiquido,   (SELECT SUM(i.metValor)      FROM metas AS i 5    WHERE  i.metMesReferencia = d.metVenMesReferencia?    AND i.metAnoReferencia =d.metVenAnoReferencia) AS metGlobalFROM    vendas AS a,    funcionarios AS b,    metas AS c,    metaVendedor AS d,    metaVendedorItem AS e,    fornecedores AS k!WHERE a.venFunCodigo=b.funCodigo AND a.venMetCodigo=c.metCodigo "AND b.funCodigo=d.metVenFunCodigo +AND d.metVenCodigo=e.metVenIteMetVenCodigo %AND e.metVenIteForCodigo=k.forCodigo  AND d.metVenMesReferencia= :pMes AND d.metVenAnoReferencia= :pAnoAND b.funNome = :pFunNomeAND k.forRazao = :pForRazao Left� Top� 	TBCDField queryGraMetVendedormetVenIteMeta	FieldNamemetVenIteMeta	Precision  	TBCDFieldqueryGraMetVendedorvalLiquido	FieldName
valLiquido	Precision  	TBCDFieldqueryGraMetVendedormetGlobal	FieldName	metGlobal	Precision   	TADOQueryqueryGraVendas
Connectionconexao
CursorTypectStatic
ParametersName	pForRazao
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepMes
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepAno
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value   SQL.StringsSELECT    a.forRazao,     c.metValor, *   SUM(b.venValLiquido) AS venValLiquido, .   SUM(b.venValFatLiquido) AS venValFatLiquidoFROM -   fornecedores AS a, vendas AS b, metas AS c"WHERE a.forCodigo = b.venForCodigo!AND  a.forCodigo = c.metForCodigoAND a.forStatus = 1AND a.forRazao = :pForRazaoAND c.metMesReferencia = :pMesAND c.metAnoReferencia = :pAno8AND b.VenDatVenda BETWEEN c.metDatInicio AND c.metDatFimAND b.venTipo = 1GROUP BY a.forRazao, c.metValor Left@Top� TWideStringFieldqueryGraVendasforRazao	FieldNameforRazaoSize2  	TBCDFieldqueryGraVendasmetValor	FieldNamemetValor	Precision  	TBCDFieldqueryGraVendasvenValLiquido	FieldNamevenValLiquido	Precision  	TBCDFieldqueryGraVendasvenValFatLiquido	FieldNamevenValFatLiquido	Precision   	TADOQueryqueryGraVenVendedor
Connectionconexao
CursorTypectStatic
ParametersNamepFunNome
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepMes
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepAno
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value   SQL.StringsSELECT   a.forRazao, %   SUM(b.venValLiquido) AS valLiquidoBFROM fornecedores AS a, vendas AS b, metas AS c, funcionarios AS d"WHERE a.forCodigo = b.venForCodigo!AND  a.forCodigo = c.metForCodigo AND b.venFunCodigo = d.funCodigoAND a.forStatus = 1AND d.funStatus=1AND d.funNome = :pFunNomeAND c.metMesReferencia = :pMesAND c.metAnoReferencia = :pAno8AND b.VenDatVenda BETWEEN c.metDatInicio AND c.metDatFimAND b.venTipo = 1GROUP BY a.forRazao, c.metValorORDER BY SUM(b.venValLiquido); Left�Top� 	TBCDFieldqueryGraVenVendedorvalLiquido	FieldName
valLiquido	Precision  TWideStringFieldqueryGraVenVendedorforRazao	FieldNameforRazaoSize2   	TADOQueryqueryRelClientesMaisAtivos
Connectionconexao
CursorTypectStatic
Parameters SQL.StringsSELECT TOP 18   c.forRazao,   a.cliRazSocial,    a.cliCidade,    a.cliUf,+   SUM (b.venValLiquido) AS venValLiquido, /   SUM (b.venValFatLiquido) AS venValFatLiquido>FROM clientes AS a, vendas AS b, fornecedores AS c, metas AS d"WHERE a.cliCodigo = b.venCliCodigo AND b.venForCodigo = c.forCodigo AND c.ForCodigo = d.metForCodigoAND b.venTipo = 1 DAND c.ForRazao = 'MEREJE BRAZIL INDUSTRIA DE METALURGIA DE PRECISAO':AND b.venDatVenda BETWEEN   d.MetDatInicio AND d.metDatFim!   AND d.metMesReferencia = 'MARÇO' AND d.metAnoReferencia = '2016' GROUP BY c.forRazao,"                  a.cliRazSocial,                   a.cliCidade,                   a.cliUf%ORDER BY SUM (b.venValLiquido) DESC, +                  SUM (b.venValFatLiquido)  LeftHTop� TWideStringField&queryRelClientesMaisAtivoscliRazSocial	FieldNamecliRazSocialSize2  TWideStringField#queryRelClientesMaisAtivoscliCidade	FieldName	cliCidadeSize#  TWideStringFieldqueryRelClientesMaisAtivoscliUf	FieldNamecliUfSize  	TBCDField'queryRelClientesMaisAtivosvenValLiquido	FieldNamevenValLiquido	Precision  	TBCDField*queryRelClientesMaisAtivosvenValFatLiquido	FieldNamevenValFatLiquido	Precision  TWideStringField"queryRelClientesMaisAtivosforRazao	FieldNameforRazaoSize2   	TADOQuery$queryRelClientesMaisAtivos20Porcento
Connectionconexao
CursorTypectStatic
Parameters SQL.StringsSELECT )  ( INT (COUNT (*) * 0.2)) AS 20Porcento FROM clientes WHERE cliStatus = 1 LeftHTop TFloatField8queryRelClientesMaisAtivos20PorcentoFloatField20Porcento	FieldName
20PorcentoReadOnly	   	TADOQueryqueryRelRepresentatividade
Connectionconexao
CursorTypectStaticOnCalcFields$queryRelRepresentatividadeCalcFields
ParametersNamepMes1
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepAno1
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepMes2
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepAno2
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  Name	pForRazao
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepMes3
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepAno3
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value   SQL.StringsSELECT a.funNome,              b.forRazao, 5             SUM (c.venValLiquido) AS venValLiquido, ;             SUM (c.venValFatLiquido) AS venValFatLiquido, +             (SELECT SUM (g.venValLiquido) P              FROM funcionarios AS e, fornecedores AS f, vendas AS g, metas AS h4              WHERE f.[forCodigo] = g.[venForCodigo]2              AND e.[funCodigo] = g.[venFunCodigo].              AND h.metCodigo = g.venMetCodigo!              AND e.funStatus = 1              AND g.venTipo = 1F              AND g.venDatVenda BETWEEN h.metDatInicio AND h.metDatFim)              AND f.forRazao = b.forRazao-              AND h.metMesReferencia = :pMes1?              AND h.metAnoReferencia = :pAno1) AS valLiqGeral, /              (SELECT SUM (g.venValFatLiquido) O             FROM funcionarios AS e, fornecedores AS f, vendas AS g, metas AS h3             WHERE f.[forCodigo] = g.[venForCodigo]1             AND e.[funCodigo] = g.[venFunCodigo]-             AND h.metCodigo = g.venMetCodigo              AND e.funStatus = 1             AND g.venTipo = 1E             AND g.venDatVenda BETWEEN h.metDatInicio AND h.metDatFim(             AND f.forRazao = b.forRazao,             AND h.metMesReferencia = :pMes2?             AND h.metAnoReferencia = :pAno2) AS valFatLiqGeralBFROM funcionarios AS a, fornecedores AS b, vendas AS c, metas AS d&WHERE b.[forCodigo] = c.[venForCodigo]$AND a.[funCodigo] = c.[venFunCodigo] AND d.metCodigo = c.venMetCodigoAND a.funStatus = 1AND c.venTipo = 18AND c.venDatVenda BETWEEN d.metDatInicio AND d.metDatFimAND b.forRazao = :pForRazaoAND d.metMesReferencia = :pMes3AND d.metAnoReferencia = :pAno3GROUP BY a.funNome, b.forRazao Left�Top� TWideStringField!queryRelRepresentatividadefunNome	FieldNamefunNomeSize2  TWideStringField"queryRelRepresentatividadeforRazao	FieldNameforRazaoSize2  	TBCDField'queryRelRepresentatividadevenValLiquido	FieldNamevenValLiquido	Precision  	TBCDField*queryRelRepresentatividadevenValFatLiquido	FieldNamevenValFatLiquido	Precision  	TBCDField%queryRelRepresentatividadevalLiqGeral	FieldNamevalLiqGeral	Precision  	TBCDField(queryRelRepresentatividadevalFatLiqGeral	FieldNamevalFatLiqGeral	Precision  TFloatField+queryRelRepresentatividadevenPerValLiqGeral	FieldKindfkCalculated	FieldNamevenPerValLiqGeral
Calculated	  TFloatField.queryRelRepresentatividadevenPerValFatLiqGeral	FieldKindfkCalculated	FieldNamevenPerValFatLiqGeral
Calculated	   	TADOQueryqueryGrpUsuarios
Connectionconexao
CursorTypectStatic
ParametersName
pUsuCodigo
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value   SQL.Strings8SELECT * FROM funcionarios WHERE funCodigo = :pUsuCodigo Left@Top  	TADOTabletabGrupoXUsuarios
Connectionconexao
CursorTypectStatic
BeforePosttabGrupoXUsuariosBeforePostIndexFieldNamesgrpXUsuUsuCodigo	TableNamegrupoXUsuariosLeftH	Top0 TIntegerField$tabGrupoXUsuariosgrpXUsuGrpUsuCodigo	FieldNamegrpXUsuGrpUsuCodigo  TIntegerField!tabGrupoXUsuariosgrpXUsuUsuCodigo	FieldNamegrpXUsuUsuCodigo  TStringFieldtabGrupoXUsuariosgrpXUsuUsuNomeDisplayLabel   Usuário	FieldKindfkLookup	FieldNamegrpXUsuUsuNomeLookupDataSettabFuncionariosLookupKeyFields	funCodigoLookupResultFieldfunNome	KeyFieldsgrpXUsuUsuCodigoSize(Lookup	   TDataSourceDSTabGrupoXUsuariosDataSettabGrupoXUsuariosLeftH	Toph  	TADOQueryqueryGrpUsuariosStatus
Connectionconexao
CursorTypectStatic
ParametersName
pUsuCodigo
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value   SQL.Strings8SELECT * FROM funcionarios WHERE funCodigo = :pUsuCodigo Left@TopH TIntegerFieldqueryGrpUsuariosStatusfunStatus	FieldName	funStatus  TWideStringFieldqueryGrpUsuariosStatusfunNome	FieldNamefunNomeSize2   	TADOQueryqueryImpSKUSelProdutos
Connectionconexao
CursorTypectStatic
Parameters SQL.Stringsselect * from produtos Left
Top� TWideStringField&queryImpSKUSelProdutosproCodFornecedor	FieldNameproCodFornecedorSize�   TIntegerField!queryImpSKUSelProdutosproQtdCaixa	FieldNameproQtdCaixa  TIntegerField"queryImpSKUSelProdutosproCatCodigo	FieldNameproCatCodigo  TWideStringFieldqueryImpSKUSelProdutosproNome	FieldNameproNomeSize2  TIntegerField#queryImpSKUSelProdutosproQtdEstoque	FieldNameproQtdEstoque  TWideStringField queryImpSKUSelProdutosproUnidade	FieldName
proUnidadeSize  TIntegerField"queryImpSKUSelProdutosproForCodigo	FieldNameproForCodigo  TWideStringField$queryImpSKUSelProdutosproNomReduzido	FieldNameproNomReduzidoSize(  TFloatField&queryImpSKUSelProdutosproUndPesLiquido	FieldNameproUndPesLiquido  TFloatField$queryImpSKUSelProdutosproUndPesBruto	FieldNameproUndPesBruto  TFloatField'queryImpSKUSelProdutosproUndComprimento	FieldNameproUndComprimento  TFloatField#queryImpSKUSelProdutosproUndLargura	FieldNameproUndLargura  TFloatField&queryImpSKUSelProdutosproCxaPesLiquido	FieldNameproCxaPesLiquido  TFloatField#queryImpSKUSelProdutosproCxPesBruto	FieldNameproCxPesBruto  TFloatField'queryImpSKUSelProdutosproCxaComprimento	FieldNameproCxaComprimento  TFloatField#queryImpSKUSelProdutosproCxaLargura	FieldNameproCxaLargura  TIntegerField!queryImpSKUSelProdutosproValidade	FieldNameproValidade  TFloatFieldqueryImpSKUSelProdutosproICMS	FieldNameproICMS  TFloatField"queryImpSKUSelProdutosproICMSFonte	FieldNameproICMSFonte  TFloatFieldqueryImpSKUSelProdutosproIPI	FieldNameproIPI  TWideStringField"queryImpSKUSelProdutosproClaFiscal	FieldNameproClaFiscalSize2  TWideStringField&queryImpSKUSelProdutosproSitTributaria	FieldNameproSitTributaria  TWideStringFieldqueryImpSKUSelProdutosproEAN13	FieldNameproEAN13Size  TWideStringFieldqueryImpSKUSelProdutosproDUN14	FieldNameproDUN14Size  TIntegerFieldqueryImpSKUSelProdutosproStatus	FieldName	proStatus  TAutoIncFieldqueryImpSKUSelProdutosproId	FieldNameproIdReadOnly	   	TADOTabletabGrupoClientes
Connectionconexao
CursorTypectStaticIndexFieldNamesgrpCliCodigo	TableNamegrupoClientesLeft�	Top0 TAutoIncFieldtabGrupoClientesgrpCliCodigoDisplayLabel   Código	FieldNamegrpCliCodigoReadOnly	  TWideStringFieldtabGrupoClientesgrpCliDescricaoDisplayLabel   Descrição	FieldNamegrpCliDescricaoSize2  TFloatFieldtabGrupoClientesgrpCliPerGanhoDisplayLabel
% de ganho	FieldNamegrpCliPerGanho   TDataSourceDSTabGrupoClientesAutoEditDataSettabGrupoClientesLeft�	Toph  	TADOTabletabGrupoXClientes
Connectionconexao
CursorTypectStatic
BeforeEdittabGrupoXClientesBeforeEdit	TableNamegrupoXClientesLeftH
Top0 TIntegerField$tabGrupoXClientesgrpXCliGrpCliCodigoDisplayLabelGrupo	FieldNamegrpXCliGrpCliCodigo  TIntegerField!tabGrupoXClientesgrpXCliCliCodigoDisplayLabel   Código cliente	FieldNamegrpXCliCliCodigo  TStringField tabGrupoXClientesgrpXCliCliRazaoDisplayLabelCliente	FieldKindfkLookup	FieldNamegrpXCliCliNomeLookupDataSettabClientesLookupKeyFields	cliCodigoLookupResultFieldcliNome	KeyFieldsgrpXCliCliCodigoSize(Lookup	   TDataSourceDSTabGrupoXClientesDataSettabGrupoXClientesLeftH
Toph  	TADOQueryqueryGrpClientesStatus
Connectionconexao
CursorTypectStatic
ParametersName
pCliCodigo
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value   SQL.Strings5SELECT *  FROM clientes WHERE cliCodigo = :pCliCodigo Left� TopH TIntegerFieldqueryGrpClientesStatuscliStatus	FieldName	cliStatus  TWideStringFieldqueryGrpClientesStatuscliNome	FieldNamecliNomeSize2   	TADOQueryqueryGrpClientes
Connectionconexao
Parameters Left� Top  	TADOTabletabDiretosAcesso
Connectionconexao
CursorTypectStatic
BeforeEdittabGrupoXClientesBeforeEdit	TableNamedireitosAcessoLeft�
Top8 TIntegerField"tabDiretosAcessodirAceGrpUsuCodigo	FieldNamedirAceGrpUsuCodigo  TIntegerFieldtabDiretosAcessodirAceMnuTag	FieldNamedirAceMnuTag   TDataSourceDSTabDiretosAcessoDataSettabDiretosAcessoLeft�
Topp  	TADOQueryqueryGrpXUsuDirAcesso
Connectionconexao
CursorTypectStatic
Parameters SQL.Strings     Left8Top  	TADOQueryqueryImpSKU
Connectionconexao
CursorTypectStatic
Parameters SQL.Strings_SELECT a.proId, a.proForCodigo,a.proCodFornecedor, a.proCatCodigo, a.proNome, a.proNomReduzido,T a.proStatus, b.forCodigo, b.forRazao, b.forNomFantasia, c.catCodigo, c.catDescricao8FROM produtos AS a, fornecedores AS b , categorias AS c "WHERE b.forCodigo = a.proForCodigo AND c.catCodigo = a.proCatCodigo'ORDER BY b.forRazao, a.proCodFornecedor LeftHTop�  	TADOQueryqueryImpSKUSelPrzPagamentos
Connectionconexao
CursorTypectStatic
Parameters SQL.Strings    8SELECT przPagCodigo,przPagDescricao FROM prazoPagamentos Leftp	Top� TAutoIncField'queryImpSKUSelPrzPagamentosprzPagCodigo	FieldNameprzPagCodigoReadOnly	  TWideStringField*queryImpSKUSelPrzPagamentosprzPagDescricao	FieldNameprzPagDescricaoSize�    	TADOQueryqueryImpSKUUpdate
Connectionconexao
CursorTypectStatic
ParametersNamepProNome
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProNomReduzido
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  Name
pProStatus
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProCatCodigo
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProUnidade
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProValidade
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProICMS
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProICMSFonte
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProIPI
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProClaFiscal
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProSitTributaria
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProUndPesLiquido
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProUndPesBruto
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProUndComprimento
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProUndLargura
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProQtdCaixa
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProCxaPesLiquido
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProCxPesBruto
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProCxaComprimento
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProCxaLargura
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProForCodigo
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  Name	pProEAN13
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  Name	pProDUN14
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProCodfornecedor
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value   SQL.StringsUPDATE produtos SETproNome=:pProNome ,proNomReduzido=:pProNomReduzido,proStatus=:pProStatus,proCatCodigo=:pProCatCodigo,proUnidade=:pProUnidade,proValidade= :pProValidade,proICMS=:pProICMS,proICMSFonte=:pProICMSFonte,proIPI= :pProIPI,proClaFiscal=:pProClaFiscal$,proSitTributaria=:pProSitTributaria$,proUndPesLiquido=:pProUndPesLiquido!,proUndPesBruto= :pProUndPesBruto(,proUndComprimento = :pProUndComprimento,proUndLargura=:pProUndLargura,proQtdCaixa= :pProQtdCaixa$,proCxaPesLiquido=:pProCxaPesLiquido,proCxPesBruto=:pProCxPesBruto',proCxaComprimento= :pProCxaComprimento,proCxaLargura=:pProCxaLargura,proForCodigo = :pProForCodigo,proEAN13=:pProEAN13,proDUN14=:pProDUN14,WHERE proCodfornecedor = :pProCodfornecedor  Left�TopW  	TADOQueryqueryImpSKUInsert
Connectionconexao
CursorTypectStatic
ParametersNamepProCodfornecedor
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProNome
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProNomReduzido
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  Name
pProStatus
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProCatCodigo
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProUnidade
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProValidade
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProICMS
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProICMSFonte
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProIPI
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProClaFiscal
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProSitTributaria
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProUndPesLiquido
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProUndPesBruto
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProUndComprimento
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProUndLargura
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProQtdCaixa
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProCxaPesLiquido
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProCxPesBruto
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProCxaComprimento
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProCxaLargura
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NamepProForCodigo
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  Name	pProEAN13
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  Name	pProDUN14
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value   SQL.Strings&INSERT INTO produtos (proCodfornecedor    				 ,proNome					 ,proNomReduzido					 ,proStatus					 ,proCatCodigo					 ,proUnidade					 ,proValidade					 ,proICMS					 ,proICMSFonte					 ,proIPI					 ,proClaFiscal					 ,proSitTributaria					 ,proUndPesLiquido					 ,proUndPesBruto					 ,proUndComprimento					 ,proUndLargura					 ,proQtdCaixa					 ,proCxaPesLiquido					 ,proCxPesBruto					 ,proCxaComprimento					 ,proCxaLargura					 ,proForCodigo					 ,proEAN13					 ,proDUN14)VALUES (:pProCodfornecedor       ,:pProNome       ,:pProNomReduzido       ,:pProStatus       ,:pProCatCodigo       ,:pProUnidade       ,:pProValidade       ,:pProICMS       ,:pProICMSFonte       ,:pProIPI       ,:pProClaFiscal       ,:pProSitTributaria       ,:pProUndPesLiquido       ,:pProUndPesBruto       ,:pProUndComprimento       ,:pProUndLargura       ,:pProQtdCaixa       ,:pProCxaPesLiquido       ,:pProCxPesBruto       ,:pProCxaComprimento       ,:pProCxaLargura       ,:pProForCodigo       ,:pProEAN13       ,:pProDUN14) Left@TopW  	TADOQueryqueryImpVendasMaxIteVenId
Connectionconexao
CursorTypectStatic
Parameters SQL.Strings0SELECT MAX(iteVenId)  AS iteVenId FROM itemvenda Left�Top� TIntegerField!queryImpVendasMaxIteVenIditeVenId	FieldNameiteVenId   	TADOQueryqueryDelSubTipo
Connectionconexao
CursorTypectStatic
ParametersNamepSubTipCodigoSize�Value   SQL.Strings6DELETE FROM subTipo WHERE subTipCodigo= :pSubTipCodigo LeftMTopP  	TADOQueryquerySelTipLancamento
Connectionconexao
CursorTypectStatic
Parameters LeftpTop� TAutoIncField!querySelTipLancamentotipLanCodigo	FieldNametipLanCodigoReadOnly	   TBindSourceDBBindSourceDB1DataSetquerySelTipLancamentoScopeMappings LeftPTop   	TADOQueryquerySelSubTipo
Connectionconexao
CursorTypectStatic
Parameters SQL.StringsOSELECT subTipDescricao, subTipCodigo FROM subTIpos WHERE subTipTipLanCodigo = 7 Left�Top� TAutoIncFieldquerySelSubTiposubTipCodigo	FieldNamesubTipCodigoReadOnly	  TWideStringFieldquerySelSubTiposubTipDescricao	FieldNamesubTipDescricaoSize2   	TADOQueryqueryConAReceberInsert
Connectionconexao
CursorTypectStatic
Parameters Left8Top� TWideStringFieldWideStringField1	FieldNamesubTipDescricaoSize2  TAutoIncFieldAutoIncField2	FieldNamesubTipCodigoReadOnly	   	TADOQueryqueryClientes
Connectionconexao
CursorTypectStatic
Parameters SQL.Strings�select a.cliNome, a.cliUF, a.cliCidade,a.cliNumTelefone,a.cliStatus, b.conNome from clientes AS a, contatos AS b where a.cliConCodigo = b.conCodigoAND cliStatus = 1 Left�Top� TWideStringFieldqueryClientescliNome	FieldNamecliNomeSize2  TWideStringFieldqueryClientescliUF	FieldNamecliUFSize  TWideStringFieldqueryClientescliCidade	FieldName	cliCidadeSize#  TWideStringFieldqueryClientescliNumTelefone	FieldNamecliNumTelefoneSize  TWideStringFieldqueryClientesconNome	FieldNameconNomeSize2  TIntegerFieldqueryClientescliStatus	FieldName	cliStatus   	TADOQueryqueryUpdateConReceber
Connectionconexao
Parameters Left8Top�  	TADOQueryqueryVerificaConta
Connectionconexao
CursorTypectStatic
ParametersNamepCotDescricao
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value   SQL.Strings@select cotCodigo from contas where cotDescricao = :pCotDescricao Left�TopH TAutoIncFieldqueryVerificaContacotCodigo	FieldName	cotCodigoReadOnly	   	TADOQueryqueryRelConAReceber
Connectionconexao
CursorTypectStatic
Parameters SQL.Strings�SELECT r.recDatVencimento, r.recValor,r.recFornecedor,r.recHistorico, t.tipLanDescricao, s.subTipDescricao FROM receber r,  tiposLancamento t, subTipos s(WHERE r.recTipLanCodigo = t.tipLanCodigo&AND r.recSubTipCodigo = s.subTipCodigoAND r.recDatPaga  is nullAND r.recValpago is nullgAND r.recDatVencimento between FORMAT(#01/10/2016#,"mm/dd/yyyy") AND FORMAT(#31/10/2016#,"mm/dd/yyyy")  Left@Topx TDateTimeField#queryRelConAReceberrecDatVencimento	FieldNamerecDatVencimento  	TBCDFieldqueryRelConAReceberrecValor	FieldNamerecValor	Precision  TWideStringField queryRelConAReceberrecFornecedor	FieldNamerecFornecedorSize2  TWideStringFieldqueryRelConAReceberrecHistorico	FieldNamerecHistoricoSize2  TWideStringField"queryRelConARecebertipLanDescricao	FieldNametipLanDescricaoSize2  TWideStringField"queryRelConARecebersubTipDescricao	FieldNamesubTipDescricaoSize2   	TADOQueryqueryConAPagarInsert
Connectionconexao
CursorTypectStatic
Parameters Left8Top  TWideStringFieldWideStringField2	FieldNamesubTipDescricaoSize2  TAutoIncFieldAutoIncField3	FieldNamesubTipCodigoReadOnly	   	TADOQueryquerySelContasAPagar
Connectionconexao
CursorTypectStatic
Parameters Left@Top� TAutoIncFieldAutoIncField4	FieldNameproIdReadOnly	  TIntegerFieldIntegerField1	FieldName	proStatus  TIntegerFieldIntegerField2	FieldNameproQtdCaixa   	TADOQueryqueryRelConAPagar
Connectionconexao
CursorTypectStatic
Parameters SQL.Strings    �SELECT p.pagDatVencimento, p.pagValor,p.pagPagarA,p.pagHistorico, t.tipLanDescricao, s.subTipDescricao FROM pagar p,  tiposLancamento t, subTipos s(WHERE p.pagTipLanCodigo = t.tipLanCodigo&AND p.pagSubTipCodigo = s.subTipCodigoAND p.pagDatPaga  is nullAND p.pagValpago is nullgAND p.pagDatVencimento between FORMAT(#07/09/2015#,"mm/dd/yyyy") AND FORMAT(#31/10/2015#,"mm/dd/yyyy")  Left� Top� TDateTimeField!queryRelConAPagarpagDatVencimento	FieldNamepagDatVencimento  	TBCDFieldqueryRelConAPagarpagValor	FieldNamepagValor	Precision  TWideStringFieldqueryRelConAPagarpagPagarA	FieldName	pagPagarASize2  TWideStringFieldqueryRelConAPagarpagHistorico	FieldNamepagHistoricoSize2  TWideStringField queryRelConAPagartipLanDescricao	FieldNametipLanDescricaoSize2  TWideStringField queryRelConAPagarsubTipDescricao	FieldNamesubTipDescricaoSize2   	TADOQueryquerySumMovimentacao
Connectionconexao
CursorTypectStatic
ParametersNamepCotDescricao
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value   SQL.Strings�SELECT c.cotCodigo, ((select sum(movValor) from movimentacao m1 where m1.movCotCodigo=c.cotCodigo and m1.movTipo=1)-(select sum(movValor) from movimentacao m1 where m1.movCotCodigo=c.cotCodigo and m1.movTipo=0)) as Valor#FROM movimentacao AS m, contas AS c"WHERE m.movCotCodigo = c.cotCodigoand c.cotDescricao ='2014'"and c.cotDescricao =:pCotDescricaogroup by c.cotCodigo Left-Top@ TAutoIncFieldquerySumMovimentacaocotCodigo	FieldName	cotCodigoReadOnly	  	TBCDFieldquerySumMovimentacaoValor	FieldNameValor	Precision   	TADOQueryquerySelCotCodigo
Connectionconexao
CursorTypectStatic
Parameters SQL.StringsSELECT c.cotCodigoFROM contas AS cWHERE c.cotDescricao ='2014' Left�Top@ TAutoIncFieldquerySelCotCodigocotCodigo	FieldName	cotCodigoReadOnly	   	TADOQueryqueryVerificaPrazoPagamento
Connectionconexao
CursorTypectStatic
Parameters SQL.StringsRselect przPagPar1,przPagPar2,przPagPar3,przPagPar4,przPagPar5 from prazoPagamentos LeftHTop� TFloatField%queryVerificaPrazoPagamentoprzPagPar1	FieldName
przPagPar1  TFloatField%queryVerificaPrazoPagamentoprzPagPar2	FieldName
przPagPar2  TIntegerField%queryVerificaPrazoPagamentoprzPagPar3	FieldName
przPagPar3  TIntegerField%queryVerificaPrazoPagamentoprzPagPar4	FieldName
przPagPar4  TIntegerField%queryVerificaPrazoPagamentoprzPagPar5	FieldName
przPagPar5   	TADOQueryqueryVerificaVendaAReceber
Connectionconexao
CursorTypectStatic
Parameters SQL.Stringsmselect f.forRazao,f.forComissao,f.forLiquidacao, v.venDatFaturamento,v.venValFatLiquido,v.venCodPedFornecedor from vendas v, fornecedores f Left�Top� TWideStringField"queryVerificaVendaAReceberforRazao	FieldNameforRazaoSize2  TFloatField%queryVerificaVendaAReceberforComissao	FieldNameforComissao  TDateTimeField+queryVerificaVendaARecebervenDatFaturamento	FieldNamevenDatFaturamento  	TBCDField*queryVerificaVendaARecebervenValFatLiquido	FieldNamevenValFatLiquido	Precision  TWideStringField-queryVerificaVendaARecebervenCodPedFornecedor	FieldNamevenCodPedFornecedorSize7  TIntegerField'queryVerificaVendaAReceberforLiquidacao	FieldNameforLiquidacao   	TADOTablev_relMovimentacao
Connectionconexao
CursorTypectStaticOnCalcFieldsv_relMovimentacaoCalcFields	TableNamev_relMovimentacaoLeft(Top�  TWideStringFieldv_relMovimentacaocotDescricao	FieldNamecotDescricaoSize2  TDateTimeFieldv_relMovimentacaomovData	FieldNamemovData  TWideStringFieldv_relMovimentacaomovDescricao	FieldNamemovDescricaoSized  TWideStringFieldv_relMovimentacaomovContato	FieldName
movContatoSize2  TWideStringField v_relMovimentacaotipLanDescricao	FieldNametipLanDescricaoSize2  TWideMemoFieldv_relMovimentacaoTipo	FieldNameTipoReadOnly	BlobType
ftWideMemo  	TBCDFieldv_relMovimentacaomovValor	FieldNamemovValorReadOnly	currency		Precision  TCurrencyFieldv_relMovimentacaoSaldo	FieldKindfkCalculated	FieldNameSaldo
Calculated	   TDataSourceDSv_relMovimentacaoDataSetv_relMovimentacaoLeft(Top�    