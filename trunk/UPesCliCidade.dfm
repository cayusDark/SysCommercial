object frmPesCliCidade: TfrmPesCliCidade
  Left = 0
  Top = 0
  Caption = 'Selecionar clientes por cidade'
  ClientHeight = 95
  ClientWidth = 299
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 6
    Width = 277
    Height = 48
    TabOrder = 0
    object Label1: TLabel
      Left = 4
      Top = 16
      Width = 33
      Height = 13
      Caption = 'Cidade'
    end
    object edtCidade: TEdit
      Left = 41
      Top = 13
      Width = 228
      Height = 21
      TabOrder = 0
      OnKeyPress = edtCidadeKeyPress
    end
  end
  object BitBtn1: TBitBtn
    Left = 210
    Top = 61
    Width = 75
    Height = 25
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 1
  end
end
