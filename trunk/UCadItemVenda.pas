unit UCadItemVenda;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UCadPadrao, ComCtrls, ExtCtrls, DBCtrls, Grids, DBGrids, StdCtrls,
  Buttons,DB, Mask;

type
  TfrmMovItemVenda = class(TfrmCadPadrao)
    Label3: TLabel;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    Label5: TLabel;
    DBEdtValUnitario: TDBEdit;
    Label6: TLabel;
    DBEdtDesconto: TDBEdit;
    Label7: TLabel;
    DBEdtQuantidade: TDBEdit;
    DBLkpCbBxProduto: TDBLookupComboBox;
    Label8: TLabel;
    DBEdtCodigo: TDBEdit;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid1TitleClick(Column: TColumn);
    procedure DBNavigator1Click(Sender: TObject; Button: TNavigateBtn);
    procedure edtPesquisaChange(Sender: TObject);
    procedure DBNvgInsDelEditBeforeAction(Sender: TObject;
      Button: TNavigateBtn);
    procedure DBNvgInsDelEditClick(Sender: TObject; Button: TNavigateBtn);
    procedure DBNvgPosCancelClick(Sender: TObject; Button: TNavigateBtn);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBEdtCodigoExit(Sender: TObject);
    procedure DBEdtDescontoKeyPress(Sender: TObject; var Key: Char);
    procedure DBEdtValUnitarioKeyPress(Sender: TObject; var Key: Char);
    procedure DBEdtQuantidadeKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMovItemVenda: TfrmMovItemVenda;

implementation

uses UDM, UFuncoes;

{$R *.dfm}

procedure TfrmMovItemVenda.DBEdtDescontoKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
    //chama funcao para aceitar somente numeros
    ApenasNumero(Key);
end;

procedure TfrmMovItemVenda.DBEdtQuantidadeKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
    //chama funcao para aceitar somente numeros
    ApenasNumero(Key);
end;

procedure TfrmMovItemVenda.DBEdtCodigoExit(Sender: TObject);
begin
  inherited;
    Dm.tabItemVendaiteVenValUnitario.Value := dm.tabProdutosproPreVenda.Value;
end;

procedure TfrmMovItemVenda.DBEdtValUnitarioKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
    //chama funcao para aceitar somente numeros
    ApenasNumero(Key);
end;

procedure TfrmMovItemVenda.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;
  //deixa o dbgrid zebrado
    If Odd(dm.tabItemVenda.RecNo) and (dm.tabItemVenda.State <> dsInsert) then
    begin //Lembre-se de colocar a unit DB na cl�usula uses na unit da tela.
      DBGrid1.Canvas.Brush.Color := clMoneyGreen; // muda a cor do pincel
      DBGrid1.Canvas.FillRect(Rect); // Preenche o fundo com a cor especificada
      DBGrid1.DefaultDrawDataCell(Rect,Column.Field,State);// desenha as c�lulas da grade
    end;
end;

procedure TfrmMovItemVenda.DBGrid1TitleClick(Column: TColumn);
begin
  inherited;
    Ascendente:= not Ascendente ;
    If Ascendente then
      Dm.tabItemVenda.IndexFieldNames := Column.FieldName + '   ASC'
    else
 	    Dm.tabItemVenda.IndexFieldNames := Column.FieldName + '    DESC';
end;

procedure TfrmMovItemVenda.DBNavigator1Click(Sender: TObject;
  Button: TNavigateBtn);
begin
  inherited;
    if (dm.tabItemVenda.State = dsInsert) or (dm.tabItemVenda.State = dsEdit)then
      DBLkpCbBxProduto.SetFocus;
end;

procedure TfrmMovItemVenda.DBNvgInsDelEditBeforeAction(Sender: TObject;
  Button: TNavigateBtn);
begin
  inherited;
    if (Button = nbInsert) then
    begin
      Dm.tabItemVenda.Cancel;
      Dm.tabItemVenda.Append;
      DBLkpCbBxProduto.SetFocus;
    end;
end;

procedure TfrmMovItemVenda.DBNvgInsDelEditClick(Sender: TObject; Button: TNavigateBtn);
var
  i : Integer;
begin
  inherited;
    if (dm.tabItemVenda.State = dsInsert)or (dm.tabItemVenda.State=dsEdit) then
    begin
      DBEdtCodigo.SetFocus;
      DBLkpCbBxProduto.ReadOnly := false;
      //desativa propriedade ReadOnly para coluna Produto
      DBGrid1.Columns[2].ReadOnly := false;
    end;



end;

procedure TfrmMovItemVenda.DBNvgPosCancelClick(Sender: TObject;
  Button: TNavigateBtn);
begin
  inherited;
    DBLkpCbBxProduto.ReadOnly := true;
    //desativa propriedade ReadOnly para coluna Produt0o
    DBGrid1.Columns[2].ReadOnly := true;
end;

procedure TfrmMovItemVenda.edtPesquisaChange(Sender: TObject);
begin
  inherited;
    //pesquisa parcial
    Dm.tabItemVenda.Locate( 'iteVenID',edtPesquisa.Text, [loCaseInsensitive, loPartialKey] );
end;

procedure TfrmMovItemVenda.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;

    dm.tabVenda.Post;
end;

procedure TfrmMovItemVenda.FormCreate(Sender: TObject);
begin
  inherited;
    dm.tabItemVenda.Close;//fecha a conexao com a tabela ItemVenda
end;

procedure TfrmMovItemVenda.FormShow(Sender: TObject);
begin
  inherited;
    dm.tabItemVenda.Open;//abre a conexao com a tabela itemVenda
    dm.tabVenda.Edit;
end;

end.
