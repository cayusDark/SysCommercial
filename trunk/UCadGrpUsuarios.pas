unit UCadGrpUsuarios;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.Grids, DB,
  Vcl.DBGrids;

type
  TfrmCadGrpUsuario = class(TForm)
    GroupBox2: TGroupBox;
    DBGrdGrpUsuarios: TDBGrid;
    GroupBox5: TGroupBox;
    BtnPrior: TBitBtn;
    BtnFirst: TBitBtn;
    BtnNext: TBitBtn;
    BtnLast: TBitBtn;
    BtnInsert: TBitBtn;
    BtnDelete: TBitBtn;
    BtnEdit: TBitBtn;
    BtnPost: TBitBtn;
    BtnCancel: TBitBtn;
    edtPesquisar: TEdit;
    GroupBox3: TGroupBox;
    btnGrpXUsuarios: TBitBtn;
    btnDirAcesso: TBitBtn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure BtnFirstClick(Sender: TObject);
    procedure BtnPriorClick(Sender: TObject);
    procedure BtnNextClick(Sender: TObject);
    procedure BtnLastClick(Sender: TObject);
    procedure BtnInsertClick(Sender: TObject);
    procedure BtnDeleteClick(Sender: TObject);
    procedure BtnEditClick(Sender: TObject);
    procedure BtnPostClick(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure edtPesquisarChange(Sender: TObject);
    procedure DBGrdGrpUsuariosDblClick(Sender: TObject);
    procedure DBGrdGrpUsuariosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure DBGrdGrpUsuariosKeyPress(Sender: TObject; var Key: Char);
    procedure btnGrpXUsuariosClick(Sender: TObject);
    procedure DBGrdGrpUsuariosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnDirAcessoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nRegistro: Integer;
  end;

var
  frmCadGrpUsuario: TfrmCadGrpUsuario;

implementation

{$R *.dfm}

uses UDM, UMain, UCadGrpXUsuarios, UCadDirAcesso;

procedure TfrmCadGrpUsuario.btnGrpXUsuariosClick(Sender: TObject);
begin
  frmCadGrpXUsuarios:= TfrmCadGrpXUsuarios.Create(Self); //cria��o manual
  frmCadGrpXUsuarios.ShowModal; //exibe a tela no modo modal
  frmCadGrpXUsuarios.Release; //libera a tela da mem�ria
  frmCadGrpXUsuarios:= nil; //atribui o conte�do nulo para a vari�vel frmCadGrpXUsuarios

  nRegistro :=DM.tabGrupoUsuariosgrpUsuCodigo.AsInteger;
end;

procedure TfrmCadGrpUsuario.BtnCancelClick(Sender: TObject);
begin
  //cancela a altera��o
  dm.tabGrupoUsuarios.Cancel;

  //desabilita os bot�es
  BtnFirst.Enabled := true;
  BtnPrior.Enabled := true;
  edtPesquisar.Enabled := true;
  BtnNext.Enabled := true;
  BtnLast.Enabled := true;
  BtnInsert.Enabled := true;
  BtnDelete.Enabled := true;
  BtnEdit.Enabled := true;
  DBGrdGrpUsuarios.ReadOnly := true;

  //habilita os bot�es
  BtnPost.Enabled := false;
  BtnCancel.Enabled := false;
end;

procedure TfrmCadGrpUsuario.BtnDeleteClick(Sender: TObject);
begin
  try
    //apaga um registro
    if MessageDlg('Deseja excluir o registro?', mtConfirmation,[mbYes, mbNo], 0) = mrYes then
      dm.tabGrupoUsuarios.Delete;
  except
    on E: Exception do
      MessageDlg('Erro ao excluir registro: '+E.Message+#13+
                 'Class name: '+E.ClassName,mtError,[mbOK],0);
  end;
end;

procedure TfrmCadGrpUsuario.btnDirAcessoClick(Sender: TObject);
begin
  frmCadDirAcesso:= TfrmCadDirAcesso.Create(Self); //cria��o manual
  frmCadDirAcesso.ShowModal; //exibe a tela no modo modal
  frmCadDirAcesso.Release; //libera a tela da mem�ria
  frmCadDirAcesso:= nil; //atribui o conte�do nulo para a vari�vel frmCadDirAcesso

  nRegistro :=DM.tabGrupoUsuariosgrpUsuCodigo.AsInteger;
end;

procedure TfrmCadGrpUsuario.BtnEditClick(Sender: TObject);
begin
  //edita um registro
  dm.tabGrupoUsuarios.Edit;

  //desabilita os bot�es
  BtnFirst.Enabled := false;
  BtnPrior.Enabled := false;
  edtPesquisar.Enabled := false;
  BtnNext.Enabled := false;
  BtnLast.Enabled := false;
  BtnInsert.Enabled := false;
  BtnDelete.Enabled := false;
  BtnEdit.Enabled := false;
  DBGrdGrpUsuarios.ReadOnly := false;

  //habilita os bot�es
  BtnPost.Enabled := true;
  BtnCancel.Enabled := true;

  DBGrdGrpUsuarios.SetFocus;
end;

procedure TfrmCadGrpUsuario.BtnFirstClick(Sender: TObject);
begin
  if (BtnNext.Enabled = false) or (BtnLast.Enabled = false) then
  begin
    BtnNext.Enabled := true;
    BtnLast.Enabled := true;
  end;

  BtnPrior.Enabled := false;
  BtnFirst.Enabled := false;

  //primeiro registro
  dm.tabGrupoUsuarios.First;
end;

procedure TfrmCadGrpUsuario.BtnInsertClick(Sender: TObject);
begin
  //adiciona um registro
  dm.tabGrupoUsuarios.Insert;

  //desabilita os bot�es
  BtnFirst.Enabled := false;
  BtnPrior.Enabled := false;
  edtPesquisar.Enabled := false;
  BtnNext.Enabled := false;
  BtnLast.Enabled := false;
  BtnInsert.Enabled := false;
  BtnDelete.Enabled := false;
  BtnEdit.Enabled := false;
  DBGrdGrpUsuarios.ReadOnly := false;

  //habilita os bot�es
  BtnPost.Enabled := true;
  BtnCancel.Enabled := true;

  DBGrdGrpUsuarios.SetFocus;
end;

procedure TfrmCadGrpUsuario.BtnLastClick(Sender: TObject);
begin
  if (BtnPrior.Enabled = false) or (BtnFirst.Enabled = false) then
  begin
    BtnPrior.Enabled := true;
    BtnFirst.Enabled := true;
  end;

  BtnNext.Enabled :=false;
  BtnLast.Enabled := false;

  //�ltimo registro
  dm.tabGrupoUsuarios.Last;
end;

procedure TfrmCadGrpUsuario.BtnNextClick(Sender: TObject);
begin
  //enquanto n�o for o ultimo registro
  if dm.tabGrupoUsuarios.Eof then
  begin
    BtnNext.Enabled :=false;
    BtnLast.Enabled := false;
  end
  else
  begin
    if (BtnPrior.Enabled = false) or (BtnFirst.Enabled = false) then
    begin
      BtnPrior.Enabled := true;
      BtnFirst.Enabled := true;
    end;

    //pr�ximo registro
    dm.tabGrupoUsuarios.Next;
  end;
end;

procedure TfrmCadGrpUsuario.BtnPostClick(Sender: TObject);
begin
  //confirma as altera�oes no registro
  if (dm.tabGrupoUsuarios.State = dsEdit) or (dm.tabGrupoUsuarios.State = dsInsert) then
    dm.tabGrupoUsuarios.Post;

  //desabilita os bot�es
  BtnFirst.Enabled := true;
  BtnPrior.Enabled := true;
  edtPesquisar.Enabled := true;
  BtnNext.Enabled := true;
  BtnLast.Enabled := true;
  BtnInsert.Enabled := true;
  BtnDelete.Enabled := true;
  BtnEdit.Enabled := true;
  DBGrdGrpUsuarios.ReadOnly := true;

  //habilita os bot�es
  BtnPost.Enabled := false;
  BtnCancel.Enabled := false;
end;

procedure TfrmCadGrpUsuario.BtnPriorClick(Sender: TObject);
begin
  //verifica se esta no primeiro registro
  if dm.tabGrupoUsuarios.Bof then
  begin
    BtnPrior.Enabled := false;
    BtnFirst.Enabled := false;
  end
  else
  begin
    if (BtnNext.Enabled = false) or (BtnLast.Enabled = false) then
    begin
      BtnNext.Enabled := true;
      BtnLast.Enabled := true;
    end;

    //registro anterior
    dm.tabGrupoUsuarios.Prior;
  end;
end;

procedure TfrmCadGrpUsuario.DBGrdGrpUsuariosDblClick(Sender: TObject);
begin
  if (dm.tabGrupoUsuarios.State <> dsInsert) or (dm.tabGrupoUsuarios.State <> dsEdit) then
  begin
    ShowMessage('Clique editar ou inserir para modifica��o no grid!!');
    DBGrdGrpUsuarios.SetFocus;
  end;

end;

procedure TfrmCadGrpUsuario.DBGrdGrpUsuariosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  //deixa o dbgrid zebrado
  If Odd(dm.tabGrupoUsuarios.RecNo) and (dm.tabGrupoUsuarios.State <> dsInsert) then
  begin //Lembre-se de colocar a unit DB na cl�usula uses na unit da tela.
      DBGrdGrpUsuarios.Canvas.Brush.Color := clMoneyGreen; // muda a cor do pincel
      DBGrdGrpUsuarios.Canvas.FillRect(Rect); // Preenche o fundo com a cor especificada
      DBGrdGrpUsuarios.DefaultDrawDataCell(Rect,Column.Field,State);// desenha as c�lulas da grade
  end;

end;

procedure TfrmCadGrpUsuario.DBGrdGrpUsuariosKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  confExcluir: Integer;
  nRegistro: Integer;
  nReg: Integer;
begin
  nRegistro := DM.tabGrupoUsuariosgrpUsuCodigo.AsInteger;

  if Key = VK_DELETE Then
  begin
    confExcluir:= Application.MessageBox('Deseja excluir o lan�amento ?', 'Aten��o', MB_YesNo+mb_DefButton2+mb_IconQuestion);
    if confExcluir = IDYes then
    begin
      with dm.tabGrupoUsuarios do
      begin
        DM.queryItemVenda.Close;
        DM.queryItemVenda.SQL.Clear;
        DM.queryItemVenda.SQL.Add('DELETE FROM grupoUsuarios WHERE grpUsuCodigo =:nReg');
        DM.queryItemVenda.Parameters.ParamByName('nReg').Value := nRegistro;
        DM.queryItemVenda.ExecSQL;
      end;
      dm.tabGrupoUsuarios.Close;
      dm.tabGrupoUsuarios.Open;
    end;
  end;
end;

procedure TfrmCadGrpUsuario.DBGrdGrpUsuariosKeyPress(Sender: TObject;
  var Key: Char);
begin
  Key := UpCase(Key)
end;

procedure TfrmCadGrpUsuario.edtPesquisarChange(Sender: TObject);
begin
  //pesquisa aproximada por descri�ao
  dm.tabGrupoUsuarios.Locate('grpUsuDescricao', edtPesquisar.Text, [loCaseInsensitive, loPartialKey]);
end;

procedure TfrmCadGrpUsuario.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  //FECHA a conex�o com a tabela
  if (dm.tabGrupoUsuarios.State=dsEdit) or (dm.tabGrupoUsuarios.State=dsInsert) then
  begin
    if MessageDlg('Cadastro em modo de inser��o ou edi��o. Deseja fechar a tela?', mtConfirmation,[mbYes, mbNo], 0) = mrYes then
    begin
      dm.tabGrupoUsuarios.Close;
      Action := caFree; //a��o do objeto TCloseAction para fechar a tela
    end
    else
      Action := caNone ; //a��o do objeto TCloseAction para n�o fechar a tela
  end
  else
    dm.tabGrupoUsuarios.Close;
end;

procedure TfrmCadGrpUsuario.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  confExcluir: Integer;
  nRegistro: Integer;
  nReg: Integer;
begin
  nRegistro := DM.tabGrupoUsuariosgrpUsuCodigo.AsInteger;

  if Key = VK_DELETE Then
  begin
    confExcluir:= Application.MessageBox('Deseja excluir o lan�amento ?', 'Aten��o', MB_YesNo+mb_DefButton2+mb_IconQuestion);
    if confExcluir = IDYes then
    begin
      with DM.queryGrpUsuarios do
      begin
        Close;
        SQL.Clear;
        SQL.Add('DELETE FROM GrupoUsuarios WHERE grpUsuCodigo =:nReg');
        Parameters.ParamByName('nReg').Value := nRegistro;
        ExecSQL;
      end;
      dm.tabGrupoUsuarios.Close;
      dm.tabGrupoUsuarios.Open;
    end;
  end;
end;

procedure TfrmCadGrpUsuario.FormKeyPress(Sender: TObject; var Key: Char);
begin
  key := UpCase(key);
end;

procedure TfrmCadGrpUsuario.FormShow(Sender: TObject);
begin
  //abre a conex�o com a tabela
  dm.tabGrupoUsuarios.Open;
end;

end.
