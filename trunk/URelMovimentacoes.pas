unit URelMovimentacoes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, frxClass, frxExportPDF, frxDBSet,
  Vcl.StdCtrls, Vcl.Buttons, Vcl.ComCtrls;

type
  TfrmRelMovimentacao = class(TForm)
    GroupBox1: TGroupBox;
    CmbBxConta: TComboBox;
    GroupBox4: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    DtTmPckInicio: TDateTimePicker;
    DtTmPckFim: TDateTimePicker;
    GroupBox6: TGroupBox;
    BitBtn1: TBitBtn;
    frxRptRelMovimentacoes: TfrxReport;
    frxDBDSRelMovimentacoes: TfrxDBDataset;
    frxPDFExport1: TfrxPDFExport;
    procedure FormShow(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DtTmPckInicioExit(Sender: TObject);
    procedure DtTmPckFimExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelMovimentacao: TfrmRelMovimentacao;

implementation

{$R *.dfm}

uses UDM, UFuncoes;

procedure TfrmRelMovimentacao.BitBtn1Click(Sender: TObject);
var
  datInicio, datFim, filtro :string;
begin
  try
    //filtro := '(movData BETWEEN (#'+DateToStr(FormatDateTime('dd/mm/yyyy',DtTmPckInicio.DateTime))+'#) AND (#'+ DateToStr(FormatDateTime('dd/mm/yyyy',DtTmPckFim.DateTime))+'#))';
    //ShowMessage(filtro);
    DM.saldo := 0;
    if dm.v_relMovimentacao.IsEmpty then
      MessageDlg('N�o h� dados!',mtInformation,[mbOK],0)
    else
      begin
        with dm.v_relMovimentacao do
      begin
        Filtered := false;
        Filter:= '(movData>=(#'+FormatDateTime('dd/mm/yyyy',DtTmPckInicio.DateTime)+
                 '#) AND movData<=(#'+FormatDateTime('dd/mm/yyyy',DtTmPckFim.DateTime)+'#))'+
                 'AND cotDescricao ='+QuotedStr(CmbBxConta.Items[CmbBxConta.ItemIndex]);
        Filtered := true;
      end;

      if dm.v_relMovimentacao.IsEmpty then
        MessageDlg('N�o h� dados!',mtInformation,[mbOK],0)
      else
        begin
          datInicio := DateToStr(DtTmPckInicio.DateTime);
          frxRptRelMovimentacoes.Variables['datInicio'] := QuotedStr(datInicio);
          datFim := DateToStr(DtTmPckFim.DateTime);
          frxRptRelMovimentacoes.Variables['datFim'] := QuotedStr(datFim);
          dm.saldo :=0;
          frxRptRelMovimentacoes.ShowReport();
        end;
      end;
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro: '+ frmRelMovimentacao.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro: '+ frmRelMovimentacao.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmRelMovimentacao.DtTmPckFimExit(Sender: TObject);
begin
  if DtTmPckFim.DateTime < DtTmPckInicio.DateTime then
  begin
    MessageDlg('Data final deve ser maior ou igual � data inicial', mtWarning,[mbOK], 0);
    DtTmPckFim.SetFocus;
  end;
end;

procedure TfrmRelMovimentacao.DtTmPckInicioExit(Sender: TObject);
begin
  if DtTmPckInicio.DateTime > DtTmPckFim.DateTime then
  begin
    MessageDlg('Data inicial deve ser menor ou igual � data final', mtWarning,[mbOK], 0);
    DtTmPckInicio.SetFocus;
  end;

end;

procedure TfrmRelMovimentacao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  //fecha a conex�o com a tabela de contas
  dm.tabContas.Close;
  DM.v_relMovimentacao.Close;

  //limpa o campo cmbBxConta
  CmbBxConta.Clear;
end;

procedure TfrmRelMovimentacao.FormShow(Sender: TObject);
var
  rCampo : string;
begin
  //abre a conexao com a tabela
  dm.tabContas.Open;

  if not DM.v_relMovimentacao.IsEmpty then
  begin
    DM.v_relMovimentacao.Open;

    //filtro de fornecedores

    dm.tabContas.First;
    while not DM.tabContas.Eof do
    begin
      rCampo:= dm.tabContascotDescricao.AsString;
      CmbBxConta.Items.Add(rCampo);
      DM.tabContas.Next;
    end;
    CmbBxConta.ItemIndex:=0;

    DtTmPckInicio.Date := now-30;
    DtTmPckFim.Date := now;
  end;

end;

end.
