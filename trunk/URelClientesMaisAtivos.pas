unit URelClientesMaisAtivos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, frxClass,
  frxExportPDF, frxDBSet, System.UITypes,Data.DB;

type
  TfrmRelClientesMaisAtivos = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    cmbBxRepresentada: TComboBox;
    lblMesReferencia: TLabel;
    lblAnoReferencia: TLabel;
    RdoBtnMes: TRadioButton;
    RdoBtnTrimestre: TRadioButton;
    BtnImprimir: TBitBtn;
    CmbBxMesReferencia: TComboBox;
    CmbBxAnoReferencia: TComboBox;
    lblTrimestre: TLabel;
    cmbBxTrimeste: TComboBox;
    frxDBDSTClientesMaisAtivos: TfrxDBDataset;
    frxRptRelClientesMaisAtivos: TfrxReport;
    frxPDFExport: TfrxPDFExport;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure RdoBtnMesClick(Sender: TObject);
    procedure RdoBtnTrimestreClick(Sender: TObject);
    procedure BtnImprimirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelClientesMaisAtivos: TfrmRelClientesMaisAtivos;

implementation

{$R *.dfm}

uses UDM, UFuncoes;

procedure TfrmRelClientesMaisAtivos.BtnImprimirClick(Sender: TObject);
begin
 try
  //verifica quando � os 20% do total dos clientes ativos
  with dm.queryRelClientesMaisAtivos20Porcento do
  begin
    Close;
    Open;
  end;

  if dm.queryRelClientesMaisAtivos20PorcentoFloatField20Porcento.AsInteger = 0 then
    MessageDlg('N�o h� dados!', mtConfirmation,[mbOK], 0)
  else
    begin
      with dm.queryRelClientesMaisAtivos do
      begin
        Close;
        SQL.Clear;
        SQL.Add('SELECT TOP '+FloatToStr(dm.queryRelClientesMaisAtivos20PorcentoFloatField20Porcento.Value));
        SQL.Add('   c.forRazao,');
        SQL.Add('   a.cliRazSocial,');
        SQL.Add('   a.cliCidade, ');
        SQL.Add('   a.cliUf,');
        SQL.Add('   SUM (b.venValLiquido) AS venValLiquido,');
        SQL.Add('   SUM (b.venValFatLiquido) AS venValFatLiquido');
        SQL.Add('FROM clientes AS a, vendas AS b, fornecedores AS c, metas AS d ');
        SQL.Add('WHERE a.cliCodigo = b.venCliCodigo');
        SQL.Add('AND b.venForCodigo = c.forCodigo');
        SQL.Add('AND c.ForCodigo = d.metForCodigo');
        SQL.Add('AND b.venTipo = 1');
        SQL.Add('AND c.ForRazao = :pForRazao');
        SQL.Add('AND b.venDatVenda BETWEEN   d.MetDatInicio AND d.metDatFim');

        if RdoBtnMes.Checked = true then
        begin
          SQL.Add('AND d.metMesReferencia = :pMes');
          SQL.Add('AND d.metAnoReferencia = :pAno');
        end
        else
          if RdoBtnTrimestre.Checked = true then
            case (cmbBxTrimeste.ItemIndex) of
              0:begin
                  SQL.Add('AND b.venDatVenda BETWEEN (SELECT metDatInicio');
                  SQL.Add('                           FROM metas AS e');
                  SQL.Add('                           WHERE e.metMesReferencia ='+ QuotedStr('JANEIRO'));
                  SQL.Add('                           AND e.metAnoReferencia = '+QuotedStr(CmbBxAnoReferencia.Items[CmbBxAnoReferencia.ItemIndex]));
                  SQL.Add('                           AND e.metForCodigo = c.forCodigo)');
                  SQL.Add('                           AND (SELECT metDatFim FROM metas AS e');
                  SQL.Add('                                WHERE e.metMesReferencia = '+QuotedStr('MAR�O'));
                  SQL.Add('                                AND e.metAnoReferencia = '+QuotedStr(CmbBxAnoReferencia.Items[CmbBxAnoReferencia.ItemIndex]));
                  SQL.Add('                                 AND e.MetForCodigo = c.forCodigo)');
                end;
              1:begin
                  SQL.Add('AND b.venDatVenda BETWEEN (SELECT metDatInicio');
                  SQL.Add('                           FROM metas AS e');
                  SQL.Add('                           WHERE e.metMesReferencia = '+ QuotedStr('ABRIL'));
                  SQL.Add('                           AND e.metAnoReferencia = '+QuotedStr(CmbBxAnoReferencia.Items[CmbBxAnoReferencia.ItemIndex]));
                  SQL.Add('                           AND e.metForCodigo = c.forCodigo)');
                  SQL.Add('                           AND (SELECT metDatFim FROM metas AS e');
                  SQL.Add('                                WHERE e.metMesReferencia ='+ QuotedStr(' JUNHO'));
                  SQL.Add('                                AND e.metAnoReferencia = '+QuotedStr(CmbBxAnoReferencia.Items[CmbBxAnoReferencia.ItemIndex]));
                  SQL.Add('                                 AND e.MetForCodigo = c.forCodigo)');
                 end;
              2: begin
                  SQL.Add('AND b.venDatVenda BETWEEN (SELECT metDatInicio');
                  SQL.Add('                           FROM metas AS e');
                  SQL.Add('                           WHERE e.metMesReferencia = '+ QuotedStr('JULHO'));
                  SQL.Add('                           AND e.metAnoReferencia = '+QuotedStr(CmbBxAnoReferencia.Items[CmbBxAnoReferencia.ItemIndex]));
                  SQL.Add('                           AND e.metForCodigo = c.forCodigo)');
                  SQL.Add('                           AND (SELECT metDatFim FROM metas AS e');
                  SQL.Add('                                WHERE e.metMesReferencia = '+ QuotedStr('SETEMBRO'));
                  SQL.Add('                                AND e.metAnoReferencia = '+QuotedStr(CmbBxAnoReferencia.Items[CmbBxAnoReferencia.ItemIndex]));
                  SQL.Add('                                 AND e.MetForCodigo = c.forCodigo)');
                 end;
              3: begin
                  SQL.Add('AND b.venDatVenda BETWEEN (SELECT metDatInicio');
                  SQL.Add('                           FROM metas AS e');
                  SQL.Add('                           WHERE e.metMesReferencia = '+ QuotedStr('OUTUBRO'));
                  SQL.Add('                           AND e.metAnoReferencia = '+QuotedStr(CmbBxAnoReferencia.Items[CmbBxAnoReferencia.ItemIndex]));
                  SQL.Add('                           AND e.metForCodigo = c.forCodigo)');
                  SQL.Add('                           AND (SELECT metDatFim FROM metas AS e');
                  SQL.Add('                                WHERE e.metMesReferencia = '+ QuotedStr('DEZEMBRO'));
                  SQL.Add('                                AND e.metAnoReferencia = '+QuotedStr(CmbBxAnoReferencia.Items[CmbBxAnoReferencia.ItemIndex]));
                  SQL.Add('                                 AND e.MetForCodigo = c.forCodigo)');
                 end;
        end;

        SQL.Add('GROUP BY c.forRazao, a.cliRazSocial,');
        SQL.Add('         a.cliCidade,');
        SQL.Add('         a.cliUf');
        SQL.Add('ORDER BY SUM (b.venValLiquido) DESC,');
        SQL.Add('         SUM (b.venValFatLiquido) ');

        //parametros
        Parameters.ParamByName('pForRazao').Value := cmbBxRepresentada.Items[cmbBxRepresentada.ItemIndex];
        if RdoBtnMes.Checked = true then
        begin
          Parameters.ParamByName('pMes').Value := CmbBxMesReferencia.Items[CmbBxMesReferencia.ItemIndex];
          Parameters.ParamByName('pAno').Value := CmbBxAnoReferencia.Items[CmbBxAnoReferencia.ItemIndex];
        end;
        Open;
      end;

      if dm.queryRelClientesMaisAtivos.IsEmpty then
        MessageDlg('N�o h� dados!', mtConfirmation,[mbOK], 0)
      else
        begin
          if RdoBtnMes.Checked = true then
            frxRptRelClientesMaisAtivos.Variables['mes'] := QuotedStr(CmbBxMesReferencia.Items[CmbBxMesReferencia.ItemIndex])
          else
            if RdoBtnTrimestre.Checked = true then
              frxRptRelClientesMaisAtivos.Variables['mes'] := QuotedStr(cmbBxTrimeste.Items[cmbBxTrimeste.ItemIndex]);
          frxRptRelClientesMaisAtivos.Variables['ano'] := QuotedStr(CmbBxAnoReferencia.Items[CmbBxAnoReferencia.ItemIndex]);
          frxRptRelClientesMaisAtivos.ShowReport();
        end;
    end;

 except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro: '+ frmRelClientesMaisAtivos.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro:'+ frmRelClientesMaisAtivos.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
 end;
end;

procedure TfrmRelClientesMaisAtivos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  //fecha a conex�o com a tabela de fornecedor
  dm.tabFornecedores.Close;
end;

procedure TfrmRelClientesMaisAtivos.FormShow(Sender: TObject);
var
  rCampo : string;
  mes: integer;
begin
  //abre a conex�o com a tabela de fornecedor
  dm.tabFornecedores.Open;

  ///////////////////////INICIALIZA��O DOS COMBOBOX ////////////////////////
  //filtro de fornecedores
  if not DM.tabFornecedores.Eof then
  begin
    dm.tabFornecedores.First;
    while not DM.tabFornecedores.Eof do
    begin
      if dm.tabFornecedoresforStatus.AsInteger = 1 then
      begin
        rCampo:= dm.tabFornecedoresforRazao.AsString;
        CmbBxRepresentada.Items.Add(rCampo);
      end;
      DM.tabFornecedores.Next;
    end;
    CmbBxRepresentada.ItemIndex:=0;
  end;

  //limpa o CmbBxAnoReferencia
  CmbBxAnoReferencia.Items.Clear;

  //adiciona o campos
  rCampo:= FormatDateTime('yyyy',IncMonth(Now(),-24));
  CmbBxAnoReferencia.Items.Add(rCampo);
  rCampo:= FormatDateTime('yyyy',IncMonth(Now(),-12));
  CmbBxAnoReferencia.Items.Add(rCampo);
  rCampo:= FormatDateTime('yyyy',Now);
  CmbBxAnoReferencia.Items.Add(rCampo);
  CmbBxAnoReferencia.ItemIndex:=2;

  //posicionamento do cmbBxMesReferencia
  mes := StrToInt(FormatDateTime('MM', Now));
  CmbBxMesReferencia.ItemIndex := mes -1;

end;

procedure TfrmRelClientesMaisAtivos.RdoBtnMesClick(Sender: TObject);
begin
  lblMesReferencia.Visible := true;
  CmbBxMesReferencia.Visible := true;
  lblTrimestre.Visible := false;
  cmbBxTrimeste.Visible := false;
end;

procedure TfrmRelClientesMaisAtivos.RdoBtnTrimestreClick(Sender: TObject);
begin
  lblMesReferencia.Visible := false;
  CmbBxMesReferencia.Visible := false;
  lblTrimestre.Visible := true;
  cmbBxTrimeste.Visible := true;
end;

end.
