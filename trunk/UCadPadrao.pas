unit UCadPadrao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls, DBCtrls, Grids, DBGrids, Buttons, StdCtrls;

type
  TfrmCadPadrao = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    Label1: TLabel;
    edtPesquisa: TEdit;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    DBGrid1: TDBGrid;
    DBNvgInsDelEdit: TDBNavigator;
    DBNvgPosCancel: TDBNavigator;
    DBNvigFirPriNexLast: TDBNavigator;
    StatusBar1: TStatusBar;
    procedure SpeedButton3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBNvgInsDelEditBeforeAction(Sender: TObject; Button: TNavigateBtn);
    procedure DBNvgPosCancelClick(Sender: TObject; Button: TNavigateBtn);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
    Ascendente: Boolean;
  end;

var
  frmCadPadrao: TfrmCadPadrao;

implementation

{$R *.dfm}

procedure TfrmCadPadrao.DBNvgInsDelEditBeforeAction(Sender: TObject;
  Button: TNavigateBtn);
begin
  if Button in [nbInsert, nbEdit] then
  begin
    GroupBox1.Enabled := False; //desabilita o GroupBox1 e todos seus componentes.
    GroupBox2.Enabled := False;//desabilita o GroupBox2 e todos seus componentes.
    DBNvigFirPriNexLast.Enabled := False;//desabilita o DBNAvigator1
    DBNvgInsDelEdit.Enabled := False;//desabilita o DBNAvigator2
    DBNvgPosCancel.Enabled := True;
  end;
end;

procedure TfrmCadPadrao.DBNvgPosCancelClick(Sender: TObject;
  Button: TNavigateBtn);
begin
  GroupBox1.Enabled := True;
  GroupBox2.Enabled := True;
  DBNvigFirPriNexLast.Enabled := True;
  DBNvgInsDelEdit.Enabled := True;
  DBNvgPosCancel.Enabled := False;
end;

procedure TfrmCadPadrao.FormKeyPress(Sender: TObject; var Key: Char);
begin
//troca TAB por ENTER
  if Key = #13 then
  begin
    keybd_event(9,0,0,0);
    Key := #0;
  end;
end;

procedure TfrmCadPadrao.FormShow(Sender: TObject);
begin
  Ascendente := False;
end;

procedure TfrmCadPadrao.SpeedButton3Click(Sender: TObject);
begin
  close;//fecha a tela
end;

end.
