unit URelMeta;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ComCtrls,
  Vcl.DBCtrls, frxClass, frxDBSet, frxExportPDF;

type
  TfrmRelMeta = class(TForm)
    GroupBox1: TGroupBox;
    CmbBxUf: TComboBox;
    ChkBxUf: TCheckBox;
    GroupBox3: TGroupBox;
    ChkBxRepresentada: TCheckBox;
    GroupBox4: TGroupBox;
    GroupBox6: TGroupBox;
    BitBtn1: TBitBtn;
    CmbBxMesReferencia: TComboBox;
    Label8: TLabel;
    CmbBxAnoReferencia: TComboBox;
    Label9: TLabel;
    frxDBDSMeta: TfrxDBDataset;
    frxRptMeta: TfrxReport;
    CmbBxRepresentada: TComboBox;
    frxPDFExport1: TfrxPDFExport;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ChkBxUfClick(Sender: TObject);
    procedure ChkBxRepresentadaClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRelMeta: TfrmRelMeta;

implementation

{$R *.dfm}

uses UDM;

procedure TfrmRelMeta.BitBtn1Click(Sender: TObject);
begin
  {chamada de relat�rio de meta
  v.1.1.0: altera��o no filtro do relat�rio e inser��o dos campos metGlobal e metGloPercentual }
  if (ChkBxUf.Checked = false) and (ChkBxRepresentada.Checked = false)then
  begin
    with dm.queryRelMeta do
    begin
      close;
      SQL.Clear;
      SQL.Add('SELECT a.metCodigo, a.metDatInicio, a.metDatFim, a.metValor, a.metValAtual,');
      SQL.Add('a.metValFaturado, a.metValFatLiquido, a.metMesReferencia,');
      SQL.Add('a.metAnoReferencia, b.forRazao,(SELECT SUM (a.metValor)');
      SQL.Add('FROM metas AS a, fornecedores AS b');
      SQL.Add('WHERE a.metForCodigo = b.forCodigo');
      SQL.Add('AND a.metMesReferencia=:pMetMesReferencia1');
      SQL.Add('AND a.metAnoReferencia = :pMetAnoReferencia1) AS metGlobal');
      SQL.Add('FROM metas AS a, fornecedores AS b');
      SQL.Add('WHERE a.metForCodigo = b.forCodigo');
      SQL.Add('AND b.ForRazao = :pForRazao');
      SQL.Add('AND b.forUF = :pForUf');
      SQL.Add('AND a.metMesReferencia = :pMetMesReferencia');
      SQL.Add('AND a.metAnoReferencia = :pMetAnoReferencia');
      SQL.Add('ORDER BY b.forRazao, a.metDatInicio');

      //passagem de paramentros
      Parameters.ParamByName('pForRazao').Value := CmbBxRepresentada.Items[CmbBxRepresentada.ItemIndex];
      Parameters.ParamByName('pForUf').Value := CmbBxUf.Items[CmbBxUf.ItemIndex];
      Parameters.ParamByName('pMetMesReferencia1').Value := CmbBxMesReferencia.Items[CmbBxMesReferencia.ItemIndex];
      Parameters.ParamByName('pMetAnoReferencia1').Value := CmbBxAnoReferencia.Items[CmbBxAnoReferencia.ItemIndex];
      Parameters.ParamByName('pMetMesReferencia').Value := CmbBxMesReferencia.Items[CmbBxMesReferencia.ItemIndex];
      Parameters.ParamByName('pMetAnoReferencia').Value := CmbBxAnoReferencia.Items[CmbBxAnoReferencia.ItemIndex];

      Open;
    end;
    if dm.queryRelMeta.IsEmpty then
      MessageDlg('N�o h� dados!', mtConfirmation,[mbOK], 0)
    else
      begin
        frxRptMeta.Variables['uF'] := QuotedStr(CmbBxUf.Items[CmbBxUf.ItemIndex]);
        frxRptMeta.Variables['mes'] := QuotedStr(CmbBxMesReferencia.Items[CmbBxMesReferencia.ItemIndex]);
        frxRptMeta.Variables['ano'] := QuotedStr(CmbBxAnoReferencia.Items[CmbBxAnoReferencia.ItemIndex]);
        frxRptMeta.ShowReport();
      end;
  end
  else if (ChkBxUf.Checked = true) and (ChkBxRepresentada.Checked = false) then
    begin
      with dm.queryRelMeta do
      begin
        close;
        SQL.Clear;
        SQL.Add('SELECT a.metCodigo, a.metDatInicio, a.metDatFim, a.metValor, a.metValAtual,');
        SQL.Add('a.metValFaturado, a.metValFatLiquido, a.metMesReferencia,');
        SQL.Add('a.metAnoReferencia, b.forRazao,(SELECT SUM (a.metValor)');
        SQL.Add('FROM metas AS a, fornecedores AS b');
        SQL.Add('WHERE a.metForCodigo = b.forCodigo');
        SQL.Add('AND a.metMesReferencia=:pMetMesReferencia1');
        SQL.Add('AND a.metAnoReferencia = :pMetAnoReferencia1) AS metGlobal');
        SQL.Add('FROM metas AS a, fornecedores AS b');
        SQL.Add('WHERE a.metForCodigo = b.forCodigo');
        SQL.Add('AND b.ForRazao = :pForRazao');
        //SQL.Add('AND b.forUF = :pForUf');
        SQL.Add('AND a.metMesReferencia = :pMetMesReferencia');
        SQL.Add('AND a.metAnoReferencia = :pMetAnoReferencia');
        SQL.Add('ORDER BY b.forRazao, a.metDatInicio');

        //passagem de paramentros
        Parameters.ParamByName('pForRazao').Value := CmbBxRepresentada.Items[CmbBxRepresentada.ItemIndex];
        //Parameters.ParamByName('pForUf').Value := CmbBxUf.Items(CmbBxUf.ItemIndex);
        Parameters.ParamByName('pMetMesReferencia1').Value := CmbBxMesReferencia.Items[CmbBxMesReferencia.ItemIndex];
        Parameters.ParamByName('pMetAnoReferencia1').Value := CmbBxAnoReferencia.Items[CmbBxAnoReferencia.ItemIndex];
        Parameters.ParamByName('pMetMesReferencia').Value := CmbBxMesReferencia.Items[CmbBxMesReferencia.ItemIndex];
        Parameters.ParamByName('pMetAnoReferencia').Value := CmbBxAnoReferencia.Items[CmbBxAnoReferencia.ItemIndex];
        Open;
      end;
      if dm.queryRelMeta.IsEmpty then
        MessageDlg('N�o h� dados!', mtConfirmation,[mbOK], 0)
      else
        begin
          frxRptMeta.Variables['uF'] := QuotedStr('Todos');
          frxRptMeta.Variables['mes'] := QuotedStr(CmbBxMesReferencia.Items[CmbBxMesReferencia.ItemIndex]);
          frxRptMeta.Variables['ano'] := QuotedStr(CmbBxAnoReferencia.Items[CmbBxAnoReferencia.ItemIndex]);
          frxRptMeta.ShowReport();
        end;
    end
    else if (ChkBxUf.Checked = true) and (ChkBxRepresentada.Checked = true) then
      begin
        with dm.queryRelMeta do
        begin
          close;
          SQL.Clear;
          SQL.Add('SELECT a.metCodigo, a.metDatInicio, a.metDatFim, a.metValor, a.metValAtual,');
          SQL.Add('a.metValFaturado, a.metValFatLiquido, a.metMesReferencia,');
          SQL.Add('a.metAnoReferencia, b.forRazao,(SELECT SUM (a.metValor)');
          SQL.Add('FROM metas AS a, fornecedores AS b');
          SQL.Add('WHERE a.metForCodigo = b.forCodigo');
          SQL.Add('AND a.metMesReferencia=:pMetMesReferencia1');
          SQL.Add('AND a.metAnoReferencia = :pMetAnoReferencia1) AS metGlobal');
          SQL.Add('FROM metas AS a, fornecedores AS b');
          SQL.Add('WHERE a.metForCodigo = b.forCodigo');
          //SQL.Add('AND b.ForRazao = :pForRazao');
          //SQL.Add('AND b.forUF = :pForUf');
          SQL.Add('AND a.metMesReferencia = :pMetMesReferencia');
          SQL.Add('AND a.metAnoReferencia = :pMetAnoReferencia');
          SQL.Add('ORDER BY b.forRazao, a.metDatInicio');

          //passagem de paramentros
          //Parameters.ParamByName('pForRazao').Value := DBCBxRepresentada.Items(DBCBxRepresentada.ItemIndex);
          //Parameters.ParamByName('pForUf').Value := CmbBxUf.Items[CmbBxUf.ItemIndex];
          Parameters.ParamByName('pMetMesReferencia1').Value := CmbBxMesReferencia.Items[CmbBxMesReferencia.ItemIndex];
          Parameters.ParamByName('pMetAnoReferencia1').Value := CmbBxAnoReferencia.Items[CmbBxAnoReferencia.ItemIndex];
          Parameters.ParamByName('pMetMesReferencia').Value := CmbBxMesReferencia.Items[CmbBxMesReferencia.ItemIndex];
          Parameters.ParamByName('pMetAnoReferencia').Value := CmbBxAnoReferencia.Items[CmbBxAnoReferencia.ItemIndex];

          Open;
        end;
        if dm.queryRelMeta.IsEmpty then
          MessageDlg('N�o h� dados!', mtConfirmation,[mbOK], 0)
        else
          begin
            frxRptMeta.Variables['uF'] := QuotedStr('Todos');
            frxRptMeta.Variables['mes'] := QuotedStr(CmbBxMesReferencia.Items[CmbBxMesReferencia.ItemIndex]);
            frxRptMeta.Variables['ano'] := QuotedStr(CmbBxAnoReferencia.Items[CmbBxAnoReferencia.ItemIndex]);
            frxRptMeta.ShowReport();
          end;
      end
      else if (ChkBxUf.Checked = false) and (ChkBxRepresentada.Checked = true) then
        begin
          with dm.queryRelMeta do
          begin
            close;
            SQL.Clear;
            SQL.Add('SELECT a.metCodigo, a.metDatInicio, a.metDatFim, a.metValor, a.metValAtual,');
            SQL.Add('a.metValFaturado, a.metValFatLiquido, a.metMesReferencia,');
            SQL.Add('a.metAnoReferencia, b.forRazao,(SELECT SUM (a.metValor)');
            SQL.Add('FROM metas AS a, fornecedores AS b');
            SQL.Add('WHERE a.metForCodigo = b.forCodigo');
            SQL.Add('AND a.metMesReferencia=:pMetMesReferencia1');
            SQL.Add('AND a.metAnoReferencia = :pMetAnoReferencia1) AS metGlobal');
            SQL.Add('FROM metas AS a, fornecedores AS b');
            SQL.Add('WHERE a.metForCodigo = b.forCodigo');
            //SQL.Add('AND b.ForRazao = :pForRazao');
            SQL.Add('AND b.forUF = :pForUf');
            SQL.Add('AND a.metMesReferencia = :pMetMesReferencia');
            SQL.Add('AND a.metAnoReferencia = :pMetAnoReferencia');
            SQL.Add('ORDER BY b.forRazao, a.metDatInicio');

            //passagem de paramentros
            //Parameters.ParamByName('pForRazao').Value := DBCBxRepresentada.Items(DBCBxRepresentada.ItemIndex);
            Parameters.ParamByName('pForUf').Value := CmbBxUf.Items[CmbBxUf.ItemIndex];
            Parameters.ParamByName('pMetMesReferencia1').Value := CmbBxMesReferencia.Items[CmbBxMesReferencia.ItemIndex];
            Parameters.ParamByName('pMetAnoReferencia1').Value := CmbBxAnoReferencia.Items[CmbBxAnoReferencia.ItemIndex];
            Parameters.ParamByName('pMetMesReferencia').Value := CmbBxMesReferencia.Items[CmbBxMesReferencia.ItemIndex];
            Parameters.ParamByName('pMetAnoReferencia').Value := CmbBxAnoReferencia.Items[CmbBxAnoReferencia.ItemIndex];
            Open;
          end;
          if dm.queryRelMeta.IsEmpty then
            MessageDlg('N�o h� dados!', mtConfirmation,[mbOK], 0)
          else
            begin
              frxRptMeta.Variables['uF'] := QuotedStr(CmbBxUf.Items[CmbBxUf.ItemIndex]);
              frxRptMeta.Variables['mes'] := QuotedStr(CmbBxMesReferencia.Items[CmbBxMesReferencia.ItemIndex]);
              frxRptMeta.Variables['ano'] := QuotedStr(CmbBxAnoReferencia.Items[CmbBxAnoReferencia.ItemIndex]);
              frxRptMeta.ShowReport();
            end;
        end;
end;

procedure TfrmRelMeta.ChkBxRepresentadaClick(Sender: TObject);
begin
  //ativa e desativa o cdbcombobox quando o checkbox � marcado
  if ChkBxRepresentada.Checked = true then
    CmbBxRepresentada.Enabled := false
  else
    CmbBxRepresentada.Enabled := true;

end;

procedure TfrmRelMeta.ChkBxUfClick(Sender: TObject);
begin
  //ativa e desativa o cdbcombobox quando o checkbox � marcado
  if ChkBxUf.Checked = true then
    CmbBxUf.Enabled := false
  else
    CmbBxUf.Enabled := true;
end;

procedure TfrmRelMeta.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //fecha a conexao com a tabela
  dm.tabFornecedores.Close;
  dm.tabMetas.Close;
end;

procedure TfrmRelMeta.FormShow(Sender: TObject);
var
  rCampo: string;
begin
  //abre a conexao com a tabelas
  dm.tabFornecedores.Open;
  dm.tabMetas.Open;

  //limpa o comboBox
  CmbBxAnoReferencia.Items.Clear;
  //adiciona o campos
  rCampo:= FormatDateTime('yyyy',IncMonth(Now(),-12));
  CmbBxAnoReferencia.Items.Add(rCampo);
  rCampo:= FormatDateTime('yyyy',Now);
  CmbBxAnoReferencia.Items.Add(rCampo);
  rCampo:= FormatDateTime('yyyy',IncMonth(Now(),12));
  CmbBxAnoReferencia.Items.Add(rCampo);
  CmbBxAnoReferencia.ItemIndex:=1;

  //filtro de fornecedores
  if not DM.tabFornecedores.Eof then
  begin
    dm.tabFornecedores.First;
    while not DM.tabFornecedores.Eof do
    begin
      if dm.tabFornecedoresforStatus.AsInteger = 1 then
      begin
        rCampo:= dm.tabFornecedoresforRazao.AsString;
        CmbBxRepresentada.Items.Add(rCampo);
      end;
      DM.tabFornecedores.Next;
    end;
    CmbBxRepresentada.ItemIndex:=0;
  end;

  ChkBxUf.Checked := true;
  ChkBxRepresentada.Checked := true;

end;

end.
