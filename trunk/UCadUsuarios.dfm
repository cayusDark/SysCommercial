inherited frmCadUsuarios: TfrmCadUsuarios
  Caption = 'Cadastro de usu'#225'rios'
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  inherited GroupBox1: TGroupBox
    inherited edtPesquisa: TEdit
      OnChange = edtPesquisaChange
    end
  end
  inherited GroupBox2: TGroupBox
    inherited DBGrid1: TDBGrid
      DataSource = DM.DSTabUsuarios
      OnDrawColumnCell = DBGrid1DrawColumnCell
      OnTitleClick = DBGrid1TitleClick
      Columns = <
        item
          Expanded = False
          FieldName = 'usuCodigo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'usuApelido'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'usuSenha'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'usuDepartamento'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'usuNivel'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Funcionarios'
          Width = 168
          Visible = True
        end>
    end
  end
  inherited GroupBox3: TGroupBox
    inherited DBNvgInsDelEdit: TDBNavigator
      DataSource = DM.DSTabUsuarios
      Hints.Strings = ()
      OnClick = DBNvgInsDelEditClick
    end
    inherited DBNvgPosCancel: TDBNavigator
      DataSource = DM.DSTabUsuarios
      Hints.Strings = ()
    end
    inherited DBNvigFirPriNexLast: TDBNavigator
      DataSource = DM.DSTabUsuarios
      Hints.Strings = ()
    end
  end
  inherited GroupBox4: TGroupBox
    object Label2: TLabel
      Left = 4
      Top = 1
      Width = 33
      Height = 13
      Caption = 'C'#243'digo'
      FocusControl = DBEdtCodigo
    end
    object Label3: TLabel
      Left = 4
      Top = 37
      Width = 35
      Height = 13
      Caption = 'Apelido'
      FocusControl = DBEdit2
    end
    object Label4: TLabel
      Left = 4
      Top = 73
      Width = 30
      Height = 13
      Caption = 'Senha'
      FocusControl = DBEdit3
    end
    object Label5: TLabel
      Left = 4
      Top = 109
      Width = 69
      Height = 13
      Caption = 'Departamento'
      FocusControl = DBEdit4
    end
    object Label6: TLabel
      Left = 398
      Top = 109
      Width = 23
      Height = 13
      Caption = 'N'#237'vel'
      FocusControl = DBEdit5
    end
    object Label7: TLabel
      Left = 139
      Top = 1
      Width = 55
      Height = 13
      Caption = 'Funcion'#225'rio'
    end
    object DBEdtCodigo: TDBEdit
      Left = 3
      Top = 20
      Width = 134
      Height = 21
      CharCase = ecUpperCase
      DataField = 'usuCodigo'
      DataSource = DM.DSTabUsuarios
      ReadOnly = True
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Left = 4
      Top = 53
      Width = 199
      Height = 21
      CharCase = ecUpperCase
      DataField = 'usuApelido'
      DataSource = DM.DSTabUsuarios
      TabOrder = 2
    end
    object DBEdit3: TDBEdit
      Left = 4
      Top = 89
      Width = 95
      Height = 21
      CharCase = ecLowerCase
      DataField = 'usuSenha'
      DataSource = DM.DSTabUsuarios
      PasswordChar = '*'
      TabOrder = 3
    end
    object DBEdit4: TDBEdit
      Left = 4
      Top = 125
      Width = 394
      Height = 21
      CharCase = ecUpperCase
      DataField = 'usuDepartamento'
      DataSource = DM.DSTabUsuarios
      TabOrder = 4
    end
    object DBEdit5: TDBEdit
      Left = 398
      Top = 125
      Width = 134
      Height = 21
      CharCase = ecUpperCase
      DataField = 'usuNivel'
      DataSource = DM.DSTabUsuarios
      TabOrder = 5
    end
    object DBLkpCbBxFuncionarios: TDBLookupComboBox
      Left = 143
      Top = 20
      Width = 274
      Height = 21
      DataField = 'Funcionarios'
      DataSource = DM.DSTabUsuarios
      ReadOnly = True
      TabOrder = 1
    end
  end
end
