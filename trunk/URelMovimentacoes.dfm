object frmRelMovimentacao: TfrmRelMovimentacao
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Relat'#243'rio de movimenta'#231#227'o'
  ClientHeight = 152
  ClientWidth = 333
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 333
    Height = 50
    Align = alTop
    Caption = 'Conta'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object CmbBxConta: TComboBox
      Left = 2
      Top = 18
      Width = 165
      Height = 27
      CharCase = ecUpperCase
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
  end
  object GroupBox4: TGroupBox
    Left = 0
    Top = 50
    Width = 333
    Height = 56
    Align = alTop
    Caption = 'P'#233'riodo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object Label1: TLabel
      Left = 2
      Top = 23
      Width = 39
      Height = 19
      Caption = #205'nicio'
    end
    object Label2: TLabel
      Left = 173
      Top = 23
      Width = 26
      Height = 19
      Caption = 'Fim'
    end
    object DtTmPckInicio: TDateTimePicker
      Left = 45
      Top = 19
      Width = 122
      Height = 27
      Date = 42242.000000000000000000
      Time = 42242.000000000000000000
      TabOrder = 0
      OnExit = DtTmPckInicioExit
    end
    object DtTmPckFim: TDateTimePicker
      Left = 206
      Top = 19
      Width = 122
      Height = 27
      Date = 42242.000000000000000000
      Time = 42242.000000000000000000
      TabOrder = 1
      OnExit = DtTmPckFimExit
    end
  end
  object GroupBox6: TGroupBox
    Left = 0
    Top = 106
    Width = 333
    Height = 45
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object BitBtn1: TBitBtn
      Left = 101
      Top = 6
      Width = 109
      Height = 32
      Caption = 'Im&primir'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      Glyph.Data = {
        46060000424D4606000000000000360400002800000016000000160000000100
        08000000000010020000C40E0000C40E00000001000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
        A6000020400000206000002080000020A0000020C0000020E000004000000040
        20000040400000406000004080000040A0000040C0000040E000006000000060
        20000060400000606000006080000060A0000060C0000060E000008000000080
        20000080400000806000008080000080A0000080C0000080E00000A0000000A0
        200000A0400000A0600000A0800000A0A00000A0C00000A0E00000C0000000C0
        200000C0400000C0600000C0800000C0A00000C0C00000C0E00000E0000000E0
        200000E0400000E0600000E0800000E0A00000E0C00000E0E000400000004000
        20004000400040006000400080004000A0004000C0004000E000402000004020
        20004020400040206000402080004020A0004020C0004020E000404000004040
        20004040400040406000404080004040A0004040C0004040E000406000004060
        20004060400040606000406080004060A0004060C0004060E000408000004080
        20004080400040806000408080004080A0004080C0004080E00040A0000040A0
        200040A0400040A0600040A0800040A0A00040A0C00040A0E00040C0000040C0
        200040C0400040C0600040C0800040C0A00040C0C00040C0E00040E0000040E0
        200040E0400040E0600040E0800040E0A00040E0C00040E0E000800000008000
        20008000400080006000800080008000A0008000C0008000E000802000008020
        20008020400080206000802080008020A0008020C0008020E000804000008040
        20008040400080406000804080008040A0008040C0008040E000806000008060
        20008060400080606000806080008060A0008060C0008060E000808000008080
        20008080400080806000808080008080A0008080C0008080E00080A0000080A0
        200080A0400080A0600080A0800080A0A00080A0C00080A0E00080C0000080C0
        200080C0400080C0600080C0800080C0A00080C0C00080C0E00080E0000080E0
        200080E0400080E0600080E0800080E0A00080E0C00080E0E000C0000000C000
        2000C0004000C0006000C0008000C000A000C000C000C000E000C0200000C020
        2000C0204000C0206000C0208000C020A000C020C000C020E000C0400000C040
        2000C0404000C0406000C0408000C040A000C040C000C040E000C0600000C060
        2000C0604000C0606000C0608000C060A000C060C000C060E000C0800000C080
        2000C0804000C0806000C0808000C080A000C080C000C080E000C0A00000C0A0
        2000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0E000C0C00000C0C0
        2000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0A000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFF08F7F7
        F7F7F7F7F7F7F7F7F7F7F7F7F6FFFFFF0000FFFFFFF707070707070707070707
        070707F7FFFFFFFF0000F6F7A4A407F6F6F6F6F6F608F6F6F6F6F6A4F7A4A408
        0000A4070707F7080808080808080808080807A4070707A40000A40807085252
        52525252525252525252495B080708A40000A4080808A4494949494949494949
        494949F70808F6A40000A4F60808F6F6F6F6F6F6F6F6F6F6F6F6F6F60808F6A4
        0000A4F6F6F6F6F6F6F6F6F6F6F6F6090908F6F6F6F6FFA40000A4FFFFFFFF09
        E2E2E2E2E2E2E2D9D9D9F6FFFFFFFFA40000A4FFFFFFFF09E2E3EBEBEBEBE2E2
        DAD909FFFFFFFFF70000A4FFFFFFFF09E2E2E2E2E2E2DAD9D9D909FFFFFFFFF7
        0000F7FFFFFFFF09E3ECECECEBEBEBE3EBE309FFFFFFFFF70000FFF707F6FF09
        090909090909090909EC09F60807F7FF0000FFFFFFF708ED0909090909090909
        09EC070708FFFFFF0000FFFFFF070808FFFFFFF6F6F6F608080808F7FFFFFFFF
        0000FFFFFF07F6F6FFFFFFFFF6F6F6F6F60808F7FFFFFFFF0000FFFFFF07F6F6
        FFFFFFFFFFF6F6F6F60808F7FFFFFFFF0000FFFFFFF7F6F6FFFFFFFFFFF6F6F6
        F6F608F7FFFFFFFF0000FFFFFF08F708FFFFFFFFFFFFF6F6F6F608F7FFFFFFFF
        0000FFFFFFFFF608FFFFFFFFFFFFFFF6F6F608F7FFFFFFFF0000FFFFFFFF0808
        F6F6F6F6F6F6F6F6F6F6F6F7FFFFFFFF0000FFFFFFFFFF070707070707070707
        0707F7F7FFFFFFFF0000}
      ParentFont = False
      TabOrder = 0
      OnClick = BitBtn1Click
    end
  end
  object frxRptRelMovimentacoes: TfrxReport
    Version = '5.1.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42049.537297442100000000
    ReportOptions.Name = 'Relat'#243'rio de vendas'
    ReportOptions.LastChange = 42257.947419942100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 272
    Top = 111
    Datasets = <
      item
        DataSet = frxDBDSRelMovimentacoes
        DataSetName = 'frxDBDSRelMovimentacoes'
      end>
    Variables = <
      item
        Name = ' Vendas'
        Value = Null
      end
      item
        Name = 'datInicio'
        Value = Null
      end
      item
        Name = 'datFim'
        Value = Null
      end
      item
        Name = 'uF'
        Value = Null
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 279.685220000000000000
        Width = 718.110700000000000000
        object TotalPages: TfrxMemoView
          Left = 604.724800000000000000
          Top = 1.220470000000000000
          Width = 132.283550000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'P'#193'GINA [Page] DE [TotalPages#]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 28.015770000000000000
        Top = 192.756030000000000000
        Width = 718.110700000000000000
        DataSet = frxDBDSRelMovimentacoes
        DataSetName = 'frxDBDSRelMovimentacoes'
        RowCount = 0
        object frxDBDSVendascliNome: TfrxMemoView
          Top = 1.000000000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          DataField = 'movData'
          DataSet = frxDBDSRelMovimentacoes
          DataSetName = 'frxDBDSRelMovimentacoes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBDSRelMovimentacoes."movData"]')
          ParentFont = False
        end
        object frxDBDSVendasvenValLiquido: TfrxMemoView
          Left = 455.055118110000000000
          Top = 1.220470000000000000
          Width = 109.606370000000000000
          Height = 18.897650000000000000
          DataSet = frxDBDSRelMovimentacoes
          DataSetName = 'frxDBDSRelMovimentacoes'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Highlight.Font.Charset = DEFAULT_CHARSET
          Highlight.Font.Color = clRed
          Highlight.Font.Height = -11
          Highlight.Font.Name = 'Arial'
          Highlight.Font.Style = []
          Highlight.Condition = 'Value <=0'
          Highlight.FillType = ftBrush
          Memo.UTF8W = (
            '[frxDBDSRelMovimentacoes."movValor"]')
          ParentFont = False
        end
        object frxDBDSVendasvenValProdutos: TfrxMemoView
          Left = 302.063080000000000000
          Top = 1.000000000000000000
          Width = 136.063080000000000000
          Height = 18.897650000000000000
          DataField = 'tipLanDescricao'
          DataSet = frxDBDSRelMovimentacoes
          DataSetName = 'frxDBDSRelMovimentacoes'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBDSRelMovimentacoes."tipLanDescricao"]')
          ParentFont = False
        end
        object frxDBDSVendasvenDatVenda: TfrxMemoView
          Left = 68.897650000000000000
          Top = 1.000000000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          DataField = 'movDescricao'
          DataSet = frxDBDSRelMovimentacoes
          DataSetName = 'frxDBDSRelMovimentacoes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDBDSRelMovimentacoes."movDescricao"]')
          ParentFont = False
        end
        object Line3: TfrxLineView
          Top = 20.456710000000000000
          Width = 718.110023860000000000
          Color = clBlack
          Diagonal = True
        end
        object frxDBDSRelMovimentacoessaldo: TfrxMemoView
          Left = 625.520100000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataSet = frxDBDSRelMovimentacoes
          DataSetName = 'frxDBDSRelMovimentacoes'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Highlight.Font.Charset = DEFAULT_CHARSET
          Highlight.Font.Color = clRed
          Highlight.Font.Height = -13
          Highlight.Font.Name = 'Arial'
          Highlight.Font.Style = []
          Highlight.Condition = 'value <=0 '
          Highlight.FillType = ftBrush
          Memo.UTF8W = (
            '[frxDBDSRelMovimentacoes."saldo"]')
          ParentFont = False
        end
        object frxDBDSRelMovimentacoesTipo: TfrxMemoView
          Left = 566.252320000000000000
          Top = 1.000000000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          DataSet = frxDBDSRelMovimentacoes
          DataSetName = 'frxDBDSRelMovimentacoes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Highlight.Font.Charset = DEFAULT_CHARSET
          Highlight.Font.Color = clRed
          Highlight.Font.Height = -13
          Highlight.Font.Name = 'Arial'
          Highlight.Font.Style = []
          Highlight.Condition = 'value='#39'D'#39
          Highlight.FillType = ftBrush
          Memo.UTF8W = (
            '[frxDBDSRelMovimentacoes."Tipo"]')
          ParentFont = False
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 113.385900000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Shape4: TfrxShapeView
          Left = -0.220470000000000000
          Top = 28.236240000000000000
          Width = 714.331170000000000000
          Height = 41.574830000000000000
        end
        object Memo1: TfrxMemoView
          Left = 234.756030000000000000
          Top = 2.559060000000000000
          Width = 306.141930000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'RELAT'#211'RIO DE MOVIMENTA'#199#213'ES')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = -0.763760000000000000
          Top = 30.574830000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Conta:')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = -0.204700000000000000
          Top = 48.149660000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
        end
        object Date: TfrxMemoView
          Left = 821.732840000000000000
          Top = 45.354360000000000000
          Width = 211.653680000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = 'dd '#39'de'#39' mmmm '#39'de'#39' yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Manaus, [Date]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 47.031540000000000000
          Top = 48.267780000000000000
          Width = 192.756030000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[datInicio] - [datFim]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Top = 93.149660000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'DATA')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 301.984251968504000000
          Top = 93.149660000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'CATEGORIA')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 68.787401574803100000
          Top = 93.149660000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'DESCRI'#199#195'O')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Top = 108.811070000000000000
          Width = 718.110023860000000000
          Color = clBlack
          Diagonal = True
        end
        object frxDBDSMovimentacoescotDescricao: TfrxMemoView
          Left = 37.795300000000000000
          Top = 30.236240000000000000
          Width = 400.630180000000000000
          Height = 18.897650000000000000
          DataField = 'cotDescricao'
          DataSet = frxDBDSRelMovimentacoes
          DataSetName = 'frxDBDSRelMovimentacoes'
          Memo.UTF8W = (
            '[frxDBDSRelMovimentacoes."cotDescricao"]')
        end
        object Memo2: TfrxMemoView
          Left = 455.149970000000000000
          Top = 92.708720000000000000
          Width = 83.149660000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 625.511811023622000000
          Top = 94.488250000000000000
          Width = 83.149660000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'SALDO')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 521.575140000000000000
          Top = 48.000000000000000000
          Width = 219.212740000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = 'dd '#39'de'#39' mmmm '#39'de'#39' yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Manaus, [Date]')
          ParentFont = False
        end
      end
    end
  end
  object frxDBDSRelMovimentacoes: TfrxDBDataset
    UserName = 'frxDBDSRelMovimentacoes'
    CloseDataSource = False
    DataSource = DM.DSv_relMovimentacao
    BCDToCurrency = False
    Left = 69
    Top = 111
  end
  object frxPDFExport1: TfrxPDFExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    PrintOptimized = False
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Transparency = False
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    PdfA = False
    Left = 8
    Top = 106
  end
end
