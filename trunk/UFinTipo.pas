unit UFinTipo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.StdCtrls, Vcl.Buttons, DB,
  Vcl.Mask, Vcl.DBCtrls, Vcl.Grids, Vcl.DBGrids;

type
  TfrmFinTipo = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox5: TGroupBox;
    BtnPrior: TBitBtn;
    BtnFirst: TBitBtn;
    BtnNext: TBitBtn;
    BtnLast: TBitBtn;
    BtnInsert: TBitBtn;
    BtnDelete: TBitBtn;
    BtnEdit: TBitBtn;
    BtnPost: TBitBtn;
    BtnCancel: TBitBtn;
    edtPesquisar: TEdit;
    StatusBar1: TStatusBar;
    Label1: TLabel;
    DBedtDescricao: TDBEdit;
    Label2: TLabel;
    DBEdtCodigo: TDBEdit;
    GroupBox2: TGroupBox;
    DBGrdSubTipLancamentos: TDBGrid;
    procedure BtnFirstClick(Sender: TObject);
    procedure BtnPriorClick(Sender: TObject);
    procedure BtnNextClick(Sender: TObject);
    procedure BtnLastClick(Sender: TObject);
    procedure BtnInsertClick(Sender: TObject);
    procedure BtnDeleteClick(Sender: TObject);
    procedure BtnEditClick(Sender: TObject);
    procedure BtnPostClick(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure edtPesquisarChange(Sender: TObject);
    procedure DBGrdSubTipLancamentosKeyPress(Sender: TObject; var Key: Char);
    procedure DBGrdSubTipLancamentosDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure DBGrdSubTipLancamentosTitleClick(Column: TColumn);
    procedure DBGrdSubTipLancamentosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFinTipo: TfrmFinTipo;
  //variavel auxiliar para ordena��o do DBGRID clicando no titulo do grid
  ascendente : Boolean;

implementation

{$R *.dfm}

uses UDM, UFuncoes, UMain;

procedure TfrmFinTipo.BtnCancelClick(Sender: TObject);
begin
  try
    //cancela altera��o na tabela
    dm.tabTipLancamentos.Cancel;

    //ativa edi��o no objeto
    GroupBox1.Enabled := false;
    DBGrdSubTipLancamentos.ReadOnly := true;
    BtnPost.Enabled := false;
    BtnCancel.Enabled := false;

    //desativa edi��o no objeto
    BtnFirst.Enabled := true;
    BtnPrior.Enabled := true;
    BtnNext.Enabled := true;
    BtnLast.Enabled := true;
    edtPesquisar.Enabled := true;
    BtnInsert.Enabled := true;
    BtnEdit.Enabled := true;
    BtnDelete.Enabled := true;

    //log de cancelamento
    Log('A��o de inser��o ou edi��o do tipo de lan�amento '+ DBedtDescricao.Text+' cancelado pelo usu�rio '+ frmMain.usuario+'.');
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro:'+ frmFinTipo.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro:'+ frmFinTipo.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;

end;

procedure TfrmFinTipo.BtnDeleteClick(Sender: TObject);
var
  descricao : string;
begin
  try
    //apaga um registro da tabela
    if MessageDlg('Deseja excluir o registro?', mtConfirmation,[mbYes, mbNo], 0) = mrYes then
    begin
      if dm.tabTipLancamentostipLanCodigo.AsInteger = 1 then
      begin
        MessageDlg('N�o � poss�vel excluir este cadastro!!',mtWarning,[mbOk],0);
      end
      else
        begin
          descricao := DBedtDescricao.Text;
          with dm.queryDelSubTipo do
          begin
            Close;
            SQL.Clear;
            SQL.Text := 'DELETE FROM subTipos WHERE subTipTipLanCodigo= '+ DBEdtCodigo.Text;
            ExecSQL;
          end;

          dm.tabTipLancamentos.Delete;
          Log('Tipo de lan�amento '+ descricao+' apagado pelo usu�rio '+ frmMain.usuario+'.');
        end;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro:'+ frmFinTipo.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro:'+ frmFinTipo.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmFinTipo.BtnEditClick(Sender: TObject);
begin
  try
    if dm.tabTipLancamentostipLanCodigo.AsInteger = 1 then
    begin
      MessageDlg('N�o � poss�vel alterar este registro!',mtWarning,[mbOK],0);
    end
    else
      begin
        //edita um registro da tabela
        dm.tabTipLancamentos.Edit;

        //ativa edi��o no objeto
        GroupBox1.Enabled := true;
        DBGrdSubTipLancamentos.ReadOnly := false;
        BtnPost.Enabled := true;
        BtnCancel.Enabled := true;

        //desativa edi��o no objeto
        BtnFirst.Enabled := false;
        BtnPrior.Enabled := false;
        BtnNext.Enabled := false;
        BtnLast.Enabled := false;
        edtPesquisar.Enabled := false;
        BtnInsert.Enabled := false;
        BtnEdit.Enabled := false;
        BtnDelete.Enabled := false;

        //foco no objeto DBedtDescricao
         DBedtDescricao.SetFocus;
        //log de edi��o
        Log('Tipo de lan�amento '+ DBedtDescricao.Text+' editado pelo usu�rio '+ frmMain.usuario+'.');
      end;
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro:'+ frmFinTipo.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro:'+ frmFinTipo.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmFinTipo.BtnFirstClick(Sender: TObject);
begin
  if (BtnNext.Enabled = false) or (BtnLast.Enabled = false) then
  begin
    BtnNext.Enabled := true;
    BtnLast.Enabled := true;
  end;

  BtnPrior.Enabled := false;
  BtnFirst.Enabled := false;


  //vai ao primeiro registro da tabela
  dm.tabTipLancamentos.First;
end;

procedure TfrmFinTipo.BtnInsertClick(Sender: TObject);
begin
  try
    //adiciona um novo registro na tabela
    dm.tabTipLancamentos.Insert;

    //ativa edi��o no objeto
    GroupBox1.Enabled := true;
    DBGrdSubTipLancamentos.ReadOnly := false;
    BtnPost.Enabled := true;
    BtnCancel.Enabled := true;

    //desativa edi��o no objeto
    BtnFirst.Enabled := false;
    BtnPrior.Enabled := false;
    BtnNext.Enabled := false;
    BtnLast.Enabled := false;
    edtPesquisar.Enabled := false;
    BtnInsert.Enabled := false;
    BtnEdit.Enabled := false;
    BtnDelete.Enabled := false;

    //foco no objeto DBedtDescricao
     DBedtDescricao.SetFocus;
    //log de inser��o
    Log('Inser��o de um novo tipo de lan�amento pelo usu�rio '+ frmMain.usuario+'.');
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro:'+ frmFinTipo.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro:'+ frmFinTipo.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmFinTipo.BtnLastClick(Sender: TObject);
begin
  if (BtnPrior.Enabled = false) or (BtnFirst.Enabled = false) then
  begin
    BtnPrior.Enabled := true;
    BtnFirst.Enabled := true;
  end;

  BtnNext.Enabled :=false;
  BtnLast.Enabled := false;

  //vai ao �ltimo registro da tabela
  dm.tabTipLancamentos.Last;
end;

procedure TfrmFinTipo.BtnNextClick(Sender: TObject);
begin
  //enquanto n�o for o ultimo registro
  if dm.tabTipLancamentos.Eof then
  begin
    BtnNext.Enabled :=false;
    BtnLast.Enabled := false;
  end
  else
  begin
    if (BtnPrior.Enabled = false) or (BtnFirst.Enabled = false) then
    begin
      BtnPrior.Enabled := true;
      BtnFirst.Enabled := true;
    end;

    //vai ao pr�ximo regitro da tabela
    dm.tabTipLancamentos.Next;
  end;
end;

procedure TfrmFinTipo.BtnPostClick(Sender: TObject);
begin
  try
    if DBedtDescricao.Text='' then //valida��o de campo obrigat�rio
      MessageDlg('Campo descri��o obrigat�rio!!',mtWarning,[mbOK],0)
    else
      begin
        //confirma altera�ao na tabela
        dm.tabTipLancamentos.Post;

        //ativa edi��o no objeto
        GroupBox1.Enabled := false;
        DBGrdSubTipLancamentos.ReadOnly := true;
        BtnPost.Enabled := false;
        BtnCancel.Enabled := false;

        //desativa edi��o no objeto
        BtnFirst.Enabled := true;
        BtnPrior.Enabled := true;
        BtnNext.Enabled := true;
        BtnLast.Enabled := true;
        edtPesquisar.Enabled := true;
        BtnInsert.Enabled := true;
        BtnEdit.Enabled := true;
        BtnDelete.Enabled := true;
        //log de confirma��o
        Log('A��o de inser��o ou edi��o do tipo de lan�amento '+ DBedtDescricao.Text+' confirmado pelo usu�rio '+ frmMain.usuario+'.');
      end;
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro:'+ frmFinTipo.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro:'+ frmFinTipo.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmFinTipo.BtnPriorClick(Sender: TObject);
begin
  //verifica se esta no primeiro registro
  if dm.tabTipLancamentos.Bof then
  begin
    BtnPrior.Enabled := false;
    BtnFirst.Enabled := false;
  end
  else
  begin
    if (BtnNext.Enabled = false) or (BtnLast.Enabled = false) then
    begin
      BtnNext.Enabled := true;
      BtnLast.Enabled := true;
    end;

    //vai ao registro anterior da tabela
    dm.tabTipLancamentos.Prior;
  end;
end;

procedure TfrmFinTipo.DBGrdSubTipLancamentosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  //deixa o dbgrid zebrado
  If Odd(dm.tabSubTipos.RecNo) and (dm.tabSubTipos.State <> dsInsert) then
  begin //Lembre-se de colocar a unit DB na cl�usula uses na unit da tela.
      DBGrdSubTipLancamentos.Canvas.Brush.Color := clMoneyGreen; // muda a cor do pincel
      DBGrdSubTipLancamentos.Canvas.FillRect(Rect); // Preenche o fundo com a cor especificada
      DBGrdSubTipLancamentos.DefaultDrawDataCell(Rect,Column.Field,State);// desenha as c�lulas da grade
  end;

end;

procedure TfrmFinTipo.DBGrdSubTipLancamentosKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  confExcluir: Integer;
  nRegistro: Integer;
  nReg: Integer;
begin
  try
    nRegistro := DM.tabSubTipossubTipCodigo.AsInteger;
    if Key = VK_DELETE Then
    begin
      confExcluir:= Application.MessageBox('Deseja excluir o subtipo?', 'Aten��o', MB_YesNo+mb_DefButton2+mb_IconQuestion);
      if confExcluir = IDYes then
      begin
        with dm.queryDelSubTipo do
        begin
          Close;
          SQL.Clear;
          SQL.Text := 'DELETE FROM subTipos WHERE subTipCodigo= '+ IntToStr(nRegistro);
          ExecSQL;
          Log('subTipo de lan�amento '+ dm.tabSubTipossubTipDescricao.AsString+' apagado pelo usu�rio '+ frmMain.usuario+'.');
        end;
        dm.tabSubTipos.Close;
        dm.tabSubTipos.Open;
      end;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro:'+ frmFinTipo.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro:'+ frmFinTipo.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmFinTipo.DBGrdSubTipLancamentosKeyPress(Sender: TObject; var Key: Char);
begin
  //DBGRID com letras em maiusculo
  Key := UpCase(Key);
end;

procedure TfrmFinTipo.DBGrdSubTipLancamentosTitleClick(Column: TColumn);
begin
  //ordenar os dados da grade ao clicar no t�tulo do campo
  ascendente:= not ascendente ;
  If ascendente then
    Dm.tabSubTipos.IndexFieldNames := Column.FieldName + '   ASC'
  else
    Dm.tabSubTipos.IndexFieldNames := Column.FieldName + '    DESC';
end;

procedure TfrmFinTipo.edtPesquisarChange(Sender: TObject);
begin
  //pesquisa aproximada por c�digo
  dm.tabTipLancamentos.Locate('tipLanCodigo', edtPesquisar.Text, [loCaseInsensitive, loPartialKey]);
end;

procedure TfrmFinTipo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //FECHA A CONEX�0 COM A TABELA
  if (dm.tabTipLancamentos.State=dsEdit) or (dm.tabTipLancamentos.State=dsInsert) then
  begin
    if MessageDlg('Cadastro em modo de inser��o ou edi��o. Deseja fechar a tela?', mtConfirmation,[mbYes, mbNo], 0) = mrYes then
    begin
      dm.tabTipLancamentos.Close;
      dm.tabSubTipos.Close;
      Action := caFree; //a��o do objeto TCloseAction para fechar a tela
    end
    else
      Action := caNone ; //a��o do objeto TCloseAction para n�o fechar a tela
  end
  else
  begin
    dm.tabTipLancamentos.Close;
    dm.tabSubTipos.Close;
  end;
end;

procedure TfrmFinTipo.FormShow(Sender: TObject);
begin
  //abre a conexao com as tabelas
  dm.tabTipLancamentos.Open;
  dm.tabSubTipos.Open;

  //variavel da ordena��o
  ascendente := False;

end;

end.

