unit UBaiConAReceber;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ComCtrls;

type
  TfrmBaiConAReceber = class(TForm)
    DatTimPckDatRecebimento: TDateTimePicker;
    Label1: TLabel;
    Label2: TLabel;
    edtValor: TEdit;
    BtnPost: TBitBtn;
    BtnCancel: TBitBtn;
    Label7: TLabel;
    CmbBxConta: TComboBox;
    Label4: TLabel;
    lblDtVencimento: TLabel;
    lblDescricao: TLabel;
    Label6: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnPostClick(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmBaiConAReceber: TfrmBaiConAReceber;

implementation

{$R *.dfm}

uses UFuncoes, UDM, UMain, UFinConAReceber;

procedure TfrmBaiConAReceber.BtnCancelClick(Sender: TObject);
begin
  try
    Log('Baixa da conta a receber '+ lblDescricao.Caption+' cancelada pelo usuario '+ frmMain.usuario+'.');
    frmBaiConAReceber.Close;
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro: '+ frmBaiConAReceber.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro: '+ frmBaiConAReceber.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmBaiConAReceber.BtnPostClick(Sender: TObject);
begin
  try
    //inicia a transação
    dm.conexao.BeginTrans;

    //ShowMessage(CmbBxConta.Items[CmbBxConta.ItemIndex]);
    with dm.queryVerificaConta do
    begin
      close;
      SQL.Clear;
      SQL.Text := 'select cotCodigo from contas where cotDescricao = '+QuotedStr(CmbBxConta.Items[CmbBxConta.ItemIndex]);
      //ShowMessage(sql.Text);
      Open;
    end;

    //ShowMessage(intToStr(dm.queryVerificaContacotCodigo.AsInteger));

    with dm.queryUpdateConReceber do
    begin
      Close;
      SQL.Clear;
      SQL.Text := ('UPDATE receber SET recDatPaga =#'+DateToStr(DatTimPckDatRecebimento.date)+'# ,recValPago='+QuotedStr( edtValor.Text)+', recCotCodigo='+IntToStr(dm.queryVerificaContaCotCodigo.asinteger)+' WHERE recCodigo='+IntToStr(dm.tabReceber.FieldByName('recCodigo').AsInteger));
      //ShowMessage(SQL.Text);
      ExecSQL;
    end;

    //insere o valor na tabela movimentação
    with dm.queryConAPagarInsert do
    begin
      SQL.Clear;
      Close;
      SQL.Add('INSERT INTO movimentacao ( movData,');
      SQL.Add(              'movDescricao,');
      SQL.Add(              'movContato,');
      SQL.Add(              'movTipLanCodigo,');
      SQL.Add(              'movTipo,');
      SQL.Add(              'movValor,');
      SQL.Add(              'movRecCodigo,');
      SQL.Add(              'movCotCodigo)');
      SQL.Add('VALUES ( FORMAT(#'+DateToStr(DatTimPckDatRecebimento.Date)+'#,"mm/dd/yyyy"),');
      SQL.Add(  QuotedStr(lblDescricao.Caption)+',');
      SQL.Add(  QuotedStr(dm.tabReceber.FieldByName('recFornecedor').AsString)+ ',');
      SQL.Add(  IntToStr(dm.tabReceber.FieldByName('recTipLanCodigo').AsInteger)+',');
      SQL.Add(  '1,');
      SQL.Add(  QuotedStr(edtValor.Text)+',');
      SQL.Add(  IntToStr(dm.tabReceber.FieldByName('recCodigo').AsInteger)+',');
      SQL.Add(  IntToStr(verificaCotCodigo(CmbBxConta.Items[CmbBxConta.ItemIndex]))+')');
      //ShowMessage(SQL.Text);
      ExecSQL;
    end;

    //finaliza a transação
    dm.conexao.CommitTrans;

    Log('Baixa da conta a receber '+ lblDescricao.Caption+' confirmada pelo usuario '+ frmMain.usuario+'.');

    frmBaiConAReceber.Close;
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro: '+ frmBaiConAReceber.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro: '+ frmBaiConAReceber.Name +#13+
                'Error: '+E.Message+#13+
               'Classe Name: '+E.ClassName);
      //cancela transacoes
      dm.conexao.RollbackTrans;
    end;
  end;
end;

procedure TfrmBaiConAReceber.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  //fecha a conexao com a tabela
  dm.tabContas.Close;

  dm.tabReceber.Close;
  dm.tabReceber.Open;
end;

procedure TfrmBaiConAReceber.FormShow(Sender: TObject);
var
  rCampo:string;
begin
  try
    //abre a conexao com a tabela
    dm.tabContas.Open;
    //inicializacao dos campos
    lblDtVencimento.Caption := DateToStr(dm.tabReceber.FieldByName('recDatVencimento').AsDateTime);
    lblDescricao.Caption := dm.tabReceber.FieldByName('recHistorico').asString;
    DatTimPckDatRecebimento.Date := dm.tabReceber.FieldByName('recDatVencimento').AsDateTime;
    edtValor.Text := dm.tabReceber.FieldByName('recValor').Value;

    //lancamento das contas
    CmbBxConta.Clear;
    while not dm.tabContas.Eof do
    begin
      rCampo := dm.tabContascotDescricao.AsString;
      CmbBxConta.Items.Add(rCampo);
      dm.tabContas.Next;
    end;
    CmbBxConta.ItemIndex := 0;


  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro: '+ frmBaiConAReceber.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro: '+ frmBaiConAReceber.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;

end;

end.
