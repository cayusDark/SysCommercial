unit UPesCliCidade;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons;

type
  TfrmPesCliCidade = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    edtCidade: TEdit;
    BitBtn1: TBitBtn;
    procedure edtCidadeKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPesCliCidade: TfrmPesCliCidade;

implementation

uses UDM, UMain;

{$R *.dfm}

procedure TfrmPesCliCidade.edtCidadeKeyPress(Sender: TObject; var Key: Char);
begin
  // se �ltima tecla for letra, espa�o ou Backspace
  If not (Key in ['A'..'Z','a'..'z', #32, #8]) then
    Key := #0; // ignora a �ltima tecla digitada
end;

end.
