unit UGraMetRepresentada;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons,
  VclTee.TeeGDIPlus, VCLTee.TeEngine, Vcl.ExtCtrls, VCLTee.TeeProcs,
  VCLTee.Chart, VCLTee.DBChart, VCLTee.Series,VCLTee.TeeEdiGene, VCLTee.EditChar,
  VCLTee.TeeDBCrossTab;

type
  TfrmGraMetRepresentada = class(TForm)
    GroupBox3: TGroupBox;
    CmbBxRepresentada: TComboBox;
    GroupBox4: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    CmbBxMesReferencia: TComboBox;
    CmbBxAnoReferencia: TComboBox;
    GroupBox1: TGroupBox;
    DBChart1: TDBChart;
    BitBtn1: TBitBtn;
    Series1: TBarSeries;
    Series2: TBarSeries;
    Label1: TLabel;
    Series3: TBarSeries;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmbBxRepresentadaChange(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGraMetRepresentada: TfrmGraMetRepresentada;

implementation

{$R *.dfm}

uses UMain, UDM;

procedure TfrmGraMetRepresentada.BitBtn1Click(Sender: TObject);
begin
  ChartPreview(Owner,DBChart1)
end;

procedure TfrmGraMetRepresentada.CmbBxRepresentadaChange(Sender: TObject);
begin
  try
    with dm.queryGraMetRepresentada do
    begin
       Close;
       Parameters.ParamByName('pForRazao').Value := CmbBxRepresentada.Items[CmbBxRepresentada.ItemIndex];
       Parameters.ParamByName('pMes').Value := CmbBxMesReferencia.Items[CmbBxMesReferencia.ItemIndex];
       Parameters.ParamByName('pAno').Value := CmbBxAnoReferencia.Items[CmbBxAnoReferencia.ItemIndex];
       Open;
    end;
  except
    on E: Exception do
    begin
      ShowMessage('Error: ' + E.Message +#13+'class name = '+E.ClassName);
    end
  end;
end;

procedure TfrmGraMetRepresentada.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  dm.tabFornecedores.close;
  dm.tabMetas.close;

  with dm.queryGraMetRepresentada do
  begin
    Close;
  end;
end;

procedure TfrmGraMetRepresentada.FormShow(Sender: TObject);
var
  rcampo : string;
  mes: Integer;
begin
  try
    //abre a conexao com a tabelas
    dm.tabFornecedores.Open;
    dm.tabMetas.Open;

    mes := StrToInt(FormatDateTime('MM', Now));
    CmbBxMesReferencia.ItemIndex := mes -1;

    //limpa o comboBox
    CmbBxAnoReferencia.Items.Clear;

    //inicialização do componente CBxAno
    rCampo:= FormatDateTime('yyyy',IncMonth(Now(),-24));
    CmbBxAnoReferencia.Items.Add(rCampo);
    rCampo:= FormatDateTime('yyyy',IncMonth(Now(),-12));
    CmbBxAnoReferencia.Items.Add(rCampo);
    rCampo:= FormatDateTime('yyyy',Now);
    CmbBxAnoReferencia.Items.Add(rCampo);
    CmbBxAnoReferencia.ItemIndex:=2;

    //filtro de fornecedores
    if not DM.tabFornecedores.Eof then
    begin
      dm.tabFornecedores.First;
      while not DM.tabFornecedores.Eof do
      begin
        if dm.tabFornecedoresforStatus.AsInteger = 1 then
        begin
          rCampo:= dm.tabFornecedoresforRazao.AsString;
          CmbBxRepresentada.Items.Add(rCampo);
        end;
        DM.tabFornecedores.Next;
      end;
      CmbBxRepresentada.ItemIndex:=0;
    end;

    with dm.queryGraMetRepresentada do
    begin
       Close;
       Parameters.ParamByName('pForRazao').Value := CmbBxRepresentada.Items[CmbBxRepresentada.ItemIndex];
       Parameters.ParamByName('pMes').Value := CmbBxMesReferencia.Items[CmbBxMesReferencia.ItemIndex];
       Parameters.ParamByName('pAno').Value := CmbBxAnoReferencia.Items[CmbBxAnoReferencia.ItemIndex];
       Open;
    end;
  except
    on E: Exception do
    begin
      ShowMessage('Error: ' + E.Message +#13+'class name = '+E.ClassName);
    end
  end;
end;

end.
