unit UGraMetVendedor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons,
  VclTee.TeeGDIPlus, VCLTee.TeEngine, VCLTee.Series, Vcl.ExtCtrls,
  VCLTee.TeeProcs, VCLTee.Chart, VCLTee.DBChart, VCLTee.TeeEdiGene, VCLTee.EditChar,
  VCLTee.TeeDBCrossTab;

type
  TfrmGraMetVendedor = class(TForm)
    GroupBox3: TGroupBox;
    CmbBxVendedor: TComboBox;
    GroupBox4: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    CmbBxMesReferencia: TComboBox;
    CmbBxAnoReferencia: TComboBox;
    btnImprimir: TBitBtn;
    GroupBox2: TGroupBox;
    CmbBxRepresentada: TComboBox;
    DBChart1: TDBChart;
    Label1: TLabel;
    Series3: TBarSeries;
    Series1: TBarSeries;
    Series2: TBarSeries;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure CmbBxVendedorChange(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGraMetVendedor: TfrmGraMetVendedor;

implementation

{$R *.dfm}

uses UDM;

procedure TfrmGraMetVendedor.btnImprimirClick(Sender: TObject);
begin
  ChartPreview(Owner,DBChart1);
end;

procedure TfrmGraMetVendedor.CmbBxVendedorChange(Sender: TObject);
begin
  try
    with dm.queryGraMetVendedor do
    begin
      close;
      Parameters.ParamByName('pMes').Value :=CmbBxMesReferencia.Items[CmbBxMesReferencia.ItemIndex];
      Parameters.ParamByName('pAno').Value :=CmbBxAnoReferencia.Items[CmbBxAnoReferencia.ItemIndex];
      Parameters.ParamByName('pFunNome').Value :=CmbBxVendedor.Items[CmbBxVendedor.ItemIndex];
      Parameters.ParamByName('pForRazao').Value :=CmbBxRepresentada.Items[CmbBxRepresentada.ItemIndex];
      open;
    end;
  except
    on E: Exception do
      ShowMessage('Error: ' + E.Message +#13+'class name = '+E.ClassName);
  end;
end;

procedure TfrmGraMetVendedor.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  dm.tabFornecedores.Close;
  dm.tabFuncionarios.Close;
end;

procedure TfrmGraMetVendedor.FormShow(Sender: TObject);
var
  rCampo: string;
  mes: integer;
begin
  try
    dm.tabFornecedores.Open;
    dm.tabFuncionarios.Open;

    mes := StrToInt(FormatDateTime('MM', Now));
    CmbBxMesReferencia.ItemIndex := mes -1;

    //limpa o comboBox
    CmbBxAnoReferencia.Items.Clear;

    //inicialização do componente CBxAno
    rCampo:= FormatDateTime('yyyy',IncMonth(Now(),-24));
    CmbBxAnoReferencia.Items.Add(rCampo);
    rCampo:= FormatDateTime('yyyy',IncMonth(Now(),-12));
    CmbBxAnoReferencia.Items.Add(rCampo);
    rCampo:= FormatDateTime('yyyy',Now);
    CmbBxAnoReferencia.Items.Add(rCampo);
    CmbBxAnoReferencia.ItemIndex:=2;

    //limpa os itens do CmbBxRepresentada
    CmbBxRepresentada.Items.Clear;

    //filtro de fornecedores
    if not DM.tabFornecedores.Eof then
    begin
      dm.tabFornecedores.First;
      while not DM.tabFornecedores.Eof do
      begin
        if dm.tabFornecedoresforStatus.AsInteger = 1 then
        begin
          rCampo:= dm.tabFornecedoresforRazao.AsString;
          CmbBxRepresentada.Items.Add(rCampo);
        end;
        DM.tabFornecedores.Next;
      end;
      CmbBxRepresentada.ItemIndex:=0;
    end;

    //limpa os itens do CmbBxVendedor
    CmbBxVendedor.Items.Clear;

    //filtro de funcionarios
    if not dm.tabFuncionarios.Eof then
    begin
      dm.tabFuncionarios.First;
      while not DM.tabFuncionarios.Eof do
      begin
        if dm.tabFuncionariosfunStatus.AsInteger = 1 then
        begin
          if dm.tabFuncionariosfunFcaCodigo.AsInteger=1 then
          begin
            rCampo:= dm.tabFuncionariosfunNome.AsString;
            CmbBxVendedor.Items.Add(rCampo);
          end;
        end;
        DM.tabFuncionarios.Next;
      end;
      CmbBxVendedor.ItemIndex:=0;
    end;

    with dm.queryGraMetVendedor do
    begin
      close;
      Parameters.ParamByName('pMes').Value :=CmbBxMesReferencia.Items[CmbBxMesReferencia.ItemIndex];
      Parameters.ParamByName('pAno').Value :=CmbBxAnoReferencia.Items[CmbBxAnoReferencia.ItemIndex];
      Parameters.ParamByName('pFunNome').Value :=CmbBxVendedor.Items[CmbBxVendedor.ItemIndex];
      Parameters.ParamByName('pForRazao').Value :=CmbBxRepresentada.Items[CmbBxRepresentada.ItemIndex];
      open;
    end;
  except
    on E: Exception do
      ShowMessage('Error: ' + E.Message +#13+'class name = '+E.ClassName);
  end;
end;

end.
