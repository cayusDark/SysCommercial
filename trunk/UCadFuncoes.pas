unit UCadFuncoes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.Mask, Vcl.DBCtrls,
  Vcl.StdCtrls, Vcl.Buttons, DB;

type
  TfrmCadFuncoes = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox5: TGroupBox;
    BtnPrior: TBitBtn;
    BtnFirst: TBitBtn;
    BtnNext: TBitBtn;
    BtnLast: TBitBtn;
    BtnInsert: TBitBtn;
    BtnDelete: TBitBtn;
    BtnEdit: TBitBtn;
    BtnPost: TBitBtn;
    BtnCancel: TBitBtn;
    edtPesquisar: TEdit;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdtDescricao: TDBEdit;
    StatusBar1: TStatusBar;
    procedure BtnFirstClick(Sender: TObject);
    procedure BtnPriorClick(Sender: TObject);
    procedure BtnNextClick(Sender: TObject);
    procedure BtnLastClick(Sender: TObject);
    procedure BtnInsertClick(Sender: TObject);
    procedure BtnDeleteClick(Sender: TObject);
    procedure BtnEditClick(Sender: TObject);
    procedure BtnPostClick(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure edtPesquisarChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCadFuncoes: TfrmCadFuncoes;

implementation

{$R *.dfm}

uses UDM, UFuncoes, UMain;

procedure TfrmCadFuncoes.BtnCancelClick(Sender: TObject);
begin
  //cancela a a��o atual na base de dados
  dm.tabFuncoes.Cancel;

  //desativa os bot�es
  BtnPost.Enabled := false;
  BtnCancel.Enabled := false;

  //ativa os bot�es
  BtnFirst.Enabled:= true;
  BtnPrior.Enabled:= true;
  BtnNext.Enabled:= true;
  BtnLast.Enabled:= true;
  BtnInsert.Enabled := true;
  BtnDelete.Enabled := true;
  BtnEdit.Enabled := true;
  edtPesquisar.Enabled := true;
end;

procedure TfrmCadFuncoes.BtnDeleteClick(Sender: TObject);
begin
  //exclui um regitro na tabela e verifica se a funcao e vendedor
  try
    if dm.tabFuncoesfcaDescricao.AsString='VENDEDOR' then
      ShowMessage('Fun��o n�o pode ser excluida do sistema!!!')
    else
      if MessageDlg('Deseja excluir o registro?', mtConfirmation,[mbYes, mbNo], 0) = mrYes then
      begin
        dm.tabFuncoes.Delete;
        Log('Fun��o '+ DBEdtDescricao.Text+' apagado pelo usu�rio '+ frmMain.usuario+'.');
      end;
    except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro: '+ frmCadFuncoes.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro:'+ frmCadFuncoes.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;

  end;
end;

procedure TfrmCadFuncoes.BtnEditClick(Sender: TObject);
begin
  //edita um registro da tabela
  dm.tabFuncoes.Edit;

  //ativa o objeto para o modo de edicao
  GroupBox1.Enabled := true;

  //ativa os bot�es
  BtnPost.Enabled := true;
  BtnCancel.Enabled := true;

  //desativa os bot�es
  BtnFirst.Enabled := false;
  BtnPrior.Enabled := false;
  BtnNext.Enabled := false;
  BtnLast.Enabled := false;
  BtnInsert.Enabled := false;
  BtnDelete.Enabled := false;
  BtnEdit.Enabled := false;
  edtPesquisar.Enabled := false;

  //ao inserir novo regitro cursor vai ao  campo DBEdtDatAdesao
  DBEdtDescricao.SetFocus;

end;

procedure TfrmCadFuncoes.BtnFirstClick(Sender: TObject);
begin
  if (BtnNext.Enabled = false) or (BtnLast.Enabled = false) then
  begin
    BtnNext.Enabled := true;
    BtnLast.Enabled := true;
  end;

  BtnPrior.Enabled := false;
  BtnFirst.Enabled := false;

  //vai ao primeiro registro da tabela
  dm.tabFuncoes.First;
end;

procedure TfrmCadFuncoes.BtnInsertClick(Sender: TObject);
begin
  //insere um registro na tabela
  dm.tabFuncoes.Insert;

  //ativa o objeto para o modo de edicao
  GroupBox1.Enabled := true;

  //ativa os bot�es
  BtnPost.Enabled := true;
  BtnCancel.Enabled := true;

  //desativa os bot�es
  BtnFirst.Enabled := false;
  BtnPrior.Enabled := false;
  BtnNext.Enabled := false;
  BtnLast.Enabled := false;
  BtnInsert.Enabled := false;
  BtnDelete.Enabled := false;
  BtnEdit.Enabled := false;
  edtPesquisar.Enabled := false;

  //ao inserir novo regitro cursor vai ao  campo DBEdtDatAdesao
  DBEdtDescricao.SetFocus;

end;

procedure TfrmCadFuncoes.BtnLastClick(Sender: TObject);
begin
  if (BtnPrior.Enabled = false) or (BtnFirst.Enabled = false) then
  begin
    BtnPrior.Enabled := true;
    BtnFirst.Enabled := true;
  end;

  BtnNext.Enabled :=false;
  BtnLast.Enabled := false;

  //vai aso ultimo registro da tabela
  dm.tabFuncoes.Last;

end;

procedure TfrmCadFuncoes.BtnNextClick(Sender: TObject);
begin
  //enquanto n�o for o ultimo registro
  if dm.tabFuncoes.Eof then
  begin
    BtnNext.Enabled :=false;
    BtnLast.Enabled := false;
  end
  else
  begin
    if (BtnPrior.Enabled = false) or (BtnFirst.Enabled = false) then
    begin
      BtnPrior.Enabled := true;
      BtnFirst.Enabled := true;
    end;

    //vai ao pr�ximo registro
    dm.tabFuncoes.Next;
  end;

end;

procedure TfrmCadFuncoes.BtnPostClick(Sender: TObject);
begin
  if DBEdtDescricao.Text='' then
    ShowMessage('Campo descri��o obrigatorio!')
  else
  begin
    //confirma uma a��o de inser��o ou edi��o no banco
    dm.tabFuncoes.Post;

    //desativa a edicao do componente
    GroupBox1.Enabled := false;

    //desativa os bot�es
    BtnPost.Enabled := false;
    BtnCancel.Enabled := false;

    //ativa os bot�es
    BtnFirst.Enabled:= true;
    BtnPrior.Enabled:= true;
    BtnNext.Enabled:= true;
    BtnLast.Enabled:= true;
    BtnInsert.Enabled := true;
    BtnDelete.Enabled := true;
    BtnEdit.Enabled := true;
    edtPesquisar.Enabled := true;
  end;

end;

procedure TfrmCadFuncoes.BtnPriorClick(Sender: TObject);
begin
  //verifica se esta no primeiro registro
  if dm.tabFuncoes.Bof then
  begin
    BtnPrior.Enabled := false;
    BtnFirst.Enabled := false;
  end
  else
  begin
    if (BtnNext.Enabled = false) or (BtnLast.Enabled = false) then
    begin
      BtnNext.Enabled := true;
      BtnLast.Enabled := true;
    end;

    //vai ao registro anterior
    dm.tabFuncoes.Prior;
  end;

end;

procedure TfrmCadFuncoes.edtPesquisarChange(Sender: TObject);
begin
  //pesquisa aproximada por c�digo
  dm.tabFuncoes.Locate('fcaCodigo', edtPesquisar.Text, [loCaseInsensitive, loPartialKey]);
end;

procedure TfrmCadFuncoes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //fecha a conex�o com a tabela
  if (dm.tabFuncoes.State=dsEdit) or (dm.tabFuncoes.State=dsInsert) then
  begin
    if MessageDlg('Cadastro em modo de inser��o ou edi��o. Deseja fechar a tela?', mtConfirmation,[mbYes, mbNo], 0) = mrYes then
    begin
      dm.tabFuncoes.Close;
      Action := caFree; //a��o do objeto TCloseAction para fechar a tela
    end
    else
      Action := caNone ; //a��o do objeto TCloseAction para n�o fechar a tela
  end
  else
    dm.tabFuncoes.Close;
end;

procedure TfrmCadFuncoes.FormShow(Sender: TObject);
begin
  //abre a conex�o com tabela
  dm.tabFuncoes.Open;
end;

end.
