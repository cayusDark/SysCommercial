unit UCadPrzPagamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Mask, Vcl.DBCtrls, Vcl.StdCtrls, DB,
  Vcl.Buttons, Vcl.ComCtrls;

type
  TfrmCadPraPagamento = class(TForm)
    GroupBox2: TGroupBox;
    GroupBox5: TGroupBox;
    BtnPrior: TBitBtn;
    BtnFirst: TBitBtn;
    BtnNext: TBitBtn;
    BtnLast: TBitBtn;
    BtnInsert: TBitBtn;
    BtnDelete: TBitBtn;
    BtnEdit: TBitBtn;
    BtnPost: TBitBtn;
    BtnCancel: TBitBtn;
    edtPesquisar: TEdit;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdtDescricao: TDBEdit;
    StatusBar1: TStatusBar;
    Label3: TLabel;
    DBEdtPriParcela: TDBEdit;
    Label4: TLabel;
    DBEdtSegParcela: TDBEdit;
    Label5: TLabel;
    DBEdtTerParcela: TDBEdit;
    Label6: TLabel;
    DBEdtQuaParcela: TDBEdit;
    Label7: TLabel;
    DBEdtQuiParcela: TDBEdit;
    procedure BtnFirstClick(Sender: TObject);
    procedure BtnPriorClick(Sender: TObject);
    procedure BtnNextClick(Sender: TObject);
    procedure BtnLastClick(Sender: TObject);
    procedure BtnInsertClick(Sender: TObject);
    procedure BtnDeleteClick(Sender: TObject);
    procedure BtnEditClick(Sender: TObject);
    procedure BtnPostClick(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure edtPesquisarChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCadPraPagamento: TfrmCadPraPagamento;

implementation

{$R *.dfm}

uses UDM, UFuncoes, UMain;

procedure TfrmCadPraPagamento.BtnCancelClick(Sender: TObject);
begin
  try
    //cancela a a��o atual na base de dados
    dm.tabPrzPagamentos.Cancel;

    //desativa os bot�es
    BtnPost.Enabled := false;
    BtnCancel.Enabled := false;
    GroupBox2.Enabled := false;

    //ativa os bot�es
    BtnFirst.Enabled:= true;
    BtnPrior.Enabled:= true;
    BtnNext.Enabled:= true;
    BtnLast.Enabled:= true;
    BtnInsert.Enabled := true;
    BtnDelete.Enabled := true;
    BtnEdit.Enabled := true;
    edtPesquisar.Enabled := true;
    Log('A��o de inser��o ou edi��o do prazo de pagamento '+ DBEdtDescricao.Text+' cancelado pelo usu�rio '+ frmMain.usuario+'.');
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro!'+#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro:'+#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmCadPraPagamento.BtnDeleteClick(Sender: TObject);
begin
  //exclui um regitro na tabela
  try
    if MessageDlg('Deseja excluir o registro?', mtConfirmation,[mbYes, mbNo], 0) = mrYes then
    begin
      dm.tabPrzPagamentos.Delete;
      Log('Prazo de pagamento '+ DBEdtDescricao.Text+' apagado pelo usu�rio '+ frmMain.usuario+'.');
    end;
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro!'+#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro:'+#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmCadPraPagamento.BtnEditClick(Sender: TObject);
begin
  //edita um registro da tabela
  try
    dm.tabPrzPagamentos.Edit;

    //ativa os bot�es
    BtnPost.Enabled := true;
    BtnCancel.Enabled := true;
    GroupBox2.Enabled := true;

    //desativa os bot�es
    BtnFirst.Enabled := false;
    BtnPrior.Enabled := false;
    BtnNext.Enabled := false;
    BtnLast.Enabled := false;
    BtnInsert.Enabled := false;
    BtnDelete.Enabled := false;
    BtnEdit.Enabled := false;
    edtPesquisar.Enabled := false;

    //ao inserir novo regitro cursor vai ao  campo DBEdtDatAdesao
    DBEdtDescricao.SetFocus;
    Log('Prazo de pagamento '+ DBEdtDescricao.Text+' editado pelo usu�rio '+ frmMain.usuario+'.');
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro!'+#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro:'+#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmCadPraPagamento.BtnFirstClick(Sender: TObject);
begin
  if (BtnNext.Enabled = false) or (BtnLast.Enabled = false) then
  begin
    BtnNext.Enabled := true;
    BtnLast.Enabled := true;
  end;

  BtnPrior.Enabled := false;
  BtnFirst.Enabled := false;

  //vai ao primeiro registro da tabela
  dm.tabPrzPagamentos.First;
end;

procedure TfrmCadPraPagamento.BtnInsertClick(Sender: TObject);
begin
  //insere um registro na tabela
  try
    dm.tabPrzPagamentos.Insert;

    //ativa os componentes
    BtnPost.Enabled := true;
    BtnCancel.Enabled := true;
    GroupBox2.Enabled := true;

    //desativa os bot�es
    BtnFirst.Enabled := false;
    BtnPrior.Enabled := false;
    BtnNext.Enabled := false;
    BtnLast.Enabled := false;
    BtnInsert.Enabled := false;
    BtnDelete.Enabled := false;
    BtnEdit.Enabled := false;
    edtPesquisar.Enabled := false;

    //ao inserir novo regitro cursor vai ao  campo DBEdtDatAdesao
    DBEdtDescricao.SetFocus;

    Log('Inser��o de um novo prazo de pagamento pelo usu�rio '+ frmMain.usuario+'.');
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro!'+#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro:'+#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmCadPraPagamento.BtnLastClick(Sender: TObject);
begin
  if (BtnPrior.Enabled = false) or (BtnFirst.Enabled = false) then
  begin
    BtnPrior.Enabled := true;
    BtnFirst.Enabled := true;
  end;

  BtnNext.Enabled :=false;
  BtnLast.Enabled := false;

  //vai ao �ltimo registro da tabela
  dm.tabPrzPagamentos.Last;

end;

procedure TfrmCadPraPagamento.BtnNextClick(Sender: TObject);
begin
  //enquanto n�o for o ultimo registro
  if dm.tabPrzPagamentos.Eof then
  begin
    BtnNext.Enabled :=false;
    BtnLast.Enabled := false;
  end
  else
  begin
    if (BtnPrior.Enabled = false) or (BtnFirst.Enabled = false) then
    begin
      BtnPrior.Enabled := true;
      BtnFirst.Enabled := true;
    end;

    //vai ao pr�ximo registro da tabela
    dm.tabPrzPagamentos.Next;
  end;

end;

procedure TfrmCadPraPagamento.BtnPostClick(Sender: TObject);
begin
  try
    if DBEdtPriParcela.Text='' then
      ShowMessage('Campo Parcela obrigatorio!')
    else
    begin
       //confirma uma a��o de inser��o ou edi��o no banco
      dm.tabPrzPagamentos.Post;

      //desativa os componentes
      BtnPost.Enabled := false;
      BtnCancel.Enabled := false;
      GroupBox2.Enabled := false;

      //ativa os bot�es
      BtnFirst.Enabled:= true;
      BtnPrior.Enabled:= true;
      BtnNext.Enabled:= true;
      BtnLast.Enabled:= true;
      BtnInsert.Enabled := true;
      BtnDelete.Enabled := true;
      BtnEdit.Enabled := true;
      edtPesquisar.Enabled := true;
      Log('A��o de inser��o ou edi��o do prazo de pagamento '+ DBEdtDescricao.Text+' confirmado pelo usu�rio '+ frmMain.usuario+'.');
    end;
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro!'+#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro:'+#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmCadPraPagamento.BtnPriorClick(Sender: TObject);
begin
  //verifica se esta no primeiro registro
  if dm.tabPrzPagamentos.Bof then
  begin
    BtnPrior.Enabled := false;
    BtnFirst.Enabled := false;
  end
  else
  begin
    if (BtnNext.Enabled = false) or (BtnLast.Enabled = false) then
    begin
      BtnNext.Enabled := true;
      BtnLast.Enabled := true;
    end;

    //vai ao registro anterior
    dm.tabPrzPagamentos.Prior;
  end;

end;

procedure TfrmCadPraPagamento.edtPesquisarChange(Sender: TObject);
begin
  //pesquisa aproximada por c�digo
  dm.tabPrzPagamentos.Locate('przPagDescricao', edtPesquisar.Text, [loCaseInsensitive, loPartialKey]);
end;

procedure TfrmCadPraPagamento.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //fecha a conex�o com a tabela
  if (dm.tabCategorias.State=dsEdit) or (dm.tabCategorias.State=dsInsert) then
  begin
    if MessageDlg('Cadastro em modo de inser��o ou edi��o. Deseja fechar a tela?', mtConfirmation,[mbYes, mbNo], 0) = mrYes then
    begin
      dm.tabPrzPagamentos.Close;
      Action := caFree; //a��o do objeto TCloseAction para fechar a tela
    end
    else
      Action := caNone ; //a��o do objeto TCloseAction para n�o fechar a tela
  end
  else
    dm.tabPrzPagamentos.Close;
end;

procedure TfrmCadPraPagamento.FormShow(Sender: TObject);
begin
  //abre a conex�o com a tabela
  dm.tabPrzPagamentos.Open;
end;

end.
