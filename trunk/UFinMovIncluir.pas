unit UFinMovIncluir;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ComCtrls;

type
  TfrmFinMovIncluir = class(TForm)
    Label1: TLabel;
    DatTimPckData: TDateTimePicker;
    edtDescricao: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    CmbBxTipo: TComboBox;
    Label2: TLabel;
    edtValor: TEdit;
    BtnPost: TBitBtn;
    BtnCancel: TBitBtn;
    cmbBxClassificacao: TComboBox;
    cmbBxFornecedor: TComboBox;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnPostClick(Sender: TObject);
    procedure CmbBxTipoExit(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFinMovIncluir: TfrmFinMovIncluir;
  movCodigo : Integer;

implementation

{$R *.dfm}

uses UFuncoes, UMain, UDM, UFinMovimentacoes;

procedure TfrmFinMovIncluir.BtnCancelClick(Sender: TObject);
begin
  Close();
end;

procedure TfrmFinMovIncluir.BtnPostClick(Sender: TObject);
begin
  try
    if edtDescricao.Text ='' then
      MessageDlg('Campo descri��o obrigat�rio!!',mtWarning,[mbOK],0)
    else
      if cmbBxFornecedor.Text = '' then
        MessageDlg('Campo fornecedor obrigat�rio!!',mtWarning,[mbOK],0)
      else
        if CmbBxTipo.Text = '' then
          MessageDlg('Campo categoria obrigat�rio!!',mtWarning,[mbOK],0)
        else
          if edtValor.Text='' then
            MessageDlg('Campo valor obrigat�rio!!',mtWarning,[mbOK],0)
          else
            begin
              //abre a transa��o com a base de dados
              dm.conexao.BeginTrans;

              //update da movimenta��o
              if statusContas=0 then
              begin
                with dm.queryConAPagarInsert do
                begin
                  SQL.Clear;
                  Close;
                  SQL.Add('UPDATE movimentacao SET movData = FORMAT(#'+DateToStr(DatTimPckData.Date)+'#,"mm/dd/yyyy"),');
                  SQL.Add('           movDescricao = '+QuotedStr(edtDescricao.Text)+',');
                  SQL.Add('           movContato = '+QuotedStr(cmbBxFornecedor.Text)+ ',');
                  SQL.Add('           movTipLanCodigo = '+IntToStr(dm.querySelTipLancamentotipLanCodigo.AsInteger)+',');
                  SQL.Add('           movTipo = '+inttostr(cmbBxClassificacao.ItemIndex)+',');
                  SQL.Add('           movValor = '+QuotedStr(edtValor.Text) );
                  SQL.Add('WHERE movCodigo = '+ IntToStr(movCodigo) +';');
                  //ShowMessage(sql.Text);
                  ExecSQL;
                end;

                //finaliza transa��o
                dm.conexao.CommitTrans;

                //log de confirma��o
                Log('A��o de edi�ao da movimenta��o '+ EdtDescricao.Text+' confirmado pelo usu�rio '+ frmMain.usuario+'.');
              end
              else
                begin
                  with dm.queryConAPagarInsert do
                  begin
                    SQL.Clear;
                    Close;
                    SQL.Add('INSERT INTO movimentacao ( movData,');
                    SQL.Add(              'movDescricao,');
                    SQL.Add(              'movContato,');
                    SQL.Add(              'movTipLanCodigo,');
                    SQL.Add(              'movTipo,');
                    SQL.Add(              'movValor,');
                    SQL.Add(              'movCotCodigo)');
                    SQL.Add('VALUES ( FORMAT(#'+DateToStr(DatTimPckData.Date)+'#,"mm/dd/yyyy"),');
                    SQL.Add(  QuotedStr(edtDescricao.Text)+',');
                    SQL.Add(  QuotedStr(cmbBxFornecedor.Text)+ ',');
                    SQL.Add(  IntToStr(dm.querySelTipLancamentotipLanCodigo.AsInteger)+',');
                    SQL.Add(  inttostr(cmbBxClassificacao.ItemIndex)+',');
                    SQL.Add(  QuotedStr(edtValor.Text)+',');
                    SQL.Add(  IntToStr(cotCodigo)+')');
                    ExecSQL;
                  end;

                  //fecha a transa��o
                  dm.conexao.CommitTrans;

                  //log de confirma��o
                  Log('A��o de inser��o da movimenta��o '+ EdtDescricao.Text+' confirmado pelo usu�rio '+ frmMain.usuario+'.');
                end;

              dm.tabMovimentacao.Close;
              dm.tabMovimentacao.Open;

              //soma o valor da conta em quest�o
              with dm.querySumMovimentacao do
              begin
                close;
                SQL.Clear;
                SQL.Add('SELECT c.cotCodigo, ((select sum(movValor) from movimentacao m1 where m1.movCotCodigo=c.cotCodigo and m1.movTipo=1)-(select sum(movValor) from movimentacao m1 where m1.movCotCodigo=c.cotCodigo and m1.movTipo=0)) as Valor');
                SQL.Add('FROM movimentacao AS m, contas AS c');
                SQL.Add('WHERE m.movCotCodigo = c.cotCodigo');
                SQL.Add('and c.cotDescricao =' + QuotedStr(frmFinMovimentacoes.CmbBxContas.Items[frmFinMovimentacoes.CmbBxContas.ItemIndex]));
                SQL.Add('group by c.cotCodigo;');
                Open;
              end;

              //chama a fun��o atualizaValorConta para atualizar o valor da conta no label
              frmFinMovimentacoes.lblTotalEmConta.Caption := 'R$ '+ FloatToStr(atualizaValorConta(frmFinMovimentacoes.CmbBxContas.Items[frmFinMovimentacoes.CmbBxContas.ItemIndex]));

              //fecha a tela
              Close();
            end;
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro:'+ frmFinMovIncluir.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro:'+ frmFinMovIncluir.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
      dm.conexao.RollbackTrans;
    end;
  end;
end;

procedure TfrmFinMovIncluir.CmbBxTipoExit(Sender: TObject);
begin
  try
    //filtra categoria
    with DM.querySeltipLancamento do
    begin
      Close;
      SQL.Clear;
      SQL.Text := 'select tipLanCodigo from TiposLancamento where tipLanDescricao LIKE '+ QuotedStr(CmbBxTipo.Text);
      Open;
    end;
  except
    on E: Exception do
    begin
      MessageDlg('Tipo de lan�amento '+ CmbBxTipo.Text +' n�o localizada!',mtWarning,[mbOK], 0);
      ErroGrave('Tipo de lan�amento  '+CmbBxTipo.Text +' n�o localizada!');
      CmbBxTipo.SetFocus;
    end;
  end;
end;

procedure TfrmFinMovIncluir.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  try
    //fecha a conexao com as tabelas
    dm.tabTipLancamentos.Close;
    dm.tabFornecedores.Close;
    dm.tabContatos.Close;
    DM.tabFuncionarios.Close;
    if statusContas = 0 then
      Log('A��o de edi��o da movimenta��o  '+ EdtDescricao.Text+' cancelada pelo usu�rio '+ frmMain.usuario+'.')
    else
      Log('A��o de inser��o da movimenta��o  '+ EdtDescricao.Text+' cancelada pelo usu�rio '+ frmMain.usuario+'.');
  except
    on E: Exception do
    begin
      MessageDlg('Ocorreu um erro: '+ frmFinMovIncluir.Name +#13+
                 'Error: '+E.Message+#13+
                 'Classe Name: '+E.ClassName, mtError,[mbOK], 0);
      ErroGrave('Ocorreu um erro: '+ frmFinMovIncluir.Name +#13+
                'Error: '+E.Message+#13+
                'Classe Name: '+E.ClassName);
    end;
  end;
end;

procedure TfrmFinMovIncluir.FormShow(Sender: TObject);
var
  rCampo : string;
begin
  try
    //abre a conexao com as tabelas
    dm.tabTipLancamentos.Open;
    dm.tabFornecedores.Open;
    dm.tabContatos.Open;
    DM.tabFuncionarios.Open;

    //filtro de fornecedores
    dm.tabFornecedores.First;
    while not DM.tabFornecedores.Eof do
    begin
      if dm.tabFornecedoresforStatus.AsInteger = 1 then
      begin
        rCampo:= dm.tabFornecedoresforRazao.AsString;
        cmbBxFornecedor.Items.Add(rCampo);
      end;
      DM.tabFornecedores.Next;
    end;

    //filtro de vendedores
    dm.tabFuncionarios.First;
    while not DM.tabFuncionarios.Eof do
    begin
      if dm.tabFuncionariosfunStatus.AsInteger = 1 then
      begin
        rCampo:= dm.tabFuncionariosfunNome.AsString;
        cmbBxFornecedor.Items.Add(rCampo);
      end;
      DM.tabFuncionarios.Next;
    end;

    //filtro de contatos
    dm.tabContatos.First;
    while not DM.tabContatos.Eof do
    begin
      rCampo:= dm.tabContatosConNome.AsString;
      cmbBxFornecedor.Items.Add(rCampo);
      DM.tabContatos.Next;
    end;

    cmbBxFornecedor.ItemIndex :=0;

    //filtro de tipos de lan�amento
    dm.tabTipLancamentos.First;

    dm.tabTipLancamentos.First;
    while not dm.tabTipLancamentos.Eof do
    begin
      rCampo := dm.tabTipLancamentostipLanDescricao.AsString;
      CmbBxTipo.Items.Add(rCampo);
      dm.tabTipLancamentos.Next;
    end;

    CmbBxTipo.ItemIndex := 0;

    //verifica se � nova conta ou edi��o
    if statusContas = 0 then
    begin
      Caption := 'Alterar';
      //log de inser��o
      log('Edi��o da movimenta��o '+dm.tabMovimentacao.FieldByName('movDescricao').AsString+' pelo usu�rio '+frmMain.usuario+'.');

      //sele��o do valores dos campos
      DatTimPckData.Date := dm.tabMovimentacao.FieldByName('movData').AsDateTime;
      edtDescricao.Text := dm.tabMovimentacao.FieldByName('movDescricao').AsString;
      cmbBxFornecedor.Text := dm.tabMovimentacao.FieldByName('movContato').AsString;
      CmbBxTipo.Text := dm.tabMovimentacao.FieldByName('movLanDescricao').AsString;
      cmbBxClassificacao.ItemIndex := dm.tabMovimentacao.FieldByName('movTipo').AsInteger;
      edtValor.Text := dm.tabMovimentacao.FieldByName('movValor').AsString;
      movCodigo := dm.tabMovimentacao.FieldByName('movCodigo').AsInteger;
    end
    else
      begin
        //log de inser��o
        log('Inser��o de uma movimenta��o  pelo usu�rio '+frmMain.usuario+'.');

        //inicializacao da data
        DatTimPckData.Date := Date;

        //campo para incializa��o
        DatTimPckData.SetFocus;

        //limpa combobox
        cmbBxFornecedor.ItemIndex :=-1;
        CmbBxTipo.ItemIndex:= -1;

        Caption := 'Incluir';
      end;

  except
    on E: Exception do
    begin
      MessageDlg('Tipo de lan�amento '+ CmbBxTipo.Text +' n�o localizada!',mtWarning,[mbOK], 0);
      ErroGrave('Tipo de lan�amento  '+CmbBxTipo.Text +' n�o localizada!');
      CmbBxTipo.SetFocus;
    end;
  end;
end;

end.
